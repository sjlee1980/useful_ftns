from setuptools import setup, find_packages
#from distutils.core import setup
from glob import glob

datafiles = glob("useful_ftns/data/*")
scripts = glob("scripts/*")

setup(name='useful_ftns',
      version='1.1',
      #setup_requires=['setuptools_scm'],
      author='Sang Jun Lee',
      author_email='sangjun.slac@gmail.com',
      #install_requires=['hickle'],
      #py_modules=['useful_ftns'],
      #packages = ['useful_ftns','cryoboss'],
      packages=find_packages(),
      data_files=[('useful_ftns/data', datafiles)],
      scripts=scripts,
      #include_package_data=True
      )
