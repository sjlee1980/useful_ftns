"""
typical usage:

from useful_ftns import cryoboss
c = cryoboss.Cryobss(fname)
"""

import numpy as np

class Cryoboss:
    def __init__(self, fname):
        data = np.loadtxt(fname, skiprows=4, usecols=(2,3,12), delimiter=',')
        self.i_mag = data[:,2]
        self.temp_K = data[:,1]
        self.time_hr = data[:,0]
