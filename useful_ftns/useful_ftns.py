from __future__ import division
from __future__ import print_function
try:
    from builtins import input # needed for python2 and 3 compatibility
except ImportError:
    print('ImportError: builtins package is missing. Not essential, but try to install it and import useful_ftns again')
from functools import reduce # needed for python2 and 3 compatibility
try:
    xrange
except NameError:
    xrange = range
import mass
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
from matplotlib import cm
import scipy as sp
from scipy.optimize import curve_fit
from scipy.interpolate import UnivariateSpline
# from mass.mathstat.fitting import MaximumLikelihoodHistogramFitter as MLHF # not used
#from scipy.optimize import leastsq
from scipy.special import erfc
#from mass.calibration import young
import statsmodels.api as sm

# for sj_compute_filters
from mass.core.utilities import InlineUpdater, show_progress

import h5py
try:
    import hickle as hkl
except:
    print('hickle not installed. related functions will not work')
import pickle as pkl
from scipy import interpolate
# from os import path
import os
import glob
import scipy.signal as sig
from scipy.signal import savgol_filter
# from sass.peakutils import find_precise_peaks

# def use_qt():
try:
    import PyQt4
    use_pyqt4 = True
except:
    use_pyqt4 = False
if use_pyqt4:
    from PyQt4.QtGui import QApplication, QPixmap, QImage
else:
    from PyQt5.QtWidgets import QApplication#, QWidget
    from PyQt5.QtGui import QImage#, QScreen

# from mpldatacursor import datacursor

# for customizing pyplot color cycle
from cycler import cycler
#mpl.rcParams['axes.prop_cycle'] = cycler(color=['xkcd:black','xkcd:red','xkcd:orange','xkcd:yellow','xkcd:green','xkcd:blue','xkcd:purple'])
# mpl.rcParams['axes.prop_cycle'] = cycler(color=['black','red','orange','gold','green','blue','navy','purple'])
mpl.rcParams['axes.prop_cycle'] = cycler(color=['black','red','orange','gold','olive','limegreen','deepskyblue','blue','darkblue','purple','hotpink','grey'])

def choose_color_cycle(style=None):
    if style is None:
        print('Available styles:')
        print('\n'.join(mpl.style.available))
        print("\nSet as default: choose_color_cycle('default')")
        mpl.style.use('default')
    elif style == 'sj':
        mpl.rcParams['axes.prop_cycle'] = cycler(color=['black','red','orange','gold','olive','limegreen','deepskyblue','blue','darkblue','purple','hotpink','grey'])
        print('Sang-Jun style!')
    else:
        mpl.style.use(style)

# for customizing ipython
import IPython
if IPython.version_info[0] >= 7:
    IPython.core.inputtransformer2.leading_indent(0)
elif IPython.version_info[0] >= 5:
    IPython.core.inputtransformer.leading_indent()

# reference: https://gist.github.com/takluyver/85b33db0836cdcc4baf252fd81937fa7
from IPython.terminal.prompts import Prompts, Token

class MyPrompts(Prompts):
    def continuation_prompt_tokens(self, cli=None, width=None):
        if width is None:
            width = self._width()
        return [
            (Token.Prompt, ''),
        ]
try:
    ip = get_ipython()
    # if getattr(ip, 'pt_cli'):
    #     ip.prompts = MyPrompts(ip)
    ip.prompts = MyPrompts(ip)
except:
    print('ipython customization ignored')

import warnings
# warnings.filterwarnings("ignore",".*GUI is implemented.*")
# warnings.filterwarnings("ignore",category=mpl.cbook.mplDeprecation)
import time

# from os.path import dirname, abspath, join
# mypath = dirname(abspath(__file__))
# print(mypath)

def reload():
    import sys
    current_module = sys.modules[__name__]
    import importlib
    importlib.reload(current_module)
    print(current_module, 'reloaded')


SIGMA2FWHM = 2.*np.sqrt(2.*np.log(2.))

CAL_MIX2_E_TITUS = [278.21, 392.25, 524.45, 614.84,  675.98,  705.01,  717.45,  775.31,  790.21,  848.85,  866.11,  926.98,  947.52,  1009.39, 1032.46]
CAL_MIX2_LINE_TITUS = ['C',  'N',    'O',   'Fe Ll', 'Co Ll', 'Fe La', 'Fe Lb', 'Co La', 'Co Lb', 'Ni La', 'Ni Lb', 'Cu La', 'Cu Lb', 'Zn La', 'Zn Lb']
CAL_MIX2_TITUS = {}
for (energy, name) in zip(CAL_MIX2_E_TITUS, CAL_MIX2_LINE_TITUS):
    CAL_MIX2_TITUS[name] = energy

CAL_MIX3_E_TITUS = [278.21, 392.25, 524.45, 614.84,  705.01,  717.45,  848.85,  866.11,  926.98,  947.52]
CAL_MIX3_LINE_TITUS = ['C',  'N',    'O',   'Fe Ll', 'Fe La', 'Fe Lb', 'Ni La', 'Ni Lb', 'Cu La', 'Cu Lb']
CAL_MIX3_TITUS = {}
for (energy, name) in zip(CAL_MIX3_E_TITUS, CAL_MIX3_LINE_TITUS):
    CAL_MIX3_TITUS[name] = energy

CAL_MIX2_E_LIT = [    0,    0,      0,       0,       0,      704,       0,       0,       0,       0,       0,     929.3,     0,       0,       0   ]
CAL_MIX2_LINE_LIT = ['C',  'N',    'O',   'Fe Ll', 'Co Ll', 'Fe La', 'Fe Lb', 'Co La', 'Co Lb', 'Ni La', 'Ni Lb', 'Cu La', 'Cu Lb', 'Zn La', 'Zn Lb']
CAL_MIX2_LIT_SOURCE = ['', '', '', '', '', 'Ghiringhelli', '']
CAL_MIX2_LIT = {}
for (energy, name) in zip(CAL_MIX2_E_LIT, CAL_MIX2_LINE_LIT):
    CAL_MIX2_LIT[name] = energy

CAL_MIX1_E_KL = [    183,   277,   393,    452,     500,    525,    573,     583,     615,     678,     705,     719,     743,     776,     791,     811,     852,     869,     884,     930,     950,    1012,    1035]
CAL_MIX1_LINE_KL = [ 'B K', 'C K', 'N K', 'Ti La', 'Cr Ll', 'O K', 'Cr La', 'Cr Lb', 'Fe Ll', 'Co Ll', 'Fe La', 'Fe Lb', 'Ni Ll', 'Co La', 'Co Lb', 'Cu Ll', 'Ni La', 'Ni Lb', 'Zn Ll', 'Cu La', 'Cu Lb', 'Zn La', 'Zn Lb']
CAL_MIX1_KL = {}
for (energy, name) in zip(CAL_MIX1_E_KL, CAL_MIX1_LINE_KL):
    CAL_MIX1_KL[name] = energy

CAL_MIX2_E_KL = [    183,   277,   393,    452,     500,    525,    573,     583,     615,     678,     705,     719,     743,     776,     791,     811,     852,     869,     884,     930,     950,    1012,    1035]
CAL_MIX2_LINE_KL = [ 'B K', 'C K', 'N K', 'Ti La', 'Cr Ll', 'O K', 'Cr La', 'Cr Lb', 'Fe Ll', 'Co Ll', 'Fe La', 'Fe Lb', 'Ni Ll', 'Co La', 'Co Lb', 'Cu Ll', 'Ni La', 'Ni Lb', 'Zn Ll', 'Cu La', 'Cu Lb', 'Zn La', 'Zn Lb']
CAL_MIX2_KL = {}
for (energy, name) in zip(CAL_MIX2_E_KL, CAL_MIX2_LINE_KL):
    CAL_MIX2_KL[name] = energy

CAL_MIX3_E_KL = [    277,   393,   525,    615,     705,     719,     743,    811,     852,     869,      930,     950]
CAL_MIX3_LINE_KL = ['C K', 'N K', 'O K', 'Fe Ll', 'Fe La', 'Fe Lb', 'Ni Ll', 'Cu Ll', 'Ni La', 'Ni Lb', 'Cu La', 'Cu Lb']
CAL_MIX3_KL = {}
for (energy, name) in zip(CAL_MIX3_E_KL, CAL_MIX3_LINE_KL):
    CAL_MIX3_KL[name] = energy

CAL_MIX1_E_KL_FULL = [    183,   277,   393,    452,     500,    525,    573,     583,     615,     678,     705,     719,     743,     776,     791,      811,     852,     869,     884,     930,     950,    1012,    1035]
CAL_MIX1_LINE_KL_FULL = [ 'B K', 'C K', 'N K', 'Ti La', 'Cr Ll', 'O K', 'Cr La', 'Cr Lb', 'Fe Ll', 'Co Ll', 'Fe La', 'Fe Lb', 'Ni Ll', 'Co La', 'Co Lb', 'Cu Ll', 'Ni La', 'Ni Lb', 'Zn Ll', 'Cu La', 'Cu Lb', 'Zn La', 'Zn Lb']
CAL_MIX1_KL_FULL = {}
for (energy, name) in zip(CAL_MIX1_E_KL_FULL, CAL_MIX1_LINE_KL_FULL):
    CAL_MIX1_KL_FULL[name] = energy

CAL_MIX2_E_KL_FULL = [    183,   277,   393,    452,     500,    525,    573,     583,     615,     678,     705,     719,     743,     776,     791,      811,     852,     869,     884,     930,     950,    1012,    1035]
CAL_MIX2_LINE_KL_FULL = [ 'B K', 'C K', 'N K', 'Ti La', 'Cr Ll', 'O K', 'Cr La', 'Cr Lb', 'Fe Ll', 'Co Ll', 'Fe La', 'Fe Lb', 'Ni Ll', 'Co La', 'Co Lb', 'Cu Ll', 'Ni La', 'Ni Lb', 'Zn Ll', 'Cu La', 'Cu Lb', 'Zn La', 'Zn Lb']
CAL_MIX2_KL_FULL = {}
for (energy, name) in zip(CAL_MIX2_E_KL_FULL, CAL_MIX2_LINE_KL_FULL):
    CAL_MIX2_KL_FULL[name] = energy

CAL_MIX3_E_KL_FULL = [    183,   277,   393,    525,    615,     705,     719,     743,    811,     852,     869,      930,     950]
CAL_MIX3_LINE_KL_FULL = [ 'B K', 'C K', 'N K', 'O K', 'Fe Ll', 'Fe La', 'Fe Lb', 'Ni Ll', 'Cu Ll', 'Ni La', 'Ni Lb', 'Cu La', 'Cu Lb']
CAL_MIX3_KL_FULL = {}
for (energy, name) in zip(CAL_MIX3_E_KL_FULL, CAL_MIX3_LINE_KL_FULL):
    CAL_MIX3_KL_FULL[name] = energy

CAL_MIX_KL_FULL = {}
CAL_MIX_KL = {}
for i in [1,2,3]:
    CAL_MIX_KL_FULL[i] = eval('CAL_MIX%d_KL_FULL'%i)
    CAL_MIX_KL[i] = eval('CAL_MIX%d_KL'%i)

# CAL_MIX_TITUS_FULL = {}
CAL_MIX_TITUS = {}
# for i in [1,2,3]:
#     CAL_MIX_TITUS_FULL[i] = eval('CAL_MIX%d_TITUS_FULL'%i)
#     CAL_MIX_TITUS[i] = eval('CAL_MIX%d_TITUS'%i)
CAL_MIX_TITUS[2] = CAL_MIX2_TITUS
CAL_MIX_TITUS[3] = CAL_MIX3_TITUS

ROI_DICT = {}
ROI_DICT['C'] = [200, 300]
ROI_DICT['N'] = [350, 450]
ROI_DICT['O'] = [450, 600]
ROI_DICT['Fe'] = [685, 740]

ROI_FeLab = [680., 740.]
ROI_FeLln = [590., 630.]
ROI_OK = [450., 540.]

PIXEL_GROUP_DICT = {}
PIXEL_GROUP_DICT[101] = {}
PIXEL_GROUP_DICT[101]['center'] = [15, 7, 83, 135, 443, 473, 113, 127, 367, 353, 233, 203, 375, 323, 247, 255]
PIXEL_GROUP_DICT[101]['top'] = [13, 3, 115, 77, 39, 33, 85, 79, 11, 1, 117, 107, 51, 31, 87, 81]
PIXEL_GROUP_DICT[101]['bottom'] = [321, 327, 271, 291, 347, 357, 241, 251, 319, 325, 273, 279, 317, 355, 243, 253]
PIXEL_GROUP_DICT[101]['left'] = [437, 439, 467, 441, 475, 445, 477, 447, 363, 393, 361, 391, 373, 399, 371, 411]
PIXEL_GROUP_DICT[101]['right'] = [171, 131, 159, 133, 151, 121, 153, 123, 207, 237, 205, 235, 201, 227, 199, 197]
PIXEL_GROUP_DICT[101]['tl'] = [59, 53, 25, 27, 427, 23, 55, 57, 457, 425, 455, 29, 451, 421, 453, 423]
PIXEL_GROUP_DICT[101]['tr'] = [91, 61, 97, 67, 93, 65, 143, 173, 63, 95, 175, 145, 149, 177, 147, 179]
PIXEL_GROUP_DICT[101]['bl'] = [419, 387, 417, 389, 385, 415, 335, 303, 413, 383, 305, 333, 307, 337, 301, 331]
PIXEL_GROUP_DICT[101]['br'] = [183, 213, 181, 211, 269, 215, 185, 217, 297, 295, 263, 187, 267, 265, 293, 299]
PIXEL_GROUP_DICT[133] = {}
PIXEL_GROUP_DICT[133]['center'] = [15, 7, 83, 135, 443, 473, 113, 127, 367, 353, 233, 203, 375, 323, 247, 255]
PIXEL_GROUP_DICT[133]['top'] = [13, 3, 115, 77, 39, 33, 85, 79, 11, 1, 117, 107, 51, 31, 87, 81]
PIXEL_GROUP_DICT[133]['bottom'] = [321, 327, 271, 291, 347, 357, 241, 251, 319, 325, 273, 279, 317, 355, 243, 253]
PIXEL_GROUP_DICT[133]['left'] = [171, 131, 159, 133, 151, 121, 153, 123, 207, 237, 205, 235, 201, 227, 199, 197]
PIXEL_GROUP_DICT[133]['right'] = [437, 439, 467, 441, 475, 445, 477, 447, 363, 393, 361, 391, 373, 399, 371, 411]
PIXEL_GROUP_DICT[133]['tl'] = [91, 61, 97, 67, 93, 65, 143, 173, 63, 95, 175, 145, 149, 177, 147, 179]
PIXEL_GROUP_DICT[133]['tr'] = [59, 53, 25, 27, 427, 23, 55, 57, 457, 425, 455, 29, 451, 421, 453, 423]
PIXEL_GROUP_DICT[133]['bl'] = [183, 213, 181, 211, 269, 215, 185, 217, 297, 295, 263, 187, 267, 265, 293, 299]
PIXEL_GROUP_DICT[133]['br'] = [419, 387, 417, 389, 385, 415, 335, 303, 413, 383, 305, 333, 307, 337, 301, 331]

# gaussian = lambda x, A, x0, sigma, b: A/sigma/np.sqrt(2.*np.pi)*np.exp(-(x-x0)**2/(2.*sigma**2)) + b
def gaussian(x, A, x0, sigma, b):
    return A/sigma/np.sqrt(2.*np.pi)*np.exp(-(x-x0)**2/(2.*sigma**2)) + b
# tail_ftn = lambda x, A, x0, sigma, tailfrac, tailtau: A*tailfrac/2./tailtau*np.exp((x-x0)/tailtau+sigma**2/2./tailtau**2)*erfc(1/np.sqrt(2)*((x-x0)/sigma+sigma/tailtau))
def tail_ftn(x, A, x0, sigma, tailfrac, tailtau):
    return A*tailfrac/2./tailtau*np.exp((x-x0)/tailtau+sigma**2/2./tailtau**2)*erfc(1/np.sqrt(2)*((x-x0)/sigma+sigma/tailtau))
# tail_ftn_isol = lambda x, A, x0, sigma, tailtau: A*np.exp((x-x0)/tailtau+sigma**2/2./tailtau**2)*erfc(1/np.sqrt(2)*((x-x0)/sigma+sigma/tailtau))
def tail_ftn_isol(x, A, x0, sigma, tailtau):
    return A*np.exp((x-x0)/tailtau+sigma**2/2./tailtau**2)*erfc(1/np.sqrt(2)*((x-x0)/sigma+sigma/tailtau))
#check if tail_ftn normalization is correct => correct
#[0.5*(erf(x/(np.sqrt(2)*1.2)) + np.exp((1.2**2+2*0.7*x)/2/0.7**2)*erfc((1.2/0.7+x)/np.sqrt(2))) for x in [10,-10]]
# gaussian_tailed = lambda x, A, x0, sigma, b, tailfrac, tailtau: A/sigma/np.sqrt(2.*np.pi)*np.exp(-(x-x0)**2/(2.*sigma**2)) + A*tailfrac/2./tailtau*np.exp((x-x0)/tailtau+sigma**2/2./tailtau**2)*erfc(1/np.sqrt(2)*((x-x0)/sigma+sigma/tailtau)) + b
def gaussian_tailed(x, A, x0, sigma, b, tailfrac, tailtau):
    return A/sigma/np.sqrt(2.*np.pi)*np.exp(-(x-x0)**2/(2.*sigma**2)) + A*tailfrac/2./tailtau*np.exp((x-x0)/tailtau+sigma**2/2./tailtau**2)*erfc(1/np.sqrt(2)*((x-x0)/sigma+sigma/tailtau)) + b
# gaussian_ML_old = lambda x, fwhm, x0, a, b: a*np.exp(-(x-x0)**2/(2.*(fwhm/SIGMA2FWHM)**2)) + b
def gaussian_ML_old(x, fwhm, x0, a, b):
    return a*np.exp(-(x-x0)**2/(2.*(fwhm/SIGMA2FWHM)**2)) + b
# gaussian_ML = lambda (fwhm, x0, a, b), x: a*np.exp(-(x-x0)**2/(2.*(fwhm/SIGMA2FWHM)**2)) + b # does not work in python3
# gaussian_ML = lambda x, fwhm, x0, a, b: a*np.exp(-(x-x0)**2/(2.*(fwhm/SIGMA2FWHM)**2)) + b # this might not work.
def gaussian_ML(x, fwhm, x0, a, b):
    return a*np.exp(-(x-x0)**2/(2.*(fwhm/SIGMA2FWHM)**2)) + b
# gaussian_tailed_ML_old = lambda x, a, x0, sigma, tailfrac, tailtau: a*np.exp(-(x-x0)**2/(2.*sigma**2)) + a/2./tailtau*np.exp((x-x0)/tailtau+sigma**2/2./tailtau**2)*erfc(1/np.sqrt(2)*((x-x0)/sigma+sigma/tailtau))
def gaussian_tailed_ML_old(x, a, x0, sigma, tailfrac, tailtau):
    return a*np.exp(-(x-x0)**2/(2.*sigma**2)) + a/2./tailtau*np.exp((x-x0)/tailtau+sigma**2/2./tailtau**2)*erfc(1/np.sqrt(2)*((x-x0)/sigma+sigma/tailtau))
# gaussian_tailed_ML = lambda (a, x0, sigma, tailfrac, tailtau), x: a*np.exp(-(x-x0)**2/(2.*sigma**2)) + a/2./tailtau*np.exp((x-x0)/tailtau+sigma**2/2./tailtau**2)*erfc(1/np.sqrt(2)*((x-x0)/sigma+sigma/tailtau)) # does not work in python3
# gaussian_tailed_ML = lambda x, a, x0, sigma, tailfrac, tailtau: a*np.exp(-(x-x0)**2/(2.*sigma**2)) + a/2./tailtau*np.exp((x-x0)/tailtau+sigma**2/2./tailtau**2)*erfc(1/np.sqrt(2)*((x-x0)/sigma+sigma/tailtau)) # this might not work.
def gaussian_tailed_ML(x, a, x0, sigma, tailfrac, tailtau):
    return a*np.exp(-(x-x0)**2/(2.*sigma**2)) + a/2./tailtau*np.exp((x-x0)/tailtau+sigma**2/2./tailtau**2)*erfc(1/np.sqrt(2)*((x-x0)/sigma+sigma/tailtau)) # this might not work.

# two_gaussian = lambda x, A_1, x0_1, sigma_1, A_2, x0_2, sigma_2, b: A_1/sigma_1/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_1**2)) + A_2/sigma_2/np.sqrt(2.*np.pi)*np.exp(-(x-x0_2)**2/(2.*sigma_2**2)) + b
def two_gaussian(x, A_1, x0_1, sigma_1, A_2, x0_2, sigma_2, b):
    return A_1/sigma_1/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_1**2)) + A_2/sigma_2/np.sqrt(2.*np.pi)*np.exp(-(x-x0_2)**2/(2.*sigma_2**2)) + b
#two_gaussian_tailed = lambda x, A_1, x0_1, sigma_1, A_2, x0_2, sigma_2, b, tailfrac, tailtau: A_1/sigma_1/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_1**2)) + A_1*tailfrac/2./tailtau*np.exp((x-x0_1)/tailtau+sigma_1**2/2./tailtau**2)*erfc(1/np.sqrt(2)*((x-x0_1)/sigma_1+sigma_1/tailtau)) + A_2/sigma_2/np.sqrt(2.*np.pi)*np.exp(-(x-x0_2)**2/(2.*sigma_2**2)) + b
#two_gaussian_tailed = lambda x, A_1, x0_1, sigma_1, A_2, x0_2, sigma_2, b, tailfrac, tailtau: A_1/sigma_1/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_1**2)) + A_2*tailfrac/2./tailtau*np.exp((x-x0_2)/tailtau+sigma_2**2/2./tailtau**2)*erfc(1/np.sqrt(2)*((x-x0_2)/sigma_2+sigma_2/tailtau)) + A_2/sigma_2/np.sqrt(2.*np.pi)*np.exp(-(x-x0_2)**2/(2.*sigma_2**2)) + b
# two_gaussian_tailed = lambda x, A_1, x0_1, sigma_1, A_2, x0_2, sigma_2, b, tailfrac, tailtau, sigma_3: A_1/sigma_1/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_1**2)) + A_2*tailfrac/2./tailtau*np.exp((x-x0_2)/tailtau+sigma_3**2/2./tailtau**2)*erfc(1/np.sqrt(2)*((x-x0_2)/sigma_3+sigma_3/tailtau)) + A_2/sigma_2/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_2**2)) + b
def two_gaussian_tailed(x, A_1, x0_1, sigma_1, A_2, x0_2, sigma_2, b, tailfrac, tailtau, sigma_3):
    return A_1/sigma_1/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_1**2)) + A_2*tailfrac/2./tailtau*np.exp((x-x0_2)/tailtau+sigma_3**2/2./tailtau**2)*erfc(1/np.sqrt(2)*((x-x0_2)/sigma_3+sigma_3/tailtau)) + A_2/sigma_2/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_2**2)) + b
# two_gaussian_tailed_reduced1 = lambda x, A_1, x0_1, sigma_1, A_2, x0_2, sigma_2, b, tailfrac, tailtau: A_1/sigma_1/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_1**2)) + A_2*tailfrac/2./tailtau*np.exp((x-x0_2)/tailtau+sigma_2**2/2./tailtau**2)*erfc(1/np.sqrt(2)*((x-x0_2)/sigma_2+sigma_2/tailtau)) + A_2/sigma_2/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_2**2)) + b
def two_gaussian_tailed_reduced1(x, A_1, x0_1, sigma_1, A_2, x0_2, sigma_2, b, tailfrac, tailtau):
    return A_1/sigma_1/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_1**2)) + A_2*tailfrac/2./tailtau*np.exp((x-x0_2)/tailtau+sigma_2**2/2./tailtau**2)*erfc(1/np.sqrt(2)*((x-x0_2)/sigma_2+sigma_2/tailtau)) + A_2/sigma_2/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_2**2)) + b
# two_gaussian_tailed_reduced2 = lambda x, A_1, x0_1, sigma_1, A_2, sigma_2, b, tailfrac, tailtau: A_1/sigma_1/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_1**2)) + A_2*tailfrac/2./tailtau*np.exp((x-x0_1)/tailtau+sigma_2**2/2./tailtau**2)*erfc(1/np.sqrt(2)*((x-x0_1)/sigma_2+sigma_2/tailtau)) + A_2/sigma_2/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_2**2)) + b
def two_gaussian_tailed_reduced2(x, A_1, x0_1, sigma_1, A_2, sigma_2, b, tailfrac, tailtau):
    return A_1/sigma_1/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_1**2)) + A_2*tailfrac/2./tailtau*np.exp((x-x0_1)/tailtau+sigma_2**2/2./tailtau**2)*erfc(1/np.sqrt(2)*((x-x0_1)/sigma_2+sigma_2/tailtau)) + A_2/sigma_2/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_2**2)) + b
#two_gaussian_tailed_reduced = lambda x, A_1, x0_1, sigma_1, A_2, sigma_2, b, tailfrac, tailtau: A_1/sigma_1/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_1**2)) + A_2*tailfrac/2./tailtau*np.exp((x-x0_1)/tailtau+sigma_2**2/2./tailtau**2)*erfc(1/np.sqrt(2)*((x-x0_1)/sigma_2+sigma_2/tailtau)) + A_2/sigma_2/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_2**2)) + b
#two_gaussian_tailed_reduced = lambda x, A_1, x0_1, sigma_1, A_2, sigma_2, b, tailfrac, tailtau: A_1/sigma_1/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_1**2)) + A_1*tailfrac/2./tailtau*np.exp((x-x0_1)/tailtau+sigma_1**2/2./tailtau**2)*erfc(1/np.sqrt(2)*((x-x0_1)/sigma_1+sigma_1/tailtau)) + A_2/sigma_2/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_2**2)) + b

# two_gaussian_2tailed = lambda x, A_1, x0_1, sigma_1, A_2, x0_2, sigma_2, b, tailfrac_1, tailtau_1, tailfrac_2, tailtau_2: A_1/sigma_1/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_1**2)) + A_1*tailfrac_1/2./tailtau_1*np.exp((x-x0_1)/tailtau_1+sigma_1**2/2./tailtau_1**2)*erfc(1/np.sqrt(2)*((x-x0_1)/sigma_1+sigma_1/tailtau_1)) + A_2/sigma_2/np.sqrt(2.*np.pi)*np.exp(-(x-x0_2)**2/(2.*sigma_2**2)) + A_2*tailfrac_2/2./tailtau_2*np.exp((x-x0_2)/tailtau_2+sigma_2**2/2./tailtau_2**2)*erfc(1/np.sqrt(2)*((x-x0_2)/sigma_2+sigma_2/tailtau_2)) + b
def two_gaussian_2tailed(x, A_1, x0_1, sigma_1, A_2, x0_2, sigma_2, b, tailfrac_1, tailtau_1, tailfrac_2, tailtau_2):
    return A_1/sigma_1/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_1**2)) + A_1*tailfrac_1/2./tailtau_1*np.exp((x-x0_1)/tailtau_1+sigma_1**2/2./tailtau_1**2)*erfc(1/np.sqrt(2)*((x-x0_1)/sigma_1+sigma_1/tailtau_1)) + A_2/sigma_2/np.sqrt(2.*np.pi)*np.exp(-(x-x0_2)**2/(2.*sigma_2**2)) + A_2*tailfrac_2/2./tailtau_2*np.exp((x-x0_2)/tailtau_2+sigma_2**2/2./tailtau_2**2)*erfc(1/np.sqrt(2)*((x-x0_2)/sigma_2+sigma_2/tailtau_2)) + b
# two_gaussian_2tailed_reduced = lambda x, A_1, x0_1, sigma_1, A_2, sigma_2, b, tailfrac_1, tailtau_1, tailfrac_2, tailtau_2: A_1/sigma_1/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_1**2)) + A_1*tailfrac_1/2./tailtau_1*np.exp((x-x0_1)/tailtau_1+sigma_1**2/2./tailtau_1**2)*erfc(1/np.sqrt(2)*((x-x0_1)/sigma_1+sigma_1/tailtau_1)) + A_2/sigma_2/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_2**2)) + A_2*tailfrac_2/2./tailtau_2*np.exp((x-x0_1)/tailtau_2+sigma_2**2/2./tailtau_2**2)*erfc(1/np.sqrt(2)*((x-x0_1)/sigma_2+sigma_2/tailtau_2)) + b
def two_gaussian_2tailed_reduced(x, A_1, x0_1, sigma_1, A_2, sigma_2, b, tailfrac_1, tailtau_1, tailfrac_2, tailtau_2):
    return A_1/sigma_1/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_1**2)) + A_1*tailfrac_1/2./tailtau_1*np.exp((x-x0_1)/tailtau_1+sigma_1**2/2./tailtau_1**2)*erfc(1/np.sqrt(2)*((x-x0_1)/sigma_1+sigma_1/tailtau_1)) + A_2/sigma_2/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_2**2)) + A_2*tailfrac_2/2./tailtau_2*np.exp((x-x0_1)/tailtau_2+sigma_2**2/2./tailtau_2**2)*erfc(1/np.sqrt(2)*((x-x0_1)/sigma_2+sigma_2/tailtau_2)) + b
# one_gaussian_2tailed = lambda x, A_1, x0_1, sigma_1, A_2, x0_2, sigma_2, b, tailfrac_1, tailtau_1, tailtau_2: A_1/sigma_1/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_1**2)) + A_1*tailfrac_1/2./tailtau_1*np.exp((x-x0_1)/tailtau_1+sigma_1**2/2./tailtau_1**2)*erfc(1/np.sqrt(2)*((x-x0_1)/sigma_1+sigma_1/tailtau_1)) + A_2*np.exp((x-x0_2)/tailtau_2+sigma_2**2/2./tailtau_2**2)*erfc(1/np.sqrt(2)*((x-x0_2)/sigma_2+sigma_2/tailtau_2)) + b
def one_gaussian_2tailed(x, A_1, x0_1, sigma_1, A_2, x0_2, sigma_2, b, tailfrac_1, tailtau_1, tailtau_2):
    return A_1/sigma_1/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_1**2)) + A_1*tailfrac_1/2./tailtau_1*np.exp((x-x0_1)/tailtau_1+sigma_1**2/2./tailtau_1**2)*erfc(1/np.sqrt(2)*((x-x0_1)/sigma_1+sigma_1/tailtau_1)) + A_2*np.exp((x-x0_2)/tailtau_2+sigma_2**2/2./tailtau_2**2)*erfc(1/np.sqrt(2)*((x-x0_2)/sigma_2+sigma_2/tailtau_2)) + b

# one_gaussian_tailed = lambda x, A_1, x0_1, sigma_1, A_2, x0_2, sigma_2, b, tailtau_2: A_1/sigma_1/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_1**2)) + A_2*np.exp((x-x0_2)/tailtau_2+sigma_2**2/2./tailtau_2**2)*erfc(1/np.sqrt(2)*((x-x0_2)/sigma_2+sigma_2/tailtau_2)) + b
def one_gaussian_tailed(x, A_1, x0_1, sigma_1, A_2, x0_2, sigma_2, b, tailtau_2):
    return A_1/sigma_1/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_1**2)) + A_2*np.exp((x-x0_2)/tailtau_2+sigma_2**2/2./tailtau_2**2)*erfc(1/np.sqrt(2)*((x-x0_2)/sigma_2+sigma_2/tailtau_2)) + b
# one_gaussian_tailed_reduced = lambda x, A_1, x0_1, sigma_1, A_2, x0_2, b, tailtau_2: A_1/sigma_1/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_1**2)) + A_2*np.exp((x-x0_2)/tailtau_2+sigma_1**2/2./tailtau_2**2)*erfc(1/np.sqrt(2)*((x-x0_2)/sigma_1+sigma_1/tailtau_2)) + b
def one_gaussian_tailed_reduced(x, A_1, x0_1, sigma_1, A_2, x0_2, b, tailtau_2):
    return A_1/sigma_1/np.sqrt(2.*np.pi)*np.exp(-(x-x0_1)**2/(2.*sigma_1**2)) + A_2*np.exp((x-x0_2)/tailtau_2+sigma_1**2/2./tailtau_2**2)*erfc(1/np.sqrt(2)*((x-x0_2)/sigma_1+sigma_1/tailtau_2)) + b
ph2e_poly2nd_thru_origin = lambda x, a1, a2: a1 * x + a2 * x**2
ph2e_poly3rd_thru_origin = lambda x, a1, a2, a3: a1 * x + a2 * x**2 + a3 * x**3
ph2e_poly4th_thru_origin = lambda x, a1, a2, a3, a4: a1 * x + a2 * x**2 + a3 * x**3 + a4 * x**4
ph2e_poly5th_thru_origin = lambda x, a1, a2, a3, a4, a5: a1 * x + a2 * x**2 + a3 * x**3 + a4 * x**4 * a5 * x**5
poly_dict = {2:ph2e_poly2nd_thru_origin, 3:ph2e_poly3rd_thru_origin, 4:ph2e_poly4th_thru_origin,
             5:ph2e_poly5th_thru_origin}

def lorentzian(x, A, x0, w):
    '''
    # A is area, w is full width
    # for the peak value, calculate 2*A/np.pi/w
    '''
    return 2*A/np.pi*(w/(4*(x-x0)**2+w**2))

def gaussian_no_bg(x, A, x0, w):
    '''
    # A is area, w is FWHM
    '''
    return A*np.exp((-4*np.log(2)*(x-x0)**2)/w**2)/(w*np.sqrt(np.pi/(4*np.log(2))))

def psdvoigt(x, A, xc, w, m):
    '''
    # A is area, w is full width
    '''
    return m*lorentzian(x, A, xc, w) + (1-m)*gaussian_no_bg(x, A, xc, w)

def bigaussian(x, A, x0, w1, w2):
    func = np.zeros_like(x)
    for i in range(0, len(x)):
        if x[i] > x0:
            break
        func[i] = A*np.exp(-0.5*(x[i]-x0)**2/w1**2)
    for j in range(i, len(x)):
        func[j] = A*np.exp(-0.5*(x[j]-x0)**2/w2**2)
    return func

data_file_path = os.path.dirname(os.path.abspath(__file__))
data_file_path = os.path.join(data_file_path, 'data')
# print(resp_file_path)
# resp_data = np.loadtxt(os.path.join(data_file_path, '750_ev_resp_for_mike'))
# resp_y = resp_data[1,:]
# resp_x = resp_data[0,:]
# resp_x = resp_x - resp_x[np.argmax(resp_y)]
# resp_spline = UnivariateSpline(resp_x, resp_y, ext=1, s=0)

array_map_datafile = os.path.join(data_file_path, 'ar14_30rows_map_towerorder_looking_at_array.cfg')
# array_map_data[133] = os.path.join(data_file_path, 'ar14_30rows_map_towerorder_looking_at_array.cfg') # need the file

plt.ion()

# below is needed to keep these values when useful_ftns is reloaded
if 'BASE_FOLDER' not in globals():
    BASE_FOLDER = 'no name yet'

if 'PULSE_FOLDER' not in globals():
    # global PULSE_FOLDER
    PULSE_FOLDER = 'no name yet'

if 'NOISE_FOLDER' not in globals():
    # global NOISE_FOLDER
    NOISE_FOLDER = 'no name yet'

if 'PULSE_FOLDER_CAL' not in globals():
    PULSE_FOLDER_CAL = 'no name yet'
# if 'NOISE_FOLDER_CAL' not in globals():
#     NOISE_FOLDER_CAL = 'no name yet'

# BASE_FOLDER usage example
"""
if BASE_FOLDER == 'no name yet':
    print('no name yet')
    return
else:
    print(BASE_FOLDER)
    return
"""

def set_base_folder(bname=None):
    global BASE_FOLDER
    if bname is None:
        # in python3, input returns a string while in python2 it returns an int if int was received
        try: # input() gives EOF related NameError or SyntaxError on Yanru's laptop, so decided to use try here
            BASE_FOLDER = input("""Enter BASE_FOLDER with quotation or number. e.g. (modify if needed):
    \'/Volumes/Seagate Expansion Drive/data/data/2016_scienceE\' #1
    \'/Users/sangjun2/Documents/Data\' #2
    \'/media/data/2016_scienceE\' #3
    \'/media/data/2017_scienceA\' #4
    \'/Volumes/media_data/2017_scienceA\' #5
    \'E:\data\BL101\' #6
    : """)
        except:
            BASE_FOLDER = raw_input()

    else:
        BASE_FOLDER = bname

    if BASE_FOLDER in [1, '1']:
        BASE_FOLDER = '/Volumes/Seagate Expansion Drive/data/data/2016_scienceE'
    elif BASE_FOLDER in [2, '2']:
        BASE_FOLDER = '/Users/sangjun2/Documents/Data'
    elif BASE_FOLDER in [3, '3']:
        BASE_FOLDER = '/media/data/2016_scienceE'
    elif BASE_FOLDER in [4, '4']:
        BASE_FOLDER = '/media/data/2017_scienceA'
    elif BASE_FOLDER in [5, '5']:
        BASE_FOLDER = '/Volumes/media_data/2017_scienceA'
    elif BASE_FOLDER in [6, '6']:
        BASE_FOLDER = u'E:\data\BL101'
    BASE_FOLDER = BASE_FOLDER.strip("'") # added because input() in python3 is different
    print('\nuseful_ftns.BASE_FOLDER = \'%s\'\n'%BASE_FOLDER)


import platform
if 'USERNAME' in os.environ.keys(): # username not used yet
    USERNAME = os.environ['USERNAME']
elif 'USER' in os.environ.keys():
    USERNAME = os.environ['USER']
PCNAME = platform.node()


def _guess_base_folder(folder_date):

    global PCNAME, USERNAME

    if type(folder_date) != str:
        folder_date = str(folder_date)

    guessed_base_folder = None
    if 'ssrl-pc91893' in PCNAME: # macpro
        guessed_base_folder = '/Volumes/Data/data/' + folder_date[:4]
    elif 'bl133proclx' == PCNAME:
        guessed_base_folder = '/tes/' + folder_date[:4]
    elif 'bl133plotpc' == PCNAME:
        guessed_base_folder = 'Z:\\'  + folder_date[:4] # Z is tes ubuntu mounted on bl133plotpc
    elif ('BL101PROC01' == PCNAME) or ('bl132proc01' == PCNAME):
        guessed_base_folder = '/data/'
        if folder_date < '20190319':
            guessed_base_folder += '2018'
        else:
            guessed_base_folder += folder_date[:4]
    elif 'sangjun2' == USERNAME: # MBP15
        guessed_base_folder = '/Users/sangjun2/Documents/Data/' + folder_date[:4]

    return guessed_base_folder


# def set_pulse_noise_folders(dir_p=None,dir_n=None):
#     global PULSE_FOLDER
#     global NOISE_FOLDER
#     if (dir_p is None) or (dir_n is None):
#         PULSE_FOLDER = raw_input('PULSE_FOLDER (e.g. 20170101_002): ')
#         NOISE_FOLDER = raw_input('NOISE_FOLDER (e.g. 20170101_001): ')
#     else:
#         PULSE_FOLDER = dir_p
#         NOISE_FOLDER = dir_n

def set_pulse_folder(dir_p=None):
    global PULSE_FOLDER
    if dir_p is None:
        try:
            # PULSE_FOLDER = raw_input('PULSE_FOLDER (e.g. 20170101_002): ')
            PULSE_FOLDER = input('PULSE_FOLDER (e.g. 20170101_002): ')
        except:
            PULSE_FOLDER = raw_input()
    else:
        PULSE_FOLDER = dir_p
    PULSE_FOLDER = PULSE_FOLDER.strip("'").strip('"')

def set_noise_folder(dir_n=None):
    global NOISE_FOLDER
    if dir_n is None:
        try:
            # NOISE_FOLDER = raw_input('NOISE_FOLDER (e.g. 20170101_001): ')
            NOISE_FOLDER = input('NOISE_FOLDER (e.g. 20170101_001): ')
        except:
            NOISE_FOLDER = raw_input()
    else:
        NOISE_FOLDER = dir_n
    NOISE_FOLDER = NOISE_FOLDER.strip("'").strip('"')

# def set_cal_folders(dir_p_cal=None,dir_n_cal=None):
#     global PULSE_FOLDER_CAL
#     global NOISE_FOLDER_CAL
#     if (dir_p_cal is None) or (dir_n_cal is None):
#         PULSE_FOLDER_CAL = raw_input('PULSE_FOLDER_CAL (e.g. 20170101_002): ')
#         NOISE_FOLDER_CAL = raw_input('NOISE_FOLDER_CAL (e.g. 20170101_001): ')
#     else:
#         PULSE_FOLDER_CAL = dir_p_cal
#         NOISE_FOLDER_CAL = dir_n_cal


def set_cal_folder(dir_p_cal=None):
    # this only uses one cal folder, not a list of cal data. needs to be modified
    global PULSE_FOLDER_CAL
    if dir_p_cal is None:
        try:
            # PULSE_FOLDER_CAL = raw_input('PULSE_FOLDER_CAL (for multiple folders: use 20170101_002 20170101_010): ')
            PULSE_FOLDER_CAL = input('PULSE_FOLDER_CAL (e.g. 20170101_002 20170101_010 to have two folders, or [0, 3, -1] or 0 if called inside batch1of3): ')
        except:
            PULSE_FOLDER_CAL = raw_input()
        if ' ' in PULSE_FOLDER_CAL: # this is the case with multiple folders
            PULSE_FOLDER_CAL = PULSE_FOLDER_CAL.split()
    else:
        PULSE_FOLDER_CAL = dir_p_cal
    PULSE_FOLDER_CAL = PULSE_FOLDER_CAL.strip("'").strip('"')


def select_folders():
    # copied from https://stackoverflow.com/questions/38252419/qt-get-qfiledialog-to-select-and-return-multiple-folders
    from PyQt4 import QtGui as qt
    file_dialog = qt.QFileDialog()
    file_dialog.setFileMode(qt.QFileDialog.DirectoryOnly)
    file_dialog.setOption(qt.QFileDialog.DontUseNativeDialog, True)
    file_view = file_dialog.findChild(qt.QListView, 'listView')

    # to make it possible to select multiple directories:
    if file_view:
        file_view.setSelectionMode(qt.QAbstractItemView.MultiSelection)
    f_tree_view = file_dialog.findChild(qt.QTreeView)
    if f_tree_view:
        f_tree_view.setSelectionMode(qt.QAbstractItemView.MultiSelection)

    if file_dialog.exec_():
        paths = file_dialog.selectedFiles()

    dir_ps = []
    for path in paths:
        dir_ps.append(str(path).split(os.sep)[-1])
    dir_ps.sort()

    return dir_ps


def delete_noise_hdf5(dir_n=None, hdf5_filename_n=None):

    if hdf5_filename_n is None:
        if dir_n is None:
            if NOISE_FOLDER == 'no name yet':
                print('noise folder should be provided')
                return
            else:
                dir_n = NOISE_FOLDER
        else:
            if NOISE_FOLDER != dir_n:
                print('dir_n does not match NOISE_FOLDER')
                return
        path_n = os.path.join(BASE_FOLDER, dir_n.split('_')[0], dir_n) # full path for noise folder
        hdf5_filename_n = os.path.join(path_n, dir_n.strip(os.sep)+'_mass.hdf5')
    if os.path.isfile(hdf5_filename_n):
        os.remove(hdf5_filename_n)
        print('# %s was deleted'%hdf5_filename_n)


def dir_ps_generator(base_list, start_end_per_base=None, suffix=None, skip_cal_folders=False):
    '''
    This does not skip missing folders or folders without x-rays.
    So maybe missing folders should be taken care of during pulse processing.
    example:
    base_list = [20170103, 20170104], start_end_per_base = [5,17,1,4] or [5,-1,1,-1] (-1 is for the last folder)
    base_list = 20201004, suffix=[1,2,5] will make ['20201004_001', '20201004_002', '20201004_005']
    base_list = ['20201003_002', '20201004_002']
    '''

    from datetime import date, timedelta

    if type(base_list) in [str, int]:
        base_list = [base_list]
    base_list[:] = [str(x) for x in base_list]

    if '_' in base_list[0]: # when the input is like ['20201003_002','20201004_002']
        if len(base_list[0].split('_')) != 2:
            print('Provide a valid folder format')
            return
        if len(base_list) != 2:
            print('Provide a list with two folder names')
            return
        if (start_end_per_base is not None) or (suffix is not None):
            print('Do not provide start_end_per_base or suffix for this type of args')
            return
        if base_list[0].split('_')[0] == base_list[1].split('_')[0]:
            start_end_per_base = [int(base_list[0].split('_')[1]),int(base_list[1].split('_')[1])]
            if start_end_per_base[1] < start_end_per_base[0]:
                print('Seems like the starting and ending folders are reversed. Check again.')
                return
            base_list = [base_list[0].split('_')[0]]
        else:
            first_day_str = base_list[0].split('_')[0]
            last_day_str = base_list[1].split('_')[0]
            start_end_per_base = [int(base_list[0].split('_')[1]),int(base_list[1].split('_')[1])]
            base_list = [first_day_str, last_day_str] # this should be here (after finding start_end_per_base)
            if last_day_str < first_day_str:
                print('Seems like the starting and ending folders are reversed. Check again.')
                return
            first_day_iso = first_day_str[:4] + '-' + first_day_str[4:6] + '-' + first_day_str[6:]
            last_day_iso = last_day_str[:4] + '-' + last_day_str[4:6] + '-' + last_day_str[6:]
            first_day = date.fromisoformat(first_day_iso)
            last_day = date.fromisoformat(last_day_iso)
            delta_days = int((last_day-first_day).days)
            start_end_per_base[1:1] = [-1, 1]*delta_days
            base_list[1:1] = [str(first_day+timedelta(dd)).replace('-', '') for dd in range(1, delta_days)]
            print(base_list)
            print(start_end_per_base)
    else:
        if (start_end_per_base is None) and (suffix is None):
            print('Provide indices or suffix')
            return
        if (start_end_per_base is not None) and (suffix is not None):
            print('Do not provide both indices and suffix')
            return

    dir_ps = []

    if start_end_per_base is not None:
        if 2 * len(base_list) != len(start_end_per_base):
            print('\nwrong format arguments')
            return
        if (base_list != sorted(base_list)):
            print('Please provide base_list in order')
            return
        # if string is given for start_end_per_base entries, convert them to integer
        for entry in start_end_per_base:
            entry = int(entry)

        if BASE_FOLDER == 'no name yet':
            print('# base folder not given.')
            guessed_base_folder = _guess_base_folder(base_list[0])
            if os.path.isdir(os.path.join(guessed_base_folder,base_list[0])):
                print('# base folder automatically identified as %s. If not correct, set it manually.'%guessed_base_folder)
                set_base_folder(guessed_base_folder)

        if BASE_FOLDER == 'no name yet':
            print('BASE_FOLDER is not yet set, but making the dir_p list anyway')
            for ii, base in enumerate(base_list):
                for jj in range(start_end_per_base[ii*2],start_end_per_base[ii*2+1]+1):
                    # if type(base) is not str:
                    #     base = str(base)
                    dir_ps.append(base+'_%03d'%jj)
        else:
            start_end_per_base[:] = [10000 if x==-1 else x for x in start_end_per_base] # 10000 is an arbitrary large number
            for ii, base in enumerate(base_list):
                for jj in range(start_end_per_base[ii*2],start_end_per_base[ii*2+1]+1):
                    # if type(base) is not str:
                    #     base = str(base)
                    dir_p = base+'_%03d'%jj
                    full_dir_p = os.path.join(BASE_FOLDER.strip("''"),base,dir_p)
                    if os.path.isdir(full_dir_p):
                        log_fname = os.path.join(full_dir_p,dir_p+'_log_old')
                        if os.path.exists(log_fname) is False:
                            log_fname = os.path.join(full_dir_p,dir_p+'_log')
                            if os.path.exists(log_fname) is False:
                                print('# log file (both log or log_old) does not exist in', full_dir_p)
                        if _is_data_folder(log_fname):
                            if skip_cal_folders is False:
                                dir_ps.append(dir_p)
                            else:  # if skipping cal folders
                                if _is_data_folder(log_fname) == 'data':
                                    dir_ps.append(dir_p)
                                else:
                                    print('Cal folder %s not included'%dir_p)
                        else:
                            print('Noise folder %s not included'%dir_p)
        if len(dir_ps) == 0:
            raise Exception('No folders found. Check again.')

    if suffix is not None: # rarely used.
        if len(base_list) > 1:
            print('Provide only one base in this case')
            return
        if type(suffix) in [str, int]:
            suffix = [suffix]
        # if string is given for suffix entries, convert them to integer
        for entry in suffix:
            dir_ps.append(base_list[0]+'_%03d'%int(entry))

    print('# number of folders: %d'%len(dir_ps))
    return dir_ps


def batch1of3(dir_ps, dir_n=None, dir_p_cals=None, forceNew=False, reset_cal=False, auto_cut=False, just_load=False, just_summarize=False, just_cut=False, skip_summarize=False, skip_cut=False, fast=False, attr='p_peak_value', dc_method='mass', use_hdf5=False, hdf5_type=None, use_new_filters=False, individual=False, share_filter=False, category=None, invert_data=False):
    '''
    1st step: make filter and apply it to all the dir_ps and dir_p_cals, and do drift correction all together
    returns data_list, which consists of dir_ps data + dir_p_cals data
    forceNew=True renames previous hdf5 files, which will automatically reset all the reduction
    reset_cal=True on the contrary, will only remake avg and filter of the cal data (only for non-hdf5)
    dir_ps: list of dir_p, including cal folders
    dir_p_cals: list of dir_p_cal. E.g. ['20181111_001', '20181111_004'] or [0, 3, -1] or 0 (indices for dir_ps)
        If not provided, it will ask for batch, but will just take the first dir_ps for individual
    ignore for now: if you want to reduce dir_ps only, use dir_p_cals = []
    use_hdf5=False is added (hdf5 is first created with TESGroup)
    use_hdf5=True
    hdf5_type = None, 'quick_mass', 'quick_reduced', 'pope'
    just_summarize=True & use_hdf5=False will stop after summarizing, before cutting
    just_summarize=True & use_hdf5=True will stop after TESGroup, assuming up to filtering is done.
    skip_cut=True will skip cutting, to resume processing after cutting, i.e., averaging
    use_new_filters=False is default
    individual=True processes a list of data separately
    individual=True & share_filter=True processes data separately but all the data uses the filter from the first data
    fast=False is added. If True, the analysis uses attr (p_peak_value by default) instead of p_filt_value
    dc_method='mass' is the default. another option is 'sj'
    to-do: check if hdf5 exists and if so, use it, if not, reduce from scratch
    '''
    global PULSE_FOLDER
    # global BASE_FOLDER

    if use_hdf5 is False:
        global NOISE_FOLDER
        dir_n_from_old_log = None
    global PULSE_FOLDER_CAL

    if type(dir_ps) in [int, str]: # this is to handle a single string input to dir_ps instead of a list
        if type(dir_ps) == int:  # needed?
            dir_ps = str(dir_ps)
        if type(dir_ps) == str:
            dir_ps = [dir_ps]

    if len(dir_ps) == 1:
        individual = True

    dir_p_cals_from_old_log = []

    # folder name checking skipped
    # for dir_p in dir_ps:
    #     if len(dir_p) != 12:
    #         print('# Seems like a wrong folder is given: %s'%dir_p)
    #         return # maybe skip that one instead of return?
    if np.any([not os.path.exists(dir_p) for dir_p in dir_ps]):  # if any of dir_ps is in the form of date_ddd, base folder needs to be defined
        if BASE_FOLDER == "no name yet":
            guessed_base_folder = _guess_base_folder(dir_ps[0])
            print('# base folder automatically identified as %s. If not correct, set it manually.'%guessed_base_folder)
            if os.path.isdir(guessed_base_folder):
                set_base_folder(guessed_base_folder)
            else:
                # print '# BASE_FOLDER is not set up yet. Run:\nuseful_ftns.set_base_folder()'
                # return
                set_base_folder()
        else:
            print('# BASE_FOLDER: %s'%BASE_FOLDER)
    else:  # this is the case when all of dir_ps is in full path such as /xxx/xxx/2022/20220619/20220619_001/
        dir_ps0 = dir_ps[0].split(os.sep)[-1]
        dir_ps0_one_up = dir_ps[0].split(os.sep)[-2]
        base_folder = dir_ps[0][:-len(dir_ps0)-len(dir_ps0_one_up)-1]
        set_base_folder(base_folder)
        dir_ps_temp = [dir_p.split(os.sep)[-1] for dir_p in dir_ps]
        dir_ps = dir_ps_temp[:]

    if not os.path.isdir(os.path.join(BASE_FOLDER,dir_ps[0][:8])):
        print('BASE_FOLDER exists, but data folder does not. Please check.')
        return

    if (mass.__version__ >= '0.6.5') and (category is None):
        category={}

    # look for noise folder from old_log
    log_fname = None
    if individual is True:
        dir_ns = []
    if use_hdf5 is False:
        for dir_p in dir_ps:
            path_log = os.path.join(BASE_FOLDER, dir_p.split('_')[0] ,dir_p)
            log_fnames = glob.glob(os.path.join(path_log,'*log*'))
            old_log_fnames = glob.glob(os.path.join(path_log,'*log_old'))
            if len(old_log_fnames) == 0:
                if len(log_fnames) == 0:
                    print('# no log file found. Provide log file name!')
                    # return reduced_data
                else:
                    log_fname = max(log_fnames, key=os.path.getctime) # the most recent one
            else:
                log_fname = max(old_log_fnames, key=os.path.getctime) # the most recent one
            if log_fname is not None:
                with open(log_fname, 'r') as f:
                    for line in f:
                        # n_lines += 1
                        if "dtype cal" in line:
                            print('Cal data detected')
                            dir_p_cals_from_old_log.append(dir_p)
                        if "noise_folder" in line:
                            dir_n_from_old_log = line.strip().split(os.sep)[-1]
                            if len(dir_n_from_old_log) != 12: # needed?
                                print('log file exists, but could not find the noise folder')
                                dir_n_from_old_log = None
                                break
                            dir_n = line.split('noise_folder')[-1].split(os.sep)[-1].strip()
                            print('noise folder found from the log file:', dir_n)
                            break # probably not needed because this would be already at the end of the for loop
            if individual is True:
                dir_ns.append(dir_n)

        if dir_n is None:
            set_noise_folder()
            dir_n = NOISE_FOLDER
            # if dir_n_from_old_log is not None:
            #     if NOISE_FOLDER == "no name yet":
            #         dir_n = dir_n_from_old_log
            #     else:
            #         if dir_n_from_old_log == NOISE_FOLDER:
            #             dir_n = dir_n_from_old_log
            #         else:
            #             print('Check: noise folder in the log file (%s) is different from NOISE_FOLDER (%s)'%(dir_n_from_old_log,NOISE_FOLDER))
            #             print('Passing a dir_n will solve the problem')
            #             return
            # elif (dir_n_from_old_log is None) and (NOISE_FOLDER == "no name yet"):
            #     # print 'dir_n is needed'
            #     # return
            #     set_noise_folder()
            #     dir_n = NOISE_FOLDER
            # else:
            #     dir_n = NOISE_FOLDER
        if individual is not True:
            path_n = os.path.join(BASE_FOLDER, dir_n.split('_')[0], dir_n) # full path for noise folder
            hdf5_filename_n = os.path.join(path_n, dir_n.strip(os.sep)+'_mass.hdf5')
        else:
            path_ns = [os.path.join(BASE_FOLDER, dir_n.split('_')[0], dir_n) for dir_n in dir_ns]
            hdf5_filename_ns = [os.path.join(path_n, dir_n.strip(os.sep)+'_mass.hdf5') for (path_n, dir_n) in zip(path_ns, dir_ns)]

    # if len(dir_ps) == 1:
    #     individual = True

    if (individual is True) or (just_load is True) or (just_summarize is True) or (just_cut is True):
        dir_p_cals = [dir_ps[0]]
    else:
        if dir_p_cals is None: # if list of dir_p_cals is not given, use just one cal dataset
            if len(dir_p_cals_from_old_log) == 0:
                set_cal_folder()
                if type(PULSE_FOLDER_CAL) is not list:
                    PULSE_FOLDER_CAL = [PULSE_FOLDER_CAL]
                if len(PULSE_FOLDER_CAL[0]) > 4: # if not like 20190319_001
                    dir_p_cals = PULSE_FOLDER_CAL
                else: # this is when indices are given
                    dir_p_cals = [dir_ps[int(PULSE_FOLDER_CAL_i)] for PULSE_FOLDER_CAL_i in PULSE_FOLDER_CAL]
                # old
                # if PULSE_FOLDER_CAL == "no name yet":
                #     set_cal_folder()
                # else:
                #     print('# PULSE_FOLDER_CAL: %s'%PULSE_FOLDER_CAL)
                # if type(PULSE_FOLDER_CAL) is not list:
                #     dir_p_cals = [PULSE_FOLDER_CAL]
                # else:
                #     dir_p_cals = PULSE_FOLDER_CAL
            else:
                dir_p_cals = dir_p_cals_from_old_log
        else:
            if type(dir_p_cals) is not list:
                dir_p_cals = [dir_p_cals]
            if type(dir_p_cals[0]) is int: # this is to handle dir_ps_cals = [0, 3, -1] or 0
                dir_p_cals = [dir_ps[dir_p_cal] for dir_p_cal in dir_p_cals]
                # PULSE_FOLDER_CAL = "no name yet"

    data_list = []
    cal_data_list = []

    if use_hdf5 is False:
        default_cuts = mass.core.controller.AnalysisControl(
            #pulse_average=(0.0, None), #0
            pretrigger_rms=(None, 55.0), #1
            pretrigger_mean_departure_from_median=(-200.0, 200.0), #2, allows a pretty big drift (previously 40, now 200)
            peak_value=(0.0, None), #3
            postpeak_deriv=(None, 35.0), #4
            rise_time_ms=(0, 0.12), #5
            peak_time_ms=(0, 0.17) #6
        )

        # available_chans = [None] * len(set.union(set(dir_ps),set(dir_p_cals)))
        available_chans = [None] * len(dir_ps)
        # for ii, dir_p in enumerate(sorted(set.union(set(dir_ps),set(dir_p_cals)))):
        if individual is not True:
            for ii, dir_p in enumerate(dir_ps):
                if mass.__version__ < '0.6.2':
                    available_chans[ii]=set(mass.ljh_get_channels_both(os.path.join(BASE_FOLDER, dir_p.split('_')[0],dir_p),os.path.join(BASE_FOLDER, dir_n.split('_')[0],dir_n)))
                else:
                    available_chans[ii]=set(mass.ljh_util.ljh_get_channels_both(os.path.join(BASE_FOLDER, dir_p.split('_')[0],dir_p),os.path.join(BASE_FOLDER, dir_n.split('_')[0],dir_n)))
                if len(available_chans[ii]) < 10:
                    print('# There is only %d chan data in %s folder. Check it.'%(len(available_chans[ii]),dir_ps))
            common_chans = reduce(set.intersection, available_chans)

    if individual is True:
        all_data_set = sorted(set(dir_ps))
    else:
        all_data_set = sorted(set.union(set(dir_ps),set(dir_p_cals)))

    for ii, dir_p in enumerate(all_data_set):
        path_p = os.path.join(BASE_FOLDER, dir_p.split('_')[0] ,dir_p) # full path for pulse folder
        if hdf5_type is None:
            hdf5_fname = path_p+os.sep+dir_p+"_mass.hdf5"  # this is the default hdf5
        elif hdf5_type=='quick_mass':
            hdf5_fname = path_p+os.sep+dir_p+"_quick_mass.hdf5"
        elif hdf5_type=='quick_reduced':
            hdf5_fname = path_p+os.sep+dir_p+"_quick_reduced.hdf5"
        elif hdf5_type=='pope': # e.g., 20180712_003.ljh_pope.hdf5
            hdf5_fname = path_p+os.sep+dir_p+".ljh_pope.hdf5"
        if os.path.exists(hdf5_fname):
            print(hdf5_fname, 'is found and will be used unless it is too small')
        if forceNew is True:
            # hdf5_filename = os.path.join(path_n, dir_n.strip(os.sep)+'_mass.hdf5')
            if os.path.isfile(hdf5_fname):
                hdf5_fname_bak = hdf5_fname[:-5]+'_'+time.strftime('%Y%m%d', time.localtime(os.path.getmtime(hdf5_fname)))+'.hdf5'
                os.rename(hdf5_fname, hdf5_fname_bak)
                print('# %s was renamed to %s\n'%(hdf5_fname,hdf5_fname_bak))
            else:
                print('# %s not found\n'%hdf5_fname)
        if use_hdf5 is False:
            if individual is True:
                if mass.__version__ < '0.6.2':
                    available_chans = mass.ljh_get_channels_both(path_p, path_ns[ii]) # for older mass
                    pulse_files = mass.ljh_chan_names(path_p, available_chans) # for older mass
                    noise_files = mass.ljh_chan_names(path_ns[ii], available_chans) # for older mass
                else:
                    available_chans = mass.ljh_util.ljh_get_channels_both(path_p, path_ns[ii])
                    pulse_files = mass.ljh_util.ljh_chan_names(os.path.join(BASE_FOLDER, dir_p.split('_')[0],dir_p),available_chans)
                    pulse_files = [pulse_file+'.ljh' for pulse_file in pulse_files]
                    noise_files = mass.ljh_util.ljh_chan_names(os.path.join(BASE_FOLDER, dir_ns[ii].split('_')[0],dir_ns[ii]),available_chans)
                    noise_files = [noise_file+'.ljh' for noise_file in noise_files]
            else:
                if mass.__version__ < '0.6.2':
                    # common_chans = mass.ljh_get_channels_both(path_p, path_n) # for older mass
                    pulse_files = mass.ljh_chan_names(path_p, common_chans) # for older mass
                    noise_files = mass.ljh_chan_names(path_n, common_chans) # for older mass
                else:
                    pulse_files = mass.ljh_util.ljh_chan_names(os.path.join(BASE_FOLDER, dir_p.split('_')[0],dir_p),common_chans)
                    pulse_files = [pulse_file+'.ljh' for pulse_file in pulse_files]
                    noise_files = mass.ljh_util.ljh_chan_names(os.path.join(BASE_FOLDER, dir_n.split('_')[0],dir_n),common_chans)
                    noise_files = [noise_file+'.ljh' for noise_file in noise_files]

            try:
                data = mass.TESGroup(pulse_files, noise_files)
                if invert_data is True:
                    for ds in data:
                        ds.invert_data = True
            except ValueError as e:
                print('first 10 pulse file names:', pulse_files[:10])
                print('first 10 noise file names:', noise_files[:10])
                print('\n', str(e))
                raise Exception('something wrong with the file names. Check folder names')
            except OSError as e: # this is the case with noise hdf5 file creation error
                print('\n', str(e))
                delete_noise_hdf5(hdf5_filename_n=hdf5_filename_n)
                print('noise hdf5 file is deleted.')
                data = mass.TESGroup(pulse_files, noise_files)

            if just_load is False:
                if (skip_summarize is False) or (just_summarize is True):
                    # data.sj_summarize_data(forceNew=forceNew) # mass now calculates peak time, so my function not needed.
                    data.summarize_data(forceNew=forceNew)
                    print('\n# summarizing done for %s\n'%path_p)
                if just_summarize is False:
                    if skip_cut is False:
                        if auto_cut is True:
                            print('# auto_cuts used')
                            data.auto_cuts(forceNew=True) # mass's auto-cut
                        else:
                            print('# default_cuts used')
                            data.sj_apply_cuts(cuts=default_cuts, forceNew=True, write2file=False, ptm_median_or_max='median') # forceNew=True, because False does not do cutting
                        print('\n# cutting done for %s\n'%path_p)
        else: # if use_hdf5 is True:
            if use_hdf5 is True: # to make it easier to read the code
                if hdf5_type is None: # hdf5_types: None/quick_mass/quick_reduced/pope
                    if (not os.path.isfile(hdf5_fname)) or (os.path.isfile(hdf5_fname) and os.path.getsize(hdf5_fname)<1e6): # if mass hdf5 does not exist or it exists but is too small
                        print('default hdf5 file (%s) is not found or is too small'%hdf5_fname)
                        hdf5_fname = path_p+os.sep+dir_p+"_quick_mass.hdf5"
                        if (not os.path.isfile(hdf5_fname)) or (os.path.isfile(hdf5_fname) and os.path.getsize(hdf5_fname)<1e6): # if mass hdf5 does not exist or it exists but is too small
                            print('quick_mass.hdf5 file (%s) is not found or is too small'%hdf5_fname)
                            hdf5_fname = path_p+os.sep+dir_p+".ljh_pope.hdf5"
                if (not os.path.isfile(hdf5_fname)) or (os.path.isfile(hdf5_fname) and os.path.getsize(hdf5_fname)<1e6): # if mass hdf5 does not exist or it exists but is too small
                    raise Exception('Cannot find a proper hdf5 file or %s for loading'%hdf5_fname)
            print(hdf5_fname, 'is being loaded.')
            try:
                data = mass.TESGroupHDF5(hdf5_fname)
            except KeyError: # can happen for some strange reason even for a pulse data
                print('KeyError happened in %s.'%dir_p)
                continue
            except OSError:
                print('OSError happened in %s.'%dir_p)
                continue
            for key, value in data.hdf5_file.items():
                try:
                    if type(value.attrs['why_bad'][0]) is str:
                        why_bad = value.attrs['why_bad'][0].split('')
                    else:
                        why_bad = value.attrs['why_bad'][0].decode() # this is to handle b'str'
                    data.set_chan_bad(int(key[4:]), why_bad)
                except KeyError:
                    continue
            if just_load is False:
                if just_summarize is not True: # TESGroupHDF5 cannot summarize <= ??
                    if np.sum(data.first_good_dataset.p_peak_value[:])==0:  # if not summarized (not tested yet)
                        data.summarize_data(forceNew=forceNew)
                if (('quick' not in hdf5_fname) and np.sum(data.first_good_dataset.cuts.bad()) == 0):  # it means cut hasn't been done
                    if auto_cut is True:
                        print('# auto_cuts used')
                        data.auto_cuts(forceNew=True) # mass's auto-cut
                    else:
                        default_cuts = mass.core.controller.AnalysisControl(
                            #pulse_average=(0.0, None), #0
                            pretrigger_rms=(None, 55.0), #1
                            pretrigger_mean_departure_from_median=(-200.0, 200.0), #2, allows a pretty big drift (previously 40, now 200)
                            peak_value=(0.0, None), #3
                            postpeak_deriv=(None, 35.0), #4
                            rise_time_ms=(0, 0.12), #5
                            peak_time_ms=(0, 0.17) #6
                        )
                        print('# default_cuts used')
                        data.sj_apply_cuts(cuts=default_cuts, forceNew=True, write2file=True, ptm_median_or_max='median') # forceNew=True, because False does not do cutting
        for ds in data:
            if sum(ds.good()) < 10:
                data.set_chan_bad([ds.channum], 'too few good pulses after cut')

        if (dir_p in dir_p_cals) and (individual is False):
            cal_data_list.append(data)
        # if len(set.intersection(set(dir_ps),set(dir_p_cals))) > 0: # if there is overlap
        #     data_list.append(data) # data_list is data + cal
        # else: # if no overlap
        #     if dir_p not in dir_p_cals:
        #         data_list.append(data) # data_list is data only
        # not sure why I wrote like above. think below should be fine
        data_list.append(data)

    for data in data_list:
        for ds in data:
            if 'energy_dc' in ds.hdf5_group.keys():
                ds.p_energy_dc = ds.hdf5_group['energy_dc'][:]

    if (just_load is True) or (just_summarize is True) or (just_cut is True):
        if len(dir_ps) != len(data_list):
            print('\nSome of the data sets were not properly loaded/reduced')
        return data_list

    if use_hdf5 is False:
        # from multiprocessing import pool  # not implemented yet
        if fast is False:
            if individual is True:
                for data in data_list:
                    data.sj_avg_pulses_auto_masks([0.99, 1.01], quantity="p_peak_value", forceNew=forceNew, use_tallest=True, restrain_ptm=True)
                    print('\n# computing avg pulses done for %s\n'%data)
                    if mass.__version__ >= '0.7.0':
                        data.compute_noise_spectra(max_excursion=500)
                        data.compute_ats_filter(f_3db=10e3, forceNew=forceNew)
                        print('# compute_ats_filter is called')
                    else:
                        data.sj_compute_filters(f_3db=20000.0, forceNew=forceNew, use_new_filters=use_new_filters) # 10000 vs 20000?
                        print('# sj_compute_filters is called')
                    print('\n# computing filters done for %s\n'%data)
                    data.filter_data(forceNew=forceNew)
                    print('\n# filtering done for %s\n'%data)
                    data.drift_correct(forceNew=forceNew, category=category)
                    print('\n# drift correcting done for %s\n'%data)
            else:
                data_cal = cal_data_list[0] # hard-coded to use the first cal data. need to change to allow to select?
                # use pulses around the tallest one, around the median of the ptm
                if reset_cal is True: # this is to remake avg and filter and reapply them.
                    forceNew = True
                data_cal.sj_avg_pulses_auto_masks([0.99, 1.01], quantity="p_peak_value", forceNew=forceNew, use_tallest=True, restrain_ptm=True)
                print('\n# computing avg pulses done for %s\n'%data_cal)
                if mass.__version__ >= '0.7.0':
                    data_cal.compute_noise_spectra(max_excursion=500)
                    data_cal.compute_ats_filter(f_3db=10e3, forceNew=forceNew)
                    print('# compute_ats_filter is called')
                else:
                    data_cal.sj_compute_filters(f_3db=20000.0, forceNew=forceNew, use_new_filters=use_new_filters) # 10000 vs 20000?
                print('\n# computing filters done for %s\n'%data_cal)
                data_cal.filter_data(forceNew=forceNew)
                print('\n# filtering done for %s\n'%data_cal)

                for channum in data_cal.why_chan_bad.keys():
                    for data in data_list:
                        if data_cal != data:
                            data.set_chan_bad(channum, "bad channel in cal data")

                for data in data_list:
                    print(data)
                    data._sj_use_new_filters(use_new_filters=use_new_filters)
                    if data_cal != data:
                        for ds in data:
                            ds_cal = data_cal.channel[ds.channum]
                            ds.filter = ds_cal.filter
                            ds.average_pulse = ds_cal.average_pulse
                        print('# filtering of %s has started'%data.filenames[0].split(os.sep)[-2])
                        data.filter_data(forceNew=forceNew)
    else:  # if use_hdf5 is True:
        if fast is False:
            if individual is True:  # below hasn't been tested
                for data in data_list:
                    if 'quick' not in hdf5_fname:
                        if np.sum(data.first_good_dataset.average_pulse[:])==0:  # not averaged yet
                            data.sj_avg_pulses_auto_masks([0.99, 1.01], quantity="p_peak_value", forceNew=forceNew, use_tallest=True, restrain_ptm=True)
                            print('\n# computing avg pulses done for %s\n'%data)
                        if data.first_good_dataset.filter is None:
                            if mass.__version__ >= '0.7.0':
                                data.compute_noise_spectra(max_excursion=500)
                                data.compute_ats_filter(f_3db=10e3, forceNew=forceNew)
                                print('# compute_ats_filter is called')
                            else:
                                data.sj_compute_filters(f_3db=20000.0, forceNew=forceNew, use_new_filters=use_new_filters) # 10000 vs 20000?
                                print('# sj_compute_filters is called')
                            print('\n# computing filters done for %s\n'%data)
                            data.filter_data(forceNew=forceNew)
                            print('\n# filtering done for %s\n'%data)
                        if np.sum(data.first_good_dataset.p_filt_value_dc[:])==0:
                            data.drift_correct(forceNew=forceNew, category=category)
                            print('\n# drift correcting done for %s\n'%data)
            else:
                data_cal = cal_data_list[0] # hard-coded to use the first cal data. need to change to allow to select?
                # Below does not work because updater in mass keeps failing. Maybe just ljh processing should be invoked instead of hdf5
                if (reset_cal is True) or (('quick' not in hdf5_fname) and (np.sum(data_cal.first_good_dataset.average_pulse[:])==0)): # this is to remake avg and filter and reapply them or when hdf5 is not fully processed.
                    forceNew = True
                    from mass.core.utilities import NullUpdater
                    for ds in data_cal:
                        ds.tes_group.updater = NullUpdater()  # this is to handle error caused by mass channel.py
                    # below doesn't work because compute_average_pulse doesn't work with hdf5
                    data_cal.sj_avg_pulses_auto_masks([0.99, 1.01], quantity="p_peak_value", forceNew=forceNew, use_tallest=True, restrain_ptm=True)
                    print('\n# computing avg pulses done for %s\n'%data_cal)
                    if mass.__version__ >= '0.7.0':
                        data_cal.compute_noise_spectra(max_excursion=500)
                        data_cal.compute_ats_filter(f_3db=10e3, forceNew=forceNew)
                        print('# compute_ats_filter is called')
                    else:
                        data_cal.sj_compute_filters(f_3db=20000.0, forceNew=forceNew, use_new_filters=use_new_filters) # 10000 vs 20000?
                        print('# sj_compute_filters is called')
                    print('\n# computing filters done for %s\n'%data_cal)
                    data_cal.filter_data(forceNew=forceNew)
                    print('\n# filtering done for %s\n'%data_cal)

                    for channum in data_cal.why_chan_bad.keys():
                        for data in data_list:
                            if data_cal != data:
                                data.set_chan_bad(channum, "bad channel in cal data")

                    for data in data_list:
                        print(data)
                        data._sj_use_new_filters(use_new_filters=use_new_filters)
                        if data_cal != data:
                            for ds in data:
                                ds_cal = data_cal.channel[ds.channum]
                                ds.filter = ds_cal.filter
                                ds.average_pulse = ds_cal.average_pulse
                            print('# filtering of %s has started'%data.filenames[0].split(os.sep)[-2])
                            data.filter_data(forceNew=forceNew)

    # data_list below should include data_cal
    if individual is False:
        if len(dir_ps) != len(data_list):
            print('\nSome of the data sets were not properly loaded/reduced')
            return data_list
        else:
            if fast is False:
                co_drift_correct(data_list, cal_data_list, method=dc_method, category=category) # if want to use sass one: from sass import reduction => reduction.co_drift_correct(data_list)
            else:
                co_drift_correct(data_list, cal_data_list, attr=attr, method=dc_method, category=category) # if want to use sass one: from sass import reduction => reduction.co_drift_correct(data_list)

    # detailed cut will be applied later, maybe
    # for data in data_list:
    #     data.sj_plot_summaries(2, lines_at=[-15.0, 15.], downsample=10)
    #     pretrigger_mean_departure_from_median = input('pretrigger_mean_departure_from_median for %s: '%data.datasets[0].filename[-22:-10])

    return data_list

batch_1of3 = batch1of3


def co_drift_correct(data_list, cal_data_list=None, attr=None, method='mass', forceNew=True, category=None, dc_on_energy=False): # modified from sass
    """
    Co-drift-correct data. Not sure how to implement forceNew yet. Probably
    need to check to figure out if the dc_info is there. But need to figure out
    what it looks like for an unreduced data set (what is null case).

    SJL: cal_data_list is the list that is used for generating drift correct info
    data_list is the whole data sets, including cal_data
    forceNew has no effect
    if cal_data_list is not given, drift data_list based on themselves all together
    if cal_data_list is given, drift data_list based on cal_data_list
    dc_on_energy=True corrects p_energy vs p_pretrig_mean. Do this after batch3of3
    """
    if attr is None:
        attr = 'p_filt_value'
    dc_dict = {}
    if cal_data_list is None:
        good_chans = reduce(set.intersection, [set(data.good_channels) for data in data_list])
    else:
        good_chans = reduce(set.intersection, [set(data.good_channels) for data in cal_data_list])
    if category is None:
        category = {"calibration": "in"}

    if dc_on_energy is False:
        print("# Doing Drift Correct based on", attr)
        if cal_data_list is None:
            print("# drift_correct_info being made with all the data")
        else:
            print("# drift_correct_info being made with %s"%[data_cal.first_good_dataset.filename.split(os.sep)[-1].split('_chan')[0] for data_cal in cal_data_list])
        for chan in good_chans:
            ds_list = [data.channel[chan] for data in data_list] # ds_list for the same channum.
            if cal_data_list is not None: # if cal data provided, use them
                cal_ds_list = [data.channel[chan] for data in cal_data_list] # ds_list for the same channum
                ptmean = np.hstack([ds.p_pretrig_mean[ds.cuts.good(**category)] for ds in cal_ds_list]) # can use category. previously just all [:]
                filt = np.hstack([getattr(ds, attr)[ds.cuts.good(**category)] for ds in cal_ds_list])
            else: # if no cal data, get drift info from all the data
                ptmean = np.hstack([ds.p_pretrig_mean[ds.cuts.good(**category)] for ds in ds_list])
                filt = np.hstack([getattr(ds, attr)[ds.cuts.good(**category)] for ds in ds_list])
            if method == 'mass':
                try:
                    dc_param, dc_info = mass.core.analysis_algorithms.drift_correct(ptmean, filt)
                    print("Chan %d Drift Correct Parameter:"%chan, dc_param)
                    dc_dict[chan] = dc_info
                    for ds in ds_list: # ds_list includes data_cal and data
                        ds.p_filt_value_dc.attrs.update(dc_info)
                        if mass.__version__ >= '0.7.8':
                            ds._apply_drift_correction(attr=attr)
                        else:
                            ds._apply_drift_correction()
                except ValueError:
                    for data in data_list:
                        data.set_chan_bad(chan, "Failed co-drift-correct ValueError")
            elif method == 'sj':
                dc_param, dc_info = drift_correct_ss_opt(ptmean, filt)
                print("Chan %d Drift Correct Parameter:"%chan, dc_param)
                dc_dict[chan] = dc_info
                for ds in ds_list: # ds_list includes data_cal and data
                    ds.p_filt_value_dc.attrs.update(dc_info)
                    if mass.__version__ >= '0.7.8':
                        ds._apply_drift_correction(attr=attr)
                    else:
                        ds._apply_drift_correction()

    else: # below is a dummy copy. Does not work
        print("# Individual drift correction of p_energy vs p_pretrig_mean")
        for data in data_list:
            ptm_offset = self.p_filt_value_dc.attrs["median_pretrig_mean"]
            gain = 1+(self.p_pretrig_mean[:]-ptm_offset)*self.p_filt_value_dc.attrs["slope"]
            self.p_energy_dc[:] = self.p_energy[:]*gain
            self.hdf5_group.file.flush()


def drift_correct_ss_opt(indicator, uncorrected):

    ptm_offset = np.median(indicator)
    indicator = np.array(indicator) - ptm_offset

    limit = 1.25 * np.percentile(uncorrected, 99)
    
    indicator = indicator[uncorrected<limit]
    uncorrected = uncorrected[uncorrected<limit]

    n_bin = ss_opt_num_bin(uncorrected)
    
    def entropy(param, indicator, uncorrected):
        corrected = uncorrected * (1+indicator*param)
        counts, bin_edges = np.histogram(corrected, n_bin)
        w = counts > 0
        return -(np.log(counts[w])*counts[w]).sum()

    drift_corr_param = sp.optimize.brent(entropy, (indicator, uncorrected), brack=[0, .001])

    drift_correct_info = {'type': 'ptmean_gain',
                                  'slope': drift_corr_param,
                                  'median_pretrig_mean': ptm_offset}
    return drift_corr_param, drift_correct_info


def check_drift_correct(data_list, channum=None, attr=None, category=None):
    """
    plot p_filt_value & p_filt_value_dc vs p_timestamp for data_list
    attr can be 'p_peak_value'. the corrected parameter name is always p_filt_value_dc anyway
    """

    if channum is None:
        channum = data_list[0].first_good_dataset.channum

    if channum not in set.intersection(*[set(data.good_channels) for data in data_list]):
        print('This channel is missing in one or more data. Check again.')
        return

    if attr is None:
        attr = 'p_filt_value'

    if category is None:
        category = {"calibration": "in"}

    dir_ps = []
    for data in data_list:
        if hasattr(data, 'filenames'): # ljh
            dir_ps.append(data.filenames[0].split(os.sep)[-2])
        else: # hdf5
            dir_ps.append(data.first_good_dataset.filename.split(os.sep)[-1][:12])

    fig1, ax1 = plt.subplots(2,1, figsize=(12,12), sharex=True, sharey=True)
    fig1.subplots_adjust(bottom=0.05, top=0.95, hspace=0.1)
    for data in data_list:
        ds = data.channel[channum]
        ax1[0].scatter(ds.p_timestamp[ds.good(**category)], getattr(ds, attr)[ds.good(**category)], s=1, alpha=0.3)
        ax1[1].scatter(ds.p_timestamp[ds.good(**category)], ds.p_filt_value_dc[ds.good(**category)], s=1, alpha=0.3)
    ax1[0].set_ylabel(attr)
    ax1[0].legend(dir_ps)
    ax1[1].set_ylabel('p_filt_value_dc')
    ax1[1].set_xlabel('p_timestamp')
    fig1.suptitle("Chan %s"%channum)

    fig2, ax2 = plt.subplots(2,1, figsize=(12,12), sharex=True, sharey=True)
    fig2.subplots_adjust(bottom=0.05, top=0.95, hspace=0.1)
    for data in data_list:
        ds = data.channel[channum]
        ax2[0].scatter(ds.p_pretrig_mean[ds.good(**category)], getattr(ds, attr)[ds.good(**category)], s=1, alpha=0.3)
        ax2[1].scatter(ds.p_pretrig_mean[ds.good(**category)], ds.p_filt_value_dc[ds.good(**category)], s=1, alpha=0.3)
    ax2[0].set_ylabel(attr)
    ax2[0].legend(dir_ps)
    ax2[1].set_ylabel('p_filt_value_dc')
    ax2[1].set_xlabel('pretrigger_mean')
    fig2.suptitle("Chan %s"%channum)


# def co_calibrate(data_list):
#     return 0


def compare_xes(data_list, cal_index, norm=None, bin_start=0, bin_end=1200, bin_width=0.25):
    """
    plot xes of (cal) data
    norm = 'area' or 'max'
    """

    fig, ax = plt.subplots(1,1)
    for index in cal_index:
        xes = data_list[index].sj_plot_xes(return_only=True, bin_start=bin_start, bin_end=bin_end, bin_width=bin_width)
        norm_factor = np.sum(xes[1]) if norm=='area' else np.max(xes[1]) if norm=='max' else 1
        ax.plot(xes[0], xes[1]/norm_factor)
    ax.set_xlabel('energy (eV)')
    ax.set_ylabel('counts per bin (%s eV)'%bin_width)
    ax.legend(range(len(cal_index)))
    ax.set_title("Normalized by %s"%norm)


def generate_category(reduced_data, field_name='motor', category_names=['custom'], incl_range=None, excl_range=None, boundary_values=None, return_cut_bool_dict=False):
    """
    register category in data, and return data_cut_bool_dict
    With field_name of 'motor' and category_names of ['custom'], a category {"motor": "custom"} is registered in data
    cut is either by incl_range and excl_range or boundary_values. Not simultaneously yet.
        incl_range and excl_range is based on motor values. E.g. incl_range=[60, 70]
        boundary_values are p_timestamp values between two conditions including the before and after (later will change to any quantities)
        if all three are None, it assumes boundary_values case, and asks to choose boundary values from a figure. When done, click Esc.
    It might not be possible to use multiple categories. Might need to update calibration, plotting functions
    """
    # print('# Categorical cut being registered..')

    if hasattr(reduced_data, 'filenames'): # if mass dataset type
        dir_p = reduced_data.first_good_dataset.filename.split(os.sep)[-2]
    elif not hasattr(reduced_data, 'hdf5_noisefile'): # if mass hdf5
        # dir_p = reduced_data.first_good_dataset.filename.split(': ')[1][:12]
        dir_p = reduced_data.first_good_dataset.filename.split(os.sep)[-1].split('_chan')[0]
    else: # if my own data type
        # dir_p = reduced_data.first_good_dataset.filename.split(':')[-1].split('.')[0][1:]
        dir_p = reduced_data[0].dir_p

    try:
        reduced_data.unregister_categorical_cut_field(field_name)
        print('\n# field (%s) unregistered, and being re-registered..\n'%field_name)
        reduced_data.register_categorical_cut_field(field_name, category_names)
    except KeyError:
        reduced_data.register_categorical_cut_field(field_name, category_names)

    data_cut_bool_dict = {}

    if boundary_values is None:
        if (incl_range is None) and (excl_range is None):
            if len(plt.get_fignums()) == 0:
                print("# provide cut conditions")
                return
            else:
                ginput_pts = plt.ginput(0, 0)
                boundary_values = [ginput_pt[0] for ginput_pt in ginput_pts]
        else:
            if incl_range is None:
                incl_range = [-np.inf, np.inf]
            if excl_range is None:
                excl_range = [np.inf, -np.inf]

    if boundary_values is None:  # if it's still None
        if os.path.isfile(dir_p+'_log_old'): # first look at the current folder
            log_fname = dir_p+'_log_old'
        elif os.path.isfile(dir_p+'_log'):
            log_fname = dir_p+'_log'
        else:
            if BASE_FOLDER == "no name yet":
                set_base_folder()
            else:
                print('# BASE_FOLDER: %s'%BASE_FOLDER)

            path_log = os.path.join(BASE_FOLDER, dir_p.split('_')[0] ,dir_p)
            log_fnames = glob.glob(os.path.join(path_log,'*log*'))
            old_log_fnames = glob.glob(os.path.join(path_log,'*log_old'))
            if len(old_log_fnames) == 0:
                if len(log_fnames) == 0:
                    print('# no log file found. data returned. Provide log file name!')
                    return reduced_data
                else:
                    log_fname = max(log_fnames, key=os.path.getctime) # the most recent one
            else:
                log_fname = max(old_log_fnames, key=os.path.getctime) # the most recent one

        (scan_mode, n_spots, scan_numbers, nominal_mono_from_log, i0_matrix, time_start_end, htxs_class) = _log_file_parser(log_fname, 133, None) # BL=133 for now
        tiled_motor_array = np.tile(nominal_mono_from_log, n_spots)
        cut_time_start_end = time_start_end[(tiled_motor_array>=incl_range[0]) & (tiled_motor_array<=incl_range[1]) & ((tiled_motor_array<excl_range[0]) | (tiled_motor_array>excl_range[1]))]
        for ds in reduced_data:
            print("category cutting %s channel"%ds.channum)
            ds_cut_bool_dict = {}
            for category_name in category_names:
                ds_cut_bool_dict[category_name] = reduce(np.logical_or, [(ds.p_timestamp[:]>=start_end[0]) & (ds.p_timestamp[:]<=start_end[1]) for start_end in cut_time_start_end])
            ds.cuts.cut_categorical(field_name, ds_cut_bool_dict)
            data_cut_bool_dict[str(ds.channum)] = ds_cut_bool_dict
    else:
        if len(category_names)+1 != len(boundary_values):
            reduced_data.unregister_categorical_cut_field(field_name)
            raise Exception('len(category_names+1) = %s != len(boundary_values) = %s'%(len(category_names)+1, len(boundary_values)))
        # boundary_values = [0] + sorted(boundary_values) + [np.inf]
        for ds in reduced_data:
            # print("category cutting %s channel"%ds.channum)
            ds_cut_bool_dict = {}
            for ii, category_name in enumerate(category_names):
                ds_cut_bool_dict[category_name] = (ds.p_timestamp[:]>=boundary_values[ii]) & (ds.p_timestamp[:]<=boundary_values[ii+1])
            ds.cuts.cut_categorical(field_name, ds_cut_bool_dict)
            data_cut_bool_dict[str(ds.channum)] = ds_cut_bool_dict

    if return_cut_bool_dict is True:
        return data_cut_bool_dict


def batch2of3(data_list, cal_lines, individual=False, attr=None, cal_data_index=0, forceNew=False, smoothing_range=range(10,20), nextra=None, maxacc=0.075, linearity=1.1, neighbor_limit=80, auto_inclusion=None, fast=False, use_dc=True, num_rounds=3, rms_threshold=2, cal_method='spl', quartic_only=False, thru_origin=True, refine_method=None, refine_bw=0.8, n_refine=1, category=None, write2file=True, strong_line_indices=[], verbosity=1):
    """
    # this is calibration step, a work in progress
    # now returns data_list (20190312), not data_cal
    # no need to store the returned data_list in a variable, so it's fine to just run batch2of3(data_list)
    # can be skipped if energy cal is already done
    # for now this function energy-calibrates only the first data or the one specified by cal_data_index in the data_list
    # default values:
    # nextra=10, auto_inclusion=1
    # cal_lines = ['300','400','500'] or 'mixv4_1000' or '1000' or 'mixv4_450' or '450'
    # cal_method: 'poly', 'spl', 'nist'
    # quartic_only=True used 4th order poly only when finding optimial assignment
    # refine_method: None, 'jamie', 'sj'
    # to do:
    # 1) display quick histogram
    # 2)
    """
    if not isinstance(data_list, list):
        data_list = [data_list]
    if type(cal_lines) == str:
        if '1000' in cal_lines:
            cal_lines = cal_lines_energies(1000)[0]
            if auto_inclusion is None:
                auto_inclusion = 5
            if nextra is None:
                nextra = 12
        elif '450' in cal_lines:
            cal_lines = [278.2, 392.3, 450, 524.5] # for 10-1 TES
            if auto_inclusion is None:
                autoauto_inclusion = 2
            if nextra is None:
                nextra = 5
            strong_line_indices = [1]
        else:
            if '_' in cal_lines:
                cal_mono = float(cal_lines.split('_')[1])
            else:
                cal_mono = float(cal_lines)
            if cal_mono > 520:
                cal_lines = cal_lines_energies(cal_mono)[0]
                if auto_inclusion is None:
                    auto_inclusion = 4 # needs to be tested
                if nextra is None:
                    nextra = 12
            else:
                print('Provide a valid cal_lines')
                return

    if nextra is None:
        nextra = 10
    if auto_inclusion is None:
        auto_inclusion = 1
    cal_params = {}
    cal_params['cal_lines'] = cal_lines
    cal_params['smoothing_range'] = smoothing_range
    cal_params['nextra'] = nextra
    cal_params['neighbor_limit'] = neighbor_limit
    cal_params['auto_inclusion'] = auto_inclusion
    cal_params['fast'] = fast
    cal_params['maxacc'] = maxacc
    cal_params['linearity'] = linearity
    cal_params['num_rounds'] = num_rounds
    cal_params['use_dc'] = use_dc

    for ii, data in enumerate(data_list):
        if individual is True:
            pass # if individual is True, execute the for loop
        else:
            if cal_data_index < 0:
                cal_data_index = len(data_list) + cal_data_index
            if ii != cal_data_index:
                continue # if individual is False, and if not the cal data, skip the loop
        # data_cal = data_list[cal_data_index]
        if hasattr(data, 'filenames'): # ljh
            dir_p_cal = data.filenames[0].split(os.sep)[-2] # dir_p_cal not used
        else: # hdf5
            # dir_p_cal = data.first_good_dataset.filename.split(': ')[1][:12]
            dir_p_cal = data.first_good_dataset.filename.split(os.sep)[-1][:12]

        print('calibrating: ', dir_p_cal)

        rerun_chans = np.asarray(data.good_channels)
        try:
            for i in range(num_rounds):
                print('## round %d'%(i+1))
                if neighbor_limit is not None:
                    neighbor_limit = neighbor_limit - 10*i # - might be correct
                if attr is None:
                    if fast is True:
                        attr = 'p_peak_value'
                    else:
                        if use_dc is True: # default is True
                            attr = 'p_filt_value_dc'
                        else:
                            attr = 'p_filt_value'
                result_matrix = data.sj_calibrate(attr, cal_lines, size_related_to_energy_resolution=smoothing_range,
                                maxacc=maxacc, nextra=nextra+i, plot_on_fail=False, bin_size_ev=0.2, forceNew=True, verbosity=verbosity, category=category,
                                neighbor_limit=neighbor_limit, auto_inclusion=auto_inclusion-i, linearity=1.1, cal_method=cal_method, quartic_only=quartic_only, thru_origin=thru_origin, rerun_chans=rerun_chans, strong_line_indices=strong_line_indices)

                # result_matrix[9] is rms_poly
                # rerun_chans=rerun_chans[(result_matrix[9]==0) | (result_matrix[9]==1000)]
                if len(cal_lines)>1:
                    rerun_chans=rerun_chans[result_matrix[9]>rms_threshold] # if residual is worse than rms_threshold [eV]
        except KeyboardInterrupt:
            raise KeyboardInterrupt('KeyboardInterrupt called')
            return

        if refine_method is not None: # testing purpose. using the same as in cal_method='nist'
            print('\n# calibration is being refined by the method: %s'%refine_method)
            # attr = 'p_filt_value_dc' # attr harded coded for now
            for ii in range(n_refine):
                print("## %d-th round of calibration refining"%ii)
                for ds in data:
                    ecal = ds.calibration[attr]
                    phs = ecal.cal_point_phs
                    energies = ecal.cal_point_energies
                    # ds.convert_to_energy(attr) # commented out because I do this separately
                    epeaks = get_peaks_window(ds.p_energy[ds.good()], bw=refine_bw, centers=cal_lines, width=1.5, refine_method=refine_method) # think bw=0.8 might work better
                    if epeaks is None:
                        print('Ch %s peak position refinement skipped'%ds.channum)
                        continue
                    refined_phs = ecal.energy2ph(epeaks)
                    refined_cal = mass.calibration.energy_calibration.EnergyCalibration()
                    for opt_ph, cal_ev in zip(refined_phs, energies):
                        refined_cal.add_cal_point(opt_ph, cal_ev)
                    ds.calibration[attr] = refined_cal
                    ds.convert_to_energy(attr)

        if len(cal_lines)>1:
            if len(rerun_chans) == len(data.good_channels):
                print('# All failed. Calibration parameters need to be changed. Use data.sj_plot_quick_spectra()')
            data.set_chan_bad(np.sort(rerun_chans), "failed sj_calibrate")

            cal_params['failed_chans'] = rerun_chans
        data.cal_params = cal_params

        # after showing XES, choose ill calibrated pixels, re-calibrate them (optional) and then save to file
        # gui

        if write2file is True:
            data.sj_save_cal(refine_method=refine_method) # sj_save_cal needs to be modified to save more parameters such as fast

        with h5py.File(data.hdf5_file.filename, "a") as h5file:
            for ds in data:
                h5file['chan%d/energy'%(ds.channum)][:] = ds.p_energy

    return data_list

batch_2of3 = batch2of3

'''
def batch_2of3b(data_list, bad_chans, cal_data_index=0, forceNew=False):
    # probably this one is to visualize or somehow check why cal is not working well.
    # can be ignored for now (20180126)
    data_cal = data_list[cal_data_index]
    cal_params = data_cal.cal_params
    cal_lines = cal_params['cal_lines']
    smoothing_range = cal_params['smoothing_range']
    nextra = cal_params['nextra']
    neighbor_limit = cal_params['neighbor_limit']
    auto_inclusion = cal_params['auto_inclusion']
    fast = cal_params['fast']
    maxacc = cal_params['maxacc']
    linearity = cal_params['linearity']
    num_rounds = cal_params['num_rounds']
    use_dc = cal_params['use_dc']
    if fast:
        pulse_param = "p_peak_value"
    else:
        if use_dc:
            pulse_param = "p_filt_value_dc"
        else:
            pulse_param = "p_filt_value"
    i = 0
    result_matrix = data_cal.sj_calibrate("p_peak_value", cal_lines, size_related_to_energy_resolution=smoothing_range,
                    maxacc=0.1, nextra=nextra+i, plot_on_fail=True, bin_size_ev=0.2, forceNew=True, verbosity=1,
                    neighbor_limit=neighbor_limit+10*i, auto_inclusion=auto_inclusion-i, linearity=1.1, rerun_chans=bad_chans)


def batch_2of3c(data_list, cal_data_index=0):
    # this one seems to be work in progress. seems like the last step of calibration (saving)
    data_cal = data_list[cal_data_index]
    dir_p_cal = data_cal.filenames[0].split(os.sep)[-2]
    data_cal.sj_save_cal()
    if len(glob.glob(dir_p_cal+'_cal*'))>1:
        import filecmp
        if filecmp.cmp(glob.glob(dir_p_cal+'_cal*')[-1], glob.glob(dir_p_cal+'_cal*')[-2]):
            os.remove(glob.glob(dir_p_cal+'_cal*')[-2]) # if the same cut, previous one is removed
            print '# new cal file (%s) was identical to the previous one, so the previous one (%s) is deleted'%(glob.glob(dir_p_cal+'_cal*')[-1], glob.glob(dir_p_cal+'_cal*')[-2])
'''

def batch3of3(data_list, cal_data_index=0, attr=None, forceNew=False, cal_method='spl', refine_method=None):
    """
    # energy calibration being applied
    # assumes data_list has data_cal
    # forceNew = True filters data again. Do not try this
    # batch3of3 is not necessary if data are individually calibrated
    # probably no need to receive the returned data_list. just do useful_ftns.batch3of3(data_list)
    """
    if cal_data_index < 0:
        cal_data_index = len(data_list) + cal_data_index
    data_cal = data_list[cal_data_index]
    if hasattr(data_cal, 'filenames'): # ljh
        dir_p_cal = data_cal.filenames[0].split(os.sep)[-2]
        filetype = 'ljh'
    else:
        # dir_p_cal = data_cal.first_good_dataset.filename.split(': ')[1][:12]
        dir_p_cal = data_cal.first_good_dataset.filename.split(os.sep)[-1][:12]
        filetype = 'mass_hdf5'
    # print('cal folder:',dir_p_cal)
    cal_fnames = glob.glob(dir_p_cal+'*_cal*')
    if len(cal_fnames) == 0:
        raise Exception('# no calibration information found. Calibration not performed.')
    else:
        most_recent_cal_fname = max(cal_fnames, key=os.path.getctime)
        if 'fast' in most_recent_cal_fname: # when using pulse height
            fast = True
            print('# fast mode')
        else: # when using filtered values (normal way)
            fast = False
            print('# normal (non-fast) mode')
        with open(most_recent_cal_fname, "rb") as f:
            cal_dict = pkl.load(f)
            print("\n## calibration file (%s) loaded ##"%most_recent_cal_fname)
            if refine_method is None:
                if 'refine_method' in cal_dict:
                    refine_method = cal_dict['refine_method']

    # if cal_dict['fast'] is True: # when using p_peak_value
    #     fast = True
    # path_cal = os.path.join(BASE_FOLDER, dir_p_cal.split('_')[0] ,dir_p_cal)
    # hdf5_filename_cal = os.path.join(path_cal, dir_p_cal.strip(os.sep)+'_mass.hdf5')
    # available_chans_cal = mass.ljh_get_channels_both(path_cal, path_n)
    # chan_nums_cal = available_chans_cal[:] # might use part_of_pixel here as well
    # pulse_files_cal = mass.ljh_chan_names(path_cal, chan_nums_cal)
    # noise_files_cal = mass.ljh_chan_names(path_n, chan_nums_cal)
    # data_cal = mass.TESGroup(pulse_files_cal, noise_files_cal)

    # below is commented out 20220624
    # if (fast is not True) and (filetype == 'ljh'): # when using filtered values
    #     if mass.__version__ >= '0.7.0':
    #         # data_cal.compute_noise_spectra(max_excursion=500)
    #         data_cal.compute_ats_filter(f_3db=10e3)
    #         print('# compute_ats_filter is called')
    #     else:
    #         data_cal.compute_filters()

    for channum in data_cal.why_chan_bad.keys():
        for ii, data in enumerate(data_list):
            if ii == cal_data_index:
                continue
            else:
                data.set_chan_bad(channum, "bad channel in cal data")

    # instead of computing filters, grab them from data_cal  # 20220624: aren't they already calculated during batch1of3?
    for ii, data in enumerate(data_list):
        if ii == cal_data_index:
            continue
        else:
            # below is commented out 20220624
            # if fast is not True: # when using filtered values (normal way)
            #     for ds in data:
            #         try:
            #             ds_cal = data_cal.channel[ds.channum]
            #             ds.filter = ds_cal.filter
            #             ds.average_pulse = ds_cal.average_pulse
            #         except:
            #             data.set_chan_bad(ds.channum, "copying avg pulse from cal failed")
            #     if filetype == 'ljh':
            #         data.filter_data(forceNew=forceNew) # forceNew=True does not make sense
            #     # work out drift corr

            for ds in data:
                try:
                    # temp_spl = mass.mathstat.interpolate.CubicSpline(map(np.float32, cal_dict[ds.channum]), cal_dict['cal'])
                    temp_spl = mass.mathstat.interpolate.CubicSpline(np.asfarray(cal_dict[ds.channum]), cal_dict['cal'])
                    if fast is True: # when using p_peak_value
                        if attr is None:
                            attr = 'p_peak_value'
                        ds.p_energy = np.array(temp_spl(getattr(ds, attr)), dtype=np.float32)
                    else:
                        if attr is None:
                            attr = 'p_filt_value_dc'
                        if cal_method == 'spl':
                            # ds.p_energy_from_cal = np.array(temp_spl(ds.p_filt_value), dtype=np.float32)
                            ds.p_energy_from_cal_dc = np.array(temp_spl(getattr(ds, attr)), dtype=np.float32)
                            ds.p_energy_spl = ds.p_energy_from_cal_dc
                            ds.p_energy = ds.p_energy_from_cal_dc[:] # heads-up: this might not work as expected.
                        elif cal_method == 'poly4':
                            popt, pcov = curve_fit(ph2e_poly4th_thru_origin, np.sort(cal_dict[ds.channum]), np.sort(cal_dict['cal']), p0=[0.1, 0.1, 0.1, 0.1])
                            ds.p_energy_from_cal_poly_dc = np.array(ph2e_poly4th_thru_origin(getattr(ds, attr)[:], *popt), dtype=np.float32)
                            ds.p_energy_sj_poly_4th = ds.p_energy_from_cal_poly_dc
                            ds.p_energy = ds.p_energy_sj_poly_4th[:]
                        elif cal_method == 'nist':
                            energy_cal = mass.calibration.energy_calibration.EnergyCalibration()
                            for opt_ph, cal_ev in zip(np.sort(cal_dict[ds.channum]), np.sort(cal_dict['cal'])):
                                energy_cal.add_cal_point(opt_ph, cal_ev)
                            ds.calibration[attr] = energy_cal
                            ds.convert_to_energy(attr)

                except KeyError:
                    print('KeyError in chan',ds)
                    # data.set_chan_bad(ds.channum, "calibration information doesn't exist")

            with h5py.File(data.hdf5_file.filename, "a") as h5file:
                for ds in data:
                    h5file['chan%d/energy'%(ds.channum)][:] = ds.p_energy

    return data_list # not sure if necessary

batch_3of3 = batch3of3


def batch4of3(data_list):
    '''
    # optional drift correction of p_energy
    p_energy_dc is generated
    '''
    for data in data_list:
        print("Working on", data)
        for ds in data:
            g = ds.good()
            indicator = ds.p_pretrig_mean[g]
            indicator[:] = indicator[:] - np.median(indicator)
            uncorrected = ds.p_energy[g]

            pct99 = sp.stats.scoreatpercentile(uncorrected, 99)
            limit = 1.25 * pct99
            smoother = mass.analysis_algorithms.HistogramSmoother(0.5, [0, limit])

            n_bin = ss_opt_num_bin(uncorrected)

            drift_corr_param = sp.optimize.brent(_entropy, (indicator, uncorrected, smoother, n_bin), brack=[0, .001])
            gain = 1 + (ds.p_pretrig_mean[:] - np.median(ds.p_pretrig_mean[g])) * drift_corr_param
            ds.p_energy_dc = ds.p_energy[:]
            ds.p_energy_dc[:] = ds.p_energy[:]*gain

            print('chan %d best drift correction parameter: %s' % (ds.channum, drift_corr_param))

            h = ds.hdf5_group.require_dataset("energy_dc", (ds.nPulses,), dtype=np.float32)
            h[:] = ds.p_energy_dc
            # ds.hdf5_file.flush()
        data.hdf5_file.flush()

    return data_list


def batch5of3(data_list):
    '''
    # optional time drift correction of p_energy
    '''
    for data in data_list:
        data.time_drift_correct(attr="p_energy_dc", forceNew=True)
        for ds in data:
            ds.p_energy_tdc = ds.p_filt_value_tdc[:]


def batch_add_to_data_list(data_list, dir_ps, cal_data_index=0):
    '''
    cal_data_index is the index for data in data_list that has the main OF.
    def batch1of3(dir_ps, dir_n=None, dir_p_cals=None, forceNew=False, auto_cut=False, just_load=False, just_summarize=False, fast=False, use_hdf5=False, use_new_filters=False, individual=False, category=None):
    '''
    new_data_list = batch1of3(dir_ps)
    data_list.append(data)


def load_jamie_quick_reduced(hdf5_fname):
    concat_p_energy = []
    concat_timestamp = []
    nused = 0 # number of channels
    with h5py.File(hdf5_fname, "r") as h5file:
        for key in h5file['channels'].keys():
            concat_p_energy.append(h5file['channels/%s/pulses'%key][:])
            concat_timestamp.append(h5file['channels/%s/timestamps'%key][:])
            nused += 1
    return np.concatenate(concat_p_energy), np.concatenate(concat_timestamp), nused


def prep_pope_hdf5_for_mass(filename):
    with h5py.File(filename) as h5:
        for k,v in h5.iteritems():
            print(k,v)
            if not k.startswith("chan"): continue
            channum = int(k[4:])
            if "channum" not in v.attrs: v.attrs["channum"]=channum
            if "noise_filename" not in v.attrs: v.attrs["noise_filename"]="analyzed by pope, used preknowledge"
            if "npulses" not in v.attrs: v.attrs["npulses"]=len(v["filt_value"])


def pope_analysis(hdf5_filename, cal_fname):

    try:
        data = mass.TESGroupHDF5(hdf5_filename)
    except KeyError:
        prep_pope_hdf5_for_mass(hdf5_filename)
        data = mass.TESGroupHDF5(hdf5_filename)

    # cal_fnames = glob.glob(dir_p_cal+'_cal*')
    # if len(cal_fnames) == 0:
    #     print '# no calibration information found. Calibration not performed.'
    #     return data
    # else:
    #     most_recent_cal_fname = cal_fnames[-1]
    #     with open(most_recent_cal_fname, "rb") as f:
    #         cal_dict = pkl.load(f)
    #         print "\n## calibration file (%s) loaded ##"%most_recent_cal_fname
    with open(cal_fname, "rb") as f:
        cal_dict = pkl.load(f)
        print("\n## calibration file (%s) loaded ##"%cal_fname)

    for ds in data:
        try:
            # temp_spl = mass.mathstat.interpolate.CubicSpline(map(np.float32, cal_dict[ds.channum]), cal_dict['cal'])
            temp_spl = mass.mathstat.interpolate.CubicSpline(np.asfarray(cal_dict[ds.channum]), cal_dict['cal'])
            # ds.p_energy_from_cal_dc = np.array(temp_spl(ds.p_filt_value_dc), dtype=np.float32)
            ds.p_energy_from_cal = np.array(temp_spl(ds.p_filt_value), dtype=np.float32)
            # popt, pcov = curve_fit(ph2e_poly4th_thru_origin, np.sort(cal_dict[ds.channum]), np.sort(cal_dict['cal']), p0=[0.1, 0.1, 0.1, 0.1])
            # ds.p_energy_from_cal_poly_dc = np.array(ph2e_poly4th_thru_origin(ds.p_filt_value_dc[:], *popt), dtype=np.float32)
            ds.p_energy = ds.p_energy_from_cal
        except KeyError:
            data.set_chan_bad(ds.channum, "calibration information doesn't exist")

    return data


def sj_plot_traces(self, pulsenums=0, linestyle='.-', return_trace=False, plot_it=True, deriv=True):
    """
    plot many traces
    example usage: ds.sj_plot_traces(); ds.sj_plot_traces('.')
    ds.sj_plot_traces(3); ds.sj_plot_traces(3,'.');
    ds.sj_plot_traces([3,5]); ds.sj_plot_traces([3,5],'.');
    ds.sj_plot_traces(np.arange(5,15)); ds.sj_plot_traces(np.arange(5,15),'.')
    return_trace=True returns dt and traces
    """
    dt = (np.arange(self.nSamples)-self.nPresamples)*self.timebase*1e3
    if isinstance(pulsenums, str): # to handle ds.sj_plot_traces('.')
        linestyle = pulsenums
        pulsenums = 0 # by default, plot the first pulse
    if isinstance(pulsenums, int):
        pulsenums = list([pulsenums])
    # raw pulse
    if plot_it:
        fig1 = plt.figure()
        ax1 = fig1.add_subplot(111)
        fig2 = plt.figure()
        ax2 = fig2.add_subplot(111)
    values = []
    value_derivs = []
    for pn in pulsenums:
        value = self.read_trace(pn)
        values.append(value)
        value_deriv = np.asarray(value, dtype=int)[1:] - np.asarray(value, dtype=int)[:-1]
        value_derivs.append(value_deriv)
        if plot_it:
            ax1.plot(dt, value, linestyle, label='pulse index: %d'%pn)
            ax2.plot(dt[1:], value_deriv, linestyle, label='pulse index: %d'%pn)
    if plot_it:
        ax1.set_title('raw pulses')
        ax1.legend(fontsize='small')
        ax1.set_xlabel("Time after trigger (ms)")
        ax1.set_ylabel("Feedback (or mix) in [Volts/16384]")
        ax2.set_title('diff(raw pulses)')
        ax2.legend(fontsize='small')
        ax2.set_xlabel("Time after trigger (ms)")
        ax2.set_ylabel("Feedback (or mix) in [Volts/16384]")

    if return_trace is True:
        return dt, values, value_derivs

mass.MicrocalDataSet.sj_plot_traces = sj_plot_traces


def sj_plot_channum_traces(self, channum=None, pulsenums=range(10), linestyle='-', deriv=False, legend=True):
    """
    plot many traces
    example usage: data.sj_plot_channum_traces(); data.sj_plot_channum_traces('.')
    data.sj_plot_channum_traces(3); data.sj_plot_channum_traces(3,'.');
    data.sj_plot_channum_traces([3,5]); data.sj_plot_channum_traces([3,5],'.');
    data.sj_plot_channum_traces(1, [3,5]); data.sj_plot_channum_traces(1, [3,5],'.');
    data.sj_plot_channum_traces(np.arange(5,15)); data.sj_plot_channum_traces(np.arange(5,15),'.')
    data.sj_plot_channum_traces(3,3,'.')
    """
    if channum is None:
        ds = [self.first_good_dataset] # to handle data.sj_plot_channum_traces()
    else:
        if isinstance(channum, str): # to handle data.sj_plot_channum_traces('.')
            ds = [self.first_good_dataset]
            linestyle = channum
        elif isinstance(channum, int): # to handle data.sj_plot_channum_traces(3)
            try:
                ds = [self.channel[channum]]
                if isinstance(pulsenums, str): # to handle data.sj_plot_channum_traces([3,5],'.')
                    linestyle = pulsenums
                    pulsenums = range(10)
            except:
                print("\nOops! Channel number %d doesn't exist. Try another..." % channum)
                return
        else: # to handle data.sj_plot_channum_traces([3,5])
            if 'TES' in str(type(self)):
                ds = [self.channel[channum_i] for channum_i in channum]
                if isinstance(pulsenums, str): # to handle data.sj_plot_channum_traces([3,5],'.')
                    linestyle = pulsenums
                    pulsenums = range(10)

    dt = (np.arange(ds[0].nSamples)-ds[0].nPresamples)*ds[0].timebase*1e3
    if isinstance(pulsenums, str): # to handle data.sj_plot_channum_traces(3, '.')
        linestyle = pulsenums
        # pulsenums = 0 # draw first pulse by default
    if isinstance(pulsenums, int): # to handle data.sj_plot_channum_traces(3,3,'.')
        pulsenums = list([pulsenums])

    rainbow_colors = [cm.rainbow_r(x) for x in np.linspace(0,1.,len(pulsenums))]

    for ds_i in ds:
        plt.figure()

        for i, pn in enumerate(pulsenums):
            # value = np.asarray(ds_i.read_trace(pn), dtype=int)
            try:
                value = ds_i.read_trace(pn)
            except TypeError:
                # print('TypeError in mass')
                value = ds_i._sj_read_trace(pn)
            if deriv is True:
                plt.plot(dt[:-1]+ds_i.timebase/2.*1e3, np.diff(value), linestyle, color=rainbow_colors[i], label='pulse index: %d'%pn)
                plt.title('diff(channum = %d)' %ds_i.channum)
            else:
                plt.plot(dt,value, linestyle, color=rainbow_colors[i], label='pulse index: %d'%pn)
                plt.title('channum = %d' %ds_i.channum)
        if legend is True:
            plt.legend(fontsize='small')
        plt.xlabel("Time after trigger (ms)")
        plt.ylabel("Feedback (or mix) in [Volts/16384]")

mass.TESGroup.sj_plot_channum_traces = sj_plot_channum_traces


def _sj_read_trace(self, trace_num):
    if trace_num >= self.nPulses:
        raise ValueError("This VirtualFile has only %d pulses" % self.nPulses)
    # segment_num = trace_num
    # self.read_segment(segment_num)
    return self.data[trace_num]

# mass.TESGroup._sj_read_trace = _sj_read_trace
mass.MicrocalDataSet._sj_read_trace = _sj_read_trace


def sj_find_peak_time_microsec(self, channum=None):
    """
    Now checks all pulses in the all channels and returns the median value of peak_time_microsec
    example usage: peak_time_microsec = data.sj_find_peak_time_microsec()
    if channum is given, it will calculate for that channel only.
    """
    if channum is not None:
        ds = self.channel[channum]
        peak_time_microsec_ds = []
        for trace_num in range(min(ds.nPulses, 100)): # only check the first 100 pulses
            peak_time_idx = ds.pulse_records.datafile[trace_num].argmax()
            peak_time_microsec = (peak_time_idx - ds.nPresamples) * ds.timebase * 1e6
            peak_time_microsec_ds.append(peak_time_microsec)
        return np.median(peak_time_microsec_ds)
    else:
        peak_time_microsec_list = []
        for ds in self.datasets:
            # print(ds.channum)
            # peak_time_idx = np.zeros(ds.nPulses)
            peak_time_microsec_ds = []
            for trace_num in range(min(ds.nPulses, 100)): # only check the first 100 pulses
                peak_time_idx = ds.pulse_records.datafile[trace_num].argmax()
                peak_time_microsec = (peak_time_idx - ds.nPresamples) * ds.timebase * 1e6
                peak_time_microsec_ds.append(peak_time_microsec)
            peak_time_microsec_list.append(np.median(peak_time_microsec_ds))
        return np.median(peak_time_microsec_list)

mass.TESGroup.sj_find_peak_time_microsec = sj_find_peak_time_microsec


# def sj_summarize_data(self, peak_time_microsec=None, pretrigger_ignore_microsec=20.0,
#                        include_badchan=False, forceNew=False, use_cython=True):
#     """
#     same as original summarize_data, but uses calculated peak_time_microsec.
#     so no matter what value you use for peak_time_microsec, it will be ignored
#     example usage: data.sj_summarize_data()
#     I'd like to modify this one later to incorportate some cuts.
#     pretrigger_ignore_microsec=20.0 will lead to pretrigger_ignore_samples=3, which is the mass default, so it can be None
#     """
#     if peak_time_microsec is None:
#         peak_time_microsec = self.sj_find_peak_time_microsec()
#     print("peak_time_microsec = %f" %peak_time_microsec)
#     mass.TESGroup.summarize_data(self, peak_time_microsec=peak_time_microsec, pretrigger_ignore_microsec=pretrigger_ignore_microsec,
#                        include_badchan=include_badchan, forceNew=forceNew, use_cython=use_cython)
#
# mass.TESGroup.sj_summarize_data = sj_summarize_data


def sj_plot_summaries(self, quantity, dataset_numbers=None, valid='uncut', downsample=None, log=False, hist_limits=None, lines_at=None, ptm_median_or_max='median'):
    """
    sj: below is a duplicate of original code, slightly modified to prevent error
    example usage: sj_plot_summaries(0); sj_plot_summaries(0, [2,3,4,5]);
    sj_plot_summaries(6, range(10))
    sj_plot_summaries(1, range(15), valid='basic_cuts') # this is not working yet
    sj_plot_summaries(1, range(15), valid='cut') # show only bad (cut)
    sj_plot_summaries(1, range(15), valid='uncut') # show only good (uncut)
    sj_plot_summaries(1, range(15), valid='all') # show all
    if only one arg is given, it will make a figure every ch_per_fig=10 channels to make it easy to read
    ## original begins:
    Plot a summary of one quantity from the data set, including time series and histograms of
    this quantity.  This method plots all channels in the group, but only one quantity.  If you
    would rather see all quantities for one channel, then use the group's
    group.dataset[i].plot_summaries() method.

    <quantity> A case-insensitive whitespace-ignored one of the following list, or the numbers
               that go with it:
               "Pulse Avg" (0)
               "Pretrig RMS" (1)
               "Pretrig Mean" (2)
               "Peak Value" (3)
               "Max PT Deriv" (4)
               "Rise Time" (5)
               "Peak Time" (6)

    <valid> The words 'uncut' or 'cut', meaning that only uncut or cut data are to be plotted
            *OR* None, meaning that all pulses should be plotted.

    <downsample> To prevent the scatter plots (left panels) from getting too crowded,
                 plot only one out of this many samples.  If None, then plot will be
                 downsampled to 10,000 total points.

    <log>              Use logarithmic y-axis on the histograms (right panels).
    <hist_limits>
    <dataset_numbers>  A sequence of the datasets [0...n_channels-1] to plot.  If None
                       (the default) then plot all datasets in numerical order.
    """

    plottables = (
        ("p_pulse_average", 'Pulse Avg', 'purple', [0, 5000]),
        ("p_pretrig_rms", 'Pretrig RMS', 'blue', [0, 80]),
        ("p_pretrig_mean", 'Pretrig Mean', 'green', None),
        ("p_peak_value", 'Peak value', '#88cc00', None),
        ("p_postpeak_deriv", 'Max PT deriv', 'gold', [0, 100]),
        ("p_rise_time[:]*1e3", 'Rise time (ms)', 'orange', [0, 0.2]),
        ("p_peak_time[:]*1e3", 'Peak time (ms)', 'red', [0, 0.3])
    )

    quant_names = [p[1].lower().replace(" ", "") for p in plottables]
    if quantity in range(len(quant_names)):
        plottable = plottables[quantity]
    else:
        i = quant_names.index(quantity.lower().replace(" ", ""))
        plottable = plottables[i]

    ch_per_fig = 15
    datasets = self.datasets
    if dataset_numbers is None:
        dataset_numbers = range(len(datasets))
    else:
        datasets = [self.datasets[i] for i in dataset_numbers]
#    ny_plots = ch_per_fig # only 15 rows in one figure

#    plt.clf() # clear the current figure

    for i, (channum, ds) in enumerate(zip(dataset_numbers, datasets)): # here channum is different from real ch num
        print('TES%3d (Ch%3d)' % (channum, ds.channum)) # ds.channum is real ch num

        # Convert "uncut" or "cut" to array of all good or all bad data
        if isinstance(valid, str):
            if "uncut" in valid.lower():
                valid_mask = ds.cuts.good()
                print("Plotting only uncut data"),
            elif "cut" in valid.lower():
                valid_mask = ds.cuts.bad()
                print("Plotting only cut data"),
            elif 'all' in valid.lower():
                valid_mask = None
                print("Plotting all data, cut or uncut"),
            else:
                raise ValueError("If valid is a string, it must contain 'all', 'uncut' or 'cut'.")

        if valid_mask is not None:
            nrecs = valid_mask.sum()
            if downsample is None:
                downsample = nrecs // 10000
                if downsample < 1:
                    downsample = 1
            hour = ds.p_timestamp[valid_mask][::downsample] / 3600.0
        else:
            nrecs = ds.nPulses
            if downsample is None:
                downsample = ds.nPulses // 10000
                if downsample < 1:
                    downsample = 1
            hour = ds.p_timestamp[::downsample] / 3600.0
        print(" (%d records; %d in scatter plots)" % (nrecs, hour.shape[0]))

        (vect, label, color, default_limits) = plottable
        if hist_limits is None:
            limits = default_limits
        else:
            limits = hist_limits

        # Vectors are being sampled and multiplied, so eval() is needed.
        if "all" in valid.lower():
            vect = eval("ds.%s"%vect)[:]
            # vect = ds.__dict__[vect][:]
        else:
            vect = eval("ds.%s"%vect)[valid_mask]
            # vect = ds.__dict__[vect][valid_mask]  # this way, rise_time[:]*1e3 fails, so eval revived

        # Scatter plots on left half of figure
        if (i%ch_per_fig) == 0:
            plt.figure(figsize=(8,9)) # larger figure size
            ax_master = plt.subplot(ch_per_fig, 2, 1 + (i%ch_per_fig) * 2)
        else:
            plt.subplot(ch_per_fig, 2, 1 + (i%ch_per_fig) * 2, sharex=ax_master)

        if len(vect) > 0:
            plt.plot(hour, vect[::downsample], '.', ms=1, color=color)
            if lines_at is not None:
                if 'Pretrig Mean' ==  label:
                    plt.sj_hlines([lines_at[0]+np.median(vect),lines_at[1]+np.median(vect)],':')
                else:
                    plt.sj_hlines([lines_at[0],lines_at[1]],':')
        else:
            plt.text(.5, .5, 'empty', ha='center', va='center', size='large',
                     transform=plt.gca().transAxes)
        if (i%ch_per_fig) == 0:
            plt.title(label)
        plt.ylabel("TES %d\n(Ch %d)" % (channum,ds.channum), fontsize='x-small')
        if (i%ch_per_fig == ch_per_fig - 1) or (i == len(dataset_numbers)-1):
            plt.xlabel("Time since server start (hours)")

        # Histograms on right half of figure
        if (i%ch_per_fig) == 0:
            axh_master = plt.subplot(ch_per_fig, 2, 2 + (i%ch_per_fig) * 2)
            if lines_at is not None:
                plt.title('lines at %s and %s' %(lines_at[0], lines_at[1]))
            plt.suptitle(self.datasets[0].filename[-22:-10]) # display folder name
        else:
            plt.subplot(ch_per_fig, 2, 2 + (i%ch_per_fig) * 2, sharex=axh_master)
            # if 'Pretrig Mean' == label:
            #     plt.subplot(ch_per_fig, 2, 2 + (i%ch_per_fig) * 2) # pretrig mean plot doesn't share x axis
            # else:
            #     plt.subplot(ch_per_fig, 2, 2 + (i%ch_per_fig) * 2, sharex=axh_master)
        if limits is None:
            in_limit = np.ones(len(vect), dtype=np.bool)
        else:
            in_limit = np.logical_and(vect > limits[0], vect < limits[1])
        if in_limit.sum() <= 0:
            plt.text(.5, .5, 'empty', ha='center', va='center', size='large',
                     transform=plt.gca().transAxes)
        elif in_limit.sum() <= 30:
            plt.text(.5, .5, 'too few data', ha='center', va='center', size='large',
                     transform=plt.gca().transAxes)
        else:
            vect_in_limit = vect[in_limit]
            #if label in ['Pretrig RMS', 'Pretrig Mean', 'Max PT deriv', 'Rise time (ms)']:
            if 'Pretrig Mean' ==  label:
                if ptm_median_or_max == 'median':
                    vect_in_limit = vect_in_limit[np.logical_and(vect_in_limit > np.median(vect_in_limit)-3*np.std(vect_in_limit), vect_in_limit < np.median(vect_in_limit)+3*np.std(vect_in_limit))]
                    vect_in_limit = vect_in_limit - np.median(vect_in_limit)
                    label = label + " (median subtracted)"
                    opt_num_bin = ss_opt_num_bin(vect_in_limit)
                    hist, bin_edges = np.histogram(vect_in_limit, opt_num_bin*2) # bin numbers doubled
                    bin_centers = (bin_edges[1:]+bin_edges[:-1])/2
                    plt.fill_between(bin_centers, hist, color=color, alpha=0.5) # , step='mid'
                elif ptm_median_or_max == 'max':
                    opt_num_bin = ss_opt_num_bin(vect_in_limit)
                    hist, bin_edges = np.histogram(vect_in_limit, opt_num_bin*2) # bin numbers doubled
                    smoothed = running_mean_conv(hist, 5, 'same') # arbitrary smoothing factor of 5 (was 10 before)
                    bin_centers = (bin_edges[1:]+bin_edges[:-1])/2
                    try:
                        popt, _, _ = fit_gaussian(sigma = 3, tailed=False, plot_it=False, raw_data=True, xd=bin_centers, yd=smoothed, range=[bin_centers[np.argmax(smoothed)]-7, bin_centers[np.argmax(smoothed)]+7])
                        centroid = popt[1]
                        sigma = popt[2]
                    except (TypeError, RuntimeError):
                        centroid = bin_centers[np.argmax(smoothed)]
                    # vect_in_limit = vect_in_limit[np.logical_and(vect_in_limit > bin_centers[np.argmax(smoothed)]-30, vect_in_limit < bin_centers[np.argmax(smoothed)]+30)] # 30 is arbitrary for now
                    vect_in_limit = vect_in_limit[np.logical_and(vect_in_limit > centroid-30, vect_in_limit < centroid+30)] # 30 is arbitrary for now
                    vect_in_limit = vect_in_limit - centroid
                    label = label + " (max subtracted)"
                    plt.fill_between(bin_centers[np.logical_and(bin_centers > centroid-30, bin_centers < centroid+30)]-centroid, hist[np.logical_and(bin_centers > centroid-30, bin_centers < centroid+30)], color=color, alpha=0.5) # step='mid',
                    plt.plot(bin_centers[np.logical_and(bin_centers > centroid-30, bin_centers < centroid+30)]-centroid, smoothed[np.logical_and(bin_centers > centroid-30, bin_centers < centroid+30)], 'k')
                if vect_in_limit.sum() == 0:
                    plt.text(.5, .5, 'invalid', ha='center', va='center', size='large',
                             transform=plt.gca().transAxes)
                    continue
            else:
                contents, _bins, _patches = plt.hist(vect_in_limit, 200, log=log, histtype='stepfilled', fc=color, alpha=0.5)

            if lines_at is not None:
                plt.sj_vlines([lines_at[0],lines_at[1]],':')
                #plt.text(lines_at[1]*1.1, 0, lines_at[1], color='red')
            plt.text(.1, .5, '%g'%plt.xlim()[0], ha='center', va='center',
                     transform=plt.gca().transAxes)
            plt.text(.9, .5, '%g'%plt.xlim()[1], ha='center', va='center',
                     transform=plt.gca().transAxes)
        if (i%ch_per_fig == ch_per_fig - 1) or (i == len(dataset_numbers)-1):
            plt.xlabel(label)
        if log:
            plt.ylim(ymin=contents.min())

mass.TESGroup.sj_plot_summaries = sj_plot_summaries


def sj_cut_guide(self, forceNew=True, write2file=True, plot_it=True, quick_cut=True, cut_vals_list=None, downsample=10, ptm_median_or_max='max', close_plots=False):
    """
    # To help to set cut values by displaying some plot_summaries plots
    # returns final_cuts, not data
    # quick_cut=True shows only ptm_depart_from_median_cutval
    # cut_vals_list = [pretrigger_rms_cutval, risetime_cutval, peaktime_cutval, postpeak_deriv_cutval, ptm_depart_from_median_cutval]
        or [pretrigger_rms_cutval, risetime_cutval, peaktime_cutval, postpeak_deriv_cutval]
        or [pretrigger_rms_cutval, risetime_cutval, peaktime_cutval]
        or [pretrigger_rms_cutval, risetime_cutval]
        or [pretrigger_rms_cutval] allows to skip some of the first cut params display (to prevent from )
    # if default values are used,
    """
    # add: save old cut if exists and retrieve later if needed

    for ds in self:
        ds.clear_cuts()

    num_cut_vals_given = 0

    if quick_cut is True:
       cut_vals_list = [80, 0.12, 0.17, 50]

    if cut_vals_list is not None:
        num_cut_vals_given = len(cut_vals_list)


    if plot_it == True:
        # pretrigger_rms
        if num_cut_vals_given < 1:
            self.sj_plot_summaries(1, lines_at=[50., 65.], downsample=downsample)
            temp_cuts = mass.core.controller.AnalysisControl(
                pulse_average=(0.0, None), #0
                pretrigger_rms=(None, 50.0), #1
            )
            self.apply_cuts(temp_cuts)

        # rise_time_ms added
        if num_cut_vals_given < 2:
            self.sj_plot_summaries(5, lines_at=[0.11, 0.13], downsample=downsample)
            temp_cuts = mass.core.controller.AnalysisControl(
                pulse_average=(0.0, None), #0
                pretrigger_rms=(None, 50.0), #1
                rise_time_ms=(0, 0.11), #5
            )
            self.apply_cuts(temp_cuts)

        # peak_time_ms added
        if num_cut_vals_given < 3:
            self.sj_plot_summaries(6, lines_at=[0.15, 0.2], downsample=downsample)
            temp_cuts = mass.core.controller.AnalysisControl(
                pulse_average=(0.0, None), #0
                pretrigger_rms=(None, 50.0), #1
                rise_time_ms=(0, 0.11), #5
                peak_time_ms=(0, 0.15) #6
            )
            self.apply_cuts(temp_cuts)

        # postpeak_deriv added
        if num_cut_vals_given < 4:
            self.sj_plot_summaries(4, lines_at=[30., 50.], downsample=downsample)
            temp_cuts = mass.core.controller.AnalysisControl(
                pulse_average=(0.0, None), #0
                pretrigger_rms=(None, 50.0), #1
                rise_time_ms=(0, 0.11), #5
                peak_time_ms=(0, 0.15), #6
                postpeak_deriv=(None, 30.0) #4
            )
            self.apply_cuts(temp_cuts)

        # pretrigger_mean_departure_from_median or pretrigger_mean_departure_from_max added
        if num_cut_vals_given < 5:
            self.sj_plot_summaries(2, lines_at=[-15.0, 15.], downsample=downsample, ptm_median_or_max=ptm_median_or_max)

        for fig_num in plt.get_fignums()[::-1]:
            plt.figure(fig_num).canvas.manager.window.activateWindow()
            plt.pause(0.001) # for Win
            # plt.show()

    if num_cut_vals_given < 1:
        while True:
            # plt.pause(0.001)
            # plt.waitforbuttonpress()
            # pretrigger_rms_cutval = raw_input('pretrigger_rms_cutval (type n (no cut) or ch # or value): ')
            pretrigger_rms_cutval = input('pretrigger_rms_cutval (type n (no cut) or "ch #" or value): ')
            if 'ch' in pretrigger_rms_cutval:
                try:
                    channum = int(pretrigger_rms_cutval.strip().split('ch')[1])
                    self.sj_plot_channum_traces(channum, range(20))
                except ValueError:
                    continue
            elif '' == pretrigger_rms_cutval:
                continue
            elif 'n' == pretrigger_rms_cutval:
                print('# sj_cut_guide script cancelled by user')
                return None
            else:
                try:
                    pretrigger_rms_cutval = float(pretrigger_rms_cutval)
                    break
                except ValueError:
                    continue
    if num_cut_vals_given < 2:
        while True:
            # plt.pause(0.001)
            # plt.waitforbuttonpress()
            # risetime_cutval = raw_input('risetime_cutval (type n (no cut) or back or ch # or value): ')
            risetime_cutval = input('risetime_cutval (type n (no cut) or back or "ch #" or value): ')
            if 'ch' in risetime_cutval:
                try:
                    channum = int(risetime_cutval.strip().split('ch')[1])
                    self.sj_plot_channum_traces(channum, range(20))
                except ValueError:
                    continue
            elif '' == risetime_cutval:
                continue
            elif 'n' == risetime_cutval:
                print('# sj_cut_guide script cancelled by user')
                return None
            elif 'back' == risetime_cutval:
                # pretrigger_rms_cutval = float(raw_input('pretrigger_rms_cutval (type n (no cut) or back or ch # or value): '))
                pretrigger_rms_cutval = float(input('pretrigger_rms_cutval (type n (no cut) or back or "ch #" or value): '))
                continue
            else:
                try:
                    risetime_cutval = float(risetime_cutval)
                    break
                except ValueError:
                    continue
    if num_cut_vals_given < 3:
        while True:
            # plt.pause(0.001)
            # plt.waitforbuttonpress()
            # peaktime_cutval = raw_input('peaktime_cutval (type n (no cut) or back or ch # or value): ')
            peaktime_cutval = input('peaktime_cutval (type n (no cut) or back or "ch #" or value): ')
            if 'ch' in peaktime_cutval:
                try:
                    channum = int(peaktime_cutval.strip().split('ch')[1])
                    self.sj_plot_channum_traces(channum, range(20))
                except ValueError:
                    continue
            elif '' == peaktime_cutval:
                continue
            elif 'n' == peaktime_cutval:
                print('# sj_cut_guide script cancelled by user')
                return None
            elif 'back' == peaktime_cutval:
                # risetime_cutval = float(raw_input('risetime_cutval (type n (no cut) or back or ch # or value): '))
                risetime_cutval = float(input('risetime_cutval (type n (no cut) or back or "ch #" or value): '))
                continue
            else:
                try:
                    peaktime_cutval = float(peaktime_cutval)
                    break
                except ValueError:
                    continue
    if num_cut_vals_given < 4:
        while True:
            # plt.pause(0.001)
            # plt.waitforbuttonpress()
            # postpeak_deriv_cutval = raw_input('postpeak_deriv_cutval (type n (no cut) or back or ch # or value): ')
            postpeak_deriv_cutval = input('postpeak_deriv_cutval (type n (no cut) or back or "ch #" or value): ')
            if 'ch' in postpeak_deriv_cutval:
                try:
                    channum = int(postpeak_deriv_cutval.strip().split('ch')[1])
                    self.sj_plot_channum_traces(channum, range(20))
                except ValueError:
                    continue
            elif '' == postpeak_deriv_cutval:
                continue
            elif 'n' == postpeak_deriv_cutval:
                print('# sj_cut_guide script cancelled by user')
                return None
            elif 'back' == postpeak_deriv_cutval:
                # peaktime_cutval = float(raw_input('peaktime_cutval (type n (no cut) or back or ch # or value): '))
                peaktime_cutval = float(input('peaktime_cutval (type n (no cut) or back or "ch #" or value): '))
                continue
            else:
                try:
                    postpeak_deriv_cutval = float(postpeak_deriv_cutval)
                    break
                except ValueError:
                    continue
    if num_cut_vals_given < 5:
        while True:
            # plt.pause(0.001)
            # plt.waitforbuttonpress()
            # ptm_depart_from_median_cutval = raw_input('ptm_depart_from_median_cutval (type n (no cut) or back or ch # or value): ')
            ptm_depart_from_median_cutval = input('ptm_depart_from_median_cutval (type n (no cut) or back or "ch #" or value): ')
            if 'ch' in ptm_depart_from_median_cutval:
                try:
                    channum = int(ptm_depart_from_median_cutval.strip().split('ch')[1])
                    self.sj_plot_channum_traces(channum, range(20))
                except ValueError:
                    continue
            elif '' == ptm_depart_from_median_cutval:
                continue
            elif 'n' == ptm_depart_from_median_cutval:
                print('# sj_cut_guide script cancelled by user')
                return None
            elif 'back' == ptm_depart_from_median_cutval:
                # postpeak_deriv_cutval = float(raw_input('postpeak_deriv_cutval (type n (no cut) or back or ch # or value): '))
                postpeak_deriv_cutval = float(input('postpeak_deriv_cutval (type n (no cut) or back or "ch #" or value): '))
                continue
            else:
                try:
                    ptm_depart_from_median_cutval = float(ptm_depart_from_median_cutval)
                    break
                except ValueError:
                    continue

    if num_cut_vals_given >= 1:
        pretrigger_rms_cutval = cut_vals_list[0]
    if num_cut_vals_given >= 2:
        risetime_cutval = cut_vals_list[1]
    if num_cut_vals_given >= 3:
        peaktime_cutval = cut_vals_list[2]
    if num_cut_vals_given >= 4:
        postpeak_deriv_cutval = cut_vals_list[3]
    if num_cut_vals_given >= 5:
        ptm_depart_from_median_cutval = cut_vals_list[4]

    final_cuts = mass.core.controller.AnalysisControl(
        #pulse_average=(0.0, None), #0
        pretrigger_rms=(None, pretrigger_rms_cutval), #1
        pretrigger_mean_departure_from_median=(-ptm_depart_from_median_cutval, ptm_depart_from_median_cutval), #2, pretty big drift
        peak_value=(0.0, None), #3
        postpeak_deriv=(None, postpeak_deriv_cutval), #4
        rise_time_ms=(0, risetime_cutval), #5
        peak_time_ms=(0, peaktime_cutval) #6
    )
    self.sj_apply_cuts(cuts=final_cuts, forceNew=forceNew, write2file=write2file, ptm_median_or_max=ptm_median_or_max)

    if close_plots is True:
        plt.close('all')

    print('# pretrigger_rms_cutval:', pretrigger_rms_cutval)
    print('# risetime_cutval:', risetime_cutval)
    print('# peaktime_cutval:', peaktime_cutval)
    print('# postpeak_deriv_cutval:', postpeak_deriv_cutval)
    print('# ptm_depart_from_median_cutval:', ptm_depart_from_median_cutval)

    return final_cuts

mass.TESGroup.sj_cut_guide = sj_cut_guide


def sj_apply_cuts(self, cuts=None, forceNew=True, clear=True, write2file=True, verbose=2, filename=None, ptm_median_or_max='median'):
    """
    modified to be able to write cut numbers to a file
    forceNew=True writes into a file, forceNew=False doesn't do anything (actually it's setting clear=True in channel.py)
    write2file=True => was going to force to write, but not implemented yet
    todo: will add number of traces cut by each cut criterion (e.g. by risetime: 100, by ptm: 150)
    """

    from datetime import datetime

    try:  # for ljh
        dir_p = self.filenames[0].split(os.sep)[-2]
    except AttributeError:  # for hdf5
        dir_p = self.first_good_dataset.filename.split(os.sep)[-1][:12]
    timestamp = datetime.now().strftime('%Y%m%d_%H%M%S')
    if filename is None:
        filename = dir_p+'_cut_'+timestamp+'.txt'

    #fmt = '  %-'+'%d'%(max_len-2) +'s'

    text_to_write = '#'
    text_to_write += str(cuts.cuts_prm) + '\n' #writing the cut dict into txt, quick and dirty way

    if ptm_median_or_max == 'max':
        text_to_write += '#ptm_median_or_max=\'max\'\n'

    text_to_write += "%3s %9s %9s %9s %9s\n" %("#ch","good","bad","total","good_ratio")

    if cuts is None: # use generic cut
        cuts = mass.core.controller.AnalysisControl(
            #pulse_average=(0.0, None), #0
            pretrigger_rms=(None, 50.0), #1
            pretrigger_mean_departure_from_median=(-20.0, 20.0), #2
            # pretrigger_mean_departure_from_max=(-20.0, 20.0), #2
            peak_value=(0.0, None), #3
            postpeak_deriv=(None, 50.0), #4
            rise_time_ms=(0, 0.12), #5
            peak_time_ms=(0, 0.17) #6
        )

    for ds in self:
        ds.sj_apply_cuts_ds(cuts, clear=clear, verbose=verbose, forceNew=forceNew, ptm_median_or_max=ptm_median_or_max)
        # if write2file:
        text_to_write += "%3s %9s %9s %9s %.4f\n" %(ds.channum, ds.cuts.good().sum(), ds.cuts.bad().sum(), ds.nPulses, np.float16(ds.cuts.good().sum())/ds.nPulses)

    # if write2file or len(glob.glob(dir_p+'_cut*'))==0: # latter is to count existing cut files
    with open(filename, 'w') as f:
        f.write(text_to_write)
        print("# %s written" %filename)

    # not sure if I want below. commented out for now
    # if len(glob.glob(dir_p+'_cut*'))>1:
    #     import filecmp
    #     if filecmp.cmp(glob.glob(dir_p+'_cut*')[-1], glob.glob(dir_p+'_cut*')[-2]):
    #         os.remove(glob.glob(dir_p+'_cut*')[-2]) # if the same cut, previous one is removed

mass.TESGroup.sj_apply_cuts = sj_apply_cuts


def sj_apply_cuts_ds(self, controls=None, clear=False, verbose=1, forceNew=True, ptm_median_or_max='median'):
    """
    <clear>  Whether to clear previous cuts first (by default, do not clear).
    <verbose> How much to print to screen.  Level 1 (default) counts all pulses good/bad/total.
                Level 2 adds some stuff about the departure-from-median pretrigger mean cut.
    """
    if self.nPulses == 0:
        return  # don't bother current if there are no pulses
    if not forceNew:
        if self.cuts.good().sum() != self.nPulses:
            print("Chan %d skipped cuts: after %d are good, %d are bad of %d total pulses" %
                  (self.channum, self.cuts.good().sum(), self.cuts.bad().sum(), self.nPulses))
    else:
        if clear:
            self.clear_cuts()

        if controls is None:
            controls = mass.controller.standardControl()
        c = controls.cuts_prm

        self.cuts.cut_parameter(self.p_energy, c['energy'], 'energy')
        self.cuts.cut_parameter(self.p_pretrig_rms, c['pretrigger_rms'], 'pretrigger_rms')
        self.cuts.cut_parameter(self.p_pretrig_mean, c['pretrigger_mean'], 'pretrigger_mean')

        self.cuts.cut_parameter(self.p_peak_time[:]*1e3, c['peak_time_ms'], 'peak_time_ms')
        self.cuts.cut_parameter(self.p_rise_time[:]*1e3, c['rise_time_ms'], 'rise_time_ms')
        self.cuts.cut_parameter(self.p_postpeak_deriv, c['postpeak_deriv'], 'postpeak_deriv')
        self.cuts.cut_parameter(self.p_pulse_average, c['pulse_average'], 'pulse_average')
        self.cuts.cut_parameter(self.p_peak_value, c['peak_value'], 'peak_value')
        self.cuts.cut_parameter(self.p_min_value[:] - self.p_pretrig_mean[:], c['min_value'], 'min_value')
        self.cuts.cut_parameter(self.p_timestamp[:], c['timestamp_sec'], 'timestamp_sec')

        if c['timestamp_diff_sec'] is not None:
            self.cuts.cut_parameter(np.hstack((0.0, np.diff(self.p_timestamp))),
                                    c['timestamp_diff_sec'], 'timestamp_diff_sec')
        if c['pretrigger_mean_departure_from_median'] is not None and self.cuts.good().sum() > 0:
            if ptm_median_or_max=='median':
                median = np.median(self.p_pretrig_mean[self.cuts.good()])
                if verbose > 1:
                    print('applying cut on pretrigger mean around its median value of ', median)
                self.cuts.cut_parameter(self.p_pretrig_mean-median,
                                        c['pretrigger_mean_departure_from_median'],
                                        'pretrigger_mean_departure_from_median')
            elif ptm_median_or_max=='max':
                # median = np.median(self.p_pretrig_mean[self.cuts.good()])

                hist, bin_edges = np.histogram(self.p_pretrig_mean[self.cuts.good()], ss_opt_num_bin(self.p_pretrig_mean[self.cuts.good()]))
                smoothed = running_mean_conv(hist, 10, 'same') # arbitrary smoothing factor of 10
                bin_centers = (bin_edges[1:]+bin_edges[:-1])/2
                try:
                    centroid = fit_gaussian(sigma = 3, tailed=False, plot_it=False, raw_data=True, xd=bin_centers, yd=smoothed, range=[bin_centers[np.argmax(smoothed)]-7, bin_centers[np.argmax(smoothed)]+7])[0][1]
                except TypeError:
                    print('TypeError occurred during Gaussian fitting')
                    centroid = bin_centers[np.argmax(smoothed)]
                except RuntimeError:
                    print('RuntimeError occurred during Gaussian fitting')
                    centroid = bin_centers[np.argmax(smoothed)]

                if verbose > 1:
                    print('applying cut on pretrigger mean around its max value of ', centroid)
                self.cuts.cut_parameter(self.p_pretrig_mean-centroid,
                                        c['pretrigger_mean_departure_from_median'],
                                        'pretrigger_mean_departure_from_median')
        if verbose > 0:
            print("Chan %d after cuts, %d are good, %d are bad of %d total pulses" % (
                self.channum, self.cuts.good().sum(), self.cuts.bad().sum(), self.nPulses))

mass.MicrocalDataSet.sj_apply_cuts_ds = sj_apply_cuts_ds


def load_cuts(filename):
    ptm_median_or_max = 'median'
    with open(filename, 'r') as f:
        for line in f:
            if 'postpeak_deriv' in line:
                ll="cut_dict_from_file="+line.strip()[1:]
                exec(ll)
            if 'max' in line:
                ptm_median_or_max = 'max'
                break
    try:
        cuts = mass.core.controller.AnalysisControl(
            #pulse_average=(0.0, None), #0
            pretrigger_rms=(None, cut_dict_from_file['pretrigger_rms'][1]), #1
            pretrigger_mean_departure_from_median=(cut_dict_from_file['pretrigger_mean_departure_from_median'][0], cut_dict_from_file['pretrigger_mean_departure_from_median'][1]), #2
            peak_value=(0.0, None), #3
            postpeak_deriv=(None, cut_dict_from_file['postpeak_deriv'][1]), #4
            rise_time_ms=(0, cut_dict_from_file['rise_time_ms'][1]), #5
            peak_time_ms=(0, cut_dict_from_file['peak_time_ms'][1]) #6
            )
        return cuts, ptm_median_or_max
    except NameError:
        raise NameError


def sj_generate_cut_control(self):
    """
    incomplete, might be called inside sj_auto_cut
    """
    for ds in self:
        num_bin = ds.nPulses/10
        #ss_opt_num_bin()

    cuts = mass.core.controller.AnalysisControl(
        #pulse_average=(0.0, None), #0
        pretrigger_rms=(None, 50.0), #1
        pretrigger_mean_departure_from_median=(-20.0, 20.0), #2
        peak_value=(0.0, None), #3
        postpeak_deriv=(None, 50.0), #4
        rise_time_ms=(0, 0.12), #5
        peak_time_ms=(0, 0.17) #6
    )

    return cuts

mass.TESGroup.sj_generate_cut_control = sj_generate_cut_control


def sj_auto_cut(self, forceNew=True, clear=True, write2file=True, verbose=2, filename=None):
    """
    incomplete
    """
    if write2file:
        from datetime import datetime

        if filename is None:
            filename1 = self.filenames[0].split(os.sep)[-2]
            filename2 = datetime.now().strftime('%Y%m%d_%H%M%S')
            filename = filename1+'_autocut_'+filename2+'.txt'

        #fmt = '  %-'+'%d'%(max_len-2) +'s'

        text_to_write = '#'
        text_to_write += str(cuts.cuts_prm) + '\n' #writing the cut dict into txt, quick and dirty way
        '''
        if you want to read the cut dict in the header, do something like:

            with open(filename, 'r') as f:
                for line in f:
                    if 'postpeak_deriv' in line:
                        ll="cut_dict_from_file="+line.strip()[1:]
                        exec(ll)
        '''
        text_to_write += "%3s %9s %9s %9s %9s\n" %("#ch","good","bad","total","good_ratio")


    for ds in self:

        pretrigger_rms_cutval = ptm_depart_from_median_cutval = postpeak_deriv_cutval = risetime_cutval = peaktime_cutval = 0.

        autocuts = mass.core.controller.AnalysisControl(
            #pulse_average=(0.0, None), #0
            pretrigger_rms=(None, pretrigger_rms_cutval), #1
            pretrigger_mean_departure_from_median=(-ptm_depart_from_median_cutval, ptm_depart_from_median_cutval), #2
            #peak_value=(0.0, None), #3
            postpeak_deriv=(None, postpeak_deriv_cutval), #4
            rise_time_ms=(0, risetime_cutval), #5
            peak_time_ms=(0, peaktime_cutval) #6
        )

        ds.apply_cuts(autocuts, clear=clear, verbose=verbose, forceNew=forceNew)
        if write2file:
            text_to_write += "%3s %9s %9s %9s %9s\n" %(ds.channum, ds.cuts.good().sum(), ds.cuts.bad().sum(), ds.nPulses, np.float16(ds.cuts.good().sum())/ds.nPulses)

    if write2file:
        with open(filename, 'w') as f:
            f.write(text_to_write)
            print("%s written" %filename)

    plt.figure();plt.hist(ds.p_rise_time[:][ds.p_rise_time[:]<0.0002],100)
    #plt.figure();plt.hist(data.channel[293].p_rise_time[:][data.channel[293].p_rise_time[:]<0.0002],100)

mass.TESGroup.sj_auto_cut = sj_auto_cut


def sj_median_checker_for_auto_masks(self, dataset_numbers=None, limit=[0.98, 1.02], use_tallest=True):

    """
    to show histogram of p_peak_value and p_pulse_average and median and its *0.98 and *1.02 lines
    example usage: data.sj_median_checker_for_auto_masks(); data.sj_median_checker_for_auto_masks(range(15))
    data.sj_median_checker_for_auto_masks([2,3,5,8]); data.sj_median_checker_for_auto_masks([2,3,5,8], [0.99, 1.01])
    data.sj_median_checker_for_auto_masks([0.99, 1.01])
    """
    ch_per_fig = 15

    # to handle the case of data.sj_median_checker_for_auto_masks([0.99, 1.01])
    if (len(dataset_numbers) == 2) and np.logical_or(isinstance(dataset_numbers[0],float), isinstance(dataset_numbers[1],float)):
        limit = dataset_numbers
        dataset_numbers = None

    datasets = self.datasets
    if dataset_numbers is None:
        dataset_numbers = range(len(datasets))
    else:
        datasets = [self.datasets[i] for i in dataset_numbers]

    median_raw_ph = np.zeros(len(dataset_numbers))
    num_raw_ph_in_limit = np.zeros(len(dataset_numbers))
    median_pls_avg = np.zeros(len(dataset_numbers))
    num_pls_avg_in_limit = np.zeros(len(dataset_numbers))
    for i, (dataset_idx, ds) in enumerate(zip(dataset_numbers, datasets)):
        # histogram on left half of figure
        if (i%ch_per_fig) == 0:
            plt.figure()
            ax_master1 = plt.subplot(ch_per_fig, 2, 1 + (i%ch_per_fig) * 2)
            plt.title("p_peak_value")
        else:
            plt.subplot(ch_per_fig, 2, 1 + (i%ch_per_fig) * 2, sharex=ax_master1)

        if sum(ds.good()) < 30:
            plt.text(.5, .5, 'too few (<30) data', ha='center', va='center', size='large',
                     transform=plt.gca().transAxes)
        else:
            # contents, _bins, _patches = plt.hist(ds.p_peak_value[ds.good()], 200,# log=log,
            #                                      histtype='stepfilled', alpha=0.5)
            contents, _bins, _patches = plt.hist(ds.p_peak_value[ds.good()], ss_opt_num_bin(ds.p_peak_value[ds.good()]),# log=log,
                                                 histtype='stepfilled', alpha=0.5)
            if use_tallest is True:
                c, b = np.histogram(ds.p_peak_value[ds.good()], ss_opt_num_bin(ds.p_peak_value[ds.good()]))
                bin_centers = (b[1:]+b[:-1])*0.5
                median_raw_ph[i] = bin_centers[np.argmax(c)]
            else:
                median_raw_ph[i] = np.median(ds.p_peak_value[ds.good()])
            num_raw_ph_in_limit[i] = sum(np.logical_and(ds.p_peak_value[ds.good()]<= median_raw_ph[i]*limit[1], ds.p_peak_value[ds.good()]>= median_raw_ph[i]*limit[0]))
            plt.text(.8, .5, '%d pulses' % num_raw_ph_in_limit[i], ha='center', va='center', size='medium',
                     transform=plt.gca().transAxes)
            plt.sj_vlines(median_raw_ph[i])
            plt.sj_vlines(median_raw_ph[i]*limit[0],'--')
            plt.sj_vlines(median_raw_ph[i]*limit[1],'--')
        plt.ylabel("TES %d\n(Ch %d)" % (dataset_idx,ds.channum))

        if (i%ch_per_fig) == 0:
            ax_master2 = plt.subplot(ch_per_fig, 2, 2 + (i%ch_per_fig) * 2)
            plt.title("p_pulse_average")
        else:
            plt.subplot(ch_per_fig, 2, 2 + (i%ch_per_fig) * 2, sharex=ax_master2)

        if sum(ds.good()) < 30:
            plt.text(.5, .5, 'too few (<30) data', ha='center', va='center', size='large',
                     transform=plt.gca().transAxes)
        else:
            # contents, _bins, _patches = plt.hist(ds.p_pulse_average[ds.good()], 200,# log=log,
            #                                      histtype='stepfilled', alpha=0.5)
            contents, _bins, _patches = plt.hist(ds.p_pulse_average[ds.good()], ss_opt_num_bin(ds.p_pulse_average[ds.good()]),# log=log,
                                                 histtype='stepfilled', alpha=0.5)
            if use_tallest is True:
                c, b = np.histogram(ds.p_pulse_average[ds.good()], ss_opt_num_bin(ds.p_pulse_average[ds.good()]))
                bin_centers = (b[1:]+b[:-1])*0.5
                median_pls_avg[i] = bin_centers[np.argmax(c)]
            else:
                median_pls_avg[i] = np.median(ds.p_pulse_average[ds.good()])
            num_pls_avg_in_limit[i] = sum(np.logical_and(ds.p_pulse_average[ds.good()]<= median_pls_avg[i]*limit[1], ds.p_pulse_average[ds.good()]>= median_pls_avg[i]*limit[0]))
            plt.text(.8, .5, '%d pulses' % num_pls_avg_in_limit[i], ha='center', va='center', size='medium',
                     transform=plt.gca().transAxes)
            plt.sj_vlines(median_pls_avg[i])
            plt.sj_vlines(median_pls_avg[i]*limit[0],'--')
            plt.sj_vlines(median_pls_avg[i]*limit[1],'--')
        plt.ylabel("TES %d\n(Ch %d)" % (dataset_idx,ds.channum))
    plt.figure()
    plt.plot([self.datasets[i].channum for i in range(len(datasets))], median_raw_ph,'.')
    plt.xlabel('Channum number')
    plt.ylabel('Average pulse peak')
    plt.figure()
    plt.plot([self.datasets[i].channum for i in range(len(datasets))], median_pls_avg,'.')
    plt.xlabel('Channum number')
    plt.ylabel('Average pulse average')
    median_num_raw_ph = np.median(num_raw_ph_in_limit[num_raw_ph_in_limit != 0])
    median_num_pls_avg = np.median(num_pls_avg_in_limit[num_pls_avg_in_limit != 0])
    print("\nmedian number of pulses within the limit:\n%d (p_peak_value), %d (p_pulse_average)" %(median_num_raw_ph, median_num_pls_avg))
    # highest_intensity_pulse_avg = np.array([np.median(ds.p_pulse_average[ds.good()]) for ds in self])
    # masks = self.make_masks([.95, 1.05], use_gains=True, gains=median_pulse_avg)
    # for m in masks:
    #     if len(m) > max_pulses_to_use:
    #         m[max_pulses_to_use:] = False
    # self.compute_average_pulse(masks, forceNew=forceNew)

mass.TESGroup.sj_median_checker_for_auto_masks = sj_median_checker_for_auto_masks


def sj_avg_pulses_auto_masks(self, limit=[0.98, 1.02], quantity="p_peak_value", max_pulses_to_use=1000, forceNew=True, use_tallest=True, restrain_ptm=False):
    """
    use p_peak_value instead by default, but can also use p_pulse_average
    if use_tallest is True, use tallest peak, not the median of the whole pulses
    if restrain_ptm is True, use pulses only around the median of the ptm
    example usage: data.sj_avg_pulses_auto_masks(); data.sj_avg_pulses_auto_masks([0.97, 1.03])
    data.sj_avg_pulses_auto_masks([0.97, 1.03], quantity="p_pulse_average")
    forceNew = True by default
    """

    if quantity == "p_peak_value":
        threshold = 1000 # to select out random triggers
    else:
        threshold = 0
    # for ds in self:
    #     if ds.nPulses < 10:
    #         self.set_chan_bad(ds.channum, "has less than 10 pulses")
    if use_tallest is True: # if True, use tallest peak, not the median of the whole pulses
        # median = np.array([])
        median = []
        for ds in self.datasets:
            ig = ds.good() & (ds.__dict__[quantity][:]>threshold)
            if sum(ig) < 10:
                median.append(1.) # this is needed because mass.make_masks runs over all channels (good & bad)
                self.set_chan_bad(ds.channum, 'too few good pulses')
                # print("Channel %d has only %d good pulses. Median will be used." %(ds.channum, sum(ds.good())))
                # median = np.append(median, np.median(eval("ds.%s[ds.good()]"%quantity)))
                # median = np.append(median, np.median(ds.__dict__[quantity][ds.good()]))
            else:
                # c, b = np.histogram(eval("ds.%s[ds.good()]"%quantity), ss_opt_num_bin(eval("ds.%s[ds.good()]"%quantity)))
                c, b = np.histogram(ds.__dict__[quantity][ig], ss_opt_num_bin(ds.__dict__[quantity][ig]))
                bin_centers = (b[1:]+b[:-1])*0.5
                median.append(bin_centers[np.argmax(c)])
                # median = np.append(median, bin_centers[np.argmax(c)]) # actually maximum position, not median contrary to its name
        median = np.array(median)
    else:
        # median = np.array([np.median(eval("ds.%s[ds.good()]"%quantity)) for ds in self])
        median = np.array([np.median(ds.__dict__[quantity][ds.good() & (ds.__dict__[quantity][:]>threshold)]) for ds in self])

    if quantity=="p_peak_value":
        if mass.__version__ < '0.6.2':
            masks = self.make_masks(pulse_peak_range=[limit[0], limit[1]], use_gains=True, gains=median) # masks is an an array of mask of ds (including bad chans)
        else:
            masks = self.make_masks(pulse_peak_range=[limit[0], limit[1]], gains=median) # masks is an an array of mask of ds (including bad chans)
    if quantity=="p_pulse_average":
        if mass.__version__ < '0.6.2':
            masks = self.make_masks(pulse_avg_range=[limit[0], limit[1]], use_gains=True, gains=median)
        else:
            masks = self.make_masks(pulse_avg_range=[limit[0], limit[1]], gains=median)
    for i, m in enumerate(masks):
        if restrain_ptm is True:
            if np.sum(m)>0:
                # print 'retrain_ptm = True'
                ptm = self.datasets[i].p_pretrig_mean[:]
                ptm_offset = np.median(ptm[self.datasets[i].good()])
                ptm = ptm - ptm_offset
                m_ptm = np.abs(ptm) < np.std(ptm[self.datasets[i].good()]) * 0.5 # within 0.5 std
                m = np.logical_and(m, m_ptm)

        # old way
        # if np.sum(m) > max_pulses_to_use:
        #     good_so_far = np.cumsum(m)
        #     stop_at = (good_so_far == max_pulses_to_use).argmax()
        #     m[stop_at+1:] = False
        # new way
        if np.sum(m) > max_pulses_to_use:
            indices = np.arange(len(m))
            indices_random = indices[np.random.choice(indices[m], max_pulses_to_use, replace=False)]
            m_reduced = np.zeros_like(m)
            m_reduced[indices_random] = True
            masks[i] = m_reduced
        else:
            masks[i] = m
        if np.sum(masks[i]) < 500:
            print("less than 500 pulses for averaging for %dth dataset (ch%d): %d pulses" %(i,self.datasets[i].channum,np.sum(masks[i])))

    try:
        self.compute_average_pulse(masks, forceNew=forceNew)
    except ValueError:
        if len(masks) != len(self.datasets):
            print("The number of masks and number of datasets do not match. Check if there's any channels set bad and rerun.")

mass.TESGroup.sj_avg_pulses_auto_masks = sj_avg_pulses_auto_masks


def sj_plot_average_pulses(self, channum=None, axis=None, use_legend=True, energy=None, energy_range=1):
    """
    sj: modified to accept list for channum
    also to be able to use for both data and ds
    example usage: data.sj_plot_average_pulses(); data.sj_plot_average_pulses([1,2,3,4,5])
    data.sj_plot_average_pulses(range(7))
    ds.sj_plot_average_pulses()
    Plot average pulse for channel number <channum> on matplotlib.Axes <axis>, or
    on a new Axes if <axis> is None.  If <channum> is not a valid channel
    number, then plot all average pulses."""

    if axis is None:
        #plt.clf()
        plt.figure()
        axis = plt.subplot(111)

    if type(self) in [mass.core.channel_group.TESGroup, mass.core.channel_group_hdf5_only.TESGroupHDF5]:

        dt = (np.arange(self.nSamples) - self.nPresamples) * self.timebase * 1e3

        if channum is None:
            num_chans = len(self.good_channels)
        else:
            if isinstance(channum, int):
                channum = [channum]
                num_chans = 1
            else:
                num_chans = len(channum)
            for chan in channum:
                if chan not in self.channel:
                    num_chans = num_chans - 1

        if energy is None:
            rainbow_colors = [cm.rainbow_r(x) for x in np.linspace(0,1.,num_chans)]
            # axis.set_color_cycle(rainbow_colors)
            axis.set_prop_cycle(color=rainbow_colors)

            if channum is None:
                for ds in self:
                    plt.plot(dt, ds.average_pulse, label="Chan %d" % ds.channum)
            else:
                # if isinstance(channum, int):
                #     channum = [channum]
                for ch in channum:
                    if ch in self.channel:
                        plt.plot(dt, self.channel[ch].average_pulse, label='Chan %d' % ch)
                    else:
                        print("Ch %d doesn't exist; skipped" %ch)
            axis.set_title("Average pulses for %d channel(s)" %num_chans)
        else:
            try:
                energy = [float(energy)]  # if integer or float, make it a list
            except:
                pass
            rainbow_colors = [cm.rainbow_r(x) for x in np.linspace(0,1.,num_chans*len(energy))]
            # axis.set_color_cycle(rainbow_colors)
            axis.set_prop_cycle(color=rainbow_colors)
            pulse_dict = {}
            for ch in channum:
                pulse_dict[ch] = {}
                for e in energy:
                    ig = np.logical_and((self.channel[ch].p_energy[:]>e-energy_range), (self.channel[ch].p_energy[:]<e+energy_range))
                    pulse_dict[ch][e] = np.sum(self.channel[ch].data[ig], axis=0) / np.sum(ig)
                    plt.plot(dt, pulse_dict[ch][e] - np.median(pulse_dict[ch][e][:self.nPresamples]), label='Chan %d, %f eV'%(ch, e))
    else:  # this is when it's called like ds.sj_plot_average_pulses()
        dt = (np.arange(self.tes_group.nSamples) - self.tes_group.nPresamples) * self.tes_group.timebase * 1e3

        plt.plot(dt, self.average_pulse, label="Chan %d" % self.channum)
        axis.set_title("Average pulse for channel %d"%self.channum)

    plt.xlabel("Time past trigger (ms)")
    plt.ylabel("Raw counts")
    plt.xlim([dt[0], dt[-1]])

    if num_chans > 80:
        use_legend = False

    if use_legend:
        plt.legend(loc=1, fontsize='xx-small', ncol=2)

mass.TESGroup.sj_plot_average_pulses = sj_plot_average_pulses
mass.MicrocalDataSet.sj_plot_average_pulses = sj_plot_average_pulses

#def sj_plot_pulse_fft(self):

def sj_plot_noise(self, channels=None, scale_factor=1.0, sqrt_psd=False, cmap=None):
    """
    sj: removed the first arg (axis=None)
    example usage: sj_plot_noise(); sj_plot_noise([1,3,5,7])
    Compare the noise power spectra.

    <channels>    Sequence of channels to display.  If None, then show all.
    <scale_factor> Multiply counts by this number to get physical units.
    <sqrt_psd>     Whether to show the sqrt(PSD) or (by default) the PSD itself.
    <cmap>         A matplotlib color map.  Defaults to something.
    """

    if channels is None:
        channels = list(self.channel.keys())
        channels.sort()

    if scale_factor == 1.0:
        units = "Counts"
    else:
        units = "Scaled counts"

    if cmap is None:
        cmap = plt.cm.get_cmap("spectral")

    if isinstance(channels, int):
        if channels not in self.channel:
            print("\nCh%d doesn't exist; try another" %channels)
            return
        else:
            channels = [channels]

    valid_chans = []
    for channum in channels:
        if channum not in self.channel:
            print("Ch%d doesn't exist; skipped" %channum)
        else:
            valid_chans.append(channum)

    if len(valid_chans) == 0:
        print("No valid channel found; try again")
        return

    plt.figure()
    axis = plt.subplot(111)
    axis.grid(True)

    for ds_num,channum in enumerate(valid_chans):
        ds = self.channel[channum]
        yvalue = ds.noise_records.noise_psd[:] * scale_factor**2
        if sqrt_psd:
            yvalue = np.sqrt(yvalue)
            axis.set_ylabel("PSD$^{1/2}$ (%s/Hz$^{1/2}$)" % units)
        df = ds.noise_records.noise_psd.attrs['delta_f']
        freq = np.arange(1, 1 + len(yvalue)) * df
        axis.plot(freq, yvalue, label='TES chan %d' % channum,
                  color=cmap(float(ds_num) / len(valid_chans)))
    axis.set_xlim([freq[1] * 0.9, freq[-1] * 1.1])
    axis.set_ylabel("Power Spectral Density (%s^2/Hz)" % units)
    axis.set_xlabel("Frequency (Hz)")

    axis.loglog()
    plt.legend(loc='best')
    ltext = axis.get_legend().get_texts()
    plt.setp(ltext, fontsize='small')

mass.TESGroup.sj_plot_noise = sj_plot_noise


def _sj_use_new_filters(self, use_new_filters=True):
    # if use_new_filters is True:
    #     for ds in self:
    #         ds._use_new_filters = True
    #     print("Set to use new filters")
    # else:
    #     for ds in self:
    #         ds._use_new_filters = False
    #     print("Set to use old filters")
    for ds in self:
        ds._use_new_filters = use_new_filters
    print("Use new filters:", use_new_filters)

mass.TESGroup._sj_use_new_filters = _sj_use_new_filters


# @show_progress("compute_filters") # new mass style
def sj_compute_filters(self, fmax=None, f_3db=None, forceNew=False, use_new_filters=True):
    """
    almost same as original, but added try-except to handle polyfit error
    needs to be modified to be used with mass 0.5.1 and later
    """

    self._sj_use_new_filters(use_new_filters)
    # Analyze the noise, if not already done
    needs_noise = any([ds.noise_autocorr[0] == 0.0 or
                       ds.noise_psd[1] == 0 for ds in self])
    nSamples_mismatch = any([ds.nSamples != ds.noise_records.nSamples for ds in self])
    if needs_noise:
        # LOG.debug("Computing noise autocorrelation and spectrum") # new mass style. needs to import logging
        print("Computing noise autocorrelation and spectrum")
        if nSamples_mismatch is True:
            print("\nMismatch between pulse nSamples and noise nSamples detected.")
            print("Calculating noise psd and autocorrelation...")
            for ds in self:
                pulse_rec_len = ds.nSamples
                noise_rec_len = ds.noise_records.nSamples
                ds.noise_records.hdf5_group = None
                ds.noise_records.nSamples = pulse_rec_len
                ds.noise_records.compute_power_spectrum_reshape(max_excursion=1000, seg_length=pulse_rec_len)
                # ds.compute_autocorrelation()
                ds.noise_records._compute_continuous_autocorrelation(n_lags=noise_rec_len)
                ds.noise_psd[:] = ds.noise_records.noise_psd[:]
                ds.noise_autocorr[:] = ds.noise_records.autocorrelation[:pulse_rec_len]
        else:
            if mass.__version__ >= '0.7.9':
                self.compute_noise()
            else:
                self.compute_noise_spectra()

    print_updater = InlineUpdater('compute_filters')
    for ds_num, ds in enumerate(self):
        if "filters" not in ds.hdf5_group or forceNew:
            if ds.cuts.good().sum() < 10:
                ds.filter = None
                self.set_chan_bad(ds.channum, 'cannot compute filter, too few good pulses')
                continue
            if use_new_filters:
                if ds_num == 0: # by SL
                    print("new filters are being calculated") # by SL
                try:
                    f = ds.compute_newfilter(fmax=fmax, f_3db=f_3db)
                except ValueError:
                    self.set_chan_bad(ds.channum, 'polyfit failed during compute_filters') # by SL
                    continue
                except RuntimeError:
                    self.set_chan_bad(ds.channum, 'Only 0 valid arrival-time bins were found in compute_newfilter')
                    continue # by SL

            else:
                if ds_num == 0: # by SL
                    print("old filters are being calculated") # by SL
                try: # by SL
                    f = ds.compute_oldfilter(fmax=fmax, f_3db=f_3db)
                except ValueError: # by SL
                    self.set_chan_bad(ds.channum, 'polyfit failed during compute_filters') # by SL
                    continue # by SL
                except Exception as e: # LinAlgError happened once
                    self.set_chan_bad(ds.channum, 'polyfit failed during compute_filters') # by SL
                    print(e)
                    continue
            ds.filter = f
            print_updater.update((ds_num + 1) / float(self.n_channels))
            # yield (ds_num + 1) / float(self.n_channels) # new mass style, probably goes with @show_progress

            # Store all filters created to a new HDF5 group
            h5grp = ds.hdf5_group.require_group('filters')
            if f.f_3db is not None:
                h5grp.attrs['f_3db'] = f.f_3db
            if f.fmax is not None:
                h5grp.attrs['fmax'] = f.fmax
            h5grp.attrs['peak'] = f.peak_signal
            h5grp.attrs['shorten'] = f.shorten
            h5grp.attrs['newfilter'] = ds._use_new_filters # added at 0.5.1, SL
            for k in ["filt_fourier", "filt_fourier_full", "filt_noconst",
                      "filt_baseline", "filt_baseline_pretrig", 'filt_aterms']:
                if k in h5grp:
                    del h5grp[k]
                if getattr(f, k, None) is not None:
                    vec = h5grp.create_dataset(k, data=getattr(f, k))
                    vec.attrs['variance'] = f.variances.get(k.split('filt_')[1], 0.0)
                    vec.attrs['predicted_v_over_dv'] = f.predicted_v_over_dv.get(k.split('filt_')[1], 0.0)
        else:
            # LOG.info("chan %d skipping compute_filter because already done, and loading filter", ds.channum) # new mass style
            print("chan %d skipping compute_filter because already done, and loading filter" % ds.channum)
            h5grp = ds.hdf5_group['filters']
            from mass.core.optimal_filtering import Filter
            if mass.__version__ < '0.6.2':
                ds.filter = Filter(ds.average_pulse[...], self.nPresamples - ds.pretrigger_ignore_samples,
                                   ds.noise_psd[...], ds.noise_autocorr[...], sample_time=self.timebase,
                                   fmax=fmax, f_3db=f_3db, shorten=2)
            else: # this part needs to be further modified.
                ds.filter = Filter(ds.average_pulse[...], self.nPresamples - ds.pretrigger_ignore_samples,
                                   ds.noise_psd[...], ds.noise_autocorr[...], sample_time=self.timebase, shorten=2)
            ds.filter.peak_signal = h5grp.attrs['peak']
            ds.filter.shorten = h5grp.attrs['shorten']
            ds.filter.f_3db = h5grp.attrs['f_3db'] if 'f_3db' in h5grp.attrs else None
            ds.filter.fmax = h5grp.attrs['fmax'] if 'fmax' in h5grp.attrs else None
            ds.filter.variances = {}
            for name in h5grp:
                if name.startswith("filt_"):
                    setattr(ds.filter, name, h5grp[name][:])
                    if 'variance' in h5grp[name].attrs:
                        ds.filter.variances[name] = h5grp[name].attrs['variance']
                    if 'predicted_v_over_dv' in h5grp[name].attrs:
                        ds.filter.predicted_v_over_dv[name] = \
                            h5grp[name].attrs['predicted_v_over_dv']

mass.TESGroup.sj_compute_filters = sj_compute_filters


def sj_summarize_filters(self, filter_name='noconst', std_energy=5898.8):
    """
    return nep
    """
    rms_fwhm = np.sqrt(np.log(2) * 8)  # FWHM is this much times the RMS
    print('V/dV for time, Fourier filters: ')
    #nep = np.zeros(self.n_channels)
    nep = np.zeros(0)
    for i, ds in enumerate(self):
        try:
            if ds.filter is not None:
                rms = ds.filter.variances[filter_name]**0.5
            else:
                rms = ds.hdf5_group['filters/filt_%s' % filter_name].attrs['variance']**0.5
            v_dv = (1 / rms) / rms_fwhm
            print("Chan %3d filter %-15s Predicted V/dV %6.1f  Predicted res at %.1f eV: %6.2f eV" %
                  (ds.channum, filter_name, v_dv, std_energy, std_energy / v_dv))
            #nep[i] = std_energy / v_dv
            nep = np.append(nep, std_energy / v_dv)
        except Exception as e:
            print("Filter %d can't be used" % i)
            print(e)
    return nep

mass.TESGroup.sj_summarize_filters = sj_summarize_filters


def sj_filter_data(self, filter_name='filt_noconst', transform=None, include_badchan=False, forceNew=False, use_cython=True):
    """
    try-except was added to the original filter_data
    would like to add a feature that filters only good data
    """
    printUpdater = InlineUpdater('filter_data')
    if include_badchan:
        nchan = float(len(self.channel.keys()))
    else:
        nchan = float(self.num_good_channels)

    for i, chan in enumerate(self.iter_channel_numbers(include_badchan)):
        try:
            self.channel[chan].filter_data(filter_name, transform, forceNew, use_cython=use_cython)

            printUpdater.update((i + 1) / nchan)
        except KeyboardInterrupt:
            raise KeyboardInterrupt('KeyboardInterrupt called')
            print("# Script cancelled by user.")
            return
        except:
            print("Chan %s failed" %chan)
            self.set_chan_bad(chan, "failed filter_data")

mass.TESGroup.sj_filter_data = sj_filter_data


def sj_drift_correct(self, forceNew=False, write2file=False, category=None, filename=None, combined=False, use_reduced=False, ptm_median_or_max='median', new_gain=False, brent=True, new_hist=False):
    """
    drift correction information can be saved into pkl file if write2file=True
    forceNew does not do anything
    """

    ## original mass begin ##
    if 'TES' in str(type(self)): # original TESGroup
        for ds in self:
            ds.new_gain = new_gain
            if np.sum(ds.good()) < 0.02*ds.nPulses:
                print("Drift correction of channel %d skipped: too few good pulses"%ds.channum)
                continue
            else:
                ptm_offset = ds.sj_drift_correct_ds(forceNew=forceNew, category=category, combined=combined, ptm_median_or_max=ptm_median_or_max, new_gain=new_gain, brent=brent, new_hist=new_hist)
                if ptm_median_or_max == 'max':
                    ds.drift_correct_info['median_pretrig_mean'] = ptm_offset # probably not necessary
    else: # my reduced data
        for ds in self:
            ds.new_gain = new_gain
            if np.sum(ds.good()) < 0.02*ds.nPulses:
                print("Drift correction of channel %d skipped: too few good pulses"%ds.channum)
                continue
            else:
                ptm_offset = sj_drift_correct_ds(ds, forceNew=forceNew, category=category, combined=combined, ptm_median_or_max=ptm_median_or_max, new_gain=new_gain, brent=brent, new_hist=new_hist)
                if ptm_median_or_max == 'max':
                    ds.drift_correct_info['median_pretrig_mean'] = ptm_offset # probably not necessary

    if write2file:
        drift_matrix = {}
        drift_matrix['ptm_median_or_max'] = ptm_median_or_max
        drift_matrix['new_gain'] = new_gain
        for ds in self:
            drift_matrix[ds.channum] = ds.drift_correct_info

        if filename is None:
            from datetime import datetime
            pkl_fname = self.filenames[0].split(os.sep)[-2] + "_drift_dict_" + datetime.now().strftime('%Y%m%d_%H%M%S') + ".pkl"
        else:
            if os.path.isfile(filename):
                raise Exception('Filename exists. Try a different name')
            else:
                pkl_fname = filename
        with open(pkl_fname, "wb") as f:
            pkl.dump(drift_matrix, f)
            print("# drift_correct_info written into %s" %pkl_fname)
            print('# ptm_median_or_max:',ptm_median_or_max)
    else:
        print("drift_correct_info not written into a file")

mass.TESGroup.sj_drift_correct = sj_drift_correct


def sj_drift_correct_ds(self, forceNew=False, category=None, combined=False, ptm_median_or_max='median', new_gain=True, brent=True, new_hist=False):

    """SJL modified original drift_correct
    category is now a dummy argument
    Drift correct using the standard entropy-minimizing algorithm"""
    # doesnt_exist = all(self.p_filt_value_dc[:] == self.p_filt_value[:])
    # if not forceNew:
    #     print("chan %d not drift correction, p_filt_value_dc already populated" % self.channum)
    #     return
    # if category is None:
    #     category = {"calibration": "in"}
    # g = self.cuts.good(**category)
    g = self.good()
    uncorrected = self.p_filt_value[g]
    indicator = self.p_pretrig_mean[g]

    if ptm_median_or_max == 'median':
        # assert self.p_filt_value_dc.attrs["type"] == "ptmean_gain"
        # ptm_offset = self.p_filt_value_dc.attrs["median_pretrig_mean"]
        ptm_offset = np.median(indicator)
    elif ptm_median_or_max == 'max':
        hist, bin_edges = np.histogram(self.p_pretrig_mean[self.cuts.good()], ss_opt_num_bin(self.p_pretrig_mean[self.cuts.good()]))
        smoothed = running_mean_conv(hist, 5, 'same') # arbitrary smoothing factor of 5 (changed from 10)
        bin_centers = (bin_edges[1:]+bin_edges[:-1])/2
        try:
            ptm_offset = fit_gaussian(sigma = 3, tailed=False, plot_it=False, raw_data=True, xd=bin_centers, yd=smoothed, range=[bin_centers[np.argmax(smoothed)]-7, bin_centers[np.argmax(smoothed)]+7])[0][1]
        except (TypeError, RuntimeError):
            print('# gaussian fitting failed for ch %d; argmax will be used instead'%self.channum)
            ptm_offset = bin_centers[np.argmax(smoothed)]

    if combined: # not ready yet, very outdated
        timestamp_mid = (self.p_timestamp[0]+self.p_timestamp[-1])/2
        first_half_low = sp.stats.scoreatpercentile(self.p_filt_value[self.p_timestamp[:]<timestamp_mid], 2)
        first_half_high = sp.stats.scoreatpercentile(self.p_filt_value[self.p_timestamp[:]<timestamp_mid], 98)
        second_half_low = sp.stats.scoreatpercentile(self.p_filt_value[self.p_timestamp[:]>timestamp_mid], 2)
        second_half_high = sp.stats.scoreatpercentile(self.p_filt_value[self.p_timestamp[:]>timestamp_mid], 98)
        drift_corr_param, self.drift_correct_info = \
            mass.core.analysis_algorithms.drift_correct(indicator, uncorrected, limit=min(first_half_high,second_half_high))
        print('chan %d best combined drift correction parameter: %.6f' % (self.channum, drift_corr_param))
    else:
        drift_corr_param, self.drift_correct_info = \
            new_drift_correct(indicator, uncorrected, ptm_offset=ptm_offset, new_gain=new_gain, brent=brent, new_hist=new_hist)
        # print('chan %d best drift correction parameter: %.6f (new_gain=%s), %g (ptm_median_or_max=%s)' % (self.channum, drift_corr_param, new_gain, self.drift_correct_info['median_pretrig_mean'], ptm_median_or_max))
        print('chan %d best drift correction parameter: %s (new_gain=%s), %g (ptm_median_or_max=%s)' % (self.channum, drift_corr_param, new_gain, self.drift_correct_info['median_pretrig_mean'], ptm_median_or_max))

    self.p_filt_value_dc.attrs.update(self.drift_correct_info) # Store in hdf5 file
    # self._apply_drift_correction()

    if new_gain is True:
        self.p_filt_value_dc[:] = self.p_filt_value[:] + (self.p_pretrig_mean[:]-ptm_offset)*self.p_filt_value_dc.attrs["slope"]
    elif new_gain is False:
        gain = 1+(self.p_pretrig_mean[:]-ptm_offset)*self.p_filt_value_dc.attrs["slope"]
        self.p_filt_value_dc[:] = self.p_filt_value[:]*gain
    elif new_gain == 'exp':
        self.p_filt_value_dc[:] = self.p_filt_value[:]*np.exp((self.p_pretrig_mean[:]-ptm_offset)*self.p_filt_value_dc.attrs["slope"])
    elif new_gain == 'exp2':
        if self.p_filt_value_dc.attrs["slope"][0] == 1:
            print('chan %d uses exp'%(self.channum))
            self.p_filt_value_dc[:] = self.p_filt_value[:]*np.exp((self.p_pretrig_mean[:]-ptm_offset)*self.p_filt_value_dc.attrs["slope"][1])
        else:
            self.p_filt_value_dc[:] = (self.p_filt_value[:]**(1-self.p_filt_value_dc.attrs["slope"][0])+(1-self.p_filt_value_dc.attrs["slope"][0])*self.p_filt_value_dc.attrs["slope"][1]*(self.p_pretrig_mean[:]-ptm_offset))**(1/(1-self.p_filt_value_dc.attrs["slope"][0]))
            self.p_filt_value_dc[np.isnan(self.p_filt_value_dc[:])] = 0 # NaN is set to zero (safe choice for calibration in case calibration does not use positive values only)
    elif new_gain == 'exp3': # n=1.1
        self.p_filt_value_dc[:] = ((self.p_filt_value[:])**(-0.1) -0.1*(self.p_pretrig_mean[:]-ptm_offset)*self.p_filt_value_dc.attrs["slope"])**(-10)
        self.p_filt_value_dc[np.isnan(self.p_filt_value_dc[:])] = 0 # NaN is set to zero (safe choice for calibration in case calibration does not use positive values only)
    elif new_gain == 'exp4': # n=1.2
        self.p_filt_value_dc[:] = ((self.p_filt_value[:])**(-0.2) -0.2*(self.p_pretrig_mean[:]-ptm_offset)*self.p_filt_value_dc.attrs["slope"])**(-5)
        self.p_filt_value_dc[np.isnan(self.p_filt_value_dc[:])] = 0 # NaN is set to zero (safe choice for calibration in case calibration does not use positive values only)
    elif new_gain == 'exp5': # n=.9
        self.p_filt_value_dc[:] = ((self.p_filt_value[:])**(0.1) +0.1*(self.p_pretrig_mean[:]-ptm_offset)*self.p_filt_value_dc.attrs["slope"])**(10)
        self.p_filt_value_dc[np.isnan(self.p_filt_value_dc[:])] = 0 # NaN is set to zero (safe choice for calibration in case calibration does not use positive values only)
    self.hdf5_group.file.flush()

    return ptm_offset

mass.MicrocalDataSet.sj_drift_correct_ds = sj_drift_correct_ds


def new_drift_correct(indicator, uncorrected, ptm_offset, new_gain=True, brent=True, limit=None, new_hist=False):
    """Compute a drift correction that minimizes the spectral entropy of
    <uncorrected> (a filtered pulse height vector) after correction.
    The <indicator> vector should be of equal length and should have some
    linear correlation with the pulse gain. Generally it will be the
    pretrigger mean of the pulses, but you can experiment with other choices.

    The entropy will be computed on corrected values only in the range
    [0, <limit>], so <limit> should be set to a characteristic large
    value of <uncorrected>. If <limit> is None (the default), then
    in will be compute as 25% larger than the 99%ile point of
    <uncorrected>

    The model is that the filtered pulse height PH should be scaled by
    (1 + a*PTM) where a is an arbitrary parameter computed here, and
    PTM is the difference between each record's pretrigger mean and the
    median value of all pretrigger means. (Or replace "pretrigger mean"
    with whatever quantity you passed in as <indicator>.)
    """

    # ptm_offset = np.median(indicator) # commented out because it's provided

    indicator = np.array(indicator)
    indicator[:] = indicator[:] - ptm_offset

    if limit is None:
        pct99 = sp.stats.scoreatpercentile(uncorrected, 99)
        limit = 1.25 * pct99

    smoother = mass.analysis_algorithms.HistogramSmoother(0.5, [0, limit])

    n_bin = ss_opt_num_bin(uncorrected)
    # print 'n_bin:', n_bin

    if new_gain is True:
        # drift_corr_param = max([0, sp.optimize.brent(_entropy, (indicator, uncorrected, smoother), brack=[0, 0.1])])
        if brent is True:
            drift_corr_param = sp.optimize.brent(_entropy(new_gain=new_gain, new_hist=new_hist), (indicator, uncorrected, smoother, n_bin), brack=[0, 1])
        else:
            drift_corr_param = sp.optimize.minimize_scalar(_entropy(new_gain=new_gain, new_hist=new_hist), args=(indicator, uncorrected, smoother, n_bin), method='bounded', bounds=(-1.5, 3)).x
    elif new_gain is False:
        if brent is True: # this is the default mass style
            drift_corr_param = sp.optimize.brent(_entropy(new_gain=new_gain, new_hist=new_hist), (indicator, uncorrected, smoother, n_bin), brack=[0, .001])
        else:
            drift_corr_param = sp.optimize.minimize_scalar(_entropy(new_gain=new_gain, new_hist=new_hist), args=(indicator, uncorrected, smoother, n_bin), method='bounded', bounds=(-0.0015, 0.003)).x
    elif new_gain == 'exp':
        if brent is True:
            drift_corr_param = sp.optimize.brent(_entropy(new_gain=new_gain, new_hist=new_hist), (indicator, uncorrected, smoother, n_bin), brack=[0, .001])
        else:
            drift_corr_param = sp.optimize.minimize_scalar(_entropy(new_gain=new_gain, new_hist=new_hist), args=(indicator, uncorrected, smoother, n_bin), method='bounded', bounds=(-0.0015, 0.003)).x
    elif new_gain == 'exp2':
        if brent is True:
            return
            # drift_corr_param = sp.optimize.brent(_entropy2, args=(indicator, uncorrected, smoother, n_bin), brack=[0, .001])
        else:
            # drift_corr_param = sp.optimize.minimize(_entropy2, [1.1, 0.00001], args=(indicator, uncorrected, smoother, n_bin), method='SLSQP', bounds=[(1.01,None), (1e-7, 1e-4)]).x
            drift_corr_param = sp.optimize.minimize(_entropy2(new_hist=new_hist), [1.15, 0.000005], args=(indicator, uncorrected, smoother, n_bin), method='Nelder-Mead').x
            # drift_corr_param = sp.optimize.minimize(_entropy2, [1.1, 0.00001], args=(indicator, uncorrected, smoother, n_bin), method='Newton-CG').x
            if drift_corr_param[0] <= 1:
                drift_corr_param[0] = 1
                drift_corr_param[1] = sp.optimize.minimize_scalar(_entropy(new_gain=new_gain, new_hist=new_hist), args=(indicator, uncorrected, smoother, n_bin), method='bounded', bounds=(-0.0015, 0.003)).x
    elif new_gain == 'exp3':
        if brent is True:
            drift_corr_param = sp.optimize.brent(_entropy(new_gain=new_gain, new_hist=new_hist), args=(indicator, uncorrected, smoother, n_bin), brack=[0, 0.001])
        else:
            drift_corr_param = sp.optimize.minimize_scalar(_entropy(new_gain=new_gain, new_hist=new_hist), args=(indicator, uncorrected, smoother, n_bin), method='bounded', bounds=(-0.0015, 0.003)).x
    elif new_gain == 'exp4':
        if brent is True:
            drift_corr_param = sp.optimize.brent(_entropy(new_gain=new_gain, new_hist=new_hist), args=(indicator, uncorrected, smoother, n_bin), brack=[0, 0.001])
        else:
            drift_corr_param = sp.optimize.minimize_scalar(_entropy(new_gain=new_gain, new_hist=new_hist), args=(indicator, uncorrected, smoother, n_bin), method='bounded', bounds=(-0.0015, 0.003)).x
    elif new_gain == 'exp5':
        if brent is True:
            drift_corr_param = sp.optimize.brent(_entropy(new_gain=new_gain, new_hist=new_hist), args=(indicator, uncorrected, smoother, n_bin), brack=[0, 0.001])
        else:
            drift_corr_param = sp.optimize.minimize_scalar(_entropy(new_gain=new_gain, new_hist=new_hist), args=(indicator, uncorrected, smoother, n_bin), method='bounded', bounds=(-0.0015, 0.003)).x

    drift_correct_info = {'type': 'ptmean_gain',
                          'slope': drift_corr_param,
                          'median_pretrig_mean': ptm_offset}
    return drift_corr_param, drift_correct_info


def _entropy(param, indicator, uncorrected, smoother, n_bin, new_gain=False, new_hist=False):
    indicator = indicator[uncorrected>0]
    uncorrected = uncorrected[uncorrected>0]
    if new_gain is True:
        corrected = uncorrected[:] + indicator[:]*param
    elif new_gain is False:
        corrected = uncorrected[:] * (1+indicator[:]*param)
    elif (new_gain == 'exp') or (new_gain == 'exp2'):
        corrected = uncorrected[:] * np.exp(param*indicator[:])
    elif new_gain == 'exp3': # n=1.1
        corrected = (-0.1*param*indicator[:] + uncorrected[:]**(-0.1))**(-10)
    elif new_gain == 'exp4': # n=1.2
        corrected = (-0.2*param*indicator[:] + uncorrected[:]**(-0.2))**(-5)
    elif new_gain == 'exp5': # n=0.9
        corrected = (0.1*param*indicator[:] + uncorrected[:]**(0.1))**(10)
    if new_hist is True:
        counts, bin_edges_ = np.histogram(corrected, n_bin)
        w = counts > 0
        return -(np.log(counts[w])*counts[w]).sum()
    else:
        hsmooth = smoother(corrected)
        w = hsmooth > 0
        return -(np.log(hsmooth[w])*hsmooth[w]).sum()

def _entropy2(param, indicator, uncorrected, smoother, n_bin, new_hist=False):
    corrected = (uncorrected**(1-param[0])+(1-param[0])*param[1]*indicator)**(1/(1-param[0]))
    corrected = corrected[corrected<1e5]
    corrected = corrected[np.logical_not(np.isnan(corrected))]
    if new_hist is True:
        counts, bin_edges_ = np.histogram(corrected, n_bin)
        w = counts > 0
        return -(np.log(counts[w])*counts[w]).sum()
    else:
        hsmooth = smoother(corrected)
        w = hsmooth > 0
        return -(np.log(hsmooth[w])*hsmooth[w]).sum()


def sj_phase_correct(self, forceNew=False, write2file=False, category=None, filename=None):
    """
    phase correction uses the same algorithm as drift correction
    phase correction information can be saved into pkl file
    """
    for ds in self:
        # try:
        doesnt_exist = all(ds.p_filt_value_phc[:] == 0) or all(ds.p_filt_value_phc[:] == ds.p_filt_value_dc[:])
        if not (forceNew or doesnt_exist):
            print("chan %d not phase corrected, p_filt_value_phc already populated" % ds.channum)
            continue
        if category is None:
            category = {"calibration": "in"}
        g = ds.cuts.good(**category)
        uncorrected = ds.p_filt_value_dc[g]
        indicator = ds.p_filt_phase[g]
        phase_corr_param, ds.phase_correct_info = \
            mass.core.analysis_algorithms.drift_correct(indicator, uncorrected)
        ds.p_filt_value_phc.attrs.update(ds.phase_correct_info) # Store in hdf5 file
        print('chan %d best phase correction parameter: %.6f' % (ds.channum, phase_corr_param))
        # self._apply_drift_correction()

        # assert self.p_filt_value_dc.attrs["type"] == "ptmean_gain"
        ptm_offset = ds.p_filt_value_phc.attrs["median_pretrig_mean"]
        gain = 1+(ds.p_filt_phase[:]-ptm_offset)*ds.p_filt_value_phc.attrs["slope"]
        ds.p_filt_value_phc[:] = ds.p_filt_value_dc[:]*gain
            # self.hdf5_group.file.flush()
        # except:
        #     self.set_chan_bad(ds.channum, "failed drift correct")

    if write2file:
        phase_matrix = {}
        for ds in self:
            phase_matrix[ds.channum] = ds.phase_correct_info

        if filename is None:
            from datetime import datetime
            pkl_fname = self.filenames[0].split(os.sep)[-2] + "_phase_dict_" + datetime.now().strftime('%Y%m%d_%H%M%S') + ".pkl"
        else:
            if os.path.isfile(filename):
                raise Exception('Filename exists. Try a different name')
            else:
                pkl_fname = filename
        with open(pkl_fname, "wb") as f:
            pkl.dump(phase_matrix, f)
            print("# phase_correct_info written into %s" %pkl_fname)
    else:
        print("phase_correct_info not generated")

mass.TESGroup.sj_phase_correct = sj_phase_correct


def sj_import_drift(self, filename, forceNew=True, category=None, use_imported_ptm=True, plot_it=True, ptm_median_or_max=None, new_gain=None):
    '''
    setting forceNew does not do anything yet
    '''
    pkl_fname = filename
    with open(pkl_fname, "rb") as f:
        drift_dict = pkl.load(f)

    try:
        imported_ptm_median_or_max = drift_dict['ptm_median_or_max']
    except KeyError: # to handle old way of saving
        imported_ptm_median_or_max = 'median'
    print('# imported_ptm_median_or_max:', imported_ptm_median_or_max)

    if ptm_median_or_max is None:
        ptm_median_or_max = imported_ptm_median_or_max
    print('# ptm_median_or_max to be used:', ptm_median_or_max)

    try:
        imported_new_gain = drift_dict['new_gain']
    except KeyError: # to handle old way of saving
        imported_new_gain = False
    print('# imported_new_gain:', imported_new_gain)

    if new_gain is None:
        new_gain = imported_new_gain
    print('# new_gain to be used:', new_gain)

    if hasattr(self.first_good_dataset, 'new_gain'):
        if self.first_good_dataset != new_gain:
            raise AttributeError('new_gain value does not match')

    if plot_it is True:
        plt.figure()
        ax1 = plt.subplot(211)
        ax2 = plt.subplot(212, sharex=ax1)

    for ds in self:
        # try:
            # doesnt_exist = all(ds.p_filt_value_dc[:] == 0) or all(ds.p_filt_value_dc[:] == ds.p_filt_value[:])
            # if not (forceNew or doesnt_exist):
            #     print("chan %d not drift correction, p_filt_value_dc already populated" % ds.channum)
            #     continue
        if category is None:
            category = {"calibration": "in"}
        g = ds.cuts.good(**category)
        uncorrected = ds.p_filt_value[g]
        indicator = ds.p_pretrig_mean[g]
        if ptm_median_or_max=='median':
            ptm_offset = np.median(indicator)
        elif ptm_median_or_max=='max':
            hist, bin_edges = np.histogram(ds.p_pretrig_mean[g], ss_opt_num_bin(ds.p_pretrig_mean[g]))
            smoothed = running_mean_conv(hist, 5, 'same') # arbitrary smoothing factor of 5 (previously 10)
            bin_centers = (bin_edges[1:]+bin_edges[:-1])/2
            try:
                ptm_offset = fit_gaussian(sigma = 3, tailed=False, plot_it=False, raw_data=True, xd=bin_centers, yd=smoothed, range=[bin_centers[np.argmax(smoothed)]-7, bin_centers[np.argmax(smoothed)]+7])[0][1]
            except TypeError:
                ptm_offset = bin_centers[np.argmax(smoothed)]
                print('# gaussian fitting failed; argmax will be used')
            except RuntimeError:
                ptm_offset = bin_centers[np.argmax(smoothed)]
                print('# gaussian fitting failed; argmax will be used')


        imported_slope = drift_dict[ds.channum]['slope']
        imported_ptm_offset = drift_dict[ds.channum]['median_pretrig_mean'] # this is median if ptm_median_or_max is 'median', max if 'max'
        if plot_it is True:
            ax1.plot(ds.channum, ptm_offset-imported_ptm_offset, 'o')
            ax2.plot(ds.channum, float(ptm_offset-imported_ptm_offset)/ptm_offset, 'o')

        # Apply correction
        if use_imported_ptm is True:
            if new_gain is True:
                print('chan %d imported best DC parameter slope (new_gain): %.6f, ptm: %.6f' % (ds.channum, imported_slope, imported_ptm_offset))
                ds.p_filt_value_dc[:] = ds.p_filt_value[:] + (ds.p_pretrig_mean[:]-imported_ptm_offset)*imported_slope
            elif new_gain is False:
                gain = 1+(ds.p_pretrig_mean[:]-imported_ptm_offset)*imported_slope
                print('chan %d imported best DC parameter slope: %.6f, ptm: %.6f' % (ds.channum, imported_slope, imported_ptm_offset))
                ds.p_filt_value_dc[:] = ds.p_filt_value[:]*gain
            elif new_gain == 'exp':
                gain = np.exp((ds.p_pretrig_mean[:]-imported_ptm_offset)*imported_slope)
                print('chan %d imported best DC parameter slope: %.6f, ptm: %.6f' % (ds.channum, imported_slope, imported_ptm_offset))
                ds.p_filt_value_dc[:] = ds.p_filt_value[:]*gain
            elif new_gain == 'exp2':
                if imported_slope[0] == 1:
                    gain = np.exp((ds.p_pretrig_mean[:]-imported_ptm_offset)*imported_slope[1])
                else:
                    gain = (imported_slope[0]-1)*(imported_slope[1]*(ds.p_pretrig_mean[:]-imported_ptm_offset))**(1/(imported_slope[0]-1))
                print('chan %d imported best DC parameter slope: %s, ptm: %.6f' % (ds.channum, imported_slope, imported_ptm_offset))
                ds.p_filt_value_dc[:] = ds.p_filt_value[:]*gain
                ds.p_filt_value_dc[np.isnan(ds.p_filt_value_dc[:])] = 0 # NaN is set to zero (safe choice for calibration in case calibration does not use positive values only)
            elif new_gain == 'exp3': # n=1.1
                print('chan %d imported best DC parameter slope: %.6f, ptm: %.6f' % (ds.channum, imported_slope, imported_ptm_offset))
                ds.p_filt_value_dc[:] = ((ds.p_filt_value[:])**(-0.1) -0.1*(ds.p_pretrig_mean[:]-imported_ptm_offset)*imported_slope)**(-10)
                ds.p_filt_value_dc[np.isnan(ds.p_filt_value_dc[:])] = 0
            elif new_gain == 'exp4': # n=1.2
                print('chan %d imported best DC parameter slope: %.6f, ptm: %.6f' % (ds.channum, imported_slope, imported_ptm_offset))
                ds.p_filt_value_dc[:] = ((ds.p_filt_value[:])**(-0.2) -0.2*(ds.p_pretrig_mean[:]-imported_ptm_offset)*imported_slope)**(-5)
                ds.p_filt_value_dc[np.isnan(ds.p_filt_value_dc[:])] = 0
            elif new_gain == 'exp5': # n=1.2
                print('chan %d imported best DC parameter slope: %.6f, ptm: %.6f' % (ds.channum, imported_slope, imported_ptm_offset))
                ds.p_filt_value_dc[:] = ((ds.p_filt_value[:])**(0.1) +0.1*(ds.p_pretrig_mean[:]-imported_ptm_offset)*imported_slope)**(10)
                ds.p_filt_value_dc[np.isnan(ds.p_filt_value_dc[:])] = 0
        else:
            if new_gain is True:
                print('chan %d imported best DC parameter slope (new_gain): %.6f' % (ds.channum, imported_slope))
                ds.p_filt_value_dc[:] = ds.p_filt_value[:] + (ds.p_pretrig_mean[:]-ptm_offset)*imported_slope
            elif new_gain is False:
                gain = 1+(ds.p_pretrig_mean[:]-ptm_offset)*imported_slope
                print('chan %d imported best DC parameter slope: %.6f' % (ds.channum, imported_slope))
            elif new_gain == 'exp':
                gain = np.exp((ds.p_pretrig_mean[:]-ptm_offset)*imported_slope)
                print('chan %d imported best DC parameter slope: %.6f, ptm: %.6f' % (ds.channum, imported_slope, imported_ptm_offset))
                ds.p_filt_value_dc[:] = ds.p_filt_value[:]*gain
            elif new_gain == 'exp2':
                if imported_slope[0] == 1:
                    gain = np.exp((ds.p_pretrig_mean[:]-ptm_offset)*imported_slope[1])
                else:
                    gain = (imported_slope[0]-1)*(imported_slope[1]*(ds.p_pretrig_mean[:]-ptm_offset))**(1/(imported_slope[0]-1))
                print('chan %d imported best DC parameter slope: %s, ptm: %.6f' % (ds.channum, imported_slope, imported_ptm_offset))
                ds.p_filt_value_dc[:] = ds.p_filt_value[:]*gain
            elif new_gain == 'exp3': # dummy script
                gain = np.exp((ds.p_pretrig_mean[:]-ptm_offset)*imported_slope)
                print('chan %d imported best DC parameter slope: %.6f, ptm: %.6f' % (ds.channum, imported_slope, imported_ptm_offset))
                ds.p_filt_value_dc[:] = ds.p_filt_value[:]*gain
            elif new_gain == 'exp4': # dummy script
                gain = np.exp((ds.p_pretrig_mean[:]-ptm_offset)*imported_slope)
                print('chan %d imported best DC parameter slope: %.6f, ptm: %.6f' % (ds.channum, imported_slope, imported_ptm_offset))
                ds.p_filt_value_dc[:] = ds.p_filt_value[:]*gain
            elif new_gain == 'exp5': # dummy script
                gain = np.exp((ds.p_pretrig_mean[:]-ptm_offset)*imported_slope)
                print('chan %d imported best DC parameter slope: %.6f, ptm: %.6f' % (ds.channum, imported_slope, imported_ptm_offset))
                ds.p_filt_value_dc[:] = ds.p_filt_value[:]*gain
        ds.hdf5_group.file.flush()
        # except: # specify exception!!!
        #     self.set_chan_bad(ds.channum, "drift_correct_info does not exist or something failed")

    if plot_it is True:
        ax1.set_title('ptm - imported_ptm')
        ax2.set_title('(ptm - imported_ptm) / (current dataset ptm)')
        ax2.set_xlabel('Channel number')
        plt.draw()

mass.TESGroup.sj_import_drift = sj_import_drift


def import_drift_combined(data, data_cal, forceNew=True, category=None, ptm_median_or_max='median', new_gain=None, combined=True, brent=True, new_hist=False):
    # use ptm of cal
    # 1) calculate slope of cal and sci separately and average the two slopes with weight
    # 2) calculate slope of cal anc sci combined
    for ds in data:
        try:
            if combined is True:
                indicator = np.append(data_cal.channel[ds.channum].p_pretrig_mean[data_cal.channel[ds.channum].good()], ds.p_pretrig_mean[ds.good()])
                if ptm_median_or_max == 'max':
                    hist, bin_edges = np.histogram(indicator, ss_opt_num_bin(indicator))
                    smoothed = running_mean_conv(hist, 5, 'same') # arbitrary smoothing factor of 10
                    bin_centers = (bin_edges[1:]+bin_edges[:-1])/2
                    try:
                        ptm_offset = fit_gaussian(sigma = 3, tailed=False, plot_it=False, raw_data=True, xd=bin_centers, yd=smoothed, range=[bin_centers[np.argmax(smoothed)]-7, bin_centers[np.argmax(smoothed)]+7])[0][1]
                    except TypeError:
                        ptm_offset = bin_centers[np.argmax(smoothed)]
                        print('# gaussian fitting failed; argmax will be used')
                    except RuntimeError:
                        ptm_offset = bin_centers[np.argmax(smoothed)]
                        print('# gaussian fitting failed; argmax will be used')
                    indicator = indicator - ptm_offset
                elif ptm_median_or_max == 'median':
                    indicator = indicator - np.median(indicator)
                uncorrected = np.append(data_cal.channel[ds.channum].p_filt_value[data_cal.channel[ds.channum].good()], ds.p_filt_value[ds.good()])
            else:
                indicator = ds.p_pretrig_mean[ds.good()][:]
                # print indicator[:5]
                if ptm_median_or_max == 'max':
                    hist, bin_edges = np.histogram(indicator, ss_opt_num_bin(indicator))
                    smoothed = running_mean_conv(hist, 5, 'same') # arbitrary smoothing factor of 10
                    bin_centers = (bin_edges[1:]+bin_edges[:-1])/2
                    try:
                        ptm_offset = fit_gaussian(sigma = 3, tailed=False, plot_it=False, raw_data=True, xd=bin_centers, yd=smoothed, range=[bin_centers[np.argmax(smoothed)]-7, bin_centers[np.argmax(smoothed)]+7])[0][1]
                    except TypeError:
                        ptm_offset = bin_centers[np.argmax(smoothed)]
                        print('# TypeError; gaussian fitting failed; argmax will be used, ptm_offset:', ptm_offset)
                    except RuntimeError:
                        ptm_offset = bin_centers[np.argmax(smoothed)]
                        print('# RuntimeError; gaussian fitting failed; argmax will be used, ptm_offset:', ptm_offset)
                    indicator[:] = indicator[:] - ptm_offset
                elif ptm_median_or_max == 'median':
                    indicator[:] = indicator[:] - np.median(indicator)
                uncorrected = ds.p_filt_value[ds.good()][:]
            # print indicator[:5]
            # print 'uncorrected', uncorrected[:3]
            drift_corr_param, drift_correct_info = new_drift_correct(indicator, uncorrected, ptm_offset=0, new_gain=new_gain, brent=brent, new_hist=new_hist)
            # print 'indicator', indicator[:3]
            # print 'uncorrected', uncorrected[:3]
            # print drift_correct_info
            drift_correct_info['median_pretrig_mean'] = data_cal.channel[ds.channum].drift_correct_info['median_pretrig_mean']
            # drift_correct_info['slope'] = np.mean([data_cal.channel[ds.channum].drift_correct_info['slope'], drift_corr_param]) # average
            # drift_correct_info['slope'] = data_cal.channel[ds.channum].drift_correct_info['slope'] # cal slope
            ds.p_filt_value_dc.attrs.update(drift_correct_info)
            print('ch %d drift correction: slope_cal: %g, slope: %g'%(ds.channum, data_cal.channel[ds.channum].drift_correct_info['slope'], drift_correct_info['slope']))

            if new_gain is True:
                ds.p_filt_value_dc[:] = ds.p_filt_value[:] + (ds.p_pretrig_mean[:]-ds.p_filt_value_dc.attrs["median_pretrig_mean"])*ds.p_filt_value_dc.attrs["slope"]
                # print 'p_filt_value', ds.p_filt_value[:3] #######
                # print 'p_filt_value_dc', ds.p_filt_value_dc[:3] #######
            elif new_gain is False:
                gain = 1+(ds.p_pretrig_mean[:]-ds.p_filt_value_dc.attrs["median_pretrig_mean"])*ds.p_filt_value_dc.attrs["slope"]
                ds.p_filt_value_dc[:] = ds.p_filt_value[:]*gain
            elif new_gain == 'exp':
                gain = np.exp((ds.p_pretrig_mean[:]-ds.p_filt_value_dc.attrs["median_pretrig_mean"])*ds.p_filt_value_dc.attrs["slope"])
                ds.p_filt_value_dc[:] = ds.p_filt_value[:]*gain
            elif new_gain == 'exp2': # needs to modify
                gain = np.exp((ds.p_pretrig_mean[:]-ds.p_filt_value_dc.attrs["median_pretrig_mean"])*ds.p_filt_value_dc.attrs["slope"])
                ds.p_filt_value_dc[:] = ds.p_filt_value[:]*gain
            ds.hdf5_group.file.flush()
        except IndexError as err:
            print('ch %d does not coexist in data and data_cal'%ds.channum)
            continue

    return data


def sj_check_dc_phc(self, channum=None, cut=True, attr=None, color=None, cmap='rainbow_r'):
    """plot before/after drift and phase correction (cut not applied)
    example usage: data.sj_check_dc_phc()
    data.sj_check_dc_phc(cut=False) for plotting all traces
    data.sj_check_dc_phc(3)
    ds.sj_check_dc_phc()
    data.sj_check_dc_phc(cut={category_key:category_value})
    if you want to use mono color, color='black' will do
    """
    if attr is None:
        attr = 'p_filt_value'

    if type(self) in [mass.core.channel_group.TESGroup, mass.core.channel_group_hdf5_only.TESGroupHDF5]:

        if channum is None:
            ds = self.first_good_dataset
        elif channum in self.channel:
            ds = self.channel[channum]
        else:
            print("Channel number not available")
            return
        if cut is True:
            p_pretrig_mean = ds.p_pretrig_mean[ds.good()]
            p_filt_value = getattr(ds, attr)[ds.good()]
            p_filt_value_dc = ds.p_filt_value_dc[ds.good()]
            p_filt_phase = ds.p_filt_phase[ds.good()]
            p_filt_value_phc = ds.p_filt_value_phc[ds.good()]
            p_timestamp = ds.p_timestamp[ds.good()]
        elif cut is False:
            p_pretrig_mean = ds.p_pretrig_mean[:]
            p_filt_value = getattr(ds, attr)[:]
            p_filt_value_dc = ds.p_filt_value_dc[:]
            p_filt_phase = ds.p_filt_phase[:]
            p_filt_value_phc = ds.p_filt_value_phc[:]
            p_timestamp = ds.p_timestamp[:]
        else:
            p_pretrig_mean = ds.p_pretrig_mean[ds.good(**cut)]
            p_filt_value = getattr(ds, attr)[ds.good(**cut)]
            p_filt_value_dc = ds.p_filt_value_dc[ds.good(**cut)]
            p_filt_phase = ds.p_filt_phase[ds.good(**cut)]
            p_filt_value_phc = ds.p_filt_value_phc[ds.good(**cut)]
            p_timestamp = ds.p_timestamp[ds.good(**cut)]
        plt.figure(figsize=(12,9))
        ax_master1 = plt.subplot(221)
        plt.suptitle("Chan %d" % ds.channum)
        plt.title("Before dc (%s)"%attr)
        if color is not None:
            plt.scatter(p_pretrig_mean, p_filt_value, s=1, c=color)#, label="p_filt_value")
        else:
            plt.scatter(p_pretrig_mean, p_filt_value, s=1, c=p_timestamp, cmap=cmap)
        # plt.scatter(p_pretrig_mean, p_filt_value, c=ds.p_timestamp[ds.good()], s=4, linewidths=0.5, edgecolors='black', cmap='rainbow_r', alpha=0.75)
        #plt.legend(loc='best')
        plt.grid()
        plt.subplot(223, sharex=ax_master1, sharey=ax_master1)
        if color is not None:
            plt.scatter(p_pretrig_mean, p_filt_value_dc, s=1, c=color)#, label="p_filt_value_dc")
        else:
            plt.scatter(p_pretrig_mean, p_filt_value_dc, s=1, c=p_timestamp, cmap=cmap)
        plt.title("After dc")
        #plt.legend(loc='best')
        plt.grid()
        plt.xlabel("Pretrigger mean")
        ax_master2 = plt.subplot(222)
        if color is not None:
            plt.scatter(p_filt_phase, p_filt_value_dc, s=1, c=color)#, label="p_filt_value_dc")
        else:
            plt.scatter(p_filt_phase, p_filt_value_dc, s=1, c=p_timestamp, cmap=cmap)#, label="p_filt_value_dc")
        plt.title("After dc, but before phc")
        #plt.legend(loc='best')
        plt.grid()
        plt.subplot(224, sharex=ax_master2, sharey=ax_master2)
        if color is not None:
            plt.scatter(p_filt_phase, p_filt_value_phc, s=1, c=color)
        else:
            plt.scatter(p_filt_phase, p_filt_value_phc, s=1, c=p_timestamp, cmap=cmap)#, label="p_filt_value_phc")
        #plt.legend(loc='best')
        plt.grid()
        plt.title("After dc and phc")
        plt.xlabel("Phase")
    else:
        if cut is True:
            p_pretrig_mean = self.p_pretrig_mean[self.good()]
            p_filt_value = getattr(ds, attr)[self.good()]
            p_filt_value_dc = self.p_filt_value_dc[self.good()]
            p_filt_phase = self.p_filt_phase[self.good()]
            p_filt_value_phc = self.p_filt_value_phc[self.good()]
        elif cut is False:
            p_pretrig_mean = self.p_pretrig_mean
            p_filt_value = getattr(ds, attr)
            p_filt_value_dc = self.p_filt_value_dc
            p_filt_phase = self.p_filt_phase
            p_filt_value_phc = self.p_filt_value_phc
        else:
            p_pretrig_mean = self.p_pretrig_mean[self.good(**cut)]
            p_filt_value = getattr(ds, attr)[self.good(**cut)]
            p_filt_value_dc = self.p_filt_value_dc[self.good(**cut)]
            p_filt_phase = self.p_filt_phase[self.good(**cut)]
            p_filt_value_phc = self.p_filt_value_phc[self.good(**cut)]
        plt.figure()
        ax_master1 = plt.subplot(221)
        plt.suptitle("Chan %d" % self.channum)
        plt.title("Before dc (%s)"%attr)
        plt.plot(p_pretrig_mean, p_filt_value, '.')#, label="p_filt_value")
        #plt.legend(loc='best')
        plt.grid()
        plt.subplot(223, sharex=ax_master1, sharey=ax_master1)
        plt.plot(p_pretrig_mean, p_filt_value_dc, '.')#, label="p_filt_value_dc")
        plt.title("After dc")
        #plt.legend(loc='best')
        plt.grid()
        plt.xlabel("Pretrigger mean")
        ax_master2 = plt.subplot(222)
        plt.plot(p_filt_phase, p_filt_value_dc, '.r')#, label="p_filt_value_dc")
        plt.title("Before phc")
        #plt.legend(loc='best')
        plt.grid()
        plt.subplot(224, sharex=ax_master2, sharey=ax_master2)
        plt.plot(p_filt_phase, p_filt_value_phc, '.r')#, label="p_filt_value_phc")
        #plt.legend(loc='best')
        plt.grid()
        plt.title("After phc")
        plt.xlabel("Phase")

mass.TESGroup.sj_check_dc_phc = sj_check_dc_phc
mass.MicrocalDataSet.sj_check_dc_phc = sj_check_dc_phc


def sj_quick_spectra(self, chan_list=None, quantity="p_filt_value_dc", num_bin=None, energy_scale=None, mono=None, linearity=1.1, log=True, ulim=None, category=None):
    """
    sj: modified from plot_raw_spectra()
    example usage: data.sj_plot_quick_spectra(); data.sj_plot_quick_spectra(11);
    data.sj_plot_quick_spectra([1,3,7]); data.sj_plot_quick_spectra(5, "p_filt_value_dc");
    data.sj_plot_quick_spectra(5, "p_filt_value_dc");
    """
    if chan_list is None:
        chan_list = [self.first_good_dataset.channum]
    elif isinstance(chan_list, int):
        if chan_list%2 == 0:
            print('Do not try an even number')
            return
        else:
            chan_list = [chan_list]
    if sum(self.first_good_dataset.p_filt_value_dc) == 0:
        print('p_filt_value_dc is not availbe. Plotting p_filt_value instead..')
        quantity="p_filt_value"
        if sum(self.first_good_dataset.p_filt_value) == 0:
            print('Actually, p_filt_value is not availbe either. Plotting p_peak_value instead..')
            quantity="p_peak_value"
    fig, ax = plt.subplots(1,2, figsize=(16,6))
    fig.subplots_adjust(left=0.05, right=0.95, wspace=0.1)
    if category is None:
        category = {"calibration": "in"}
    for i, chan in enumerate(chan_list):
        ds = self.channel[chan]
        print(ds)
        # arb_gain = max(ds.average_pulse)
        # arb_gain_type = 'avg pls height'
        # if arb_gain == 0:
        #     print('Avg pulse does not seem to exist. Scaling done by the median..')
        #     # arb_gain = np.median(eval("ds.%s[ds.cuts.good()]"%quantity))
        arb_gain = np.median(ds.__dict__[quantity][ds.cuts.good(**category)])
        arb_gain_type = 'p_peak_value median'
        # num_bin = ss_opt_num_bin(eval("ds.%s[ds.cuts.good(**category)]"%quantity))
        if num_bin is None:
            num_bin = ss_opt_num_bin(ds.__dict__[quantity][ds.cuts.good(**category)])
        # plt.subplot(121)
        # _ = plt.hist(eval("ds.%s[ds.cuts.good(**category)]"%quantity), num_bin,
        #             [0, 2.5*arb_gain], alpha=0.33, log=log)
        # c1, b1 = np.histogram(eval("ds.%s[ds.cuts.good(**category)]"%quantity), num_bin, [0, 2.5*arb_gain])
        if ulim is None:
            ulim = min(2.5*arb_gain, 1.05*max(ds.__dict__[quantity][ds.cuts.good(**category)]))
        c1, b1 = np.histogram(ds.__dict__[quantity][ds.cuts.good(**category)], num_bin, [0, ulim])
        if log is True:
            ax[0].semilogy((b1[1:]+b1[:-1])/2, c1, alpha=0.33)
        else:
            ax[0].plot((b1[1:]+b1[:-1])/2, c1, alpha=0.33)
        ax[0].set_xlabel(quantity)
        # plt.subplot(122)
        #arb_gain = np.median(eval("ds.%s[ds.cuts.good(**category)]"%quantity))
        if energy_scale is None:
            # _ = plt.hist(eval("ds.%s[ds.cuts.good(**category)]"%quantity)/arb_gain, num_bin,
            #         [0, 3.0], alpha=0.33, log=log)
            # c2, b2 = np.histogram(eval("ds.%s[ds.cuts.good(**category)]"%quantity)/arb_gain, num_bin, [0, 3.0])
            c2, b2 = np.histogram(ds.__dict__[quantity][ds.cuts.good(**category)]/arb_gain, num_bin, [0, ulim/arb_gain])
            if log is True:
                ax[1].semilogy((b2[1:]+b2[:-1])/2, c2, alpha=0.33)
            else:
                ax[1].plot((b2[1:]+b2[:-1])/2, c2, alpha=0.33)
            ax[1].set_xlabel("%s normalized by %s"%(quantity,arb_gain_type))
        else:
            # _ = plt.hist((eval("ds.%s[ds.cuts.good(**category)]"%quantity)/arb_gain)**linearity*energy_scale, num_bin,
            #         [0, 3.0*energy_scale], alpha=0.33, log=log)
            # c2, b2 = np.histogram((eval("ds.%s[ds.cuts.good(**category)]"%quantity)/arb_gain)**linearity*energy_scale, num_bin, [0, 3.0*energy_scale])
            c2, b2 = np.histogram((ds.__dict__[quantity][ds.cuts.good(**category)]/arb_gain)**linearity*energy_scale, num_bin, [0, (ulim/arb_gain)**linearity*energy_scale])
            if log is True:
                ax[1].semilogy((b2[1:]+b2[:-1])/2, c2, alpha=0.33)
            else:
                ax[1].plot((b2[1:]+b2[:-1])/2, c2, alpha=0.33)
            ax[1].set_xlabel("Energy (eV)")
            common_lines = [277, 395, 525, 677]
            # plt.sj_vlines(common_lines)
            ylim = ax[1].get_ylim()
            ax[1].vlines(common_lines, ylim[0], ylim[1])
            if mono is not None:
                mono_lines = [mono, 2*mono, 3*mono]
                ax[1].vlines(mono_lines, ylim[0], ylim[1], colors='red')
            ax[1].set_ylim(ylim)
        # plt.subplot(121)
        # plt.xlabel(quantity)
    fig.suptitle("Channels: %s"%chan_list)

mass.TESGroup.sj_quick_spectra = sj_quick_spectra


def sj_quick_2d(self, chan_list=None, y="p_filt_value_dc", x="p_timestamp", category=None):
    """
    example usage: data.sj_quick_2d(); data.sj_quick_2d('p_filt_value');
    data.sj_quick_2d(chan_list=[1,3,7])
    category=None (good), 'all' (raw), or a custom category dictionary
    """
    if chan_list is None:
        chan_list = [self.first_good_dataset.channum]
    elif isinstance(chan_list, int):
        if chan_list%2 == 0:
            print('Do not try an even number')
            return
        else:
            chan_list = [chan_list]
    elif isinstance(chan_list, str) and (y=='p_filt_value_dc'):  # the case of sj_quick_2d('p_filt_value')
        y = chan_list
        chan_list = [1]

    fig, ax = plt.subplots()
    # fig.subplots_adjust(left=0.05, right=0.95, wspace=0.1)
    for i, chan in enumerate(chan_list):
        ds = self.channel[chan]
        print(ds)
        if category == 'all':
            ax.plot(ds.__dict__[x][:], ds.__dict__[y][:], '.')
        else:
            if category is None:
                category = {"calibration": "in"}
            ax.plot(ds.__dict__[x][ds.cuts.good(**category)], ds.__dict__[y][ds.cuts.good(**category)], '.')
    ax.set_xlabel(x)
    ax.set_ylabel(y)
    ax.set_title("Channels: %s"%chan_list)

mass.TESGroup.sj_quick_2d = sj_quick_2d


def sj_calibrate_ds(self, attr='p_filt_value_dc', cal_lines=None, name_ext="", size_related_to_energy_resolution=range(10,25),
                  fit_range_ev=10, excl=(), neighbor_limit=None, plot_on_fail=True,
                  bin_size_ev=0.2, category=None, forceNew=False, maxacc=0.1, nextra=3, line_intensity=None, linearity=1.1, auto_inclusion=1, strong_line_indices=[], manual_assign=None, verbosity=4, cal_method='spl', quartic_only=False, thru_origin=True):
    """
    works for ds, not data. uf.sj_calibrate_ds(data_list, ...) works as well.
    fit_range_ev, excl, plot_on_fail, bin_size_ev not being used yet
    nextra only appies to how many peak positions to choose in __find_opt_assignment
    nextra = 0 seems to work better if size_related_to_energy_resolution is chosen properly (fwhm in raw unit)
    it can be a fixed number, say 20, but it might be better to vary with channel.
    E.g. get filt_value_dc value for a given energy and also calculate fwhm, then we can get a proper value
    bin_opt not ready yet
    ###### all the spl related lines commented out (# at the beginning of corresponding lines)
    neighbor_limit is used when finding local maxima.
    It's been a long time, so I'm not sure. I thought if this value should be decreased to be less strict,
    but it seems it's the opposite: I mean, the larger this value, the less strict
    Needs to be checked.
    """
    #name_ext = ""
    #excl = ()
    dtype=np.float64
    n_cal_lines = len(cal_lines)
    calname = attr+name_ext
    if category is None:
        category = {"calibration": "in"}
    if isinstance(size_related_to_energy_resolution, int) or isinstance(size_related_to_energy_resolution, float):
        size_related_to_energy_resolution = [size_related_to_energy_resolution]

    try:
        # cal = young.EnergyCalibration(size_related_to_energy_resolution,
        #                             fit_range_ev, excl, plot_on_fail,
        #                             bin_size_ev=bin_size_ev, maxacc=maxacc, nextra=nextra)
        # #print(getattr(self, attr)[self.cuts.good(**category)])
        # cal.fit(getattr(self, attr)[self.cuts.good(**category)], cal_lines)
        # self.calibration[calname] = cal

        # final result will depend on bin size below, so using a proper bin size will be important
        #c, b = np.histogram(getattr(self, attr)[self.cuts.good(**category)], 1000.)
        # use ss_opt_num_bin to find optimal bin size
        # if category is None:
        #     c, b = np.histogram(getattr(self, attr)[:], ss_opt_num_bin(getattr(self, attr)[:])) # forgot why [:] was used, not good()
        # else:
        if type(self) is list:
            combined_data = np.array([getattr(data, attr)[data.cuts.good(**category)] for data in self]).flatten()
            c, b = np.histogram(combined_data, ss_opt_num_bin(combined_data))
        else:
            c, b = np.histogram(getattr(self, attr)[self.cuts.good(**category)], ss_opt_num_bin(getattr(self, attr)[self.cuts.good(**category)]))
        b_centers = (b[1:]+b[:-1])*0.5
        #print ss_opt_num_bin(getattr(self, attr)[self.cuts.good(**category)])

        #colors = ['r', 'y', 'g', 'c', 'b', 'm']
        colors = [cm.rainbow_r(x) for x in np.linspace(0,1.,len(size_related_to_energy_resolution))]

        if n_cal_lines == 1:
            num_peaks_to_consider = 1
        else:
            num_peaks_to_consider = n_cal_lines + nextra
        num_sizes = len(size_related_to_energy_resolution)

        # like to modify below, like something like in https://docs.python.org/2/tutorial/datastructures.html#nested-list-comprehensions
        peak_positions = np.zeros((num_sizes, num_peaks_to_consider), dtype=dtype)
        peak_intensity = np.zeros((num_sizes, num_peaks_to_consider), dtype=dtype)
        opt_assignment = np.zeros((num_sizes, n_cal_lines), dtype=dtype)
        opt_assignment_intensity = np.zeros((num_sizes, n_cal_lines), dtype=dtype)
        refined_peak_positions = np.zeros((num_sizes, n_cal_lines), dtype=dtype)
        acc_est = np.ones(num_sizes, dtype=dtype)
        if n_cal_lines == 2:
            residual_2nd_rms = np.zeros(num_sizes, dtype=dtype) * 1000
            popt2 = np.zeros((num_sizes,2), dtype=dtype)
            # if thru_origin is True:
            #     popt2 = np.zeros((num_sizes,2), dtype=dtype)
            # else:
            #     popt2 = np.zeros((num_sizes,2), dtype=dtype)  # linear fit
        elif n_cal_lines == 3:
            residual_3rd_rms = np.zeros(num_sizes, dtype=dtype) * 1000
            residual_2nd_rms = np.zeros(num_sizes, dtype=dtype) * 1000
            popt2 = np.zeros((num_sizes,2), dtype=dtype)
            popt3 = np.zeros((num_sizes,3), dtype=dtype)
            # if thru_origin is True:
            #     popt2 = np.zeros((num_sizes,2), dtype=dtype)
            #     popt3 = np.zeros((num_sizes,3), dtype=dtype)
            # else:
            #     popt2 = np.zeros((num_sizes,2), dtype=dtype)  # linear fit
            #     popt3 = np.zeros((num_sizes,3), dtype=dtype)  # 2nd order fit
        elif n_cal_lines >= 4:
            residual_4th_rms = np.zeros(num_sizes, dtype=dtype) * 1000
            residual_5th_rms = np.zeros(num_sizes, dtype=dtype) * 1000
        #    residual_spl_rms = np.zeros(len(size_related_to_energy_resolution), dtype=float)
            popt4 = np.zeros((num_sizes,4), dtype=dtype)
            popt5 = np.zeros((num_sizes,5), dtype=dtype)
            # if thru_origin is True:
            #     popt4 = np.zeros((num_sizes,4), dtype=dtype)
            #     popt5 = np.zeros((num_sizes,5), dtype=dtype)
            # else:
            #     popt4 = np.zeros((num_sizes,4), dtype=dtype)  # 3rd order fit
            #     popt5 = np.zeros((num_sizes,5), dtype=dtype)  # 4th order fit
        #pcov4 = np.zeros(len(size_related_to_energy_resolution), dtype=float)
        #p_energy = np.zeros((len(size_related_to_energy_resolution), len(self.p_energy)))
        # if line_intensity is None:
        #     line_intensity = np.ones(n_cal_lines, dtype=float)
        if type(self) is list:  # working on a list of datasets
            print("*** Working on chan %d ***" %self[0].channum)
        else:
            print("*** Working on chan %d ***" %self.channum)
        if n_cal_lines == 1:
            if type(self) is list:  # working on a list of datasets
                attr_double = np.array([np.hstack([np.zeros(0), getattr(data, attr)[:]]) for data in self]).flatten() # need to change type to double?
            else:
                attr_double = np.hstack([np.zeros(0), getattr(self, attr)[:]]) # need to change type to double?
            kde = sm.nonparametric.KDEUnivariate(attr_double[np.logical_and((attr_double>=100), (attr_double<50000))])
            kde.fit(bw=size_related_to_energy_resolution[0])
            x = kde.support
            y = kde.density
            # plt.figure()
            # plt.plot(x, y)
            max_pos = x[np.argmax(y)]
            # plt.sj_vlines(max_pos)
            try:
                popt, pcov, perr = fit_gaussian(xd=x, yd=y, range=[max_pos-500, max_pos+500], plot_it=False, raw_data=True, sigma=20, tailed=False)
                peak_posi = popt[1]
            except RuntimeError as e:
                print('Fitting failed, simple max being used')
                peak_posi = max_pos
        else:
            for i, size_related_to_e_res in enumerate(size_related_to_energy_resolution):
                # cal = young.EnergyCalibration(size_related_to_e_res,
                #                             fit_range_ev, excl, plot_on_fail=False,
                #                             bin_size_ev=bin_size_ev, maxacc=maxacc, nextra=nextra)
                if type(self) is list:  # working on a list of datasets
                    attr_double = np.array([np.hstack([np.zeros(0), getattr(data, attr)[:]]) for data in self]).flatten() # need to change type to double?
                else:
                    attr_double = np.hstack([np.zeros(0), getattr(self, attr)[:]]) # need to change type to double?
                # attr_double = np.hstack([np.zeros(0), getattr(self, attr)[self.cuts.good(**category)]]) # need to change type to double?
                if plot_on_fail:
                    plt.figure();plt.plot(b_centers[b_centers>0], c[b_centers>0], 'k')
                    #plt.figure();plt.plot(b_centers, c, 'k')
                    #plt.title("Channel %d, size_related_to_energy_resolution = %g"%(self.channum, size_related_to_e_res))
                    #kde = sm.nonparametric.KDEUnivariate(attr_double)
                    kde = sm.nonparametric.KDEUnivariate(attr_double[attr_double>=0])
                    kde.fit(bw=size_related_to_e_res)
                    x = kde.support
                    y = kde.density
                    plt.plot(x,y*max(c)/max(y), color=colors[i])

                # full_pp_temp, full_pp_intensity_temp = cal.sj_find_local_maxima(getattr(self, attr)[self.cuts.good(**category)], neighbor_limit=neighbor_limit)
                # if category is None:
                #     full_pp_temp, full_pp_intensity_temp = sj_find_local_maxima(getattr(self, attr)[:], neighbor_limit=neighbor_limit, size_related_to_energy_resolution=size_related_to_e_res)
                # else:
                if type(self) is list:  # working on a list of datasets
                    combined_data = np.array([getattr(data, attr)[data.cuts.good(**category)] for data in self]).flatten()
                    pp_temp, pp_intensity_temp = sj_find_local_maxima(combined_data, neighbor_limit=neighbor_limit, size_related_to_energy_resolution=size_related_to_e_res, n_cal_lines=n_cal_lines, nextra=nextra)
                else:
                    pp_temp, pp_intensity_temp = sj_find_local_maxima(getattr(self, attr)[self.cuts.good(**category)], neighbor_limit=neighbor_limit, size_related_to_energy_resolution=size_related_to_e_res, n_cal_lines=n_cal_lines, nextra=nextra)



                num_peaks_to_examine = min(num_peaks_to_consider,len(pp_temp)) # this is needed because sometimes number of local maxima is smaller than num_peaks_to_consider
                peak_positions[i][:num_peaks_to_examine] = pp_temp[:]
                peak_intensity[i][:num_peaks_to_examine] = pp_intensity_temp[:]
                temp_p = peak_positions[i]
                temp_i = peak_intensity[i]
                # if len(full_pp_temp) < num_peaks_to_consider:
                #     print "not enough number of peaks in channel %d" %self.channum
                #     break
                    # temp_p = peak_positions[i][:-(num_peaks_to_consider-len(full_pp_temp))]
                    # temp_i = peak_intensity[i][:-(num_peaks_to_consider-len(full_pp_temp))]
                if line_intensity is None:
                    opt_assignment[i], acc_est[i] = sj_find_opt_assignment(temp_p, temp_i, cal_lines, line_intensity, linearity=linearity, auto_inclusion=auto_inclusion, strong_line_indices=strong_line_indices, verbosity=verbosity, include_origin=thru_origin)
                    if verbosity > 3:
                        print(opt_assignment[i])
                    for j, oa in enumerate(opt_assignment[i]):
                        opt_assignment_intensity[i][j] = pp_intensity_temp[pp_temp==oa]
                else:
                    opt_assignment[i], opt_assignment_intensity[i], acc_est[i] = sj_find_opt_assignment(temp_p, temp_i, cal_lines, line_intensity, linearity=linearity, auto_inclusion=auto_inclusion, strong_line_indices=strong_line_indices, verbosity=verbosity)

                # in order to check if curvature is about right
                try:
                    popt_lin, pcov_lin = curve_fit((lambda x, a, n: a*x**n), np.insert(opt_assignment[i], 0, .0), np.insert(cal_lines, 0, 0.0))
                except RuntimeError:
                    popt_lin = [0, 0]


                #opt_assignment[i], opt_assignment_intensity[i] = sj_find_opt_assignment(peak_positions[i], peak_intensity[i], cal_lines, line_intensity)
                #peak_positions[i][:min(num_peaks_to_consider,len(full_pp_temp))] = full_pp_temp

                # below seems equivalent to above
                # peak_positions[i] = (cal._EnergyCalibration__find_local_maxima(getattr(self, attr)[self.cuts.good(**category)]))[:num_peaks_to_consider]

                # try:
                #     opt_assignment[i] = cal._EnergyCalibration__find_opt_assignment(peak_positions[i], cal_lines)
                # except:
                #     opt_assignment[i] = peak_positions[i][:n_cal_lines]

                #cal.fit(getattr(self, attr)[self.cuts.good(**category)], cal_lines)
                #self.p_energy[:] = cal.ph2energy(getattr(self, attr)) # if below line fails, try using this line
                #self.p_energy = cal.ph2energy(getattr(self, attr))
                #refined_peak_positions[i] = cal.refined_peak_positions
                # self.calibration[calname] = cal # commented out with mass 0.5.1
                # cal.histograms, cal.complex_fitters, cal.ph2energy are the only attrs set in fit()
                # cal.histograms used only in time_drift_correct_polynomial() in channel.py
                # cal.complex_fitters only used in young.py
                # cal.ph2energy only used in convert_to_energy() in channel.py
                if plot_on_fail:  # supported only for the dataset case
                    #plt.sj_vlines([peak_positions[i][k] for k in range(n_cal_lines+cal.nextra)],'--'+colors[i], label='sj')
                    #plt.sj_vlines(cal.refined_peak_positions,'--k', label='young') # will not use
                    #plt.sj_vlines([opt_assignment[i][k] for k in range(n_cal_lines)], '--'+colors[i], label='sj')
                    #plt.sj_vlines([peak_positions[i][k] for k in range(n_cal_lines+cal.nextra)],'--k', label='full')
                    plt.sj_vlines([peak_positions[i][k] for k in range(n_cal_lines+nextra)],'--k', label='full')
                    plt.sj_vlines([opt_assignment[i][k] for k in range(n_cal_lines)], '--', label='opt', color=colors[i])
                    plt.legend(fontsize='small')
                    plt.title("Ch %d, size_related_to_energy_resolution = %g\nacc_est=%.5f"%(self.channum, size_related_to_e_res, acc_est[i]), fontsize='small')
                    if (acc_est[i] > maxacc) or (popt_lin[1] < 1): # before, maxacc was hard coded with 0.1
                        plt.text(0.5,0.5, 'failed assignment', transform=plt.gca().transAxes)
                        plt.figure()
                        #plt.plot([0.]+[opt_assignment[i][k] for k in range(n_cal_lines)], np.insert(cal_lines, 0, 0.0), 'o-')
                        plt.plot(np.insert(opt_assignment[i], 0, .0), np.insert(cal_lines,0,0.0),'o-')
                        plt.title("Ch %d, size_related_to_energy_resolution = %g"%(self.channum, size_related_to_e_res), fontsize='small')
                        plt.xlabel('Raw pulse height')
                        plt.ylabel('Energy (eV)')
                        continue
                if ((acc_est[i] > maxacc) or (popt_lin[1] < 1)) and (verbosity > 1): # before, it was hard coded with 0.1
                    print("acc_est > maxacc or weird gain curve (pow < 1)")
                    continue
                # p_energy[i] = cal.ph2energy(getattr(self, attr)) # instead of using self.convert_to_energy(attr)
                # ph2energy = mass.mathstat.interpolate.CubicSpline(interp_peak_positions, energy) # commented out

                # young.py
                #
                # opt_assignment = self.__find_opt_assignment(peak_positions, cal_lines)
                # def __find_opt_assignment(self, peak_positions, cal_lines):
                #     name_e, e_e = zip(*sorted([[element, STANDARD_FEATURES.get(element, element)] for element in cal_lines],
                #                          key=operator.itemgetter(1)))
                #     self.elements = name_e
                # def peak_energies(self):  # this is just energies given by user
                #     return [STANDARD_FEATURES.get(el, el) for el in self.elements]
                # def fit
                #     e_e = self.peak_energies # same as energies given by user
                #     ev_spl = self.__build_calibration_spline(e_e, opt_assignment)
                #     app_slope = ev_spl(e_e, 1) # this means first derivative
                # (def __build_calibration_spline(self, pht, energy):
                #     interp_peak_positions = [.0] + pht
                #     energy = [.0] + energy
                #     ph2energy = mass.mathstat.interpolate.CubicSpline(interp_peak_positions, energy)
                #     return ph2energy)

                x = np.insert(opt_assignment[i], 0, 0.0)
                y = np.insert(cal_lines, 0, 0.0)
                if n_cal_lines == 2:
                    if thru_origin is True:
                        popt2[i], pcov2 = curve_fit(ph2e_poly2nd_thru_origin, x, y, p0=[0.1, 0.01], bounds=([-np.inf, 0], [np.inf, np.inf]))
                        residual_2nd = ph2e_poly2nd_thru_origin(opt_assignment[i], *popt2[i]) - cal_lines
                    else:
                        popt2[i], pcov2 = np.polyfit(x, y, 1, cov=True)  # linear fit
                        residual_2nd = np.polyval(popt2[i], opt_assignment[i]) - cal_lines
                    residual_2nd_rms[i] = np.sqrt(sum(np.square(residual_2nd))/n_cal_lines)
                    if verbosity >= 3:
                        print('popt2:', popt2[i], 'pcov2:', pcov2)
                elif n_cal_lines == 3:
                    if thru_origin is True:
                        popt2[i], pcov2 = curve_fit(ph2e_poly2nd_thru_origin, x, y, p0=[0.1, 0.01], bounds=([-np.inf, 0], [np.inf, np.inf]))
                        residual_2nd = ph2e_poly2nd_thru_origin(opt_assignment[i], *popt2[i])-cal_lines
                    else:
                        popt2[i], pcov2 = np.polyfit(x, y, 1, cov=True)  # linear fit, meaningless
                        residual_2nd = np.polyval(popt2[i], opt_assignment[i]) - cal_lines
                    residual_2nd_rms[i] = np.sqrt(sum(np.square(residual_2nd))/n_cal_lines)
                    if thru_origin is True:
                        popt3[i], pcov3 = curve_fit(ph2e_poly3rd_thru_origin, x, y, p0=[0.1, 0.01, 0.001])
                        residual_3rd = ph2e_poly3rd_thru_origin(opt_assignment[i], *popt3[i])-cal_lines
                    else:
                        popt3[i], pcov3 = np.polyfit(x, y, 2, cov=True)  # 2nd order fit
                        residual_3rd = np.polyval(popt3[i], opt_assignment[i]) - cal_lines
                    residual_3rd_rms[i] = np.sqrt(sum(np.square(residual_3rd))/n_cal_lines)
                    if verbosity >= 3:
                        print('popt2:', popt2[i], 'pcov2:', pcov2)
                        print('popt3:', popt3[i], 'pcov3:', pcov3)
                elif n_cal_lines >= 4:
                    try:
                        if thru_origin is True:
                            popt4[i], pcov4 = curve_fit(ph2e_poly4th_thru_origin, x, y, p0=[0.1, 0.01, 0.001, 0.0001])
                            residual_4th = ph2e_poly4th_thru_origin(opt_assignment[i], *popt4[i])-cal_lines
                            #residual_4th /= cal_lines
                        else:
                            popt4[i], pcov4 = np.polyfit(x, y, 3, cov=True)
                            residual_4th = np.polyval(popt4[i], opt_assignment[i]) - cal_lines
                        residual_4th_rms[i] = np.sqrt(sum(np.square(residual_4th))/n_cal_lines)
                    except RuntimeError:
                        residual_4th_rms[i] = 0.
                    if n_cal_lines >= 5:  # this is needed because otherwise, 4th order poly fit fails for 4 points
                        try:
                            if thru_origin is True:
                                popt5[i], pcov5 = curve_fit(ph2e_poly5th_thru_origin, x, y, p0=[0.1, 0.01, 0.001, 0.0001, 0.00001])
                                residual_5th = ph2e_poly5th_thru_origin(opt_assignment[i], *popt5[i])-cal_lines
                                #residual_5th /= cal_lines
                            else:
                                popt5[i], pcov5 = np.polyfit(x, y, 4, cov=True)  # 4th order fit
                                residual_5th = np.polyval(popt5[i], opt_assignment[i]) - cal_lines
                            residual_5th_rms[i] = np.sqrt(sum(np.square(residual_5th))/n_cal_lines)
                        except RuntimeError:
                            residual_5th_rms[i] = 0.
                    if verbosity >= 3:
                        print('popt4:', popt4[i], 'pcov4:', pcov4)
                        print('popt5:', popt5[i], 'pcov5:', pcov5)
                # if plot_on_fail:
                #     plt.figure()
                #     plt.subplot(211)
                #     plt.plot(np.insert(opt_assignment[i], 0, .0), np.insert(cal_lines,0,0.0),'.')
                #     plt.plot(np.arange(int(max(opt_assignment[i])*1.1)), ph2e_poly4th_thru_origin(np.arange(int(max(opt_assignment[i])*1.1)), *popt4[i]),'r', label="4th order poly")
                #     plt.plot(np.arange(int(max(opt_assignment[i])*1.1)), ph2e_poly5th_thru_origin(np.arange(int(max(opt_assignment[i])*1.1)), *popt5[i]),'g', label="5th order poly")
                #     plt.ylabel('Energy (eV)')
                #     plt.legend(loc=2)
                #     plt.title("Channel %d, size_related_to_energy_resolution = %d"%(self.channum, size_related_to_e_res))
        #        residual_spl = []
                # for j, line in enumerate(cal_lines):
                #     opt_assignment_copy = list(opt_assignment[i])
                #     line_names_copy = cal_lines
                #     #test_position = opt_assignment_copy[i]
                #     opt_assignment_pop = opt_assignment_copy.pop(j)
                    #line_names_pop = line_names_copy.pop(j)
        #            ph2e_spl = cal._EnergyCalibration__build_calibration_spline([.0]+opt_assignment_copy, [.0]+line_names_copy)
        #            residual_spl += [ph2e_spl(opt_assignment_pop) - line]
        #        residual_spl_rms[i] = np.sqrt(sum(np.square(residual_spl)))
        #        print "residual_spl: ", residual_spl, "residual_rms: ", residual_spl_rms[i]
                if verbosity >= 2:
                    if n_cal_lines == 2:
                        print("residual rms for size_related_to_energy_resolution of %g = %g (2nd)" %(size_related_to_e_res, residual_2nd_rms[i]))
                    elif n_cal_lines == 3:
                        print("residual rms for size_related_to_energy_resolution of %g = %g (2nd), %g (3rd)" %(size_related_to_e_res, residual_2nd_rms[i], residual_3rd_rms[i]))
                    elif n_cal_lines >= 4:
                        print("residual rms for size_related_to_energy_resolution of %g = %g (4th), %g (5th)" %(size_related_to_e_res, residual_4th_rms[i], residual_5th_rms[i]))
                if plot_on_fail:  # supported only for the dataset case
                    plt.figure()
                    ax1 = plt.subplot(211)
                    ax2 = ax1.twinx()
                    ax1.plot(np.insert(opt_assignment[i], 0, .0), np.insert(cal_lines,0,0.0),'.')

                    temp_spl = mass.mathstat.interpolate.CubicSpline(np.insert(opt_assignment[i], 0, .0), np.insert(cal_lines,0,0.0))
                    if n_cal_lines == 2:
                        if thru_origin is True:
                            ax1.plot(np.arange(int(max(opt_assignment[i])*1.1), dtype=dtype), ph2e_poly2nd_thru_origin(np.arange(int(max(opt_assignment[i])*1.1), dtype=dtype), *popt2[i]),'r', label="2nd order poly")
                            ax2.plot(np.arange(int(max(opt_assignment[i])*1.01), dtype=dtype), ph2e_poly2nd_thru_origin(np.arange(int(max(opt_assignment[i])*1.01), dtype=dtype), *popt2[i])-temp_spl(np.arange(int(max(opt_assignment[i])*1.01))),'r--', label="2nd - spline")
                            # ax1.plot(np.arange(int(max(opt_assignment[i])*1.1), dtype=dtype), ph2e_poly2nd_thru_origin(np.arange(int(max(opt_assignment[i])*1.1), dtype=dtype), *popt2[i]),'r', label="2nd order poly")
                        else:
                            ax1.plot(np.arange(int(max(opt_assignment[i])*1.1), dtype=dtype), np.polyval(popt2[i], np.arange(int(max(opt_assignment[i])*1.1), dtype=dtype)),'r', label="linear")
                            ax2.plot(np.arange(int(max(opt_assignment[i])*1.01), dtype=dtype), np.polyval(popt2[i], np.arange(int(max(opt_assignment[i])*1.01), dtype=dtype))-temp_spl(np.arange(int(max(opt_assignment[i])*1.01))),'r--', label="2nd - spline")
                    elif n_cal_lines == 3:
                        if thru_origin is True:
                            ax1.plot(np.arange(int(max(opt_assignment[i])*1.1), dtype=dtype), ph2e_poly2nd_thru_origin(np.arange(int(max(opt_assignment[i])*1.1), dtype=dtype), *popt2[i]),'r', label="2nd order poly")
                            ax1.plot(np.arange(int(max(opt_assignment[i])*1.1), dtype=dtype), ph2e_poly3rd_thru_origin(np.arange(int(max(opt_assignment[i])*1.1), dtype=dtype), *popt3[i]),'c', label="3rd order poly")
                            ax2.plot(np.arange(int(max(opt_assignment[i])*1.01), dtype=dtype), ph2e_poly2nd_thru_origin(np.arange(int(max(opt_assignment[i])*1.01), dtype=dtype), *popt2[i])-temp_spl(np.arange(int(max(opt_assignment[i])*1.01))),'r--', label="2nd - spline")
                            ax2.plot(np.arange(int(max(opt_assignment[i])*1.01), dtype=dtype), ph2e_poly3rd_thru_origin(np.arange(int(max(opt_assignment[i])*1.01), dtype=dtype), *popt3[i])-temp_spl(np.arange(int(max(opt_assignment[i])*1.01))),'c--', label="3rd - spline")
                        else:
                            ax1.plot(np.arange(int(max(opt_assignment[i])*1.1), dtype=dtype), np.polyval(popt2[i], np.arange(int(max(opt_assignment[i])*1.1), dtype=dtype)),'r', label="linear")
                            ax1.plot(np.arange(int(max(opt_assignment[i])*1.1), dtype=dtype), np.polyval(popt3[i], np.arange(int(max(opt_assignment[i])*1.1), dtype=dtype)),'c', label="2nd order poly")
                            ax2.plot(np.arange(int(max(opt_assignment[i])*1.01), dtype=dtype), np.polyval(popt2[i], np.arange(int(max(opt_assignment[i])*1.01), dtype=dtype))-temp_spl(np.arange(int(max(opt_assignment[i])*1.01))),'r--', label="2nd - spline")
                            ax2.plot(np.arange(int(max(opt_assignment[i])*1.01), dtype=dtype), np.polyval(popt3[i], np.arange(int(max(opt_assignment[i])*1.01), dtype=dtype))-temp_spl(np.arange(int(max(opt_assignment[i])*1.01))),'c--', label="3rd - spline")
                    elif n_cal_lines >= 4:
                        if thru_origin is True:
                            ax1.plot(np.arange(int(max(opt_assignment[i])*1.1), dtype=dtype), ph2e_poly4th_thru_origin(np.arange(int(max(opt_assignment[i])*1.1), dtype=dtype), *popt4[i]),'r', label="4th order poly")
                            ax2.plot(np.arange(int(max(opt_assignment[i])*1.01), dtype=dtype), ph2e_poly4th_thru_origin(np.arange(int(max(opt_assignment[i])*1.01), dtype=dtype), *popt4[i])-temp_spl(np.arange(int(max(opt_assignment[i])*1.01))),'r--', label="4th - spline")
                            if max(np.abs(ph2e_poly5th_thru_origin(opt_assignment[i], *popt5[i])))<3e3:
                                ax1.plot(np.arange(int(max(opt_assignment[i])*1.1), dtype=dtype), ph2e_poly5th_thru_origin(np.arange(int(max(opt_assignment[i])*1.1), dtype=dtype), *popt5[i]),'c', label="5th order poly")
                                ax2.plot(np.arange(int(max(opt_assignment[i])*1.01), dtype=dtype), ph2e_poly5th_thru_origin(np.arange(int(max(opt_assignment[i])*1.01), dtype=dtype), *popt5[i])-temp_spl(np.arange(int(max(opt_assignment[i])*1.01))),'c--', label="5th - spline")
                        else:
                            ax1.plot(np.arange(int(max(opt_assignment[i])*1.1), dtype=dtype), np.polyval(popt4[i], np.arange(int(max(opt_assignment[i])*1.1), dtype=dtype)),'r', label="3rd order poly")
                            ax2.plot(np.arange(int(max(opt_assignment[i])*1.01), dtype=dtype), np.polyval(popt4[i], np.arange(int(max(opt_assignment[i])*1.01), dtype=dtype))-temp_spl(np.arange(int(max(opt_assignment[i])*1.01))),'r--', label="4th - spline")
                            if max(np.abs(np.polyval(popt5[i], opt_assignment[i])))<3e3:
                                ax1.plot(np.arange(int(max(opt_assignment[i])*1.1), dtype=dtype), np.polyval(popt5[i], np.arange(int(max(opt_assignment[i])*1.1), dtype=dtype)),'c', label="4th order poly")
                                ax2.plot(np.arange(int(max(opt_assignment[i])*1.01), dtype=dtype), np.polyval(popt5[i], np.arange(int(max(opt_assignment[i])*1.01), dtype=dtype))-temp_spl(np.arange(int(max(opt_assignment[i])*1.01))),'c--', label="5th - spline")
                    ax1.plot(np.arange(int(max(opt_assignment[i])*1.1)), temp_spl(np.arange(int(max(opt_assignment[i])*1.1))),'m', label="spline")
         #           plt.plot(np.arange(int(max(opt_assignment[i])*1.1)), ph2e_spl(np.arange(int(max(opt_assignment[i])*1.1))),'g', label="spline")
                    ax1.legend(loc=2, fontsize='small')
                    ax2.legend(loc=4, fontsize='small')
                    ax1.set_ylabel('Energy (eV)')
                    plt.draw()
                    plt.title("Ch %d, size_related_to_energy_resolution = %g, pow = %.3f"%(self.channum, size_related_to_e_res, popt_lin[1]), fontsize='small')

                    plt.subplot(212)
                    if n_cal_lines == 2:
                        plt.plot(np.insert(cal_lines,0,0.0), np.insert(residual_2nd, 0, .0),'.r')
                        plt.title("Residual rms = %g (2nd)" %(residual_2nd_rms[i]), fontsize='small')
                    elif n_cal_lines == 3:
                        plt.plot(np.insert(cal_lines,0,0.0), np.insert(residual_2nd, 0, .0),'.r')
                        plt.plot(np.insert(cal_lines,0,0.0), np.insert(residual_3rd, 0, .0),'.c')
                        plt.title("Residual rms = %g (2nd), %g (3rd)" %(residual_2nd_rms[i], residual_3rd_rms[i]), fontsize='small')
                    elif n_cal_lines >= 4:
                        plt.plot(np.insert(cal_lines,0,0.0), np.insert(residual_4th, 0, .0),'.r')
                        if thru_origin is True:
                            if max(np.abs(ph2e_poly5th_thru_origin(opt_assignment[i], *popt5[i])))<3e3:
                                plt.plot(np.insert(cal_lines,0,0.0), np.insert(residual_5th, 0, .0),'.c')
                        else:
                            if max(np.abs(np.polyval(popt5[i], opt_assignment[i])))<3e3:
                                plt.plot(np.insert(cal_lines,0,0.0), np.insert(residual_5th, 0, .0),'.c')
             #           plt.plot(np.insert(cal_lines,0,0.0), np.insert(residual_spl, 0, .0),'.g')
                        plt.title("Residual rms = %g (4th), %g (5th)" %(residual_4th_rms[i], residual_5th_rms[i]), fontsize='small')
                    plt.ylabel('Residual (eV)')
                    plt.xlabel('Energy (eV)')
    #    acc_est_copy = acc_est
        if n_cal_lines == 2:
            # for i in xrange(len(acc_est)):
            #     acc_est_copy[i] = acc_est[i] if residual_2nd_rms[i] == residual_2nd_rms[residual_2nd_rms!=0.].min() else 1.
    #        acc_est_2nd = acc_est.min()

            # best_popt2 = popt2[np.argmin(acc_est_copy)]
            # residual_2nd_rms[acc_est>maxacc] = 1000.
            best_popt2 = popt2[np.argmin(residual_2nd_rms)]
            if thru_origin is True:
                self.p_energy_sj_poly_2nd = ph2e_poly2nd_thru_origin(np.asarray(getattr(self, attr)), *best_popt2)
            else:
                self.p_energy_sj_poly_2nd = np.polyval(best_popt2, np.asarray(getattr(self, attr)))
            best_size_2nd = size_related_to_energy_resolution[np.argmin(residual_2nd_rms)]
            best_rms_2nd = np.min(residual_2nd_rms)
            best_peak_positions_2nd = peak_positions[np.argmin(residual_2nd_rms)]
            best_opt_assignment_2nd = opt_assignment[np.argmin(residual_2nd_rms)]
            best_opt_assignment_intensity_2nd = opt_assignment_intensity[np.argmin(residual_2nd_rms)]
            acc_est_2nd = acc_est[np.argmin(residual_2nd_rms)]

            best_size_poly = best_size_2nd
            best_rms_poly = best_rms_2nd
            best_peak_positions_poly = best_peak_positions_2nd
            best_opt_assignment_poly = best_opt_assignment_2nd
            best_opt_assignment_intensity_poly = best_opt_assignment_intensity_2nd
            self.p_energy_sj_poly = self.p_energy_sj_poly_2nd
            print("2nd order poly residual minimum at %g with residual %g and acc_est %f (best acc_est = %f)" %(best_size_2nd, best_rms_2nd, acc_est_2nd, np.min(acc_est)))
        if n_cal_lines == 3:
            # for i in xrange(len(acc_est)):
            #     acc_est_copy[i] = acc_est[i] if residual_2nd_rms[i] == residual_2nd_rms[residual_2nd_rms!=0.].min() else 1.
            #acc_est_2nd = acc_est.min()

            # best_popt2 = popt2[np.argmin(acc_est_copy)]
            # residual_2nd_rms[acc_est>maxacc] = 1000.
            best_popt2 = popt2[np.argmin(residual_2nd_rms)]
            if thru_origin is True:
                self.p_energy_sj_poly_2nd = ph2e_poly2nd_thru_origin(np.asarray(getattr(self, attr)), *best_popt2)
            else:
                self.p_energy_sj_poly_2nd = np.polyval(best_popt2, np.asarray(getattr(self, attr)))
            best_size_2nd = size_related_to_energy_resolution[np.argmin(residual_2nd_rms)]
            best_rms_2nd = np.min(residual_2nd_rms)
            best_peak_positions_2nd = peak_positions[np.argmin(residual_2nd_rms)]
            best_opt_assignment_2nd = opt_assignment[np.argmin(residual_2nd_rms)]
            best_opt_assignment_intensity_2nd = opt_assignment_intensity[np.argmin(residual_2nd_rms)]
            acc_est_2nd = acc_est[np.argmin(residual_2nd_rms)]

            # residual_3rd_rms[acc_est>maxacc] = 1000.
            best_popt3 = popt3[np.argmin(residual_3rd_rms)]
            if thru_origin is True:
                self.p_energy_sj_poly_3rd = ph2e_poly3rd_thru_origin(np.asarray(getattr(self, attr)), *best_popt3)
            else:
                self.p_energy_sj_poly_3rd = np.polyval(best_popt3, np.asarray(getattr(self, attr)))
            best_size_3rd = size_related_to_energy_resolution[np.argmin(residual_3rd_rms)]
            best_rms_3rd = np.min(residual_3rd_rms)
            best_peak_positions_3rd = peak_positions[np.argmin(residual_3rd_rms)]
            best_opt_assignment_3rd = opt_assignment[np.argmin(residual_3rd_rms)]
            best_opt_assignment_intensity_3rd = opt_assignment_intensity[np.argmin(residual_3rd_rms)]
            acc_est_3rd = acc_est[np.argmin(residual_3rd_rms)]
            #best_opt_assignment_intensity_poly = np.zeros(n_cal_lines)

            best_size_poly = best_size_2nd
            best_rms_poly = best_rms_2nd
            best_peak_positions_poly = best_peak_positions_2nd
            best_opt_assignment_poly = best_opt_assignment_2nd
            best_opt_assignment_intensity_poly = best_opt_assignment_intensity_2nd
            self.p_energy_sj_poly = self.p_energy_sj_poly_2nd
            if best_rms_2nd > best_rms_3rd:
                best_size_poly = best_size_3rd
                best_rms_poly = best_rms_3rd
                best_peak_positions_poly = best_peak_positions_3rd
                best_opt_assignment_poly = best_opt_assignment_3rd
                best_opt_assignment_intensity_poly = best_opt_assignment_intensity_3rd
                self.p_energy_sj_poly = self.p_energy_sj_poly_3rd
            print("2nd order poly residual minimum at %g with residual %g and acc_est %f (best acc_est = %f)" %(best_size_2nd, best_rms_2nd, acc_est_2nd, np.min(acc_est)))
            print("3rd order poly residual minimum at %g with residual %g and acc_est %f (best acc_est = %f)" %(best_size_3rd, best_rms_3rd, acc_est_3rd, np.min(acc_est)))
            #print "3rd order poly residual minimum at %g with residual %g" %(best_size_3rd, best_rms_3rd)
        elif n_cal_lines >= 4:
            # residual_4th_rms[acc_est>maxacc] = 1000.
            # residual_5th_rms[acc_est>maxacc] = 1000.
            best_popt4 = popt4[np.argmin(residual_4th_rms)]
            best_popt5 = popt5[np.argmin(residual_5th_rms)]
            if thru_origin is True:
                self.p_energy_sj_poly_4th = ph2e_poly4th_thru_origin(np.asarray(getattr(self, attr)), *best_popt4)
                self.p_energy_sj_poly_5th = ph2e_poly5th_thru_origin(np.asarray(getattr(self, attr)), *best_popt5)
            else:
                self.p_energy_sj_poly_4th = np.polyval(best_popt4, np.asarray(getattr(self, attr)))
                self.p_energy_sj_poly_5th = np.polyval(best_popt5, np.asarray(getattr(self, attr)))
            best_size_4th = size_related_to_energy_resolution[np.argmin(residual_4th_rms)]
            best_rms_4th = np.min(residual_4th_rms)
            best_peak_positions_4th = peak_positions[np.argmin(residual_4th_rms)]
            best_opt_assignment_4th = opt_assignment[np.argmin(residual_4th_rms)]
            best_opt_assignment_intensity_4th = opt_assignment_intensity[np.argmin(residual_4th_rms)]

            best_size_5th = size_related_to_energy_resolution[np.argmin(residual_5th_rms)]
            best_rms_5th = np.min(residual_5th_rms)
            best_peak_positions_5th = peak_positions[np.argmin(residual_5th_rms)]
            best_opt_assignment_5th = opt_assignment[np.argmin(residual_5th_rms)]
            best_opt_assignment_intensity_5th = opt_assignment_intensity[np.argmin(residual_5th_rms)]
            #best_opt_assignment_intensity_poly = np.zeros(n_cal_lines)
            acc_est_4th = acc_est[np.argmin(residual_4th_rms)]
            acc_est_5th = acc_est[np.argmin(residual_5th_rms)]

            best_size_poly = best_size_4th
            best_rms_poly = best_rms_4th
            best_peak_positions_poly = best_peak_positions_4th
            best_opt_assignment_poly = best_opt_assignment_4th
            best_opt_assignment_intensity_poly = best_opt_assignment_intensity_4th
            self.p_energy_sj_poly = self.p_energy_sj_poly_4th
            if (best_rms_4th > best_rms_5th) and (quartic_only is False):
                best_size_poly = best_size_5th
                best_rms_poly = best_rms_5th
                best_peak_positions_poly = best_peak_positions_5th
                best_opt_assignment_poly = best_opt_assignment_5th
                best_opt_assignment_intensity_poly = best_opt_assignment_intensity_5th
                self.p_energy_sj_poly = self.p_energy_sj_poly_5th
            print("4th order poly residual minimum at %g with residual %g and acc_est %f (best acc_est = %f)" %(best_size_4th, best_rms_4th, acc_est_4th, np.min(acc_est)))
            print("5th order poly residual minimum at %g with residual %g and acc_est %f (best acc_est = %f)" %(best_size_5th, best_rms_5th, acc_est_5th, np.min(acc_est)))
        #   print residual_spl_rms

        if n_cal_lines == 1:
            self.sj_opt_assignment = peak_posi
            self.p_energy[:] = self.p_filt_value_dc[:]/peak_posi*cal_lines[0]
            return peak_posi
        else:
            self.sj_opt_assignment = best_opt_assignment_poly
            spl_ftn_from_mass = mass.mathstat.interpolate.CubicSpline(np.insert(best_opt_assignment_poly,0,0), np.insert(cal_lines,0,0))
            self.p_energy_spl = spl_ftn_from_mass(np.asarray(getattr(self, attr)))
            # added following Jamie's version
            energy_cal = mass.calibration.energy_calibration.EnergyCalibration()
            for opt_ph, cal_ev in zip(best_opt_assignment_poly, cal_lines):
                energy_cal.add_cal_point(opt_ph, cal_ev)
            self.calibration[attr] = energy_cal

            if cal_method == 'spl':
                self.p_energy[:] = self.p_energy_spl # self.p_energy = self.p_energy_spl[:] is wrong (maybe because I was using hdf5 loaded data?)
            elif cal_method == 'poly':
                self.p_energy[:] = self.p_energy_sj_poly
            elif cal_method == 'nist':
                self.convert_to_energy(attr)
        #    best_size_spl = size_related_to_energy_resolution[np.argmin(residual_spl_rms[residual_spl_rms!=0.])]
        #    best_rms_spl = np.min(residual_spl_rms[residual_spl_rms!=0.])
        #    best_peak_positions_spl = peak_positions[np.argmin(residual_5th_rms[residual_spl_rms!=0.])]
        #    best_opt_assignment_spl = opt_assignment[np.argmin(residual_5th_rms[residual_spl_rms!=0.])]

        #   best_ph2e_spl = cal._EnergyCalibration__build_calibration_spline(best_opt_assignment_spl, cal_lines)
        #   self.p_energy_sj_spl = best_ph2e_spl(np.asarray(getattr(self, attr)))


        #    return best_size_4th, best_rms_4th, best_peak_positions_4th, best_opt_assignment_4th, best_size_5th, best_rms_5th, best_peak_positions_5th, best_opt_assignment_5th, best_size_spl, best_rms_spl, best_peak_positions_spl, best_opt_assignment_spl
            if n_cal_lines == 2:
                return best_size_2nd, best_rms_2nd, best_peak_positions_2nd, best_opt_assignment_2nd, best_size_poly, best_rms_poly, best_peak_positions_poly, best_opt_assignment_poly, best_opt_assignment_intensity_poly
            if n_cal_lines == 3:
                return best_size_2nd, best_rms_2nd, best_peak_positions_2nd, best_opt_assignment_2nd, best_size_3rd, best_rms_3rd, best_peak_positions_3rd, best_opt_assignment_3rd, best_size_poly, best_rms_poly, best_peak_positions_poly, best_opt_assignment_poly, best_opt_assignment_intensity_poly
            elif n_cal_lines >= 4:
                return best_size_4th, best_rms_4th, best_peak_positions_4th, best_opt_assignment_4th, best_size_5th, best_rms_5th, best_peak_positions_5th, best_opt_assignment_5th, best_size_poly, best_rms_poly, best_peak_positions_poly, best_opt_assignment_poly, best_opt_assignment_intensity_poly
    except KeyboardInterrupt:
        raise KeyboardInterrupt('KeyboardInterrupt')
        print("# script interrupt by user")
        return

mass.MicrocalDataSet.sj_calibrate_ds = sj_calibrate_ds


def sj_find_opt_assignment(peak_positions, peak_intensity, cal_lines=None, line_intensity=None, auto_inclusion=1, strong_line_indices=[], linearity=1.05, verbosity=1, include_origin=True):
    '''
    for faster speed, always include tallest peak (use auto_inclusion >= 1)
    include_origin=True id the default
    '''
    try:
        import operator
        import itertools
        if line_intensity is None:
            name_e, e_e = zip(*sorted([[element, mass.calibration.energy_calibration.STANDARD_FEATURES.get(element, element)] for element in cal_lines],
                         key=operator.itemgetter(1)))
        else: # almost never used, but might be useful
            name_e, i_e, e_e = zip(*sorted([[element, line_intensity[i], mass.calibration.energy_calibration.STANDARD_FEATURES.get(element, element)] for i, element in enumerate(cal_lines)],
                                  key=operator.itemgetter(2)))
            intensity = np.array(i_e, dtype=np.float32)
        #sel_positions = np.insert(peak_positions, 0, 0.0)
        energies = np.array(e_e, dtype=np.float32)
        # energies_w_zero = np.insert(energies, 0, 0.0)
        scaled_energies = energies**(1./linearity)
        strong_lines = scaled_energies[strong_line_indices] # okay for empty strong_line_indices
        remaining_lines = np.array(delete_index(scaled_energies, strong_line_indices))
        rearranged_lines = np.concatenate([strong_lines, remaining_lines])
        rearranged_lines_w_zero = np.insert(rearranged_lines, 0, 0.0)
        auto_inclusion = max(auto_inclusion, len(strong_line_indices))
        # pp_combination is all possible combinations of peak_positions[auto_inclusion:]
        pp_combination = np.array(list(itertools.combinations(peak_positions[auto_inclusion:], len(cal_lines)-auto_inclusion)))
        # auto_inclusion peaks inserted at the beginning
        for i in reversed(range(auto_inclusion)):
            pp_combination = np.insert(pp_combination, 0, peak_positions[i], axis=1)
        pp_combination[:,len(strong_line_indices):] = np.sort(pp_combination[:,len(strong_line_indices):], axis=1)

        # pp_combi_sorted = []
        # pi_combi_sorted = []
        # if line_intensity is not None:
        #     assign_pi = np.array(list(itertools.combinations(peak_intensity[auto_inclusion:], len(cal_lines)-auto_inclusion)))
        #     assign_pi = np.insert(assign_pi, 0, peak_intensity[0], axis=1)
        # #pi_combi_sorted = []
        #     for temp in zip(assign_pp, assign_pi):
        #         temp0, temp1 = zip(*sorted(zip(temp[0], temp[1])))
        #         pp_combi_sorted += [list(temp0)]
        #         pi_combi_sorted += [list(temp1)]
        #     pp_combi_sorted = np.array(pp_combi_sorted)
        #     pi_combi_sorted = np.array(pi_combi_sorted)
        # else:
        #     pp_combi_sorted = np.sort(assign_pp, axis=1)
        # #assign_pi = np.array(list(itertools.combinations(peak_intensity, len(cal_lines))))

        pp_combination_w_zero = np.insert(pp_combination, 0, 0.0, axis=1)
        if include_origin is True:
            fracs = (rearranged_lines_w_zero[1:-1] - rearranged_lines_w_zero[:-2])/(rearranged_lines_w_zero[2:] - rearranged_lines_w_zero[:-2])
            est_pos = pp_combination_w_zero[:, :-2]*(1 - fracs) + pp_combination_w_zero[:, 2:]*fracs
            acc_est_pp = np.linalg.norm((est_pos - pp_combination_w_zero[:, 1:-1]) /
                                     (pp_combination_w_zero[:, 2:] - pp_combination_w_zero[:, :-2]), axis=1)
        else:
            fracs = (rearranged_lines[1:-1] - rearranged_lines[:-2])/(rearranged_lines[2:] - rearranged_lines[:-2])
            est_pos = pp_combination[:, :-2]*(1 - fracs) + pp_combination[:, 2:]*fracs
            acc_est_pp = np.linalg.norm((est_pos - pp_combination[:, 1:-1]) /
                                     (pp_combination[:, 2:] - pp_combination[:, :-2]), axis=1)

        if line_intensity is not None:
            acc_est_pi_array = []
            #acc_est_pi = np.linalg.norm(pi_combi_sorted/line_intensity, axis=1)
            for i, temp in enumerate(pi_combi_sorted):
                acc_est_pi_array += [temp/np.max(temp)/(intensity/np.max(intensity))-1]
            acc_est_pi = np.linalg.norm(acc_est_pi_array, axis=1)
            opt_assign_arg = np.argmin(acc_est_pp*acc_est_pi)
            opt_assign = pp_combi_sorted[opt_assign_arg]
            opt_assign_intensity = pi_combi_sorted[opt_assign_arg]
            if verbosity >= 2:
                print("acc_est_pp, acc_est_pi", np.min(acc_est_pp), np.min(acc_est_pi), np.argmin(acc_est_pp), np.argmin(acc_est_pi), opt_assign_arg)
            return list(opt_assign), list(opt_assign_intensity), np.min(acc_est_pp)
        else:
            opt_assign_arg = np.argmin(acc_est_pp)
            if verbosity >= 2:
                print("acc_est_pp", np.min(acc_est_pp), np.argmin(acc_est_pp))
            opt_assign = np.sort(pp_combination[opt_assign_arg])
            return list(opt_assign), np.min(acc_est_pp)
    except KeyboardInterrupt:
        raise KeyboardInterrupt('KeyboardInterrupt was called')
        return


def sj_find_local_maxima(pulse_heights, size_related_to_energy_resolution=20, n_cal_lines=3, nextra=7, neighbor_limit=None):
    '''
    returns local maximum positions & intensities in descending order of intensity
    '''
    npeaks = n_cal_lines + nextra
    try:
        data = np.hstack([np.zeros(0), pulse_heights]) # self.data = np.zeros(0)
        data=data[data>0]
        kde = sm.nonparametric.KDEUnivariate(data)
        kde.fit(bw=size_related_to_energy_resolution)
        x = kde.support
        y = kde.density

        flag = (y[1:-1] > y[:-2]) & (y[1:-1] > y[2:]) # boolean array for local max

        lm = np.arange(1, len(x)-1)[flag] # indices of local max for x and y
        # if ncal+nextra is 0:
        #     lm = lm[np.argsort(-y[lm])][:30]
        # else:
        #     lm = lm[np.argsort(-y[lm])][:(ncal+nextra)]
        lm = lm[np.argsort(-y[lm])][:100] # indices of local max for x and y

        positions = np.array(x[lm]) # sorted by descending intensity
        intensities = np.array(y[lm]) # sorted by descending intensity

        if neighbor_limit:
            for i in range(len(lm)):
                #print intensity
                # if i is len(intensities):
                if i == len(intensities):
                    break
                flag = (abs(positions-positions[i])<neighbor_limit) & (abs(positions-positions[i])>0)
                index_to_remove = np.arange(len(intensities))[flag]
                intensities = np.delete(intensities, index_to_remove)
                positions = np.delete(positions, index_to_remove)
                #print "len(intensities)", len(intensities)
            #print positions
            # pos_sort_by_pos = np.array([x for (x,y) in sorted(zip(position,intensity), key=lambda pair: pair[0])])
            # int_sort_by_pos = np.array([y for (x,y) in sorted(zip(position,intensity), key=lambda pair: pair[0])])

        positions = positions[:npeaks]
        intensities = intensities[:npeaks]

        return positions, intensities
    except KeyboardInterrupt:
        raise KeyboardInterrupt('KeyboardInterrupt was called')
        return
# mass.young.EnergyCalibration.sj_find_local_maxima = sj_find_local_maxima


def get_peaks_window(pulses, bw, centers, width, refine_method='sj'):
    '''
    modified from Jamie's calibration.py
    pulses: p_energy
    centers: calibration energy points
    width defines the range for a peak
    '''
    if refine_method == 'jamie':
        kde = sm.nonparametric.KDEUnivariate(np.array(pulses, dtype=np.float))
        kde.fit(bw=bw)
        x = kde.support
        y = kde.density
        flag = (y[1:-1] > y[:-2]) & (y[1:-1] > y[2:])
        lm = np.arange(1, len(x)-1)[flag]
        lm = lm[np.argsort(-y[lm])][:100]
        positions = np.array(x[lm])
        positions_window = []
        for center in centers:
            pos = positions[(positions < center + width) & (positions > center - width)]
            if len(pos) == 0:
                # sometimes we are way off
                pos = positions[(positions < center + 2*width) & (positions > center - 2*width)]
                # added by SJL
                if len(pos) == 0:
                    return
            positions_window.append(pos[0])
    elif refine_method == 'sj':
        positions_window = []
        for center in centers:
            partial_range_pulses = np.array(pulses, dtype=np.float)
            partial_range_pulses = partial_range_pulses[(partial_range_pulses > center - width) & (partial_range_pulses < center + width)]
            kde = sm.nonparametric.KDEUnivariate(partial_range_pulses)
            kde.fit(bw=bw)
            x = kde.support
            y = kde.density
            positions_window.append(x[np.argmax(y)])
    elif refine_method == 'sj2': # cross-correlation
        positions_window = []
        for center in centers:
            partial_range_pulses = np.array(pulses, dtype=np.float)
            partial_range_pulses = partial_range_pulses[(partial_range_pulses > center - width) & (partial_range_pulses < center + width)]
            kde = sm.nonparametric.KDEUnivariate(partial_range_pulses)
            kde.fit(bw=bw)
            x = kde.support
            y = kde.density
            positions_window.append(x[np.argmax(y)])

    return np.array(positions_window)


def sj_calibrate(self, attr, cal_lines, strong_line_indices=[], name_ext="", size_related_to_energy_resolution=20,
                  fit_range_ev=10, excl=(), neighbor_limit=None, plot_on_fail=False,
                  bin_size_ev=0.2, category=None, forceNew=False, maxacc=0.1, nextra=3, line_intensity=None, linearity=1, auto_inclusion=3, bin_opt=False, refine_old=False, cal_method='spl', quartic_only=False, thru_origin=True, rerun_chans=None, verbosity=None):

    if type(self) is list:  # this is when co-calibrating data_list. Prerequisit: co_drift_correction of data_list
        for data in self:
            if np.sum(getattr(data.first_good_dataset, attr)) == 0:
                print('# It seems like the %s is not fully reduced. Check again.'%data)
                return
            data.sj_calib_pts = cal_lines
        if rerun_chans is None:
            good_chans = reduce(set.intersection, [set(data.good_channels) for data in self])
    else:
        if np.sum(getattr(self.first_good_dataset, attr)) == 0:
            print('# It seems like the data is not fully reduced. Check again.')
            return
        if rerun_chans is None:
            good_chans = np.array(self.good_channels)
        self.sj_calib_pts = cal_lines
    if rerun_chans is not None:
        good_chans = np.array(rerun_chans)

    n_cal_lines = len(cal_lines)
    if n_cal_lines == 1:
        peak_posi = np.zeros(len(good_chans))
    elif n_cal_lines == 2:
        opt_size_rel_to_e_res_2nd = np.zeros(len(good_chans))
        rms_2nd = np.zeros(len(good_chans))
        peak_posi_2nd = np.zeros((len(good_chans), n_cal_lines+nextra))
        opt_assign_2nd = np.zeros((len(good_chans), n_cal_lines))
    elif n_cal_lines == 3:
        opt_size_rel_to_e_res_2nd = np.zeros(len(good_chans))
        rms_2nd = np.zeros(len(good_chans))
        peak_posi_2nd = np.zeros((len(good_chans), n_cal_lines+nextra))
        opt_assign_2nd = np.zeros((len(good_chans), n_cal_lines))
        opt_size_rel_to_e_res_3rd = np.zeros(len(good_chans))
        rms_3rd = np.zeros(len(good_chans))
        peak_posi_3rd = np.zeros((len(good_chans), n_cal_lines+nextra))
        opt_assign_3rd = np.zeros((len(good_chans), n_cal_lines))
    elif n_cal_lines >= 4:
        opt_size_rel_to_e_res_4th = np.zeros(len(good_chans))
        rms_4th = np.zeros(len(good_chans))
        peak_posi_4th = np.zeros((len(good_chans), n_cal_lines+nextra))
        opt_assign_4th = np.zeros((len(good_chans), n_cal_lines))
        opt_size_rel_to_e_res_5th = np.zeros(len(good_chans))
        rms_5th = np.zeros(len(good_chans))
        peak_posi_5th = np.zeros((len(good_chans), n_cal_lines+nextra))
        opt_assign_5th = np.zeros((len(good_chans), n_cal_lines))
    opt_size_rel_to_e_res_poly = np.zeros(len(good_chans))
    rms_poly = np.zeros(len(good_chans))
    peak_posi_poly = np.zeros((len(good_chans), n_cal_lines+nextra))
    opt_assign_poly = np.zeros((len(good_chans), n_cal_lines))
    opt_assign_intensity_poly = np.zeros((len(good_chans), n_cal_lines))

    try:
        for i, good_chan in enumerate(good_chans):
            if type(self) is list:
                try:
                    if n_cal_lines == 1:
                        peak_posi[i] = sj_calibrate_ds([data.channel[good_chan] for data in self], attr, cal_lines, name_ext, size_related_to_energy_resolution,
                                     fit_range_ev, excl, neighbor_limit, plot_on_fail,
                                     bin_size_ev, category, forceNew, maxacc, nextra, verbosity=verbosity, linearity=linearity, auto_inclusion=auto_inclusion, cal_method=cal_method, thru_origin=thru_origin, strong_line_indices=strong_line_indices)
                    elif n_cal_lines == 2:
                        opt_size_rel_to_e_res_2nd[i], rms_2nd[i], peak_posi_2nd[i], opt_assign_2nd[i], opt_size_rel_to_e_res_poly[i], rms_poly[i], peak_posi_poly[i], opt_assign_poly[i], opt_assign_intensity_poly[i] = sj_calibrate_ds([data.channel[good_chan] for data in self], attr, cal_lines, name_ext, size_related_to_energy_resolution,
                                     fit_range_ev, excl, neighbor_limit, plot_on_fail,
                                     bin_size_ev, category, forceNew, maxacc, nextra, verbosity=verbosity, linearity=linearity, auto_inclusion=auto_inclusion, cal_method=cal_method, thru_origin=thru_origin, strong_line_indices=strong_line_indices)
                        if refine_old: # this is an old attempt to refind peak poisition based on intensity
                            opt_size_rel_to_e_res_2nd[i], rms_2nd[i], peak_posi_2nd[i], opt_assign_2nd[i], opt_size_rel_to_e_res_poly[i], rms_poly[i], peak_posi_poly[i], opt_assign_poly[i], opt_assign_intensity_poly[i] = self.channel[good_chan].sj_calibrate_ds([data.channel[good_chan] for data in self], attr, cal_lines, name_ext, size_related_to_energy_resolution,
                                         fit_range_ev, excl, neighbor_limit, plot_on_fail,
                                         bin_size_ev, category, forceNew, maxacc, nextra, line_intensity=opt_assign_intensity_poly[i], linearity=linearity, auto_inclusion=auto_inclusion, cal_method=cal_method, thru_origin=thru_origin, strong_line_indices=strong_line_indices)
                    elif n_cal_lines == 3:
                        opt_size_rel_to_e_res_2nd[i], rms_2nd[i], peak_posi_2nd[i], opt_assign_2nd[i], opt_size_rel_to_e_res_3rd[i], rms_3rd[i], peak_posi_3rd[i], opt_assign_3rd[i], opt_size_rel_to_e_res_poly[i], rms_poly[i], peak_posi_poly[i], opt_assign_poly[i], opt_assign_intensity_poly[i] = sj_calibrate_ds([data.channel[good_chan] for data in self], attr, cal_lines, name_ext, size_related_to_energy_resolution,
                                     fit_range_ev, excl, neighbor_limit, plot_on_fail,
                                     bin_size_ev, category, forceNew, maxacc, nextra, verbosity=verbosity, linearity=linearity, auto_inclusion=auto_inclusion, cal_method=cal_method, thru_origin=thru_origin, strong_line_indices=strong_line_indices)
                        if refine_old:
                            opt_size_rel_to_e_res_2nd[i], rms_2nd[i], peak_posi_2nd[i], opt_assign_2nd[i], opt_size_rel_to_e_res_3rd[i], rms_3rd[i], peak_posi_3rd[i], opt_assign_3rd[i], opt_size_rel_to_e_res_poly[i], rms_poly[i], peak_posi_poly[i], opt_assign_poly[i], opt_assign_intensity_poly[i] = sj_calibrate_ds([data.channel[good_chan] for data in self], attr, cal_lines, name_ext, size_related_to_energy_resolution,
                                         fit_range_ev, excl, neighbor_limit, plot_on_fail,
                                         bin_size_ev, category, forceNew, maxacc, nextra, line_intensity=opt_assign_intensity_poly[i], linearity=linearity, auto_inclusion=auto_inclusion, cal_method=cal_method, thru_origin=thru_origin, strong_line_indices=strong_line_indices)
                    elif n_cal_lines >= 4:
                        opt_size_rel_to_e_res_4th[i], rms_4th[i], peak_posi_4th[i], opt_assign_4th[i], opt_size_rel_to_e_res_5th[i], rms_5th[i], peak_posi_5th[i], opt_assign_5th[i], opt_size_rel_to_e_res_poly[i], rms_poly[i], peak_posi_poly[i], opt_assign_poly[i], opt_assign_intensity_poly[i] = sj_calibrate_ds([data.channel[good_chan] for data in self], attr, cal_lines, name_ext, size_related_to_energy_resolution,
                                     fit_range_ev, excl, neighbor_limit, plot_on_fail,
                                     bin_size_ev, category, forceNew, maxacc, nextra, verbosity=verbosity, linearity=linearity, auto_inclusion=auto_inclusion, cal_method=cal_method, quartic_only=quartic_only, thru_origin=thru_origin, strong_line_indices=strong_line_indices)
                        if refine_old:
                            opt_size_rel_to_e_res_4th[i], rms_4th[i], peak_posi_4th[i], opt_assign_4th[i], opt_size_rel_to_e_res_5th[i], rms_5th[i], peak_posi_5th[i], opt_assign_5th[i], opt_size_rel_to_e_res_poly[i], rms_poly[i], peak_posi_poly[i], opt_assign_poly[i], opt_assign_intensity_poly[i] = sj_calibrate_ds([data.channel[good_chan] for data in self], attr, cal_lines, name_ext, size_related_to_energy_resolution,
                                         fit_range_ev, excl, neighbor_limit, plot_on_fail,
                                         bin_size_ev, category, forceNew, maxacc, nextra, line_intensity=opt_assign_intensity_poly[i], linearity=linearity, auto_inclusion=auto_inclusion, cal_method=cal_method, quartic_only=quartic_only, thru_origin=thru_origin, strong_line_indices=strong_line_indices)
                except ValueError as e:
                    print("Something failed at channel %s: %s"%(good_chan,e))
                    print("Trying a smaller neightbor_limit number might help")
                    for data in self:
                        data.set_chan_bad(good_chan, 'Something failed during sj_calibrate_ds')
                    continue
                except KeyboardInterrupt:
                    raise KeyboardInterrupt('KeyboardInterrupt')
                    print("# Script cancelled by user during sj_calibrate.")
                    return

            else:

                # try:
                #     opt_size_rel_to_e_res[i] = ds.sj_calibrate_ds(attr, cal_lines, name_ext, size_related_to_energy_resolution,
                #                  fit_range_ev, excl, plot_on_fail,
                #                  bin_size_ev, category, forceNew, maxacc, nextra)
                # except:
                #     print "failed calibration at chan %d" %(ds.channum)
                #     self.bad_chans += [ds.channum]
                if sum(self.channel[good_chan].good())<10:
                    #self.set_chan_bad(good_chan, "too few pulses")
                    print("too few pulses")
                else:
                    try:
                        if n_cal_lines == 1:
                            peak_posi[i] = self.channel[good_chan].sj_calibrate_ds(attr, cal_lines, name_ext, size_related_to_energy_resolution,
                                         fit_range_ev, excl, neighbor_limit, plot_on_fail,
                                         bin_size_ev, category, forceNew, maxacc, nextra, verbosity=verbosity, linearity=linearity, auto_inclusion=auto_inclusion, cal_method=cal_method, thru_origin=thru_origin, strong_line_indices=strong_line_indices)
                        elif n_cal_lines == 2:
                            opt_size_rel_to_e_res_2nd[i], rms_2nd[i], peak_posi_2nd[i], opt_assign_2nd[i], opt_size_rel_to_e_res_poly[i], rms_poly[i], peak_posi_poly[i], opt_assign_poly[i], opt_assign_intensity_poly[i] = self.channel[good_chan].sj_calibrate_ds(attr, cal_lines, name_ext, size_related_to_energy_resolution,
                                         fit_range_ev, excl, neighbor_limit, plot_on_fail,
                                         bin_size_ev, category, forceNew, maxacc, nextra, verbosity=verbosity, linearity=linearity, auto_inclusion=auto_inclusion, cal_method=cal_method, thru_origin=thru_origin, strong_line_indices=strong_line_indices)
                            if refine_old: # this is an old attempt to refind peak poisition based on intensity
                                opt_size_rel_to_e_res_2nd[i], rms_2nd[i], peak_posi_2nd[i], opt_assign_2nd[i], opt_size_rel_to_e_res_poly[i], rms_poly[i], peak_posi_poly[i], opt_assign_poly[i], opt_assign_intensity_poly[i] = self.channel[good_chan].sj_calibrate_ds(attr, cal_lines, name_ext, size_related_to_energy_resolution,
                                             fit_range_ev, excl, neighbor_limit, plot_on_fail,
                                             bin_size_ev, category, forceNew, maxacc, nextra, line_intensity=opt_assign_intensity_poly[i], linearity=linearity, auto_inclusion=auto_inclusion, cal_method=cal_method, thru_origin=thru_origin, strong_line_indices=strong_line_indices)
                        elif n_cal_lines == 3:
                            opt_size_rel_to_e_res_2nd[i], rms_2nd[i], peak_posi_2nd[i], opt_assign_2nd[i], opt_size_rel_to_e_res_3rd[i], rms_3rd[i], peak_posi_3rd[i], opt_assign_3rd[i], opt_size_rel_to_e_res_poly[i], rms_poly[i], peak_posi_poly[i], opt_assign_poly[i], opt_assign_intensity_poly[i] = self.channel[good_chan].sj_calibrate_ds(attr, cal_lines, name_ext, size_related_to_energy_resolution,
                                         fit_range_ev, excl, neighbor_limit, plot_on_fail,
                                         bin_size_ev, category, forceNew, maxacc, nextra, verbosity=verbosity, linearity=linearity, auto_inclusion=auto_inclusion, cal_method=cal_method, thru_origin=thru_origin, strong_line_indices=strong_line_indices)
                            if refine_old:
                                opt_size_rel_to_e_res_2nd[i], rms_2nd[i], peak_posi_2nd[i], opt_assign_2nd[i], opt_size_rel_to_e_res_3rd[i], rms_3rd[i], peak_posi_3rd[i], opt_assign_3rd[i], opt_size_rel_to_e_res_poly[i], rms_poly[i], peak_posi_poly[i], opt_assign_poly[i], opt_assign_intensity_poly[i] = self.channel[good_chan].sj_calibrate_ds(attr, cal_lines, name_ext, size_related_to_energy_resolution,
                                             fit_range_ev, excl, neighbor_limit, plot_on_fail,
                                             bin_size_ev, category, forceNew, maxacc, nextra, line_intensity=opt_assign_intensity_poly[i], linearity=linearity, auto_inclusion=auto_inclusion, cal_method=cal_method, thru_origin=thru_origin, strong_line_indices=strong_line_indices)
                        elif n_cal_lines >= 4:
                            opt_size_rel_to_e_res_4th[i], rms_4th[i], peak_posi_4th[i], opt_assign_4th[i], opt_size_rel_to_e_res_5th[i], rms_5th[i], peak_posi_5th[i], opt_assign_5th[i], opt_size_rel_to_e_res_poly[i], rms_poly[i], peak_posi_poly[i], opt_assign_poly[i], opt_assign_intensity_poly[i] = self.channel[good_chan].sj_calibrate_ds(attr, cal_lines, name_ext, size_related_to_energy_resolution,
                                         fit_range_ev, excl, neighbor_limit, plot_on_fail,
                                         bin_size_ev, category, forceNew, maxacc, nextra, verbosity=verbosity, linearity=linearity, auto_inclusion=auto_inclusion, cal_method=cal_method, quartic_only=quartic_only, thru_origin=thru_origin, strong_line_indices=strong_line_indices)
                            if refine_old:
                                opt_size_rel_to_e_res_4th[i], rms_4th[i], peak_posi_4th[i], opt_assign_4th[i], opt_size_rel_to_e_res_5th[i], rms_5th[i], peak_posi_5th[i], opt_assign_5th[i], opt_size_rel_to_e_res_poly[i], rms_poly[i], peak_posi_poly[i], opt_assign_poly[i], opt_assign_intensity_poly[i] = self.channel[good_chan].sj_calibrate_ds(attr, cal_lines, name_ext, size_related_to_energy_resolution,
                                             fit_range_ev, excl, neighbor_limit, plot_on_fail,
                                             bin_size_ev, category, forceNew, maxacc, nextra, line_intensity=opt_assign_intensity_poly[i], linearity=linearity, auto_inclusion=auto_inclusion, cal_method=cal_method, quartic_only=quartic_only, thru_origin=thru_origin, strong_line_indices=strong_line_indices)
                    except ValueError as e:
                        print("Something failed at channel %s: %s"%(good_chan,e))
                        print("Trying a smaller neightbor_limit number might help")
                        self.set_chan_bad(good_chan, 'Something failed during sj_calibrate_ds')
                        continue
                    except KeyboardInterrupt:
                        raise KeyboardInterrupt('KeyboardInterrupt')
                        print("# Script cancelled by user during sj_calibrate.")
                        return
                                # if i == len(good_chans)-1:
                                #     break
                # except KeyboardInterrupt:
                #     return
                            #self.set_chan_bad(good_chan, "failed sj_calibrate_ds")
                #print "\nself.bad_chans created"
                        #self.set_chan_bad(ds.channum, "failed calibration %s" % attr + name_ext)

                # good_chans = np.array(self.good_channels)
        if n_cal_lines > 1:
            print(rms_poly)
            print("# number of best_rms_poly==1000:", sum(rms_poly==1000))
            print("channels with best_rms_poly==1000:", list(good_chans[rms_poly==1000]))
            print("# number of best_rms_poly==0:", sum(rms_poly==0))
            print("channels with best_rms_poly==0:", list(good_chans[rms_poly==0]))
            print("# failed channels:", list(good_chans[(rms_poly==0) | (rms_poly==1000)]))

        if n_cal_lines == 1:
            return peak_posi
        elif n_cal_lines == 2:
            return opt_size_rel_to_e_res_2nd, rms_2nd, peak_posi_2nd, opt_assign_2nd, opt_size_rel_to_e_res_2nd, rms_2nd, peak_posi_2nd, opt_assign_2nd, opt_size_rel_to_e_res_poly, rms_poly, peak_posi_poly, opt_assign_poly
        elif n_cal_lines == 3:
            return opt_size_rel_to_e_res_2nd, rms_2nd, peak_posi_2nd, opt_assign_2nd, opt_size_rel_to_e_res_3rd, rms_3rd, peak_posi_3rd, opt_assign_3rd, opt_size_rel_to_e_res_poly, rms_poly, peak_posi_poly, opt_assign_poly
        elif n_cal_lines >= 4:
            return opt_size_rel_to_e_res_4th, rms_4th, peak_posi_4th, opt_assign_4th, opt_size_rel_to_e_res_5th, rms_5th, peak_posi_5th, opt_assign_5th, opt_size_rel_to_e_res_poly, rms_poly, peak_posi_poly, opt_assign_poly
    except KeyboardInterrupt:
        raise KeyboardInterrupt('KeyboardInterrupt')
        print("# Script cancelled by user during sj_calibrate.")
        return

mass.TESGroup.sj_calibrate = sj_calibrate


def sj_find_bad_cal_chans(self, threshold, bin_start=200, bin_end=1000, ):
    if sum([sum(ds.p_energy[:]) for ds in self]) == 0:
        print('# data not calibrated yet')
        return
    for ds in self:
        pass
    return


def sj_save_cal(self, filename=None, fast=None, cal_method=None, refine_method=None):
    # needs to be modified to save more parameters such as fast
    cal_matrix = {}
    cal_matrix['cal_method'] = cal_method
    cal_matrix['refine_method'] = refine_method
    cal_matrix['cal'] = np.append(self.sj_calib_pts, 0)
    if hasattr(self.first_good_dataset, 'new_gain'):
        cal_matrix['new_gain'] = self.first_good_dataset.new_gain
    if hasattr(self, 'cal_params'):
        cal_matrix['cal_params'] = self.cal_params # a sj-defined dictionary
        if fast is None:
            try:
                fast = self.cal_params['fast']
            except KeyError:
                fast = None

    for ds in self:
        if refine_method is not None:
            cal_matrix[ds.channum] = np.append(ds.calibration['p_filt_value_dc'].cal_point_phs, 0)
        else:
            cal_matrix[ds.channum] = np.append(ds.sj_opt_assignment, 0)
    if filename is None:
        if hasattr(self, 'filenames'): # ljh files
            fname_base = self.first_good_dataset.filename.split(os.sep)[-2] # e.g. 20181212_001
        elif 'hdf5' in self.first_good_dataset.filename: # mass_hdf5 file
        # elif not hasattr(reduced_data, 'hdf5_noisefile'): # if mass hdf5
            fname_base = self.first_good_dataset.filename.split(os.sep)[-1][:12]
        else: # my reduced data
            fname_base = self.first_good_dataset.filename.split(':')[-1].split('.')[0][1:]
        from datetime import datetime
        if all(self.first_good_dataset.p_filt_value[:]==0) or (fast==True): # case of fast calibration
            pkl_fname = fname_base + "_fast_cal_dict_" + datetime.now().strftime('%Y%m%d_%H%M%S') + ".pkl"
        else:
            pkl_fname = fname_base + "_cal_dict_" + datetime.now().strftime('%Y%m%d_%H%M%S') + ".pkl"
    else:
        if os.path.isfile(filename):
            raise Exception('Filename exists. Try a different name')
        else:
            pkl_fname = filename
    with open(pkl_fname, "wb") as f:
        pkl.dump(cal_matrix, f)
        print("# cal info written into %s" %pkl_fname)

mass.TESGroup.sj_save_cal = sj_save_cal


def sj_restore_bad_ch(self, why_str=None):
    """
    find channels flagged as failed calibration p_filt_value or p_filt_value_dc if why_str not given
    or flagged as why_str if given and flag them good
    example usage: data.sj_restore_bad_ch()
    data.sj_restore_bad_ch('failed sj_calibrate')
    """
    chans_to_restore = []
    for chan in self.why_chan_bad.keys():
        if why_str is None:
            if ('failed calibration p_filt_value' in self.why_chan_bad[chan]) or ('failed calibration p_filt_value_dc' in self.why_chan_bad[chan]):
                chans_to_restore.append(chan)
        else:
            if (why_str in self.why_chan_bad[chan]) or (why_str in self.why_chan_bad[chan][0]):
                chans_to_restore.append(chan)

    self.set_chan_good(chans_to_restore)

mass.TESGroup.sj_restore_bad_ch = sj_restore_bad_ch


# def sj_save_rixs_dict_rixs():
# maybe not needed because I can just do
# for rixs_dict in rixs_dict_list:
#     if rixs_dict is not None:
#         useful_ftns.dict2hkl(rixs_dict)


def sj_save_reduced(self, filename=None, file_format='hdf5', save_raw=True, good_only=True):

    """
    saves time stamp (p_peak_time), raw ph (p_peak_value),
    non-zero p_filt* (p_filt_value, p_filt_value_dc) p_energy*
    usage: data.sj_save_reduced()
    """

    if filename is None:
        from datetime import datetime
        filename = self.filenames[0].split(os.sep)[-2] + "_reduced_" + datetime.now().strftime('%Y%m%d_%H%M%S')
        filename += "."+file_format
    else:
        if os.path.isfile(filename):
            raise Exception('Filename exists. Try a different name')

    p_energy_family = []
    p_filt_family = []
    for parameter in self.channel[self.good_channels[0]].__dict__.keys():
        if 'p_energy' in parameter:
            p_energy_family = np.append(p_energy_family,parameter)
        if 'p_filt_value' in parameter:
            p_filt_family = np.append(p_filt_family,parameter)

    if file_format == 'hdf5':
        import h5py
    elif file_format == 'pkl':
        try:
            import cPickle
        except:
            import pickle as cPickle # probably a bad practice

    if file_format == 'hdf5':
        # f = h5py.File(filename, mode='w')
        with h5py.File(filename, mode='w') as f:
            for ds in self:
                if good_only:
                    mask = ds.good()
                else:
                    mask = np.ones_like(ds.good())
                ch_grp = f.require_group(str(ds.channum))
                if not all(ds.p_timestamp[:]==0):
                    ch_grp.create_dataset('p_timestamp', data=ds.p_timestamp[mask], dtype=np.dtype('float64'))
                    #ch_grp.require_dataset('p_timestamp', data=ds.p_timestamp[mask], dtype=np.dtype('float64'))
                if not all(ds.p_peak_value[:]==0):
                    ch_grp.create_dataset('p_peak_value', data=ds.p_peak_value[mask], dtype=np.dtype('u2'))
                    #ch_grp.require_dataset('p_peak_value', data=ds.p_peak_value[mask], dtype=np.dtype('u2'))
                if not all(ds.p_pretrig_mean[:]==0):
                    ch_grp.create_dataset('p_pretrig_mean', data=ds.p_pretrig_mean[mask], dtype=np.dtype('u2'))
                    #ch_grp.require_dataset('p_peak_value', data=ds.p_peak_value[mask], dtype=np.dtype('u2'))

                nonzero_p_filt_counter = 0
                for filt_value_param in p_filt_family:
                    # if not all(eval("ds.%s"%filt_value_param)[:]==0):
                    if not all(ds.__dict__[filt_value_param][:]==0):
                        # ch_grp.create_dataset(filt_value_param, data=eval("ds.%s"%filt_value_param)[mask], dtype=np.dtype('float32'))
                        ch_grp.create_dataset(filt_value_param, data=ds.__dict__[filt_value_param][mask], dtype=np.dtype('float32'))
                        #ch_grp.require_dataset(filt_value_param, data=eval("ds.%s"%filt_value_param)[mask], dtype=np.dtype('float32'))
                        nonzero_p_filt_counter+=1
                if nonzero_p_filt_counter == 0:
                    print("data not optimal filtered yet")
                nonzero_p_energy_counter = 0
                for energy_param in p_energy_family:
                    #print energy_param
                    if not all(np.array(eval("ds.%s"%energy_param))==0):
                        # ch_grp.create_dataset(energy_param, data=np.array(eval("ds.%s"%energy_param))[mask], dtype=np.dtype('float32'))
                        ch_grp.create_dataset(energy_param, data=np.array(ds.__dict__[energy_param])[mask], dtype=np.dtype('float32'))
                        #ch_grp.require_dataset(energy_param, data=eval("ds.%s"%energy_param)[mask], dtype=np.dtype('float32'))
                        nonzero_p_energy_counter+=1
                if nonzero_p_energy_counter == 0:
                    print("no available calibrated energy found")
        # f.close()
        print("# Reduced hdf5 file %s created"%filename)
    elif file_format == 'pkl': # this part is not done
        reduced_data_dic = {}
        reduced_data_matrix_ds = []
        for ds in self:
            reduced_data_dic[ds.channum] = {}

        with open(filename, "wb") as f:
            cPickle.dump(reduced_data_dic, f, protocol=2)
            print("# Reduced data written into %s" %filename)

mass.TESGroup.sj_save_reduced = sj_save_reduced


def dict2pkl(data_dict, dir_p_or_prefix=None, protocol=-1, compress=True, separate_files=False):
    """
    # consider using dict2hkl, which saves into a hdf5 file
    # example: useful_ftns.dict2pkl(rixs_plane, '20161207_003')
    # to create one file for a dict of rixs_dict: uf.dict2pkl(rixs_dict) or uf.dict2pkl(rixs_dict, 'CDW')
    # to create separate files for a dict of rixs_dict: uf.dict2pkl(rixs_dict, separate_files=True), where rixs_dict is a dict of rixs_dict, in which case it is not needed to provide dir_p
    """
    try:
        import cPickle
    except:
        import pickle as cPickle # probably a bad practice
    import gzip
    from datetime import datetime

    if separate_files:
        str_key_list = [key for key in data_dict.keys() if (type(key) is str)]  # added to ignore alias integer keys
        if np.all(['201' in key for key in str_key_list]) or np.all(['202' in key for key in str_key_list]): # this being true means data_dict is a dictionary of rixs_dict. Can be one rixs_dict dictionary
            for key in str_key_list:
                dict2pkl(data_dict[key], dir_p_or_prefix=key, protocol=protocol, compress=compress) # recursive
            return

    if dir_p_or_prefix is None:
        try:
            dir_p_or_prefix = data_dict['dir_p'] + '_data_dict_'
        except:
            dir_p_or_prefix = 'data_dict_'
    else:
        dir_p_or_prefix = dir_p_or_prefix + '_data_dict_'

    if compress is True:
        pkl_fname = dir_p_or_prefix + datetime.now().strftime('%Y%m%d_%H%M%S') + ".pkl.gz"
        #with gzip.open(pkl_fname, "wb") as f: # this is same as below because open is Shorthand for GzipFile
        with gzip.GzipFile(pkl_fname, "wb") as f:
            cPickle.dump(data_dict, f, protocol)
            print("# saved into %s" %pkl_fname)
    else:
        pkl_fname = dir_p_or_prefix + datetime.now().strftime('%Y%m%d_%H%M%S') + ".pkl"
        with open(pkl_fname, "wb") as f:
            cPickle.dump(data_dict, f, protocol)
            print("# saved into %s" %pkl_fname)


def pkl2dict(pkl_fname):
    """
    example: useful_ftns.pkl2dict('filename.pkl.gz')
    """
    try:
        import cPickle
    except:
        import pickle as cPickle # probably a bad practice
    import gzip
    from datetime import datetime

    try: # when loading compressed pkl
        #with gzip.open(pkl_fname, "rb") as f: # this is same as below because open is Shorthand for GzipFile
        with gzip.GzipFile(pkl_fname, "rb") as f:
            try:
                data_dict = cPickle.load(f)
                print("# gzipped %s loaded" %pkl_fname)
            except UnicodeDecodeError:
                data_dict = cPickle.load(f, encoding='latin1')
                print("# gzipped (likely python2 saved) %s loaded" %pkl_fname)
    except: # when loading regular pkl
        with open(pkl_fname, "rb") as f:
            try:
                data_dict = cPickle.load(f)
                print("# %s loaded" %pkl_fname)
            except UnicodeDecodeError:
                data_dict = cPickle.load(f, encoding='latin1')
                print("# (likely python2 saved) %s loaded" %pkl_fname)
    return data_dict


def dict2hkl(data_dict, dir_p_or_prefix=None, filename=None, timestamp=True, compress=True, separate_files=False):
    """
    # based on hkl.dump(array_obj, 'test_gzip.hkl', mode='w', compression='gzip')
    # example: useful_ftns.dict2hkl(rixs_plane, '20161207_003')
    #  to create one file for a dict of rixs_dict: uf.dict2hkl(rixs_dict) or uf.dict2hkl(rixs_dict, 'CDW')
    # to create seperate files for a dict of rixs_dict: uf.dict2hkl(rixs_dict, separate_files=True)

    """

    if separate_files:
        str_key_list = [key for key in data_dict.keys() if (type(key) is str)]  # added to ignore alias integer keys
        if np.all(['201' in key for key in str_key_list]) or np.all(['202' in key for key in str_key_list]): # this being true means data_dict is a dictionary of rixs_dict. Can be one rixs_dict dictionary
            for key in str_key_list:
                dict2hkl(data_dict[key], dir_p_or_prefix=key, filename=None, timestamp=timestamp, compress=compress) # recursive
            return

    from datetime import datetime

    if filename is None:
        if dir_p_or_prefix is None:
            try:
                prefix =  data_dict['dir_p'] + '_data_dict'
            except:
                prefix = 'data_dict'
        else:
            prefix = dir_p_or_prefix + '_data_dict'
    else:
        prefix = filename

    if timestamp:
        hkl_fname = prefix + "_" + datetime.now().strftime('%Y%m%d_%H%M%S') + ".hkl"
    else:
        hkl_fname = prefix + ".hkl"

    if compress is True:
        hkl.dump(data_dict, hkl_fname, mode='w', compression='gzip')
        print("# saved into %s with compression" %hkl_fname)
    else:
        hkl.dump(data_dict, hkl_fname, mode='w')
        print("# saved into %s" %hkl_fname)


def hkl2dict(hkl_fname):
    """
    example: useful_ftns.hkl2dict('filename.hkl')
    """
    import hickle as hkl

    data_dict = hkl.load(hkl_fname)
    print("# hkl file loaded")

    return data_dict


def dict2mat(data_dict, mat_fname=None, mat_fname_suffix='', dont_save_partial=True, save_individual_scan=False, select_rixs_planes=None, spot_ids=None):
    """
    # save python any dict or rixs_dict_list to matlab mat file
    # variable with none value as well as those with zeros are dropped
    # scipy.io.savemat doesn't seem to like a dictionary with integer keys
    # savemat seems to save int as [[int]] and array as np.array(array), or maybe loadmat does that..
    # dont_save_partial only saves the whole array map
    # save_individual_scan is True saves scan by scan RIXS maps
    # select_rixs_planes=None is to choose certain rixs_planes only. e.g. rixs_plane1_norm
    # spot_ids=None added, but this should be used with individual rixs map dict (not a dict of dicts)
    # and with save_individual_scan=False
    """
    import scipy.io as sio
    dict_for_matlab = {}

    str_key_list = [key for key in data_dict.keys() if (type(key) is str)]  # added to ignore alias integer keys
    if np.all(['201' in key for key in str_key_list]) or np.all(['202' in key for key in str_key_list]): # this being true means data_dict is a dictionary of rixs_dict. Can be one rixs_dict dictionary
        for key in str_key_list:
            dict2mat(data_dict[key], mat_fname=key+mat_fname_suffix, dont_save_partial=dont_save_partial, save_individual_scan=save_individual_scan, select_rixs_planes=select_rixs_planes) # recursive
        return

    if mat_fname is not None:
        if '.mat' not in mat_fname:
            mat_fname = mat_fname + mat_fname_suffix + '.mat'

    for key in str_key_list: # python3 and 2 compatible
        if (mat_fname is None) and (key == 'dir_p'):
            if type(data_dict[key]) is str:
                mat_fname = data_dict[key] + mat_fname_suffix + '.mat'
            elif type(data_dict[key]) is list:
                mat_fname = data_dict[key][0] + '-' + data_dict[key][-1] + mat_fname_suffix + '.mat'
        if dont_save_partial is True:
            if 'part_arr' in key:
                continue
        if data_dict[key] is not None: # drop Nones because matlab does not like them
            if select_rixs_planes is not None:
                if type(select_rixs_planes) is str:
                    select_rixs_planes = [select_rixs_planes]
                if key in select_rixs_planes:
                    if np.sum(data_dict[key]) != 0:
                        if save_individual_scan is False:
                            if spot_ids is None: # sum over every spot/scan
                                dict_for_matlab[key] = np.array([np.sum(data_dict[key], axis=0)])
                            else:
                                if type(spot_ids) is int:
                                    spot_ids = [spot_ids]
                                dict_for_matlab[key] = np.array([np.sum(data_dict[key][spot_ids,:,:], axis=0)])
                        else:
                            dict_for_matlab[key] = data_dict[key]
            else:
                if 'rixs_plane' in key:
                    if np.sum(data_dict[key]) != 0:
                        if save_individual_scan is False:
                            if spot_ids is None: # sum over every spot/scan
                                dict_for_matlab[key] = np.array([np.sum(data_dict[key], axis=0)])
                            else:
                                if type(spot_ids) is int:
                                    spot_ids = [spot_ids]
                                dict_for_matlab[key] = np.array([np.sum(data_dict[key][spot_ids,:,:], axis=0)])
                        else:
                            dict_for_matlab[key] = data_dict[key]
            if 'rixs_plane' not in key:
                dict_for_matlab[key] = data_dict[key]
            # except TypeError:
            #     print('The following key not saved:', key)
    sio.savemat(mat_fname, dict_for_matlab)
    print('# dictionary saved into %s'%mat_fname)


def dict2mat4gui(data_dict, mat_fname=None, mat_fname_suffix='_gui', dont_save_partial=True, save_individual_rixs=False):
    """
    similar to dict2mat, intended to be used in matlab gui
    """
    import scipy.io as sio
    dict_for_matlab = {}

    str_key_list = [key for key in data_dict.keys() if (type(key) is str)]  # added to ignore alias integer keys
    if np.all(['201' in key for key in str_key_list]) or np.all(['202' in key for key in str_key_list]): # this being true means data_dict is a dictionary of rixs_dict. Can be one rixs_dict dictionary
        for key in str_key_list:
            dict2mat4gui(data_dict[key], mat_fname=key+mat_fname_suffix, dont_save_partial=dont_save_partial, save_individual_rixs=save_individual_rixs) # recursive
        return

    if mat_fname is not None: # if file name is explicitly given, do not add suffix
        if '.mat' not in mat_fname:
            mat_fname = mat_fname + '.mat'
    else:
        mat_fname = data_dict['dir_p'] + mat_fname_suffix + '.mat'

    dict_for_matlab['scan_numbers'] = data_dict['scan_numbers']
    dict_for_matlab['energy_bins'] = data_dict['bin_centers']
    dict_for_matlab['motor_list_scan%s'%(data_dict['scan_numbers'][0])] = data_dict['nominal_mono']
    dict_for_matlab['maps_norm_total'] = np.transpose(np.sum(data_dict['rixs_plane0_norm'], axis=0))

    sio.savemat(mat_fname, dict_for_matlab)
    print('# dictionary saved into %s'%mat_fname)


def mat2dict(mat_fname):
    """
    # reads mat (matlab) file and drop __header__, __version__, __globals__ keys
    """
    import scipy.io as sio
    data_dict = sio.loadmat(mat_fname)
    not_used_mat_keys = ['__header__', '__version__', '__globals__']
    for key in not_used_mat_keys:
        if key in data_dict.keys():
            print(key.strip('_'), ':', data_dict[key])
            data_dict.pop(key)
    for key, value in data_dict.items():
        if (len(np.shape(value)) == 2) and (np.shape(value)[0] == 1): # this is to handle savemat's saving a length-n array into (1,n) array
            data_dict[key] = value[0]
    return data_dict


def sj_plot_stacked_xes_old(self, category=None, bin_start=0, bin_end=1200, bin_width=0.25, max_num_pixel=5, xlim=None, semilogy=True, cal_lines=None, fig_big_full='big', plot_corr=True, corr_threshold=None, corr_cut_percentile=None, dont_plot_xes=False):
    '''
    try using bin_end, bin_width, and xlim = [x1, x2]
    return corr_dict, raw_diff_sq_dict, log_diff_sq_dict, bad_corr_chans, bad_diff_chans
    category cut possible
    '''

    if category is None:
        category = {'calibration': 'in'}

    rainbow_7colors = [cm.rainbow_r(x) for x in np.linspace(0,1.,max_num_pixel)]

    # bin_start=0
    coadded_bin_edges = np.arange(bin_start, bin_end+bin_width, bin_width)
    coadded_bin_centers = (coadded_bin_edges[1:]+coadded_bin_edges[:-1])*0.5

    if xlim is None:
        xlim = [coadded_bin_centers[0], coadded_bin_centers[-1]]

    coadded_counts_dc = np.zeros_like(coadded_bin_centers, dtype="int") # forgot why the parameter name has dc
    nused = 0
    try:
        for ds in self:
            if len(ds.good())>0:
                dscounts, _ = np.histogram(ds.p_energy_from_cal_dc[ds.good(**category) & ~np.isnan(ds.p_energy_from_cal_dc)], coadded_bin_edges) # not sure why nan occurs
                coadded_counts_dc += dscounts
                nused += 1
    except AttributeError:
        try:
            for ds in self:
                if len(ds.good())>0:
                    dscounts, _ = np.histogram(ds.p_energy_spl[ds.good(**category) & ~np.isnan(ds.p_energy_spl)], coadded_bin_edges)
                    coadded_counts_dc += dscounts
                    nused += 1
        except AttributeError:
            for ds in self:
                if len(ds.good())>0:
                    dscounts, _ = np.histogram(ds.p_energy[ds.good(**category) & ~np.isnan(ds.p_energy)], coadded_bin_edges)
                    coadded_counts_dc += dscounts
                    nused += 1

    if dont_plot_xes is not True:
        plt.figure()
        plt.plot(coadded_bin_centers, coadded_counts_dc, drawstyle="steps-mid", color='k')
        plt.title('%d pixels'%nused)
        plt.xlabel("Energy (eV)")
        plt.ylabel("Counts per %0.2f eV bin"%(coadded_bin_edges[1]-coadded_bin_edges[0]))

    k=0
    dss = [ds for ds in self]
    counts_dict = {}
    corr_dict = {}
    raw_diff_sq_dict = {}
    log_diff_sq_dict = {}
    bad_corr_chans = []
    bad_raw_diff_chans = []
    bad_log_diff_chans = []
    while k<len(dss):
        if dont_plot_xes is not True:
            if fig_big_full == 'big':
                plt.figure(figsize=(12,9))
                # fig, ax = plt.subplots(1,2, figsize=(26,9))
            elif fig_big_full == 'full':
                plt.figure()
                fig_manager = plt.get_current_fig_manager()
                fig_manager.full_screen_toggle()
            else:
                plt.figure()
        i=0
        max_height=0
        while i<max_num_pixel and k<len(dss):
            ds=dss[k]
            try:
                counts,_ = np.histogram(ds.p_energy_from_cal_dc[ds.good(**category) & ~np.isnan(ds.p_energy_from_cal_dc)], coadded_bin_edges) # not sure why nan occurs
            except AttributeError:
                try:
                    counts,_ = np.histogram(ds.p_energy_spl[ds.good(**category) & ~np.isnan(ds.p_energy_spl)], coadded_bin_edges)
                except AttributeError:
                    counts,_ = np.histogram(ds.p_energy[ds.good(**category) & ~np.isnan(ds.p_energy)], coadded_bin_edges)
            # counts=np.array(map(float,counts))
            counts_dict[ds.channum]=np.asarray(counts,dtype=float)
            max_height = np.max(counts[int(xlim[0]/bin_width):int(xlim[1]/bin_width)]) if np.max(counts[int(xlim[0]/bin_width):int(xlim[1]/bin_width)]) > max_height else max_height
            corr_dict[ds.channum] = np.max(np.correlate(coadded_counts_dc/np.max(coadded_counts_dc), counts/np.max(counts)))
            raw_diff_sq_dict[ds.channum] = np.sum((np.asfarray(coadded_counts_dc[(coadded_counts_dc!=0)&(counts!=0)])/np.sum(coadded_counts_dc)-np.asfarray(counts[(coadded_counts_dc!=0)&(counts!=0)])/np.sum(counts))**2)#/np.sum(coadded_counts_dc)
            log_diff_sq_dict[ds.channum] = np.sum((np.log(np.asfarray(coadded_counts_dc[(coadded_counts_dc!=0)&(counts!=0)])/np.sum(coadded_counts_dc))-np.log(np.asfarray(counts[(coadded_counts_dc!=0)&(counts!=0)])/np.sum(counts)))**2)#/np.sum(coadded_counts_dc)
            if corr_threshold is not None:
                if corr_dict[ds.channum] < corr_threshold:
                    bad_corr_chans.append(ds.channum)
            if dont_plot_xes is not True:
                if semilogy is True:
                    counts_nonzero = counts[:]
                    counts_nonzero[counts_nonzero==0]=0.1
                    plt.semilogy(coadded_bin_centers[int(xlim[0]/bin_width):int(xlim[1]/bin_width)], counts_nonzero[int(xlim[0]/bin_width):int(xlim[1]/bin_width)], drawstyle="steps-mid", label=str(ds.channum), color=rainbow_7colors[i])
                else:
                    plt.plot(coadded_bin_centers[int(xlim[0]/bin_width):int(xlim[1]/bin_width)], counts_nonzero[int(xlim[0]/bin_width):int(xlim[1]/bin_width)], drawstyle="steps-mid", label=str(ds.channum), color=rainbow_7colors[i])
            i+=1
            k+=1
            if dont_plot_xes is not True:
                if (i==max_num_pixel) or (k==len(dss)):
                    # coadded_counts_dc=np.array(map(float,coadded_counts_dc))
                    coadded_counts_dc_nonzero=np.asarray(coadded_counts_dc,dtype=float)
                    coadded_counts_dc_nonzero[coadded_counts_dc_nonzero==0]=0.1
                    plt.plot(coadded_bin_centers[int(xlim[0]/bin_width):int(xlim[1]/bin_width)], coadded_counts_dc_nonzero[int(xlim[0]/bin_width):int(xlim[1]/bin_width)]/float(np.max(coadded_counts_dc_nonzero[int(xlim[0]/bin_width):int(xlim[1]/bin_width)]))*max_height, label="coadded_spl_dc", linestyle=':', color='black')
                    #plt.sj_vlines(calibration_energies, '--r')
                    max_height=0
        if dont_plot_xes is not True:
            plt.xlabel("energy (eV)")
            plt.ylabel("counts per %0.2f eV bin"%(coadded_bin_edges[1]-coadded_bin_edges[0]))
            plt.legend(loc='best', fontsize='small')
            plt.grid(True)
            if cal_lines is not None:
                plt.sj_vlines(cal_lines)

    corr_median = np.median([corr_dict[ch] for ch in corr_dict.keys()])
    corr_std = np.std([corr_dict[ch] for ch in corr_dict.keys()])

    raw_diff_median = np.median([raw_diff_sq_dict[ch] for ch in raw_diff_sq_dict.keys()])
    raw_diff_std = np.std([raw_diff_sq_dict[ch] for ch in raw_diff_sq_dict.keys()])
    log_diff_median = np.median([log_diff_sq_dict[ch] for ch in log_diff_sq_dict.keys()])
    log_diff_std = np.std([log_diff_sq_dict[ch] for ch in log_diff_sq_dict.keys()])

    if corr_threshold is None:
        if corr_cut_percentile is not None:
            corr_threshold = np.percentile(list(corr_dict.values()), corr_cut_percentile)
            bad_corr_chans = [channum for channum, corr_val in corr_dict.items() if corr_val < corr_threshold]
        else:
            for ch in self.good_channels:
                if corr_dict[ch] < corr_median - corr_std:
                    bad_corr_chans.append(ch)

    for ch in self.good_channels:
        if raw_diff_sq_dict[ch] > raw_diff_median + raw_diff_std:
            bad_raw_diff_chans.append(ch)
        if log_diff_sq_dict[ch] > log_diff_median + log_diff_std:
            bad_log_diff_chans.append(ch)
    bad_diff_chans = sorted(list(set(bad_raw_diff_chans).intersection(set(bad_log_diff_chans))))
    # else:
    #     bad_corr_chans = None

    if plot_corr is True:
        plt.figure()
        # plt.hist(corr_dict.values(), bins=ss_opt_num_bin(list(corr_dict.values())))
        plt.hist(list(corr_dict.values()), bins=int(self.n_good_channels()/2))
        plt.xlabel('Maximum crorr-correlation')
        plt.ylabel('Counts per bin')

        plt.figure()
        for bad_corr_chan in bad_corr_chans:
            plt.plot(coadded_bin_centers, counts_dict[bad_corr_chan], label=str(bad_corr_chan))
        plt.legend(fontsize='small')
        plt.title('Small max(cross-corr) channel spectra')

        plt.figure()
        for bad_diff_chan in bad_diff_chans:
            plt.plot(coadded_bin_centers, counts_dict[bad_diff_chan], label=str(bad_diff_chan))
        plt.legend(fontsize='small')
        plt.title('Large diff(coadded, individual) channel spectra')

    return corr_dict, raw_diff_sq_dict, log_diff_sq_dict, bad_corr_chans, bad_diff_chans

mass.TESGroup.sj_plot_stacked_xes_old = sj_plot_stacked_xes_old


def sj_plot_stacked_xes(self, category=None, bin_start=0, bin_end=1200, bin_width=0.25, last_chan=None, max_num_pixel=5, semilogy=True, cal_lines=None, fig_big_full='big', normalize=True, plot_xes=True, plot_corr=True, corr_threshold=None, corr_cut_percentile=None, cal_method='default'):
    '''
    return corr_dict, raw_diff_sq_dict, log_diff_sq_dict, bad_corr_chans, bad_diff_chans
    category cut possible
    plot_xes=False is used when one wants to see only the bad chan plots
    '''

    if sum([sum(ds.p_energy[:]) for ds in self]) == 0:
        print('\ndata not calibrated yet')
        return

    if category is None:
        category = {'calibration': 'in'}

    rainbow_7colors = [cm.rainbow_r(x) for x in np.linspace(0,1.,max_num_pixel)]

    if hasattr(self, 'filenames'): # ljh files
        dir_p = self.first_good_dataset.filename.split(os.sep)[-2] # e.g. 20181212_001
    elif 'hdf5' in self.first_good_dataset.filename: # mass_hdf5 file
    # elif not hasattr(reduced_data, 'hdf5_noisefile'): # if mass hdf5
        dir_p = self.first_good_dataset.filename.split(os.sep)[-1][:12]
    else: # my reduced data
        dir_p = self.first_good_dataset.filename.split(':')[-1].split('.')[0][1:]

    # bin_start=0
    coadded_bin_edges = np.arange(bin_start, bin_end+bin_width, bin_width)
    coadded_bin_centers = (coadded_bin_edges[1:]+coadded_bin_edges[:-1])*0.5

    coadded_counts_dc = np.zeros_like(coadded_bin_centers, dtype="int") # forgot why the parameter name has dc
    nused = 0
    for ds in self:
        if len(ds.good())>0:
            if cal_method == 'default':
                dscounts, _ = np.histogram(ds.p_energy[ds.good(**category) & ~np.isnan(ds.p_energy)], coadded_bin_edges) # not sure why nan occurs
            elif cal_method == 'spl':
                dscounts, _ = np.histogram(ds.p_energy_spl[ds.good(**category) & ~np.isnan(ds.p_energy_spl)], coadded_bin_edges) # not sure why nan occurs
            elif 'poly' in cal_method:
                dscounts, _ = np.histogram(ds.p_energy_sj_poly_4th[ds.good(**category) & ~np.isnan(ds.p_energy_sj_poly_4th)], coadded_bin_edges) # not sure why nan occurs
            coadded_counts_dc += dscounts
            nused += 1
    # try:
    #     for ds in self:
    #         if len(ds.good())>0:
    #             dscounts, _ = np.histogram(ds.p_energy_from_cal_dc[ds.good(**category) & ~np.isnan(ds.p_energy_from_cal_dc)], coadded_bin_edges) # not sure why nan occurs
    #             coadded_counts_dc += dscounts
    #             nused += 1
    # except AttributeError:
    #     try:
    #         for ds in self:
    #             if len(ds.good())>0:
    #                 dscounts, _ = np.histogram(ds.p_energy_spl[ds.good(**category) & ~np.isnan(ds.p_energy_spl)], coadded_bin_edges)
    #                 coadded_counts_dc += dscounts
    #                 nused += 1
    #     except AttributeError:
    #         for ds in self:
    #             if len(ds.good())>0:
    #                 dscounts, _ = np.histogram(ds.p_energy[ds.good(**category) & ~np.isnan(ds.p_energy)], coadded_bin_edges)
    #                 coadded_counts_dc += dscounts
    #                 nused += 1

    if plot_xes is True:
        plt.figure()
        plt.plot(coadded_bin_centers, coadded_counts_dc, drawstyle="steps-mid", color='k')
        plt.title('%d pixels'%nused)
        plt.xlabel("Energy (eV)")
        plt.ylabel("Counts per %0.2f eV bin"%(coadded_bin_edges[1]-coadded_bin_edges[0]))

    counts_dict = {}
    corr_dict = {}
    raw_diff_sq_dict = {}
    log_diff_sq_dict = {}
    bad_corr_chans = []
    bad_raw_diff_chans = []
    bad_log_diff_chans = []

    if last_chan is None:
        last_chan = 9999
    for ii, ds in enumerate(self):
        if ds.channum <= last_chan:
            if ii%(4*max_num_pixel) == 0:
                if plot_xes is True:
                    if fig_big_full == 'big':
                        fig, ax = plt.subplots(2, 2, figsize=(16,12), sharex=True)
                        fig.subplots_adjust(left=0.05, right=0.95, bottom=0.05, top=0.95, wspace=0.1, hspace=0.1)
                        # fig, ax = plt.subplots(1,2, figsize=(26,9))
                    elif fig_big_full == 'full':
                        fig, ax = plt.subplots(2, 2, sharex=True)
                        fig.subplots_adjust(left=0.05, right=0.95, bottom=0.05, top=0.95, wspace=0.1, hspace=0.1)
                        fig_manager = plt.get_current_fig_manager()
                        fig_manager.full_screen_toggle()
                    else:
                        fig, ax = plt.subplots(2, 2, sharex=True)
                        fig.subplots_adjust(left=0.05, right=0.95, bottom=0.05, top=0.95, wspace=0.1, hspace=0.1)
                    fig.suptitle(dir_p)

            if ii%max_num_pixel == 0:
                max_height = 0
            if cal_method == 'default':
                counts,_ = np.histogram(ds.p_energy[ds.good(**category) & ~np.isnan(ds.p_energy)], coadded_bin_edges) # not sure why nan occurs
            elif cal_method == 'spl':
                counts,_ = np.histogram(ds.p_energy_spl[ds.good(**category) & ~np.isnan(ds.p_energy_spl)], coadded_bin_edges) # not sure why nan occurs
            elif 'poly' in cal_method:
                counts,_ = np.histogram(ds.p_energy_sj_poly_4th[ds.good(**category) & ~np.isnan(ds.p_energy_sj_poly_4th)], coadded_bin_edges) # not sure why nan occurs
            # try:
            #     counts,_ = np.histogram(ds.p_energy_from_cal_dc[ds.good(**category) & ~np.isnan(ds.p_energy_from_cal_dc)], coadded_bin_edges) # not sure why nan occurs
            # except AttributeError:
            #     try:
            #         counts,_ = np.histogram(ds.p_energy_spl[ds.good(**category) & ~np.isnan(ds.p_energy_spl)], coadded_bin_edges)
            #     except AttributeError:
            #         counts,_ = np.histogram(ds.p_energy[ds.good(**category) & ~np.isnan(ds.p_energy)], coadded_bin_edges)
            #     # counts=np.array(map(float,counts))
            counts_dict[ds.channum]=np.asarray(counts,dtype=float)
            max_height = np.max(counts) if np.max(counts) > max_height else max_height
            corr_dict[ds.channum] = np.max(np.correlate(coadded_counts_dc/np.max(coadded_counts_dc), counts/np.max(counts)))
            raw_diff_sq_dict[ds.channum] = np.sum((np.asfarray(coadded_counts_dc[(coadded_counts_dc!=0)&(counts!=0)])/np.sum(coadded_counts_dc)-np.asfarray(counts[(coadded_counts_dc!=0)&(counts!=0)])/np.sum(counts))**2)#/np.sum(coadded_counts_dc)
            log_diff_sq_dict[ds.channum] = np.sum((np.log(np.asfarray(coadded_counts_dc[(coadded_counts_dc!=0)&(counts!=0)])/np.sum(coadded_counts_dc))-np.log(np.asfarray(counts[(coadded_counts_dc!=0)&(counts!=0)])/np.sum(counts)))**2)#/np.sum(coadded_counts_dc)
            if corr_threshold is not None:
                if corr_dict[ds.channum] < corr_threshold:
                    bad_corr_chans.append(ds.channum)
            if plot_xes is True:
                if semilogy is True:
                    counts_nonzero = counts[:]
                    counts_nonzero[counts_nonzero==0]=0.01 # this is to make semilogy look better
                    if normalize is True:
                        ax[(ii//(max_num_pixel*2))%2,(ii//(max_num_pixel))%2].semilogy(coadded_bin_centers, counts_nonzero/max(counts_nonzero), drawstyle="steps-mid", label=str(ds.channum), color=rainbow_7colors[ii%max_num_pixel])
                    else:
                        ax[(ii//(max_num_pixel*2))%2,(ii//(max_num_pixel))%2].semilogy(coadded_bin_centers, counts_nonzero, drawstyle="steps-mid", label=str(ds.channum), color=rainbow_7colors[ii%max_num_pixel])
                else:
                    if normalize is True:
                        ax[(ii//(max_num_pixel*2))%2,(ii//(max_num_pixel))%2].plot(coadded_bin_centers, counts/max(counts), drawstyle="steps-mid", label=str(ds.channum), color=rainbow_7colors[ii%max_num_pixel])
                    else:
                        ax[(ii//(max_num_pixel*2))%2,(ii//(max_num_pixel))%2].plot(coadded_bin_centers, counts, drawstyle="steps-mid", label=str(ds.channum), color=rainbow_7colors[ii%max_num_pixel])
                if (ii%max_num_pixel == max_num_pixel-1) or (ii == len(self.good_channels)-1):
                    # coadded_counts_dc=np.array(map(float,coadded_counts_dc))
                    coadded_counts_dc_nonzero=np.asarray(coadded_counts_dc,dtype=float)
                    coadded_counts_dc_nonzero[coadded_counts_dc_nonzero==0]=0.01 # this is to make semilogy look better
                    if normalize is True:
                        ax[(ii//(max_num_pixel*2))%2,(ii//(max_num_pixel))%2].plot(coadded_bin_centers, coadded_counts_dc_nonzero/float(np.max(coadded_counts_dc_nonzero)), label="coadded_spl_dc", linestyle=':', color='black')
                    else:
                        ax[(ii//(max_num_pixel*2))%2,(ii//(max_num_pixel))%2].plot(coadded_bin_centers, coadded_counts_dc_nonzero/float(np.max(coadded_counts_dc_nonzero))*max_height, label="coadded_spl_dc", linestyle=':', color='black')
                    if semilogy is True:
                        if normalize is True:
                            ax[(ii//(max_num_pixel*2))%2,(ii//(max_num_pixel))%2].set_ylim(bottom=1/float(np.max(coadded_counts_dc_nonzero)))
                        else:
                            ax[(ii//(max_num_pixel*2))%2,(ii//(max_num_pixel))%2].set_ylim(bottom=0.01)
                    if cal_lines is not None:
                        ylim = ax[(ii//(max_num_pixel*2))%2,(ii//(max_num_pixel))%2].get_ylim()
                        ax[(ii//(max_num_pixel*2))%2,(ii//(max_num_pixel))%2].vlines(cal_lines, ylim[0], ylim[1])
                if ((ii//max_num_pixel)%4 in [2, 3]):
                    ax[(ii//(max_num_pixel*2))%2,(ii//(max_num_pixel))%2].set_xlabel("energy (eV)")
                if ((ii//max_num_pixel)%2 == 0):
                    ax[(ii//(max_num_pixel*2))%2,(ii//(max_num_pixel))%2].set_ylabel("counts per %0.2f eV bin"%(coadded_bin_edges[1]-coadded_bin_edges[0]))
                ax[(ii//(max_num_pixel*2))%2,(ii//(max_num_pixel))%2].legend(loc='best', fontsize='small')
                ax[(ii//(max_num_pixel*2))%2,(ii//(max_num_pixel))%2].grid(True)


    corr_median = np.median([corr_dict[ch] for ch in corr_dict.keys()])
    corr_std = np.std([corr_dict[ch] for ch in corr_dict.keys()])

    raw_diff_median = np.median([raw_diff_sq_dict[ch] for ch in raw_diff_sq_dict.keys()])
    raw_diff_std = np.std([raw_diff_sq_dict[ch] for ch in raw_diff_sq_dict.keys()])
    log_diff_median = np.median([log_diff_sq_dict[ch] for ch in log_diff_sq_dict.keys()])
    log_diff_std = np.std([log_diff_sq_dict[ch] for ch in log_diff_sq_dict.keys()])

    if corr_threshold is None:
        if corr_cut_percentile is not None:
            corr_threshold = np.percentile(list(corr_dict.values()), corr_cut_percentile)
            bad_corr_chans = [channum for channum, corr_val in corr_dict.items() if corr_val < corr_threshold]
        else:
            for ch in self.good_channels:
                if ch <= last_chan:
                    if corr_dict[ch] < corr_median - 2 * corr_std:
                        bad_corr_chans.append(ch)

    for ch in self.good_channels:
        if ch <= last_chan:
            if raw_diff_sq_dict[ch] > raw_diff_median + raw_diff_std:
                bad_raw_diff_chans.append(ch)
            if log_diff_sq_dict[ch] > log_diff_median + log_diff_std:
                bad_log_diff_chans.append(ch)
    bad_diff_chans = sorted(list(set(bad_raw_diff_chans).intersection(set(bad_log_diff_chans))))
    # else:
    #     bad_corr_chans = None

    if plot_corr is True:
        plt.figure()
        # plt.hist(corr_dict.values(), bins=ss_opt_num_bin(list(corr_dict.values())))
        plt.hist(corr_dict.values(), bins=100)
        plt.xlabel('Maximum crorr-correlation')
        plt.ylabel('Counts per bin')
        plt.sj_vlines([corr_median, corr_median - 2 * corr_std])

        plt.figure()
        max_height = 0
        for bad_corr_chan in bad_corr_chans:
            max_height = np.max(counts_dict[bad_corr_chan]) if np.max(counts_dict[bad_corr_chan]) > max_height else max_height
            plt.plot(coadded_bin_centers, counts_dict[bad_corr_chan], label=str(bad_corr_chan))
        plt.plot(coadded_bin_centers, coadded_counts_dc/max(coadded_counts_dc)*max_height, label='coadded')
        plt.legend(fontsize='small')
        plt.title('Small max(cross-corr) channel spectra')

        plt.figure()
        max_height = 0
        for bad_diff_chan in bad_diff_chans:
            max_height = np.max(counts_dict[bad_diff_chan]) if np.max(counts_dict[bad_diff_chan]) > max_height else max_height
            plt.plot(coadded_bin_centers, counts_dict[bad_diff_chan], label=str(bad_diff_chan))
        plt.plot(coadded_bin_centers, coadded_counts_dc/max(coadded_counts_dc)*max_height, label='coadded')
        plt.legend(fontsize='small')
        plt.title('Large diff(coadded, individual) channel spectra')

    return corr_dict, raw_diff_sq_dict, log_diff_sq_dict, bad_corr_chans, bad_diff_chans

mass.TESGroup.sj_plot_stacked_xes = sj_plot_stacked_xes


def sj_plot_xes(self, channum=None, plot_coadded=True, energy_param='p_energy', bin_start=0, bin_end=1200, bin_width=0.25, color=None, semilogy=True, hold_on=False, return_only=False, category=None, cal_method='default'):
    '''
    useful_ftns.sj_plot_xes(data) if data is my reduced_data
    data.sj_plot_xes() if data is TESGroup
    category example:
    cal_mothod: 'default' (p_energy), 'spl' (p_energy_spl), 'poly' (p_energy_sj_poly), 'poly4' (p_energy_sj_poly_4th), 'poly5' (p_energy_sj_poly_5th)
    '''
    if sum([sum(ds.p_energy[:]) for ds in self]) == 0:
        print('\ndata not calibrated yet')
        return

    if category is None:
        category = {'calibration': 'in'}

    if hasattr(self, 'filenames'): # ljh files
        dir_p = self.first_good_dataset.filename.split(os.sep)[-2] # e.g. 20181212_001
    elif 'hdf5' in self.first_good_dataset.filename: # mass_hdf5 file
    # elif not hasattr(reduced_data, 'hdf5_noisefile'): # if mass hdf5
        dir_p = self.first_good_dataset.filename.split(os.sep)[-1][:12]
    else: # my reduced data
        dir_p = self.first_good_dataset.filename.split(':')[-1].split('.')[0][1:]

    coadded_bin_edges = np.arange(bin_start, bin_end+bin_width, bin_width) # change arange to linsapce?
    coadded_bin_centers = (coadded_bin_edges[1:]+coadded_bin_edges[:-1])*0.5

    # if xlim is None:
    #     xlim = [coadded_bin_centers[0], coadded_bin_centers[-1]]

    coadded_counts_dc = np.zeros_like(coadded_bin_centers, dtype="int")
    nused = 0
    # try:
    if isinstance(self, np.ndarray):
        ds0 = self[0]
        channums = [self[i].channum for i in range(len(self))]
    else:
        ds0 = self.first_good_dataset
    # if hasattr(ds0, 'p_energy_from_cal_dc'):
    #     for ds in self:
    #         if hasattr(ds, 'p_energy_from_cal_dc'):
    #             # if category is None:
    #             #     dscounts, _ = np.histogram(ds.p_energy_from_cal_dc[ds.good() & ~np.isnan(ds.p_energy_from_cal_dc)], coadded_bin_edges) # not sure why nan occurs
    #             # else:
    #             dscounts, _ = np.histogram(ds.p_energy_from_cal_dc[ds.good(**category) & ~np.isnan(ds.p_energy_from_cal_dc)], coadded_bin_edges) # not sure why nan occurs
    #             coadded_counts_dc += dscounts
    #             nused += 1
    # # except AttributeError:
    # elif hasattr(ds0, 'p_energy_spl'):
    #     for ds in self:
    #         if hasattr(ds, 'p_energy_spl'):
    #             # if category is None:
    #             #     dscounts, _ = np.histogram(ds.p_energy_spl[ds.good() & ~np.isnan(ds.p_energy_spl)], coadded_bin_edges) # not sure why nan occurs
    #             # else:
    #             dscounts, _ = np.histogram(ds.p_energy_spl[ds.good(**category) & ~np.isnan(ds.p_energy_spl)], coadded_bin_edges) # not sure why nan occurs
    #             coadded_counts_dc += dscounts
    #             nused += 1
    # elif sum(ds0.p_energy) != 0:
    #     for ds in self:
    #         dscounts, _ = np.histogram(ds.p_energy[ds.good(**category) & ~np.isnan(ds.p_energy)], coadded_bin_edges) # not sure why nan occurs
    #         coadded_counts_dc += dscounts
    #         nused += 1

    if cal_method == 'default':
        energy_param = energy_param # to help understand the code
    elif cal_method == 'spl':
        energy_param = energy_param + '_spl'
    elif cal_method == 'poly':
        energy_param = energy_param + '_sj_poly'
    elif cal_method == 'poly4':
        energy_param = energy_param + '_sj_poly_4th'
    elif cal_method == 'poly5':
        energy_param = energy_param + '_sj_poly_5th'

    for ds in self:
        dscounts, _ = np.histogram(ds.__dict__[energy_param][ds.good(**category) & ~np.isnan(ds.__dict__[energy_param])], coadded_bin_edges) # not sure why nan occurs
        coadded_counts_dc += dscounts
        nused += 1

    # coadded_counts_dc=np.array(map(float,coadded_counts_dc))
    coadded_counts_dc=np.asarray(coadded_counts_dc,dtype=float)
    # coadded_counts_dc[coadded_counts_dc==0]=0.1

    if return_only is False:
        if hold_on is False:
            plt.figure()
        ax1 = plt.subplot(111)

    if channum is None:
        if return_only is False:
            # ax1.plot(coadded_bin_centers[int(xlim[0]/bin_width):int(xlim[1]/bin_width)], coadded_counts_dc[int(xlim[0]/bin_width):int(xlim[1]/bin_width)], label="coadded", color=color)
            ax1.plot(coadded_bin_centers, coadded_counts_dc, label="coadded", color=color)
            plt.title('%s, Energy spectrum, %s channels coadded'%(dir_p, nused),fontsize='small')
            plt.xlabel("energy (eV)")
            plt.ylabel("counts per %0.2f eV bin"%(coadded_bin_edges[1]-coadded_bin_edges[0]))
            plt.legend(loc='best', fontsize='small')
            if semilogy is True:
                ax1.set_yscale('log')
            plt.draw()
        return coadded_bin_centers, coadded_counts_dc
    else:
        if (isinstance(channum, int)) or (isinstance(channum, np.int64)):
            channum = [channum]
        rainbow_7colors = [cm.rainbow_r(x) for x in np.linspace(0,1.,len(channum))]
        max_counts = []
        countss = []
        for ii, ch in enumerate(channum):
            if isinstance(self, np.ndarray):
                ds = self[get_index(channums, ch)[0]]
            else:
                ds = self.channel[ch]
            # try:
            #     counts,_ = np.histogram(ds.p_energy_from_cal_dc[ds.good(**category) & ~np.isnan(ds.p_energy_from_cal_dc)], coadded_bin_edges) # not sure why nan occurs
            # except AttributeError:
            #     try:
            #         counts,_ = np.histogram(ds.p_energy_spl[ds.good(**category) & ~np.isnan(ds.p_energy_spl)], coadded_bin_edges) # not sure why nan occurs
            #     except AttributeError:
            #         counts,_ = np.histogram(ds.p_energy[ds.good(**category) & ~np.isnan(ds.p_energy)], coadded_bin_edges) # not sure why nan occurs
            # counts=np.array(map(float,counts))
            counts, _ = np.histogram(ds.__dict__[energy_param][ds.good(**category) & ~np.isnan(ds.__dict__[energy_param])], coadded_bin_edges) # not sure why nan occurs
            counts=np.asarray(counts,dtype=float)
            # counts[counts==0]=0.1
            countss.append(counts)
            max_counts.append(np.max(counts))
            # max_counts.append(np.max(counts[int(xlim[0]/bin_width):int(xlim[1]/bin_width)]))
            if return_only is False:
                ax1.plot(coadded_bin_centers, counts, drawstyle="steps-mid", label=str(ds.channum), color=rainbow_7colors[ii])
                # ax1.plot(coadded_bin_centers[int(xlim[0]/bin_width):int(xlim[1]/bin_width)], counts[int(xlim[0]/bin_width):int(xlim[1]/bin_width)], drawstyle="steps-mid", label=str(ds.channum), color=color)
        if return_only is False:
            if plot_coadded is True:
                ax1.plot(coadded_bin_centers, coadded_counts_dc/float(np.max(coadded_counts_dc))*max(max_counts), label="coadded", linestyle=':', color='black')
            # ax1.plot(coadded_bin_centers[int(xlim[0]/bin_width):int(xlim[1]/bin_width)], coadded_counts_dc[int(xlim[0]/bin_width):int(xlim[1]/bin_width)]/float(np.max(coadded_counts_dc[int(xlim[0]/bin_width):int(xlim[1]/bin_width)]))*max(max_counts), label="coadded", linestyle=':', color='black')
            plt.title('%s, Energy spectrum, %s channels coadded'%(dir_p, nused),fontsize='small')
            plt.xlabel("energy (eV)")
            plt.ylabel("counts per %0.2f eV bin"%(coadded_bin_edges[1]-coadded_bin_edges[0]))
            plt.legend(loc='best', fontsize='small')
            if semilogy is True:
                ax1.set_yscale('log')
            plt.draw()
        return coadded_bin_centers, countss

mass.TESGroup.sj_plot_xes = sj_plot_xes


def plot_cal_lines(cal_dict=CAL_MIX3_TITUS, height=None):

    calibration_energies = sorted(cal_dict.values())
    calibration_lines = sorted(cal_dict.keys(), key=cal_dict.__getitem__)

    y_axis_max = plt.ylim()[1]
    y_axis_min = plt.ylim()[0]

    plt.sj_vlines(calibration_energies,'--r')
    if plt.gca().get_yscale() == 'log':
        if height is None:
            unit_exponent = int((np.log10(y_axis_max)-np.log10(y_axis_min))/4)
            heights = [10**unit_exponent, 10**(unit_exponent*2), 10**(unit_exponent*3)]
            for i in range(len(calibration_energies)):
                plt.text(calibration_energies[i], heights[i%3], calibration_lines[i], fontsize='x-small', color='b', horizontalalignment='center')
        else:
            for i in range(len(calibration_energies)):
                plt.text(calibration_energies[i], height+(i%3)**2*height, calibration_lines[i], fontsize='x-small', color='b', horizontalalignment='center')

    else:
        if height is None:
            unit_height = int((y_axis_max - y_axis_min)/4)
            heights = [unit_height, unit_height*2, unit_height*3]
            for i in range(len(calibration_energies)):
                plt.text(calibration_energies[i], heights[i%3], calibration_lines[i], fontsize='x-small', color='b', horizontalalignment='center')
        else:
            for i in range(len(calibration_energies)):
                plt.text(calibration_energies[i], height+(i%3)**2*height, calibration_lines[i], fontsize='x-small', color='b', horizontalalignment='center')


def combine_ljh(input_dirs, common_dir_base=None, output_dir=None):
    """
    # input_dirs is a list of dir_base, e.g. ['20161209_001', '20161209_002']
    # common_dir_base is set by useful_ftns.set_base_folder()
    """
    if common_dir_base is None:
        if BASE_FOLDER == 'no name yet':
            print('Set BASE_FOLDER first by using useful_ftns.set_base_folder()')
            return
        common_dir_base = BASE_FOLDER

    # choose which channels to do
    channels = np.arange(1,481,2)

    input_dir_full_paths = [os.path.join(common_dir_base, input_dir[:8], input_dir) for input_dir in input_dirs]

    if output_dir is None: # if not provided, automatically generate name based on input folders
        output_dir = ''
        year = input_dirs[0][:4] # assuming there's no year change
        output_dir += year
        monthday_nums = [input_dir[4:] for input_dir in input_dirs]
        monthdays = [input_dir[4:8] for input_dir in input_dirs]
        nums = [input_dir[9:] for input_dir in input_dirs]

        if monthdays[1:] == monthdays[:-1]: # meaning every entry is identical
            output_dir = year + monthdays[0] + '_(' + '+'.join(nums) + ')'
        else:
            for i, monthday in enumerate(monthdays):
                if i==0:
                    if monthday == monthdays[i+1]:
                        output_dir += monthday + '_(' + nums[0]
                    else:
                        output_dir += monthday_nums[0]
                else:
                    if len(monthdays) == 2:
                        if monthday == monthdays[i-1]:
                            output_dir += '_' + nums[i] + ')'
                        else:
                            output_dir += '+' + monthday_nums[i]
                    else:
                        if i != len(monthdays)-1:
                            if monthday == monthdays[i-1]:
                                output_dir += '+' + nums[i]
                            else:
                                if output_dir[-4] == '+':
                                    output_dir += ')+' + monthday + '_'
                                else:
                                    output_dir += '+' + monthday + '_'
                                if monthday == monthdays[i+1]:
                                    output_dir += '(' + nums[i]
                                else:
                                    output_dir += nums[i]
                        else:
                            if monthday == monthdays[i-1]:
                                output_dir += '+' + nums[i] + ')'
                            else:
                                if output_dir[-4] == '+':
                                    output_dir += ')+' + monthday_nums[i]
                                else:
                                    output_dir += '+' + monthday_nums[i]

    output_dir_full_path = os.path.join(os.path.join(common_dir_base, input_dirs[0][:8]), output_dir)

    if os.path.isdir(output_dir_full_path):
        print("# output folder exists! try a new name")
        return
    else:
        os.mkdir(output_dir_full_path)
        print("# output folder %s created"%output_dir_full_path)

    # open first file and copy the header
    num_success = 0
    for channel in channels:
        # try:
        print('working on chan%d'%channel)
        output_fname = os.path.join(output_dir_full_path, output_dir+"_chan%d.ljh"%channel)
        try:
            with open(output_fname,"wb") as fout: # "w" was fine, but not working in python3
                for i, input_dir_full_path in enumerate(input_dir_full_paths):
                    input_fname = os.path.join(input_dir_full_path,input_dirs[i]+"_chan%d.ljh"%channel)
                    ljhfile = mass.files.LJHFile(input_fname)
                    with open(input_fname,"rb") as f: # "r" was fine, but not working in python3
                        header = f.read(ljhfile.header_size) # this is needed to get to the beginning of bindata
                        bindata = f.read(ljhfile.pulse_size_bytes*ljhfile.nPulses) # same as ljhfile.binary_size
                    if i==0:
                        fout.write(header)
                        fout.write(bindata)
                    else:
                        fout.write(bindata)
            num_success += 1
        except Exception as e:
            print(e)
            os.remove(output_fname)
            print('chan%d does not seem to exist'%channel)
            pass
        # old:
        # header_fname = os.path.join(input_dir_full_paths[0], input_dirs[0]+"_chan%d.ljh"%channel)
        # output_fname = os.path.join(output_dir_full_path, output_dir+"_chan%d.ljh"%channel)
        # with open(output_fname,"w") as fout:
        #     ljhfile = mass.files.LJHFile(header_fname)
        #     with open(ljhfile.filename,"r") as f:
        #         header = f.read(ljhfile.header_size)
        #         bindata = f.read(ljhfile.pulse_size_bytes*ljhfile.nPulses)
        #     fout.write(header)
        #     fout.write(bindata)
        #
        #     for i, input_dir_full_path in enumerate(input_dir_full_paths):
        #         if i==0:
        #             continue
        #         else:
        #             input_fname = os.path.join(input_dir_full_path,input_dirs[i]+"_chan%d.ljh"%channel)
        #         ljhfile = mass.files.LJHFile(input_fname)
        #         with open(ljhfile.filename,"r") as f:
        #             header = f.read(ljhfile.header_size) # this is needed to get to the beginning of bindata
        #             bindata = f.read(ljhfile.pulse_size_bytes*ljhfile.nPulses)
        #         fout.write(bindata)
        #     num_success += 1
        # except Exception as e:
        #     print(e)
        #     os.remove(output_fname)
        #     print('chan%d does not seem to exist'%channel)
        #     pass
    if num_success == 0:
        print('Nothing combined. Created folder and files have been deleted.')
        import shutil
        shutil.rmtree(output_dir_full_path)
    else:
        print('Combined files created under %s'%output_dir_full_path)


def count_rate_calc(data, timestamp=None, check_post_peak=True, filtering=False, threshold_low=100, threshold_high=2000, n_skip=3):
    '''
    returns channels (list), counts (list), timestamp[1]-timestamp[0] (scalar)
    timestamp = [ds.p_timestamp[0], ds.p_timestamp[-1]]
    '''
    # from statsmodels.nonparametric.smoothers_lowess import lowess

    if timestamp is None:
        timestamp = [np.min([ds.p_timestamp[0] for ds in data]), np.max([ds.p_timestamp[-1] for ds in data])]

    channels = []
    counts = []
    for ds in data:
        print('working on ch %d'%ds.channum)
        channels.append(ds.channum)
        counts_ds = []
        if check_post_peak:
            for pulsenum in range(np.argmax(ds.p_timestamp[:]>timestamp[0]), np.argmax(ds.p_timestamp[:]>timestamp[1])):
                if filtering is True: # in case of fast (small number of rows)
                    # trace_deriv = np.diff(np.asarray(ds.read_trace(pulsenum), dtype=int))
                    # count = np.sum((running_mean_conv(trace_deriv,10)[1:]>threshold) & (running_mean_conv(trace_deriv,10)[:-1]<=threshold))
                    trace_deriv_sparse = (np.asarray(ds.read_trace(pulsenum), dtype=int)[ds.nPresamples-n_skip*5::n_skip][1:]\
                        -np.asarray(ds.read_trace(pulsenum),dtype=int)[ds.nPresamples-n_skip*5::n_skip][:-1])
                    # too slow
                    # filtered = lowess(trace_deriv_sparse, range(len(trace_deriv_sparse)), frac=0.01, it=0)
                    # count = np.sum((filtered[:,1]>threshold) & (filtered[:,1]<=threshold))
                    count = np.sum((sig.savgol_filter(trace_deriv_sparse,11,2)[1:]>threshold_low)\
                             & (sig.savgol_filter(trace_deriv_sparse,11,2)[:-1]<=threshold_low)\
                             & (sig.savgol_filter(trace_deriv_sparse,11,2)[:-1]>0)) # gives me RuntimeWarning: internal gelsd driver lwork query error, required iwork dimension not returned. This is likely the result of LAPACK bug 0038, fixed in LAPACK 3.2.2 (released July 21, 2010). Falling back to 'gelss' driver.
                else: # regular case with full array
                    trace_deriv = (np.asarray(ds.read_trace(pulsenum), dtype=int)[ds.nPresamples-n_skip:][1:]\
                        -np.asarray(ds.read_trace(pulsenum),dtype=int)[ds.nPresamples-n_skip:][:-1])
                    count = np.sum((trace_deriv[1:]>threshold_low) & (trace_deriv[1:]<threshold_high)\
                             & (trace_deriv[:-1]<=threshold_low) & (np.append(trace_deriv[2:],trace_deriv[-1])>threshold_low)) # gives me RuntimeWarning: internal gelsd driver lwork query error, required iwork dimension not returned. This is likely the result of LAPACK bug 0038, fixed in LAPACK 3.2.2 (released July 21, 2010). Falling back to 'gelss' driver.
                counts_ds.append(count)
            counts.append(counts_ds)
        else:
            counts.append(np.argmin(np.abs(ds.p_timestamp[:]-timestamp[1]))-np.argmin(np.abs(ds.p_timestamp[:]-timestamp[0])))
        # if ds.channum==11:
        #     break
    return channels, counts


def array_map(differentiate_column=True, BL=133, **kwargs):
    # BL = 101 or 133
    # usage: array_map() or array_map(chan_list1 = [1,3,5,7], chan_list2 = [9,11], chan_list3 = [13,15,17], differentiate_column=True, BL=101)
    # providing channel lists will color differentiate columns
    nkwargs = len(kwargs)
    if nkwargs > 7:
        print('# too many arguments')
        return
    colors = ['red','orange','gold','yellowgreen','green','blue','purple'][:nkwargs]
    chan_list_all = []
    n_entries_in_arg = []
    for key, value in kwargs.items():
        if isinstance(value, int):
            value = [value]
        chan_list_all += value
        n_entries_in_arg.append(len(value))
    end_index_array = np.cumsum(n_entries_in_arg)-1
    # print end_index_array
    # print args
    # if highlight_ch is None:
    #     highlight_ch = []
    # elif isinstance(highlight_ch, int):
    #     highlight_ch = [highlight_ch]
    channums=[]
    x=[]
    y=[]
    fig = plt.figure(figsize=(6,6))

    # pixel_group_dict = {} # groups of 16 channels. maybe move to outside this function?
    # if BL == 101:
    #     pixel_group_dict['center'] = set([15, 7, 83, 135, 443, 473, 113, 127, 367, 353, 233, 203, 375, 323, 247, 255])
    #     pixel_group_dict['top'] = set([13, 3, 115, 77, 39, 33, 85, 79, 11, 1, 117, 107, 51, 31, 87, 81])
    #     pixel_group_dict['bottom'] = set([321, 327, 271, 291, 347, 357, 241, 251, 319, 325, 273, 279, 317, 355, 243, 253])
    #     pixel_group_dict['left'] = set([437, 439, 467, 441, 475, 445, 477, 447, 363, 393, 361, 391, 373, 399, 371, 411])
    #     pixel_group_dict['right'] = set([171, 131, 159, 133, 151, 121, 153, 123, 207, 237, 205, 235, 201, 227, 199, 197])
    #     pixel_group_dict['tl'] = set([59, 53, 25, 27, 427, 23, 55, 57, 457, 425, 455, 29, 451, 421, 453, 423])
    #     pixel_group_dict['tr'] = set([91, 61, 97, 67, 93, 65, 143, 173, 63, 95, 175, 145, 149, 177, 147, 179])
    #     pixel_group_dict['bl'] = set([419, 387, 417, 389, 385, 415, 335, 303, 413, 383, 305, 333, 307, 337, 301, 331])
    #     pixel_group_dict['br'] = set([183, 213, 181, 211, 269, 215, 185, 217, 297, 295, 263, 187, 267, 265, 293, 299])
    # elif BL==133: # below hasn't been checked. just a copy of 101
    #     pixel_group_dict['center'] = set([15, 7, 83, 135, 443, 473, 113, 127, 367, 353, 233, 203, 375, 323, 247, 255])
    #     pixel_group_dict['top'] = set([13, 3, 115, 77, 39, 33, 85, 79, 11, 1, 117, 107, 51, 31, 87, 81])
    #     pixel_group_dict['bottom'] = set([321, 327, 271, 291, 347, 357, 241, 251, 319, 325, 273, 279, 317, 355, 243, 253])
    #     pixel_group_dict['left'] = set([437, 439, 467, 441, 475, 445, 477, 447, 363, 393, 361, 391, 373, 399, 371, 411])
    #     pixel_group_dict['right'] = set([171, 131, 159, 133, 151, 121, 153, 123, 207, 237, 205, 235, 201, 227, 199, 197])
    #     pixel_group_dict['tl'] = set([59, 53, 25, 27, 427, 23, 55, 57, 457, 425, 455, 29, 451, 421, 453, 423])
    #     pixel_group_dict['tr'] = set([91, 61, 97, 67, 93, 65, 143, 173, 63, 95, 175, 145, 149, 177, 147, 179])
    #     pixel_group_dict['bl'] = set([419, 387, 417, 389, 385, 415, 335, 303, 413, 383, 305, 333, 307, 337, 301, 331])
    #     pixel_group_dict['br'] = set([183, 213, 181, 211, 269, 215, 185, 217, 297, 295, 263, 187, 267, 265, 293, 299])

    with open(array_map_datafile,"r") as f: # BL = 101
        f.readline()
        for line in f:
            a,b,c,d = line.strip().split()
            channums.append(int(a))
            if BL == 101:
                x.append(int(b))
            elif BL == 133:
                x.append(-int(b))
            y.append(int(c))
            # plt.scatter(b,c,marker='$%s$'%a)
            # plt.annotate(a,(b,c))
    plt.plot(x,y,'.w')
    for x_i,y_i,ch in np.broadcast(x,y,channums):
        color = 'k'
        if nkwargs > 0:
            if ch in chan_list_all:
                # print get_index(args_flat,ch)
                # print np.argmax(end_index_array>=get_index(args_flat,ch))
                color = colors[np.argmax(end_index_array>=get_index(chan_list_all,ch)[0])]
                # print color
        # plt.annotate(ch,(x,y),color=color)
        # plt.text(x_i, y_i, ch, color=color, fontsize='small', horizontalalignment='center', verticalalignment='center', bbox={'facecolor':'none', 'edgecolor':color, 'alpha':0.5, 'pad':0, 'boxstyle':'round'})
        else:
            if differentiate_column is True:
                color = ['k', 'red','orange','gold','yellowgreen','green','blue','purple'][int(ch/60)]
        plt.text(x_i, y_i, ch, color=color, fontsize='small', horizontalalignment='center', verticalalignment='center')
        # plt.text(text_positions[i][0], text_positions[i][1], text_strings[i], fontsize=text_sizes[i], color=text_colors[i], horizontalalignment=text_alignments[i])
    # plt.plot(x,y,'o')
    # plt.gca().set_aspect('equal', adjustable='box')
    plt.title('Pixel map for BL %s'%BL)
    plt.gca().set_aspect('equal')
    # x2 = [x[(ds.channum-1)/2] for ds in data]
    # y2 = [y[(ds.channum-1)/2] for ds in data]
    #
    # x3 = [x[(ch-1)/2] for ch in available_chans]
    # y3 = [y[(ch-1)/2] for ch in available_chans]
    #
    # fig = plt.figure()
    # ax = fig.add_subplot(111, projection='3d')
    # #ax.scatter(x2, y2, [ds.nPulses for ds in data], c='r', marker='o')
    # ax.bar3d(x2, y2, np.zeros(len(x2)), np.ones(len(x2))*500, np.ones(len(x2))*500, [ds.nPulses for ds in data])
    # plt.draw()

def plot_and_ginput(x, y):
    plt.figure();
    plt.plot(x, y)
    print("\n******* Pick two points on the figure to zoom in *******")
    coordi = plt.ginput(2) # [(x0, y0), (x1, y1)]
    coordi=np.asarray(coordi)
    coordi_sorted = coordi[coordi[:,0].argsort()]
    plt.xlim([coordi_sorted[0,0], coordi_sorted[1,0]])
    plt.text(0.1,0.9, 'Pick two points as the fit range', transform=plt.gca().transAxes)
    coordi = plt.ginput(2) # [(x0, y0), (x1, y1)]
    coordi=np.asarray(coordi)
    coordi_sorted = coordi[coordi[:,0].argsort()]
    x1 = coordi_sorted[0,0]
    x2 = coordi_sorted[1,0]
    return x1, x2
    #print ginput_x0, ginput_x1

def fit_gaussian(sigma = 1, tailed=True, zero_bg=False, plot_it=True, raw_data=False, n_peak=1, p0='auto', **kwargs):
    """
    will add options for weighting
    default is fit with tail (tailed=True)
    if tailed, also print counts in each constituent function
    if zero_bg is True, hold the bg at zero.
    p0=None or 'auto'
    example usage:
    if there is a plot to fit:
        useful_ftns.fit_gaussian() # sigma = 1, Tail = True
        useful_ftns.fit_gaussian(2) # sigma = 2
        useful_ftns.fit_gaussian(3, False) # sigma = 3, fit to simple gaussian
        useful_ftns.fit_gaussian(2, verbose=2) # maximum verbosity, supported
        only for tailed gaussian because normal gaussian uses scipy.optimize.leastsq(), which doesn't take kwargs
    if called in command line window with range:
        useful_ftns.fit_gaussian(2, range=[450, 500])
    if used inside another function:
        popt, pcov, perr = useful_ftns.fit_gaussian(1, True, xd=VariableX, yd=VariableY, range=[x0, x1])
        if want to just fit, not make a plot:
            popt, pcov, perr = useful_ftns.fit_gaussian(1, True, False, xd=VariableX, yd=VariableY, range=[x0, x1])
    if called in command line window without range, it will make you choose range:
        useful_ftns.fit_gaussian(1, True, xd=VariableX, yd=VariableY)
    """
    # if raw_data is True:
    #     sigma *= 10.
    fit_range = None
    xd = None
    yd = None
    for key, value in kwargs.items():
        if key == "range":
            fit_range = value
        if key == "xd":
            xd = value
        if key == "yd":
            yd = value

    for (key, value) in zip(["range", "xd", "yd"], [fit_range, xd, yd]):
        if value is not None:
            temp = kwargs.pop(key)

    xlabel=""
    ylabel=""

    if (xd is not None) and (yd is not None): # this is the case when this was called in other functions or in command line window
        if fit_range is not None: # this is the case when this was called in other functions
            fit_start = fit_range[0]
            fit_end = fit_range[1]
        else: # this is the case when this was called in command line window without range
            fit_start, fit_end = plot_and_ginput(xd, yd)
    else: # this is the case when you start with a plot
        ax = plt.gca()
        xlabel = ax.get_xlabel()
        ylabel = ax.get_ylabel()
        try:
            line = ax.get_lines()[-1]
        except IndexError:
            print("No line found")
            return
        xd = line.get_xdata()
        yd = line.get_ydata()
        if fit_range is not None:
            fit_start = fit_range[0]
            fit_end = fit_range[1]
        else:
            print("\n******* Pick two points on the figure *******")
            coordi = plt.ginput(2) # [(x0, y0), (x1, y1)]
            coordi=np.asarray(coordi)
            coordi_sorted = coordi[coordi[:,0].argsort()]
            fit_start = coordi_sorted[0,0]
            fit_end = coordi_sorted[1,0]
            #print ginput_x0, ginput_x1
    # xd=np.array(map(float,xd))
    # yd=np.array(map(float,yd))
    xd=np.asarray(xd,dtype=float)
    yd=np.asarray(yd,dtype=float)
    x_to_fit = xd[np.logical_and(xd>=fit_start, xd<=fit_end)]
    y_to_fit = yd[np.logical_and(xd>=fit_start, xd<=fit_end)]
    err = [np.sqrt(y) if y !=0 else 1 for y in y_to_fit]
    if plot_it:
        plt.figure();
        ax_master = plt.subplot(211)
        plt.plot(x_to_fit, y_to_fit)
        plt.ylabel(ylabel)
    #print x_to_fit.max
    #initial guess
    A_i = y_to_fit.max()*sigma*np.sqrt(2.*np.pi)
    x0_i = x_to_fit[y_to_fit.argmax()]
    sigma_i = sigma
    b_i = min(np.mean(y_to_fit[:5]), np.mean(y_to_fit[-5:]))
    #print np.mean(y_to_fit[:10]), np.mean(y_to_fit[-10:])
    b_upper_lim = A_i
    # b_upper_lim = b_i + np.sqrt(b_i)*2
    # if b_upper_lim == 0:
    #     b_upper_lim = 0.001
    #print "b_upper_limit = %f"%b_upper_lim
    if tailed == False:
        if zero_bg is True:
            if p0 == 'auto':
                p0 = [A_i, x0_i, sigma_i]
            popt, pcov = curve_fit(lambda x, A, x0, sigma: gaussian(x, A, x0, sigma, 0), x_to_fit, y_to_fit, p0 = p0, sigma=err)#, bounds=([0., 0., 0.,], [np.inf, 3000., 100.]))
            perr = np.sqrt(np.diag(pcov))
            chi2 = np.sum(np.square((lambda x, A, x0, sigma: gaussian(x, A, x0, sigma, 0))(x_to_fit, *popt) - y_to_fit)/np.square(err))/(len(x_to_fit)-len(popt)-1) # according to wikipedia goodness_of_fit
            if plot_it:
                fit_plot_range_x = np.arange(int(2*fit_start-x_to_fit[y_to_fit.argmax()]), int(2*fit_end-x_to_fit[y_to_fit.argmax()]), 0.1)#x_to_fit[1]-x_to_fit[0])
                plt.plot(fit_plot_range_x, (lambda x, A, x0, sigma: gaussian(x, A, x0, sigma, 0))(fit_plot_range_x, *popt),'r')
                plt.title("A = %.2f +/- %.2f, x0 = %.2f +/- %.2f\nFWHM = %.3f +/- %.3f, b = 0" %(popt[0], perr[0], popt[1], perr[1], popt[2]*SIGMA2FWHM, perr[2]*SIGMA2FWHM))
                plt.subplot(212, sharex=ax_master)
                plt.plot(x_to_fit, y_to_fit-(lambda x, A, x0, sigma: gaussian(x, A, x0, sigma, 0))(x_to_fit, *popt))
                plt.title('Residual (reduced chi2 = %f)' %chi2)
        else:
            if p0 == 'auto':
                p0 = [A_i, x0_i, sigma_i, b_i]
            popt, pcov = curve_fit(gaussian, x_to_fit, y_to_fit, p0 = p0, sigma=err)#, bounds=([0., 0., 0.,], [np.inf, 3000., 100.]))
            perr = np.sqrt(np.diag(pcov))
            chi2 = np.sum(np.square(gaussian(x_to_fit, *popt) - y_to_fit)/np.square(err))/(len(x_to_fit)-len(popt)-1) # according to wikipedia goodness_of_fit
            if plot_it:
                fit_plot_range_x = np.arange(int(2*fit_start-x_to_fit[y_to_fit.argmax()]), int(2*fit_end-x_to_fit[y_to_fit.argmax()]), 0.1)#x_to_fit[1]-x_to_fit[0])
                plt.plot(fit_plot_range_x, gaussian(fit_plot_range_x, *popt),'r')
                plt.title("A = %.2f +/- %.2f, x0 = %.2f +/- %.2f\nFWHM = %.3f +/- %.3f, b = %.2f +/- %.2f" %(popt[0], perr[0], popt[1], perr[1], popt[2]*SIGMA2FWHM, perr[2]*SIGMA2FWHM, popt[3], perr[3]))
                plt.subplot(212, sharex=ax_master)
                plt.plot(x_to_fit, y_to_fit-gaussian(x_to_fit, *popt))
                plt.title('Residual (reduced chi2 = %f)' %chi2)
    else: # tailed gaussian case
        if p0 == 'auto':
            p0 = [A_i, x0_i, sigma_i, b_i]
        popt, pcov = curve_fit(gaussian, x_to_fit, y_to_fit, p0 = p0, sigma=err)
        p0 = (popt[0], popt[1], popt[2], b_i, 0.2, popt[2]*2)
        if raw_data is True:
            # p0 = np.array([A_i, x0_i, sigma_i, b_i, 0.2, sigma_i*2]) #[A, x0, sigma, b, tailfrac, tailtau]
            # print p0
            popt, pcov = curve_fit(gaussian_tailed, x_to_fit, y_to_fit, p0 = p0, sigma=err, bounds=([1., p0[1]*0.95, 0., 0., 0., 0.], [np.inf, p0[1]*1.05, 100., b_upper_lim, 1, 1000.]), **kwargs)
            # print popt
            # print popt*2
            # popt, pcov = curve_fit(gaussian_tailed, x_to_fit, y_to_fit, p0 = popt, sigma=err, bounds=(popt*0.9, popt*1.1), **kwargs)
            popt, pcov = curve_fit(gaussian_tailed, x_to_fit, y_to_fit, p0 = popt, sigma=err, bounds=([popt[0]*0.9, popt[1]*0.9, 0.1, 0., 0., 0.], [popt[0]*1.1, popt[1]*1.1, 100., b_upper_lim, 1, 1000.]), **kwargs)
            # print popt
            perr = np.sqrt(np.diag(pcov))
        else:
            if zero_bg is True:
                p0 = (popt[0], popt[1], popt[2], 0.2, popt[2]*2)
                popt, pcov = curve_fit(lambda x, A, x0, sigma, tailfrac, tailtau: gaussian_tailed(x, A, x0, sigma, 0, tailfrac, tailtau), x_to_fit, y_to_fit, p0 = [A_i, x0_i, sigma_i, 0.2, 10], sigma=err, bounds=([0., 0., 0., 0., 0.], [np.inf, 3000., 10., 1, 100.]), **kwargs)
                popt, pcov = curve_fit(lambda x, A, x0, sigma, tailfrac, tailtau: gaussian_tailed(x, A, x0, sigma, 0, tailfrac, tailtau), x_to_fit, y_to_fit, p0 = popt, sigma=err, bounds=([0., 0., 0., 0., 0.], [np.inf, 3000., 10., 1, 100.]), **kwargs)
                perr = np.sqrt(np.diag(pcov))
                chi2 = np.sum(np.square((lambda x, A, x0, sigma, tailfrac, tailtau: gaussian_tailed(x, A, x0, sigma, 0, tailfrac, tailtau))(x_to_fit, *popt) - y_to_fit)/np.square(err))/(len(x_to_fit)-len(popt)-1) # according to wikipedia goodness_of_fit
                if plot_it:
                    fit_plot_range_x = np.arange(int(2*fit_start-x_to_fit[y_to_fit.argmax()]), int(2*fit_end-x_to_fit[y_to_fit.argmax()]), 0.1)#x_to_fit[1]-x_to_fit[0])
                    plt.plot(fit_plot_range_x, (lambda x, A, x0, sigma, tailfrac, tailtau: gaussian_tailed(x, A, x0, sigma, 0, tailfrac, tailtau))(fit_plot_range_x, *popt),'r')
                    plt.plot(fit_plot_range_x, gaussian(fit_plot_range_x, popt[0], popt[1], popt[2], 0), '--y', label='Gausian+zero_bg')
                    plt.plot(fit_plot_range_x, tail_ftn(fit_plot_range_x, popt[0], popt[1], popt[2], popt[3], popt[4]), '--g', label='Tail')
                    plt.title("A = %.2f +/- %.2f, x0 = %.2f +/- %.2f, FWHM = %.3f +/- %.3f\nb = 0, tailfrac = %.3f +/- %.3f, tailtau = %.3f +/- %.3f" %(popt[0], perr[0], popt[1], perr[1], popt[2]*SIGMA2FWHM, perr[2]*SIGMA2FWHM, popt[3], perr[3], popt[4], perr[4]), fontsize='small')
                    plt.legend(loc="best", fontsize='small')
                    plt.subplot(212, sharex=ax_master)
                    plt.plot(x_to_fit, y_to_fit-(lambda x, A, x0, sigma, tailfrac, tailtau: gaussian_tailed(x, A, x0, sigma, 0, tailfrac, tailtau))(x_to_fit, *popt))
                    plt.title('Residual (reduced chi2 = %f)' %chi2, fontsize='small')
                    # popt[3] is background level
                    print("\n***** Calculated counts in guassian: %d, tail: %d, bg: %d, total: %d *****\n" %(sum(gaussian(x_to_fit, popt[0], popt[1], popt[2], 0)), sum(tail_ftn(x_to_fit, popt[0], popt[1], popt[2], popt[3], popt[4])), (sum((lambda x, A, x0, sigma, tailfrac, tailtau: gaussian_tailed(x, A, x0, sigma, 0, tailfrac, tailtau))(x_to_fit, *popt)) - sum(gaussian(x_to_fit, popt[0], popt[1], popt[2], 0)) - sum(tail_ftn(x_to_fit, popt[0], popt[1], popt[2], popt[3], popt[4]))), sum((lambda x, A, x0, sigma, tailfrac, tailtau: gaussian_tailed(x, A, x0, sigma, 0, tailfrac, tailtau))(x_to_fit, *popt))))
            else:
                popt, pcov = curve_fit(gaussian_tailed, x_to_fit, y_to_fit, p0 = [A_i, x0_i, sigma_i, b_i, 0.2, 10], sigma=err, bounds=([0., 0., 0., 0., 0., 0.], [np.inf, 3000., 10., b_upper_lim, 1, 100.]), **kwargs)
                popt, pcov = curve_fit(gaussian_tailed, x_to_fit, y_to_fit, p0 = popt, sigma=err, bounds=([0., 0., 0., 0., 0., 0.], [np.inf, 3000., 10., b_upper_lim, 10, 100.]), **kwargs)
                perr = np.sqrt(np.diag(pcov))
                chi2 = np.sum(np.square(gaussian_tailed(x_to_fit, *popt) - y_to_fit)/np.square(err))/(len(x_to_fit)-len(popt)-1) # according to wikipedia goodness_of_fit
                if plot_it:
                    fit_plot_range_x = np.arange(int(2*fit_start-x_to_fit[y_to_fit.argmax()]), int(2*fit_end-x_to_fit[y_to_fit.argmax()]), 0.1)#x_to_fit[1]-x_to_fit[0])
                    plt.plot(fit_plot_range_x, gaussian_tailed(fit_plot_range_x, *popt),'r')
                    plt.plot(fit_plot_range_x, gaussian(fit_plot_range_x, popt[0], popt[1], popt[2], popt[3]), '--y', label='Gausian+bg')
                    plt.plot(fit_plot_range_x, tail_ftn(fit_plot_range_x, popt[0], popt[1], popt[2], popt[4], popt[5]), '--g', label='Tail')
                    plt.title("A = %.2f +/- %.2f, x0 = %.2f +/- %.2f, FWHM = %.3f +/- %.3f\nb = %.2f +/- %.2f, tailfrac = %.3f +/- %.3f, tailtau = %.3f +/- %.3f" %(popt[0], perr[0], popt[1], perr[1], popt[2]*SIGMA2FWHM, perr[2]*SIGMA2FWHM, popt[3], perr[3], popt[4], perr[4], popt[5], perr[5]), fontsize='small')
                    plt.legend(loc="best", fontsize='small')
                    plt.subplot(212, sharex=ax_master)
                    plt.plot(x_to_fit, y_to_fit-gaussian_tailed(x_to_fit, *popt))
                    plt.title('Residual (reduced chi2 = %f)' %chi2, fontsize='small')
                    # popt[3] is background level
                    print("\n***** Calculated counts in guassian: %d, tail: %d, bg: %d, total: %d *****\n" %(sum(gaussian(x_to_fit, popt[0], popt[1], popt[2], popt[3])-popt[3]), sum(tail_ftn(x_to_fit, popt[0], popt[1], popt[2], popt[4], popt[5])), (sum(gaussian_tailed(x_to_fit, *popt)) - sum(gaussian(x_to_fit, popt[0], popt[1], popt[2], popt[3])-popt[3]) - sum(tail_ftn(x_to_fit, popt[0], popt[1], popt[2], popt[4], popt[5]))), sum(gaussian_tailed(x_to_fit, *popt))))
    if plot_it:
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.subplot(211)
        plt.ylabel(ylabel)

    return popt, pcov, perr

"""
# test gaussian fitting function
xx = np.arange(1100,1250,0.2)
SIGMA2FWHM = 2.*np.sqrt(2.*np.log(2.))
p = [660, 1200, 1.5/SIGMA2FWHM, 2, 0.1, 5]
yy = gaussian_tailed(xx, *p)+(sp.rand(len(xx))-0.5)*2.
plt.plot(xx,yy)
useful_ftns.fit_gaussian(1, True)
# fitting works well, but chi2 doesn't make much sense
# it's because of incorrect estimation of sigma
# sigma shouldn't be sqrt(count), but it's np.std(sp.rand(len(xx)))*2, which is (sigma of random function) * 2
chi2 = np.sum(np.square(gaussian_tailed(x_to_fit, *popt) - y_to_fit)/(np.std(sp.rand(len(xx)))*2)**2)/(len(x_to_fit)-len(popt)-1)
# above gives chi2 = 1.05
"""


def fit_2gaussians(sigma = 1, tailed=True, plot_it=True, raw_data=False, n_gauss=1, n_tail=2, reduced=False, n_reduce=1, x0_ratio=0.975, LET=False, p0=None, **kwargs):
    """
    returns popt, pcov, perr
    for usage, check fit_gaussian
    if tailed=False: use two simple gaussians
    if tailed=True: possible combinations of n_guass=1/2, n_tail=1/2
    """
    # if raw_data is True:
    #     sigma *= 10.
    fit_range = None
    xd = None
    yd = None
    # p0 = None
    for key, value in kwargs.items():
        if key == "range":
            fit_range = value
        if key == "xd":
            xd = value
        if key == "yd":
            yd = value
        # if key == "p0":
        #     p0 = value

    for (key, value) in zip(["range", "xd", "yd"], [fit_range, xd, yd]):
        if value is not None:
            temp = kwargs.pop(key)

    xlabel=""
    ylabel=""

    if (xd is not None) and (yd is not None): # this is the case when this was called in other functions or in command line window
        if fit_range is not None: # this is the case when this was called in other functions
            fit_start = fit_range[0]
            fit_end = fit_range[1]
        else: # this is the case when this was called in command line window without range
            plt.figure();
            plt.plot(xd, yd)
            print("\n******* Pick two points on the figure to zoom in *******")
            coordi = plt.ginput(2) # [(x0, y0), (x1, y1)]
            coordi=np.asarray(coordi)
            coordi_sorted = coordi[coordi[:,0].argsort()]
            plt.xlim([coordi_sorted[0,0], coordi_sorted[1,0]])
            plt.text(0.1,0.9, 'Pick two points as the fit range', transform=plt.gca().transAxes)
            coordi = plt.ginput(2) # [(x0, y0), (x1, y1)]
            coordi=np.asarray(coordi)
            coordi_sorted = coordi[coordi[:,0].argsort()]
            fit_start = coordi_sorted[0,0]
            fit_end = coordi_sorted[1,0]
            #print ginput_x0, ginput_x1
    else: # this is the case when you start with a plot
        ax = plt.gca()
        xlabel = ax.get_xlabel()
        ylabel = ax.get_ylabel()
        try:
            line = ax.get_lines()[-1]
        except IndexError:
            print("No line found")
            return
        xd = line.get_xdata()
        yd = line.get_ydata()
        if fit_range is not None:
            fit_start = fit_range[0]
            fit_end = fit_range[1]
        else:
            print("\n******* Pick two points on the figure *******")
            coordi = plt.ginput(2) # [(x0, y0), (x1, y1)]
            coordi=np.asarray(coordi)
            coordi_sorted = coordi[coordi[:,0].argsort()]
            fit_start = coordi_sorted[0,0]
            fit_end = coordi_sorted[1,0]
            #print ginput_x0, ginput_x1
    # xd=np.array(map(float,xd))
    # yd=np.array(map(float,yd))
    xd=np.asarray(xd,dtype=float)
    yd=np.asarray(yd,dtype=float)
    x_to_fit = xd[np.logical_and(xd>=fit_start, xd<=fit_end)]
    y_to_fit = yd[np.logical_and(xd>=fit_start, xd<=fit_end)]
    err = [np.sqrt(y) if y !=0 else 1 for y in y_to_fit]
    if plot_it:
        plt.figure();
        ax_master = plt.subplot(211)
        plt.plot(x_to_fit, y_to_fit, 'k')
        plt.ylabel(ylabel)
    #print x_to_fit.max
    #initial guess
    A_i = y_to_fit.max()*sigma*np.sqrt(2.*np.pi)
    x0_i = x_to_fit[y_to_fit.argmax()]
    sigma_i = sigma
    b_i = min(np.mean(y_to_fit[:10]), np.mean(y_to_fit[-10:]))
    #print np.mean(y_to_fit[:10]), np.mean(y_to_fit[-10:])
    b_upper_lim = b_i + np.sqrt(b_i)
    if b_upper_lim == 0:
        b_upper_lim = 0.001
    #print "b_upper_limit = %f"%b_upper_lim
    uncertainty = np.sqrt(y_to_fit)
    uncertainty[uncertainty==0]=1.
    if tailed == False:
        if p0 is None:
            p0 = [A_i, x0_i, sigma_i, A_i, x0_i*x0_ratio, sigma_i, b_i]
        popt, pcov = curve_fit(two_gaussian, x_to_fit, y_to_fit, p0 = p0)#, bounds=([0., 0., 0.,], [np.inf, 3000., 100.]))
        perr = np.sqrt(np.diag(pcov))
        chi2 = np.sum(np.square(two_gaussian(x_to_fit, *popt) - y_to_fit)/np.square(err))/(len(x_to_fit)-len(popt)-1) # according to wikipedia goodness_of_fit
        fit_plot_range_x = np.arange(int(2*fit_start-x_to_fit[y_to_fit.argmax()]), int(2*fit_end-x_to_fit[y_to_fit.argmax()]), 0.1)#x_to_fit[1]-x_to_fit[0])
        if plot_it:
            plt.plot(fit_plot_range_x, two_gaussian(fit_plot_range_x, *popt),'r')
            plt.plot(fit_plot_range_x, gaussian(fit_plot_range_x, popt[0], popt[1], popt[2], 0), '--y', label='Gausian1')
            plt.plot(fit_plot_range_x, gaussian(fit_plot_range_x, popt[3], popt[4], popt[5], 0), '--c', label='Gausian2')
            plt.sj_hlines(popt[6], '--m', label='BG')
            plt.title("A_1 = %.2f +/- %.2f, x0_1 = %.2f +/- %.2f, FWHM_1 = %.3f +/- %.3f\nA_2 = %.2f +/- %.2f, x0_2 = %.2f +/- %.2f, FWHM_2 = %.3f +/- %.3f, b = %.2f +/- %.2f" %(popt[0], perr[0], popt[1], perr[1], popt[2]*SIGMA2FWHM, perr[2]*SIGMA2FWHM, popt[3], perr[3], popt[4], perr[4], popt[5]*SIGMA2FWHM, perr[5]*SIGMA2FWHM, popt[6], perr[6]), fontsize='small')
            plt.subplot(212, sharex=ax_master)
            plt.plot(x_to_fit, y_to_fit-two_gaussian(x_to_fit, *popt))
            plt.title('Residual (reduced chi2 = %f)' %chi2)
    else:
        if n_tail == 1:
            if n_gauss == 2:
                if raw_data is True: # do not use this yet
                    if p0 is None:
                        p0 = [A_i, x0_i, sigma_i, A_i*0.2, x0_i*x0_ratio, sigma_i*5, b_i, 0.1, 10] #[A_1, x0_1, sigma_1, A_2, x0_2, sigma_2, b, tailfrac, tailtau, sigma_3]
                    popt, pcov = curve_fit(two_gaussian_tailed, x_to_fit, y_to_fit, p0 = p0, bounds=([0., 0., 0., 0., 0., 0., 0., 0., 0.], [np.inf, 30000., 100., np.inf, 30000., 100., b_upper_lim, 10, 1000.]), sigma=uncertainty, **kwargs)
                    popt, pcov = curve_fit(two_gaussian_tailed, x_to_fit, y_to_fit, p0 = popt, bounds=([0., 0., 0., 0., 0., 0., 0., 0., 0.], [np.inf, 30000., 100., np.inf, 30000., 100., b_upper_lim, 10, 1000.]), sigma=uncertainty, **kwargs)
                else:
                    if reduced is False: # n_tail=1, n_gauss=2, reduced=False
                        if p0 is None:
                            p0 = [A_i, x0_i, sigma_i, A_i*0.15, x0_i*x0_ratio, sigma_i*3, b_i, 1, 10, sigma_i*6] #[A_1, x0_1, sigma_1, A_2, x0_2, sigma_2, b, tailfrac, tailtau, sigma_3]
                        popt, pcov = curve_fit(two_gaussian_tailed, x_to_fit, y_to_fit, p0 = p0, bounds=([0., 0., 0., 0., 0., 0., 0., 0., 0., 0.], [np.inf, 3000., 1., np.inf, 3000., 10., b_upper_lim, 10, 100., 20.]), sigma=uncertainty, **kwargs)
                        popt, pcov = curve_fit(two_gaussian_tailed, x_to_fit, y_to_fit, p0 = popt, bounds=([0., 0., 0., 0., 0., 0., 0., 0., 0., 0.], [np.inf, 3000., 1., np.inf, 3000., 10., b_upper_lim, 10, 100., 20]), sigma=uncertainty, **kwargs)
                    else:
                        if n_reduce == 2: # n_tail=1, n_gauss=2, n_reduce=2
                            if p0 is None:
                                p0 = [A_i, x0_i, sigma_i, A_i*0.2, sigma_i*5, b_i, 0.1, 10] #[A_1, x0_1, sigma_1, A_2, sigma_2, b, tailfrac, tailtau]
                            popt, pcov = curve_fit(two_gaussian_tailed_reduced2, x_to_fit, y_to_fit, p0 = p0, bounds=([0., 0., 0., 0., 0., 0., 0., 0.], [np.inf, 30000., 100., np.inf, 100., b_upper_lim, 10, 1000.]), sigma=uncertainty, **kwargs)
                            popt, pcov = curve_fit(two_gaussian_tailed_reduced2, x_to_fit, y_to_fit, p0 = popt, bounds=([0., 0., 0., 0., 0., 0., 0., 0.], [np.inf, 30000., 100., np.inf, 100., b_upper_lim, 10, 1000.]), sigma=uncertainty, **kwargs)
                        elif n_reduce ==1:
                            if p0 is None: # n_tail=1, n_gauss=2, n_reduce=1
                                p0 = [A_i, x0_i, sigma_i, A_i*0.1, x0_i*x0_ratio, sigma_i*5, b_i, 1, 10] #[A_1, x0_1, sigma_1, A_2, x0_2, sigma_2, b, tailfrac, tailtau]
                            popt, pcov = curve_fit(two_gaussian_tailed_reduced1, x_to_fit, y_to_fit, p0 = p0, bounds=([0., 0., 0., 0., 0., 0., 0., 0., 0.], [np.inf, 3000., 100., np.inf, 3000., 100., b_upper_lim, 10, 100.]), sigma=uncertainty, **kwargs)
                            popt, pcov = curve_fit(two_gaussian_tailed_reduced1, x_to_fit, y_to_fit, p0 = popt, bounds=([0., 0., 0., 0., 0., 0., 0., 0., 0.], [np.inf, 3000., 100., np.inf, 3000., 100., b_upper_lim, 10, 100.]), sigma=uncertainty, **kwargs)
                perr = np.sqrt(np.diag(pcov))
                if reduced is False:
                    chi2 = np.sum(np.square(two_gaussian_tailed(x_to_fit, *popt) - y_to_fit)/np.square(err))/(len(x_to_fit)-len(popt)-1) # according to wikipedia goodness_of_fit
                else:
                    if n_reduce == 2:
                        chi2 = np.sum(np.square(two_gaussian_tailed_reduced2(x_to_fit, *popt) - y_to_fit)/np.square(err))/(len(x_to_fit)-len(popt)-1) # according to wikipedia goodness_of_fit
                    elif n_reduce == 1:
                        chi2 = np.sum(np.square(two_gaussian_tailed_reduced1(x_to_fit, *popt) - y_to_fit)/np.square(err))/(len(x_to_fit)-len(popt)-1) # according to wikipedia goodness_of_fit
                fit_plot_range_x = np.arange(int(2*fit_start-x_to_fit[y_to_fit.argmax()]), int(2*fit_end-x_to_fit[y_to_fit.argmax()]), 0.1)#x_to_fit[1]-x_to_fit[0])
                if plot_it:
                    if reduced is False:
                        plt.plot(fit_plot_range_x, two_gaussian_tailed(fit_plot_range_x, *popt),'r')
                    else:
                        if n_reduce == 2:
                            plt.plot(fit_plot_range_x, two_gaussian_tailed_reduced2(fit_plot_range_x, *popt),'r')
                        elif n_reduce == 1:
                            plt.plot(fit_plot_range_x, two_gaussian_tailed_reduced1(fit_plot_range_x, *popt),'r')
                    plt.plot(fit_plot_range_x, gaussian(fit_plot_range_x, popt[0], popt[1], popt[2], 0), '--y', label='Gausian1')
                    if reduced is False:
                        plt.plot(fit_plot_range_x, gaussian(fit_plot_range_x, popt[3], popt[1], popt[5], 0), '--c', label='Gausian2')
                        # plt.plot(fit_plot_range_x, tail_ftn(fit_plot_range_x, popt[0], popt[1], popt[2], popt[7], popt[8]), '--g', label='Tail1')
                        plt.plot(fit_plot_range_x, tail_ftn(fit_plot_range_x, popt[3], popt[4], popt[9], popt[7], popt[8]), '--g', label='Tail1')
                        plt.sj_hlines(popt[6], '--m', label='BG')
                        plt.title("A_1 = %.1f +/- %.1f, x0_1 = %.2f +/- %.3f, FWHM_1 = %.3f +/- %.3f\nA_2 = %.1f +/- %.1f, x0_2 = %.2f +/- %.3f, FWHM_2 = %.3f +/- %.3f\nb = %.2f +/- %.2f, tailfrac = %.3f +/- %.3f, tailtau = %.3f +/- %.3f" %(popt[0], perr[0], popt[1], perr[1], popt[2]*SIGMA2FWHM, perr[2]*SIGMA2FWHM, popt[3], perr[3], popt[4], perr[4], popt[5]*SIGMA2FWHM, perr[5]*SIGMA2FWHM, popt[6], perr[6], popt[7], perr[7], popt[8], perr[8]), fontsize='small')
                    else:
                        if n_reduce == 2:
                            plt.plot(fit_plot_range_x, gaussian(fit_plot_range_x, popt[3], popt[1], popt[4], 0), '--c', label='Gausian2')
                            plt.plot(fit_plot_range_x, tail_ftn(fit_plot_range_x, popt[3], popt[1], popt[4], popt[6], popt[7]), '--g', label='Tail')
                            plt.sj_hlines(popt[5], '--m', label='BG')
                            plt.title("A_1 = %.1f +/- %.1f, x0_1 = %.2f +/- %.3f, FWHM_1 = %.3f +/- %.3f\nA_2 = %.1f +/- %.1f, FWHM_2 = %.3f +/- %.3f\nb = %.2f +/- %.2f, tailfrac = %.3f +/- %.3f, tailtau = %.3f +/- %.3f" %(popt[0], perr[0], popt[1], perr[1], popt[2]*SIGMA2FWHM, perr[2]*SIGMA2FWHM, popt[3], perr[3], popt[4]*SIGMA2FWHM, perr[4]*SIGMA2FWHM, popt[5], perr[5], popt[6], perr[6], popt[7], perr[7]), fontsize='small')
                        elif n_reduce == 1:
                            plt.sj_hlines(popt[6], '--m', label='BG')
                            plt.plot(fit_plot_range_x, gaussian(fit_plot_range_x, popt[3], popt[1], popt[5], 0), '--c', label='Gausian2')
                            plt.plot(fit_plot_range_x, tail_ftn(fit_plot_range_x, popt[3], popt[4], popt[5], popt[7], popt[8]), '--g', label='Tail')
                            plt.title("A_1 = %.1f +/- %.1f, x0_1 = %.2f +/- %.3f, FWHM_1 = %.3f +/- %.3f\nA_2 = %.1f +/- %.1f, x0_2 = %.2f +/- %.3f, FWHM_2 = %.3f +/- %.3f\nb = %.2f +/- %.2f, tailfrac = %.3f +/- %.3f, tailtau = %.3f +/- %.3f" %(popt[0], perr[0], popt[1], perr[1], popt[2]*SIGMA2FWHM, perr[2]*SIGMA2FWHM, popt[3], perr[3], popt[4], perr[4], popt[5]*SIGMA2FWHM, perr[5]*SIGMA2FWHM, popt[6], perr[6], popt[7], perr[7], popt[8], perr[8]), fontsize='small')
                    plt.legend(loc="best", fontsize='small')
                    plt.subplot(212, sharex=ax_master)
                    if reduced is False:
                        plt.plot(x_to_fit, y_to_fit-two_gaussian_tailed(x_to_fit, *popt))
                    else:
                        if n_reduce == 2:
                            plt.plot(x_to_fit, y_to_fit-two_gaussian_tailed_reduced2(x_to_fit, *popt))
                        elif n_reduce == 1:
                            plt.plot(x_to_fit, y_to_fit-two_gaussian_tailed_reduced1(x_to_fit, *popt))
                    plt.title('Residual (reduced chi2 = %f)' %chi2)
                    # popt[6] is background level
                    #print "\n***** Calculated counts in guassian: %d, tail: %d, bg: %d, total: %d *****\n" %(sum(gaussian(x_to_fit, popt[0], popt[1], popt[2], popt[3])-popt[3]), sum(tail_ftn(x_to_fit, popt[0], popt[1], popt[2], popt[4], popt[5])), (sum(gaussian_tailed(x_to_fit, *popt)) - sum(gaussian(x_to_fit, popt[0], popt[1], popt[2], popt[3])-popt[3]) - sum(tail_ftn(x_to_fit, popt[0], popt[1], popt[2], popt[4], popt[5]))), sum(gaussian_tailed(x_to_fit, *popt)))
            elif n_gauss == 1: # n_tail=1, n_gauss=1
                if raw_data is True:
                    if p0 is None:
                        p0 = [A_i, x0_i, sigma_i, A_i*0.2, x0_i*x0_ratio, sigma_i*5, b_i, 10]
                    popt, pcov = curve_fit(one_gaussian_tailed, x_to_fit, y_to_fit, p0 = p0, bounds=([0., 0., 0., 0., 0., 0., 0., 0.], [np.inf, 30000., 100., np.inf, 30000., 100., b_upper_lim, 1000.]), sigma=uncertainty, **kwargs)
                    popt, pcov = curve_fit(one_gaussian_tailed, x_to_fit, y_to_fit, p0 = popt, bounds=([0., 0., 0., 0., 0., 0., 0., 0.], [np.inf, 30000., 100., np.inf, 30000., 100., b_upper_lim, 1000.]), sigma=uncertainty, **kwargs)
                else:
                    if p0 is None:
                        p0 = [A_i, x0_i, sigma_i, A_i*0.01, x0_i*x0_ratio, sigma_i*6, b_i, 10]
                    popt, pcov = curve_fit(one_gaussian_tailed, x_to_fit, y_to_fit, p0 = p0, bounds=([0., 0., 0., 0., 0., 0., 0., 0.], [np.inf, 3000., 1., np.inf, 3000., 10., b_upper_lim, 100.]), sigma=uncertainty, **kwargs)
                    popt, pcov = curve_fit(one_gaussian_tailed, x_to_fit, y_to_fit, p0 = popt, bounds=([0., 0., 0., 0., 0., 0., 0., 0.], [np.inf, 3000., 5., np.inf, 3000., 50., b_upper_lim, 100.]), sigma=uncertainty, **kwargs)
                perr = np.sqrt(np.diag(pcov))
                chi2 = np.sum(np.square(one_gaussian_tailed(x_to_fit, *popt) - y_to_fit)/np.square(err))/(len(x_to_fit)-len(popt)-1) # according to wikipedia goodness_of_fit
                fit_plot_range_x = np.arange(int(2*fit_start-x_to_fit[y_to_fit.argmax()]), int(2*fit_end-x_to_fit[y_to_fit.argmax()]), 0.1)#x_to_fit[1]-x_to_fit[0])
                if plot_it:
                    plt.plot(fit_plot_range_x, one_gaussian_tailed(fit_plot_range_x, *popt),'r')
                    plt.plot(fit_plot_range_x, gaussian(fit_plot_range_x, popt[0], popt[1], popt[2], 0), '--y', label='Gausian1')
                    plt.plot(fit_plot_range_x, tail_ftn_isol(fit_plot_range_x, popt[3], popt[4], popt[5], popt[7]), '--g', label='Tail2')
                    plt.sj_hlines(popt[6], '--m', label='BG')
                    plt.title("A_1 = %.1f +/- %.1f, x0_1 = %.2f +/- %.3f, FWHM_1 = %.3f +/- %.3f\nA_2 = %.1f +/- %.1f, x0_2 = %.2f +/- %.3f, FWHM_2 = %.3f +/- %.3f\nb = %.2f +/- %.2f, tailtau = %.3f +/- %.3f" %(popt[0], perr[0], popt[1], perr[1], popt[2]*SIGMA2FWHM, perr[2]*SIGMA2FWHM, popt[3], perr[3], popt[4], perr[4], popt[5]*SIGMA2FWHM, perr[5]*SIGMA2FWHM, popt[6], perr[6], popt[7], perr[7]), fontsize='small')
                    plt.legend(loc="best", fontsize='small')
                    plt.subplot(212, sharex=ax_master)
                    plt.plot(x_to_fit, y_to_fit-one_gaussian_tailed(x_to_fit, *popt))
                    plt.title('Residual (reduced chi2 = %f)' %chi2)
                    # popt[6] is background level
                    #print "\n***** Calculated counts in guassian: %d, tail: %d, bg: %d, total: %d *****\n" %(sum(gaussian(x_to_fit, popt[0], popt[1], popt[2], popt[3])-popt[3]), sum(tail_ftn(x_to_fit, popt[0], popt[1], popt[2], popt[4], popt[5])), (sum(gaussian_tailed(x_to_fit, *popt)) - sum(gaussian(x_to_fit, popt[0], popt[1], popt[2], popt[3])-popt[3]) - sum(tail_ftn(x_to_fit, popt[0], popt[1], popt[2], popt[4], popt[5]))), sum(gaussian_tailed(x_to_fit, *popt)))
            elif n_gauss == 0: # n_tail=1, n_gauss=0
                if raw_data is True:
                    if p0 is None:
                        p0 = [A_i, x0_i, sigma_i, A_i*0.2, x0_i*x0_ratio, sigma_i*5, b_i, 10]
                    popt, pcov = curve_fit(tail_ftn_isol, x_to_fit, y_to_fit, p0 = p0, bounds=([0., 0., 0., 0., 0., 0., 0., 0., 0.], [np.inf, 30000., 100., np.inf, 30000., 100., b_upper_lim, 1000.]), sigma=uncertainty, **kwargs)
                    popt, pcov = curve_fit(tail_ftn_isol, x_to_fit, y_to_fit, p0 = popt, bounds=([0., 0., 0., 0., 0., 0., 0., 0.], [np.inf, 30000., 100., np.inf, 30000., 100., b_upper_lim, 1000.]), sigma=uncertainty, **kwargs)
                else:
                    if p0 is None:
                        p0 = [A_i, x0_i, sigma_i, 10]
                    popt, pcov = curve_fit(tail_ftn_isol, x_to_fit, y_to_fit, p0 = p0, bounds=([0., 0., 0., 0.], [np.inf, 3000., 5., 100.]), sigma=uncertainty, **kwargs)
                    popt, pcov = curve_fit(tail_ftn_isol, x_to_fit, y_to_fit, p0 = popt, bounds=([0., 0., 0., 0.], [np.inf, 3000., 5., 100.]), sigma=uncertainty, **kwargs)
                perr = np.sqrt(np.diag(pcov))
                chi2 = np.sum(np.square(tail_ftn_isol(x_to_fit, *popt) - y_to_fit)/np.square(err))/(len(x_to_fit)-len(popt)-1) # according to wikipedia goodness_of_fit
                fit_plot_range_x = np.arange(int(2*fit_start-x_to_fit[y_to_fit.argmax()]), int(2*fit_end-x_to_fit[y_to_fit.argmax()]), 0.1)#x_to_fit[1]-x_to_fit[0])
                if plot_it:
                    plt.plot(fit_plot_range_x, tail_ftn_isol(fit_plot_range_x, *popt),'r')
                    #plt.plot(fit_plot_range_x, gaussian(fit_plot_range_x, popt[0], popt[1], popt[2], 0), '--y', label='Gausian1')
                    #plt.plot(fit_plot_range_x, tail_ftn_isol(fit_plot_range_x, popt[3], popt[4], popt[5], popt[7]), '--g', label='Tail2')
                    #plt.sj_hlines(popt[6], '--m', label='BG')
                    plt.title("A_1 = %.1f +/- %.1f, x0_1 = %.2f +/- %.3f, FWHM_1 = %.3f +/- %.3f, tailtau = %.3f +/- %.3f" %(popt[0], perr[0], popt[1], perr[1], popt[2]*SIGMA2FWHM, perr[2]*SIGMA2FWHM, popt[3], perr[3]), fontsize='small')
                    #plt.legend(loc="best", fontsize='small')
                    plt.subplot(212, sharex=ax_master)
                    plt.plot(x_to_fit, y_to_fit-tail_ftn_isol(x_to_fit, *popt))
                    plt.title('Residual (reduced chi2 = %f)' %chi2)
                    # popt[6] is background level
                    #print "\n***** Calculated counts in guassian: %d, tail: %d, bg: %d, total: %d *****\n" %(sum(gaussian(x_to_fit, popt[0], popt[1], popt[2], popt[3])-popt[3]), sum(tail_ftn(x_to_fit, popt[0], popt[1], popt[2], popt[4], popt[5])), (sum(gaussian_tailed(x_to_fit, *popt)) - sum(gaussian(x_to_fit, popt[0], popt[1], popt[2], popt[3])-popt[3]) - sum(tail_ftn(x_to_fit, popt[0], popt[1], popt[2], popt[4], popt[5]))), sum(gaussian_tailed(x_to_fit, *popt)))
        elif n_tail == 2:
            if n_gauss == 1: # n_tail=2, n_gauss=1
                if raw_data is True:
                    if p0 is None:
                        p0 = [A_i, x0_i, sigma_i, A_i*0.2, x0_i*x0_ratio, sigma_i*5, b_i, 0.1, 10, 10]
                    popt, pcov = curve_fit(one_gaussian_2tailed, x_to_fit, y_to_fit, p0 = p0, bounds=([0., 0., 0., 0., 0., 0., 0., 0., 0., 0.], [np.inf, 30000., 100., np.inf, 30000., 100., b_upper_lim, 10, 1000., 1000.]), sigma=uncertainty, **kwargs)
                    popt, pcov = curve_fit(one_gaussian_2tailed, x_to_fit, y_to_fit, p0 = popt, bounds=([0., 0., 0., 0., 0., 0., 0., 0., 0., 0.], [np.inf, 30000., 100., np.inf, 30000., 100., b_upper_lim, 10, 1000., 1000.]), sigma=uncertainty, **kwargs)
                else:
                    if p0 is None:
                        p0 = [A_i, x0_i, sigma_i, A_i*0.2, x0_i*x0_ratio, sigma_i*5, b_i, 0.1, 10, 10]
                    popt, pcov = curve_fit(one_gaussian_2tailed, x_to_fit, y_to_fit, p0 = p0, bounds=([0., 0., 0., 0., 0., 0., 0., 0., 0., 0.], [np.inf, 3000., 1., np.inf, 3000., 10., b_upper_lim, 1, 100., 100.]), sigma=uncertainty, **kwargs)
                    popt, pcov = curve_fit(one_gaussian_2tailed, x_to_fit, y_to_fit, p0 = popt, bounds=([0., 0., 0., 0., 0., 0., 0., 0., 0., 0.], [np.inf, 3000., 10., np.inf, 3000., 10., b_upper_lim, 1, 100., 100.]), sigma=uncertainty, **kwargs)
                perr = np.sqrt(np.diag(pcov))
                chi2 = np.sum(np.square(one_gaussian_2tailed(x_to_fit, *popt) - y_to_fit)/np.square(err))/(len(x_to_fit)-len(popt)-1) # according to wikipedia goodness_of_fit
                fit_plot_range_x = np.arange(int(2*fit_start-x_to_fit[y_to_fit.argmax()]), int(2*fit_end-x_to_fit[y_to_fit.argmax()]), 0.1)#x_to_fit[1]-x_to_fit[0])
                if plot_it:
                    plt.plot(fit_plot_range_x, one_gaussian_2tailed(fit_plot_range_x, *popt),'r')
                    plt.plot(fit_plot_range_x, gaussian(fit_plot_range_x, popt[0], popt[1], popt[2], 0), '--y', label='Gausian1')
                    plt.plot(fit_plot_range_x, tail_ftn(fit_plot_range_x, popt[0], popt[1], popt[2], popt[7], popt[8]), '--g', label='Tail1')
                    plt.plot(fit_plot_range_x, tail_ftn_isol(fit_plot_range_x, popt[3], popt[4], popt[5], popt[9]), '--b', label='Tail2')
                    plt.sj_hlines(popt[6], '--m', label='BG')
                    plt.title("A_1 = %.1f +/- %.1f, x0_1 = %.2f +/- %.3f, FWHM_1 = %.3f +/- %.3f\nA_2 = %.1f +/- %.1f, x0_2 = %.2f +/- %.3f, FWHM_2 = %.3f +/- %.3f, b = %.2f +/- %.2f\ntailfrac_1 = %.3f +/- %.3f, tailtau_1 = %.3f +/- %.3f, tau_2 = %.3f +/- %.3f" %(popt[0], perr[0], popt[1], perr[1], popt[2]*SIGMA2FWHM, perr[2]*SIGMA2FWHM, popt[3], perr[3], popt[4], perr[4], popt[5]*SIGMA2FWHM, perr[5]*SIGMA2FWHM, popt[6], perr[6], popt[7], perr[7], popt[8], perr[8], popt[9], perr[9]), fontsize='small')
                    plt.legend(loc="best", fontsize='small')
                    plt.subplot(212, sharex=ax_master)
                    plt.plot(x_to_fit, y_to_fit-one_gaussian_2tailed(x_to_fit, *popt))
                    plt.title('Residual (reduced chi2 = %f)' %chi2)
                    # popt[6] is background level
                    #print "\n***** Calculated counts in guassian: %d, tail: %d, bg: %d, total: %d *****\n" %(sum(gaussian(x_to_fit, popt[0], popt[1], popt[2], popt[3])-popt[3]), sum(tail_ftn(x_to_fit, popt[0], popt[1], popt[2], popt[4], popt[5])), (sum(gaussian_tailed(x_to_fit, *popt)) - sum(gaussian(x_to_fit, popt[0], popt[1], popt[2], popt[3])-popt[3]) - sum(tail_ftn(x_to_fit, popt[0], popt[1], popt[2], popt[4], popt[5]))), sum(gaussian_tailed(x_to_fit, *popt)))
            elif n_gauss == 2:
                if raw_data is True:
                    if p0 is None:
                        p0 = [A_i, x0_i, sigma_i, A_i*0.2, x0_i*x0_ratio, sigma_i*5, b_i, 0.1, 10, 0.1, 10]
                    popt, pcov = curve_fit(two_gaussian_2tailed, x_to_fit, y_to_fit, p0 = p0, bounds=([0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.], [np.inf, 30000., 100., np.inf, 30000., 100., b_upper_lim, 10, 1000., 10, 1000.]), sigma=uncertainty, **kwargs)
                    popt, pcov = curve_fit(two_gaussian_2tailed, x_to_fit, y_to_fit, p0 = popt, bounds=([0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.], [np.inf, 30000., 100., np.inf, 30000., 100., b_upper_lim, 10, 1000., 10, 1000.]), sigma=uncertainty, **kwargs)
                else:
                    if reduced is False: # n_tail=2, n_gauss=2, reduced=False
                        if p0 is None:
                            p0 = [A_i, x0_i, sigma_i, A_i*0.2, x0_i*x0_ratio, sigma_i*3, b_i, 0.1, 10, 0.1, 10]
                        # print(len(p0), p0)
                        popt, pcov = curve_fit(two_gaussian_2tailed, x_to_fit, y_to_fit, p0 = p0, bounds=([0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.], [np.inf, 3000., 1., np.inf, 3000., 10., b_upper_lim, 1, 100., 100, 100.]), sigma=uncertainty, **kwargs)
                        popt, pcov = curve_fit(two_gaussian_2tailed, x_to_fit, y_to_fit, p0 = popt, bounds=([0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.], [np.inf, 3000., 1., np.inf, 3000., 10., b_upper_lim, 1, 100., 100, 100.]), sigma=uncertainty, **kwargs)
                    else: # n_tail=2, n_gauss=2, reduced=True
                        if p0 is None:
                            p0 = [A_i, x0_i, sigma_i, A_i*0.2, sigma_i*3, b_i, 0.1, 10, 0.1, 10]
                        popt, pcov = curve_fit(two_gaussian_2tailed_reduced, x_to_fit, y_to_fit, p0 = p0, bounds=([0., 0., 0., 0., 0., 0., 0., 0., 0., 0.], [np.inf, 3000., 1., np.inf, 10., b_upper_lim, 1, 100., 100, 100.]), sigma=uncertainty, **kwargs)
                        popt, pcov = curve_fit(two_gaussian_2tailed_reduced, x_to_fit, y_to_fit, p0 = popt, bounds=([0., 0., 0., 0., 0., 0., 0., 0., 0., 0.], [np.inf, 3000., 1., np.inf, 10., b_upper_lim, 1, 100., 100, 100.]), sigma=uncertainty, **kwargs)
                perr = np.sqrt(np.diag(pcov))
                if reduced is False:
                    chi2 = np.sum(np.square(two_gaussian_2tailed(x_to_fit, *popt) - y_to_fit)/np.square(err))/(len(x_to_fit)-len(popt)-1) # according to wikipedia goodness_of_fit
                else:
                    chi2 = np.sum(np.square(two_gaussian_2tailed_reduced(x_to_fit, *popt) - y_to_fit)/np.square(err))/(len(x_to_fit)-len(popt)-1) # according to wikipedia goodness_of_fit
                fit_plot_range_x = np.arange(int(2*fit_start-x_to_fit[y_to_fit.argmax()]), int(2*fit_end-x_to_fit[y_to_fit.argmax()]), 0.1)#x_to_fit[1]-x_to_fit[0])
                if plot_it:
                    if reduced is False:
                        plt.plot(fit_plot_range_x, two_gaussian_2tailed(fit_plot_range_x, *popt),'r')
                        plt.plot(fit_plot_range_x, gaussian(fit_plot_range_x, popt[0], popt[1], popt[2], 0), '--y', label='Gausian1')
                        plt.plot(fit_plot_range_x, gaussian(fit_plot_range_x, popt[3], popt[4], popt[5], 0), '--c', label='Gausian2')
                        plt.plot(fit_plot_range_x, tail_ftn(fit_plot_range_x, popt[0], popt[1], popt[2], popt[7], popt[8]), '--g', label='Tail1')
                        plt.plot(fit_plot_range_x, tail_ftn(fit_plot_range_x, popt[3], popt[4], popt[5], popt[9], popt[10]), '--b', label='Tail2')
                        plt.sj_hlines(popt[6], '--m', label='BG')
                        plt.title("A_1 = %.1f +/- %.1f, x0_1 = %.2f +/- %.3f, FWHM_1 = %.3f +/- %.3f\nA_2 = %.1f +/- %.1f, x0_2 = %.2f +/- %.3f, FWHM_2 = %.3f +/- %.3f, b = %.2f +/- %.2f\ntailfrac_1 = %.3f +/- %.3f, tailtau_1 = %.3f +/- %.3f, frac_2 = %.3f +/- %.3f, tau_2 = %.3f +/- %.3f" %(popt[0], perr[0], popt[1], perr[1], popt[2]*SIGMA2FWHM, perr[2]*SIGMA2FWHM, popt[3], perr[3], popt[4], perr[4], popt[5]*SIGMA2FWHM, perr[5]*SIGMA2FWHM, popt[6], perr[6], popt[7], perr[7], popt[8], perr[8], popt[9], perr[9], popt[10], perr[10]), fontsize='small')
                    else:
                        plt.plot(fit_plot_range_x, two_gaussian_2tailed_reduced(fit_plot_range_x, *popt),'r')
                        plt.plot(fit_plot_range_x, gaussian(fit_plot_range_x, popt[0], popt[1], popt[2], 0), '--y', label='Gausian1')
                        plt.plot(fit_plot_range_x, gaussian(fit_plot_range_x, popt[3], popt[1], popt[4], 0), '--c', label='Gausian2')
                        plt.plot(fit_plot_range_x, tail_ftn(fit_plot_range_x, popt[0], popt[1], popt[2], popt[6], popt[7]), '--g', label='Tail1')
                        plt.plot(fit_plot_range_x, tail_ftn(fit_plot_range_x, popt[3], popt[1], popt[4], popt[8], popt[9]), '--b', label='Tail2')
                        plt.sj_hlines(popt[5], '--m', label='BG')
                        plt.title("A_1 = %.1f +/- %.1f, x0_1 = %.2f +/- %.3f, FWHM_1 = %.3f +/- %.3f\nA_2 = %.1f +/- %.1f, FWHM_2 = %.3f +/- %.3f, b = %.2f +/- %.2f\ntailfrac_1 = %.3f +/- %.3f, tailtau_1 = %.3f +/- %.3f, frac_2 = %.3f +/- %.3f, tau_2 = %.3f +/- %.3f" %(popt[0], perr[0], popt[1], perr[1], popt[2]*SIGMA2FWHM, perr[2]*SIGMA2FWHM, popt[3], perr[3], popt[4]*SIGMA2FWHM, perr[4]*SIGMA2FWHM, popt[5], perr[5], popt[6], perr[6], popt[7], perr[7], popt[8], perr[8], popt[9], perr[9]), fontsize='small')
                    plt.legend(loc="best", fontsize='small')
                    plt.subplot(212, sharex=ax_master)
                    if reduced is False:
                        plt.plot(x_to_fit, y_to_fit-two_gaussian_2tailed(x_to_fit, *popt))
                    else:
                        plt.plot(x_to_fit, y_to_fit-two_gaussian_2tailed_reduced(x_to_fit, *popt))
                    plt.title('Residual (reduced chi2 = %f)' %chi2)
                    # popt[6] is background level
                    #print "\n***** Calculated counts in guassian: %d, tail: %d, bg: %d, total: %d *****\n" %(sum(gaussian(x_to_fit, popt[0], popt[1], popt[2], popt[3])-popt[3]), sum(tail_ftn(x_to_fit, popt[0], popt[1], popt[2], popt[4], popt[5])), (sum(gaussian_tailed(x_to_fit, *popt)) - sum(gaussian(x_to_fit, popt[0], popt[1], popt[2], popt[3])-popt[3]) - sum(tail_ftn(x_to_fit, popt[0], popt[1], popt[2], popt[4], popt[5]))), sum(gaussian_tailed(x_to_fit, *popt)))
    if plot_it:
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.subplot(211)
        plt.ylabel(ylabel)

    return popt, pcov, perr


def fit_gaussian_ML_old(sigma = 1):
    '''
    now deprecated with newer version of MASS
    '''
    ax = plt.gca()
    xlabel = ax.get_xlabel()
    ylabel = ax.get_ylabel()
    try:
        line = ax.get_lines()[-1]
    except IndexError:
        print("No line found")
        return
    xd = line.get_xdata()
    yd = line.get_ydata()
    #plt.figure()
    #plt.plot(xd,yd)
    coordi = plt.ginput(2) # [(x0, y0), (x1, y1)]
    coordi=np.asarray(coordi)
    coordi_sorted = coordi[coordi[:,0].argsort()]
    ginput_x0 = coordi_sorted[0,0]
    ginput_x1 = coordi_sorted[1,0]
    #print ginput_x0, ginput_x1
    x_to_fit = xd[np.logical_and(xd>=ginput_x0, xd<=ginput_x1)]
    y_to_fit = yd[np.logical_and(xd>=ginput_x0, xd<=ginput_x1)]
    plt.figure();
    ax_master = plt.subplot(211)
    plt.plot(x_to_fit, y_to_fit)
    plt.ylabel(ylabel)
    fitter = mass.mathstat.fitting.MaximumLikelihoodGaussianFitter(x_to_fit, y_to_fit, [sigma*SIGMA2FWHM, x_to_fit[y_to_fit.argmax()], y_to_fit.max(), 0.])
    popt, pcov = fitter.fit()
    fit_plot_range_x = np.arange(int(2*ginput_x0-x_to_fit[y_to_fit.argmax()]), int(2*ginput_x1-x_to_fit[y_to_fit.argmax()]), 0.1)#x_to_fit[1]-x_to_fit[0])
    plt.plot(fit_plot_range_x, gaussian_ML(fit_plot_range_x, popt[0], popt[1], popt[2], popt[3]),'r')
    plt.subplot(212, sharex=ax_master)
    plt.plot(x_to_fit, y_to_fit-gaussian_ML(x_to_fit, popt[0], popt[1], popt[2], popt[3]))
    plt.title('residual')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    perr = np.sqrt(np.diag(pcov))
    chi_sq = fitter.chisq
    print("FWHM = %f +/- %f, chi_sq = %f" %(popt[0], perr[0], chi_sq))
    return popt, pcov, perr


def fit_gaussian_ML(sigma = 1):
    '''
    newer version
    '''
    ax = plt.gca()
    xlabel = ax.get_xlabel()
    ylabel = ax.get_ylabel()
    try:
        line = ax.get_lines()[-1]
    except IndexError:
        print("No line found")
        return
    xd = line.get_xdata()
    yd = line.get_ydata()
    #plt.figure()
    #plt.plot(xd,yd)
    coordi = plt.ginput(2) # [(x0, y0), (x1, y1)]
    coordi=np.asarray(coordi)
    coordi_sorted = coordi[coordi[:,0].argsort()]
    ginput_x0 = coordi_sorted[0,0]
    ginput_x1 = coordi_sorted[1,0]
    #print ginput_x0, ginput_x1
    x_to_fit = xd[np.logical_and(xd>=ginput_x0, xd<=ginput_x1)]
    y_to_fit = yd[np.logical_and(xd>=ginput_x0, xd<=ginput_x1)]
    plt.figure();
    ax_master = plt.subplot(211)
    plt.plot(x_to_fit, y_to_fit)
    plt.ylabel(ylabel)
    fitter = mass.mathstat.fitting.MaximumLikelihoodHistogramFitter(x_to_fit, y_to_fit, [sigma*SIGMA2FWHM, x_to_fit[y_to_fit.argmax()], y_to_fit.max(), 0.], gaussian_ML)
    popt, pcov = fitter.fit()
    fit_plot_range_x = np.arange(int(2*ginput_x0-x_to_fit[y_to_fit.argmax()]), int(2*ginput_x1-x_to_fit[y_to_fit.argmax()]), 0.1)#x_to_fit[1]-x_to_fit[0])
    plt.plot(fit_plot_range_x, gaussian_ML((popt[0], popt[1], popt[2], popt[3]), fit_plot_range_x),'r')
    plt.subplot(212, sharex=ax_master)
    plt.plot(x_to_fit, y_to_fit-gaussian_ML((popt[0], popt[1], popt[2], popt[3]), x_to_fit))
    plt.title('residual')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    perr = np.sqrt(np.diag(pcov))
    chi_sq = fitter.chisq
    print("FWHM = %f +/- %f, chi_sq = %f" %(popt[0], perr[0], chi_sq))
    return popt, pcov, perr


def fit_ML(fit_ftn, p0):
    '''
    fit_ftn = lambda ftn (maximum likelihood)
    '''
    ax = plt.gca()
    xlabel = ax.get_xlabel()
    ylabel = ax.get_ylabel()
    try:
        line = ax.get_lines()[-1]
    except IndexError:
        print("No line found")
        return
    xd = line.get_xdata()
    yd = line.get_ydata()
    #plt.figure()
    #plt.plot(xd,yd)
    coordi = plt.ginput(2) # [(x0, y0), (x1, y1)]
    coordi = np.asarray(coordi)
    coordi_sorted = coordi[coordi[:,0].argsort()]
    ginput_x0 = coordi_sorted[0,0]
    ginput_x1 = coordi_sorted[1,0]
    #print ginput_x0, ginput_x1
    x_to_fit = xd[np.logical_and(xd>=ginput_x0, xd<=ginput_x1)]
    y_to_fit = yd[np.logical_and(xd>=ginput_x0, xd<=ginput_x1)]
    plt.figure();
    ax_master = plt.subplot(211)
    plt.plot(x_to_fit, y_to_fit)
    plt.ylabel(ylabel)
    fitter = mass.mathstat.fitting.MaximumLikelihoodHistogramFitter(x_to_fit, y_to_fit, p0, fit_ftn)
    popt, pcov = fitter.fit()
    fit_plot_range_x = np.arange(int(2*ginput_x0-x_to_fit[y_to_fit.argmax()]), int(2*ginput_x1-x_to_fit[y_to_fit.argmax()]), 0.1)#x_to_fit[1]-x_to_fit[0])
    plt.plot(fit_plot_range_x, gaussian_ML((popt[0], popt[1], popt[2], popt[3]), fit_plot_range_x),'r')
    plt.subplot(212, sharex=ax_master)
    plt.plot(x_to_fit, y_to_fit-gaussian_ML((popt[0], popt[1], popt[2], popt[3]), x_to_fit))
    plt.title('residual')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    perr = np.sqrt(np.diag(pcov))
    chi_sq = fitter.chisq
    print("FWHM = %f +/- %f, chi_sq = %f" %(popt[0], perr[0], chi_sq))
    return popt, pcov, perr

# [A,i0]=max(y);
# x0=x(i0);
# i_half=get_index(y(i0:end),A/2)+i0-1;
# x2=x(i_half) - x(i0);
# Sigma=x2/sqrt(2*log(2));


def fit(fit_ftn, p0=None, bounds=(-np.inf,np.inf), hold=None, x=None, y=None, xrange=None, sigma=None, absolute_sigma=True, plot_it=True, marker=None):
    '''
    p0 and bounds should be provided
    example for holding two parameters: hold=[0, 2]
    If x and y are not given, it will call ginput()
    If xrange is not given in commandline mode, it will use the whole range
    marker=None or '.', 'o', etc.
    returns popt, pcov, perr
    '''
    global fit_ftn_global
    fit_ftn_global = fit_ftn

    ftn_name = str(fit_ftn).split(' ')[1]
    try:
        fit_ftn() # this is to grab the args list of the function
    except TypeError as e:
        variable_names = str(e).split('\'')[1::2]
        print('list of parameters:', variable_names[1:])
    # if (p0 is None) or (bounds is None):
    #     raise Exception('Provide p0 and bounds!')

    if np.shape(bounds)[0] != 2:
        print('bounds not valid')
        return
    else:
        if hold is not None:
            lower_bounds = delete_index(bounds[0], hold)
            upper_bounds = delete_index(bounds[1], hold)
            bounds = (lower_bounds, upper_bounds)
        else:
            print('lower bounds:', bounds[0])
            print('upper bounds:', bounds[1])

    xlabel = ylabel = None
    if (x is None) and (y is None):
        ax = plt.gca()
        xlabel = ax.get_xlabel()
        ylabel = ax.get_ylabel()
        try:
            line = ax.get_lines()[-1]
        except IndexError:
            print("No line found")
            return
        x = line.get_xdata()
        y = line.get_ydata()
        if xrange is None:
            coordi = plt.ginput(2) # [(x0, y0), (x1, y1)]
            coordi = np.asarray(coordi)
            coordi_sorted = coordi[coordi[:,0].argsort()]
            xrange = [coordi_sorted[0,0], coordi_sorted[1,0]]
    if xrange is None:
        xrange = [x[0], x[-1]]
    x_to_fit = x[np.logical_and(x>=xrange[0], x<=xrange[1])]
    y_to_fit = y[np.logical_and(x>=xrange[0], x<=xrange[1])]
    if plot_it:
        plt.figure();
        ax_master = plt.subplot(211)
        if sigma is None:
            plt.plot(x_to_fit, y_to_fit, label='data', marker=marker)
        else:
            plt.errorbar(x_to_fit, y_to_fit, yerr=sigma, label='data', marker=marker)
        # plt.title(ftn_name)
        plt.ylabel(ylabel)
        fit_plot_range_x = np.arange(int(2*xrange[0]-x_to_fit[y_to_fit.argmax()]), int(2*xrange[1]-x_to_fit[y_to_fit.argmax()]), 0.1)#x_to_fit[1]-x_to_fit[0])
    if hold is None:
        popt, pcov = curve_fit(fit_ftn, x_to_fit, y_to_fit, p0=p0, bounds=bounds, sigma=sigma, absolute_sigma=absolute_sigma)
        if plot_it:
            plt.plot(fit_plot_range_x, fit_ftn(fit_plot_range_x, *popt), 'r', label=ftn_name)
            plt.title(str(variable_names[1:])+':\n'+str(popt), fontsize='x-small')
            plt.legend()
            plt.subplot(212, sharex=ax_master)
            plt.plot(x_to_fit, y_to_fit-fit_ftn(x_to_fit, *popt), label='residual', marker=marker)
            # plt.title('residual')
            plt.xlabel(xlabel)
            plt.ylabel(ylabel)
            plt.legend()
        perr = np.sqrt(np.diag(pcov))
        # print("chi_sq = %f" %(chi_sq))
        return popt, pcov, perr
    else:
        reduced_p0 = delete_index(p0, hold)
        # print(reduced_p0)
        # print(p0)
        assert(len(reduced_p0)==len(variable_names)-len(hold)-1) # -1 because the first one is 'x'
        held_variable_names = [variable_names[1:][hold_i] for hold_i in hold]
        reduced_variable_names = delete_index(variable_names[1:], hold)
        print('held parameter(s):', held_variable_names)
        print('fit parameters:', reduced_variable_names)
        print('lower bounds:', bounds[0])
        print('upper bounds:', bounds[1])
        assert(len(reduced_p0)==len(reduced_variable_names))
        for hold_i in hold:
            variable_names[hold_i+1] = str(p0[hold_i]) # +1 because the first one is 'x'
        cmd = "reduced_fit_ftn = lambda x, %s: globals()['fit_ftn_global'](%s)"%(','.join(reduced_variable_names),','.join(variable_names))
        # exec(cmd, globals(), locals())
        exec(cmd)
        cmd = "fit_result = curve_fit(locals()['reduced_fit_ftn'], locals()['x_to_fit'], locals()['y_to_fit'], p0=locals()['reduced_p0'], bounds=locals()['bounds'], sigma=locals()['sigma'], absolute_sigma=locals()['absolute_sigma'])"
        # exec(cmd, globals(), locals())
        exec(cmd)
        popt, pcov = locals()['fit_result']
        if plot_it is True:
            plt.plot(fit_plot_range_x, locals()['reduced_fit_ftn'](fit_plot_range_x, *popt), 'r', label=ftn_name)
            plt.legend()
            plt.title(str(reduced_variable_names)+':\n'+str(popt), fontsize='x-small')
            plt.subplot(212, sharex=ax_master)
            plt.plot(x_to_fit, y_to_fit-locals()['reduced_fit_ftn'](x_to_fit, *popt), label='residual', marker=marker)
            # plt.title('residual')
            plt.xlabel(xlabel)
            plt.ylabel(ylabel)
            plt.legend()
        perr = np.sqrt(np.diag(pcov))
        # print("chi_sq = %f" %(chi_sq))
        return popt, pcov, perr


def ss_opt_num_bin(x):
    """
    based on Shimazaki and Shinomoto. Neural Comput, 2007, 19(6), 1503-1527
    http://toyoizumilab.brain.riken.jp/hideaki/res/histogram.html
    x should be a list or array, for now assumed to be filt_value (or filt_value_dc), not p_energy in eV
    """
    try:
        x = np.asarray(x)
        max_bin = len(x)

        bin_width_to_scan = xrange(1,30,2)
        data_span = max(x) - min(x)
        # n_bin = np.zeros(len(bin_width_to_scan))
        C = np.zeros(len(bin_width_to_scan))
        for i, bw in enumerate(bin_width_to_scan):
            n_bin = int(data_span/bw) + 1
            c, b = np.histogram(x, n_bin)
            k = 1./n_bin * sum(c)
            #print sum(c), n_bin, k
            v = 1./n_bin * sum(np.square(c-k))
            C[i] = (2*k-v)/bw**2

        opt_bw = bin_width_to_scan[np.argmin(C)]
        opt_num_bin = int(data_span/bin_width_to_scan[np.argmin(C)]) + 1
        return opt_num_bin
    except KeyboardInterrupt:
        raise KeyboardInterrupt('here')
        return
    except IndexError:
        print('max(x):', max(x))
        print('min(x):', min(x))
        print('n_bin:', n_bin)
        raise IndexError()


def sj_hlines(y, xmin=None, xmax=None, linestyle='-r', label='', **kwargs):
    """
    sj modifed original plt.hlines to be able to give only one arg (y)
    example of **kwargs: color='gold'
    """
    ax = plt.gca()
    if isinstance(xmin, str):
        linestyle = xmin
        xmin = None
    if xmin is None:
        xmin = ax.get_xlim()[0]
    if xmax is None:
        xmax = ax.get_xlim()[1]
    linestyles = ""
    colors = "r"
    for c in linestyle:
        if c in ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']:
            colors = c
        else:
            linestyles += c
    if linestyles == "":
        linestyles = "-"
    # for key, value in kwargs.items():
    #     if key == "color":
    #         colors = value

    ret = ax.hlines(y, xmin, xmax, colors=colors, linestyles=linestyles, label=label, **kwargs)
    plt.draw_if_interactive()

    return ret

plt.sj_hlines = sj_hlines


def sj_vlines(x, ymin=None, ymax=None, linestyle='-r', label='', **kwargs):
    """
    sj modifed original plt.hlines to be able to give only one arg (x)
    example of **kwargs: color='gold'
    """
    ax = plt.gca()
    if isinstance(ymin, str):
        linestyle = ymin
        ymin = None
    if ymin is None:
        ymin = ax.get_ylim()[0]
    if ymax is None:
        ymax = ax.get_ylim()[1]
    linestyles = ""
    colors = "r"
    for c in linestyle:
        if c in ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']:
            colors = c
        else:
            linestyles += c
    if linestyles == "":
        linestyles = "-"
    # for key, value in kwargs.items():
    #     if key == "color":
    #         colors = value

    ret = ax.vlines(x, ymin, ymax, colors=colors, linestyles=linestyles, label=label, **kwargs)
    plt.draw_if_interactive()

    return ret

plt.sj_vlines = sj_vlines


def sj_lines_multi_figs(vals, orientation='v', linestyle='-r', label='', **kwargs):
    for fig_num in plt.get_fignums():
        plt.figure(fig_num)
        if orientation == 'v':
            sj_vlines(vals, linestyle=linestyle, label=label, **kwargs)
        elif orientation == 'h':
            sj_hlines(vals, linestyle=linestyle, label=label, **kwargs)

plt.sj_lines_multi_figs = sj_lines_multi_figs


def multi_fig_action():
    pass


def running_mean_insert0(x, N=3):
    cumsum = np.cumsum(np.insert(x, 0, 0))
    return (cumsum[N:] - cumsum[:-N]) / N


def running_mean(x, n=3):
    """
    use running_mean_conv
    """
    ret = np.cumsum(x, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n


def running_mean_conv(x, window_size=3, mode="valid"):
    """
    available modes: "full", "same", "valid"
    """
    if isinstance(window_size, str):
        mode = window_size
        window_size = 3
    return np.convolve(x, np.ones(int(window_size))/float(window_size), mode=mode)


def convolve(y, sigma=0.001, tailfrac=None, tailtau=None, mode='same'):
    """
    sigma is unitless sigma. return convolution normalized by sum
    sigma only works when y is counts/bin[eV]
    """
    #n_step = len(y) if len(y) < 1000 else 1000
    #xx = np.arange(-0.01,0.01,0.02/n_step)
    xx = np.arange(-0.01,0.01,0.02/1000) # change arange to linsapce?
    if (tailfrac is not None) and (tailtau is not None):
        conv_filter = gaussian_tailed(xx, 1, 0, sigma, 0, tailfrac, tailtau)
    else:
        conv_filter = gaussian(xx, 1, 0, sigma, 0)
    y_conv = sig.convolve(y, conv_filter, mode)
    #y_conv = sig.convolve(y, sig.gaussian(1000,sigma), mode)
    y_conv_norm = y_conv/sum(y_conv)*sum(y)
    return y_conv_norm


def deconvolve(y, sigma=0.001, tailfrac=None, tailtau=None):
    """
    sigma is unitless sigma. return convolution normalized by sum
    """
    xx = np.arange(-0.01,0.01,0.00002) # change arange to linsapce?
    if (tailfrac is not None) and (tailtau is not None):
        yy = gaussian_tailed(xx, 1, 0, sigma, 0, tailfrac, tailtau)
    else:
        yy = gaussian(xx, 1, 0, sigma, 0)
    y_deconv, remainder = sig.deconvolve(y, sig.gaussian(10000,0.001))
    #y_conv_norm = y_conv/sum(y_conv)*sum(y)
    return y_deconv, remainder


def rebin_shape(a, newshape):
        '''Rebin an array to a new shape.
        modified from https://scipy-cookbook.readthedocs.io/items/Rebinning.html
        '''
        assert len(np.shape(a)) == len(newshape)

        slices = [ slice(0,old, float(old)/new) for old,new in zip(np.shape(a),newshape) ]
        coordinates = np.mgrid[slices]
        indices = np.asarray(coordinates, dtype='i')   #choose the biggest smaller integer index
        if type(a) == list:
            return (np.asarray(a)[tuple(indices)]).tolist()
        else:
            return a[tuple(indices)]


def rebin_factor(a, factor):
        '''Rebin an array to a new shape.
        '''
        assert len(np.shape(a)) == len(factor)
        newshape = (np.shape(a)/np.asarray(factor)).astype('i')

        return rebin_shape(a, newshape)


def sj_test():

    if 'dir_base' in vars():
        print(1)
    #cmd = "if 'dir_base' in locals():\n    print 1\nprint 1"
    #cmd = "if dir().count('dir_base'):\n    print 1\nprint 1"
    #exec(cmd)


def sj_fig2file(self=None, filename=None, ext='.txt', fig_ext=['.pdf','.png'], foldername=None, fignum=None, fillvalue='NaN', plot_only=False):
    """
    save all the lines (except those made by hlines and vlines) into a txt file and save figure as a pdf
    subplot not supported yet
    example usage: useful_ftns.sj_fig2file() => current fig is saved with a file name being today's date
    useful_ftns.sj_fig2file(fignum=3)
    useful_ftns.sj_fig2file(3)
    useful_ftns.sj_fig2file('filename')
    useful_ftns.sj_fig2file(fillvalue='NaN')
    useful_ftns.sj_fig2file(reduced_data)
    data.sj_fig2file() # need to check this
    """
    #from astropy.io import ascii
    from datetime import datetime
    try:
        from itertools import izip_longest # does not work in python3
    except ImportError:
        from itertools import zip_longest as izip_longest
    #import csv

    if isinstance(self, int):
        fignum = self

    if isinstance(self, str):
        filename = self

    if isinstance(filename, int):
        fignum = filename
        filename = None

    if fignum == None:
        ax = plt.gca()
        fignum = plt.gcf().canvas.manager.num
    else:
        if fignum in plt.get_fignums():
            plt.figure(fignum)
            ax = plt.gca()
        else:
            print("Figure not saved. Choose an existing figure!")
            return

    if plot_only is True:
        fig_ext = list(fig_ext)
        for figext in fig_ext:
            ax.get_figure().savefig(filename.split('.')[0]+figext)
        print("Fig %g was saved without an accompanying ascii file (script not ready for subplots)."%fignum)
        return

    num_lines = len(ax.get_lines())
    num_images = len(ax.images)
    num_collections = len(ax.collections)

    if num_images:
        # image = ax.images[0] # checking if it's an image made by imshow
        if 'TESGroup' in str(type(self)): # this branch is not tested yet
            self.sj_img2file(filename=filename, ext=ext, fig_ext=fig_ext, fignum=fignum, fillvalue=fillvalue)
        else:
            sj_img2file(self, filename=filename, ext=ext, fig_ext=fig_ext, fignum=fignum, fillvalue=fillvalue)
        return

    num_h_v_lines = 0
    if num_collections:
        num_h_v_lines = sum([collection.get_array() is None for collection in ax.collections])
        if num_h_v_lines:
            h_v_line_list = []
            for i, v in enumerate([collection.get_array() is None for collection in ax.collections]):
                if v==True:
                    h_v_line_list.append(i)
        if num_collections - num_h_v_lines > 0: # this means there is a collection (probably made by pcolormesh)
            if 'TESGroup' in str(type(self)): # this branch is not tested yet
                self.sj_img2file(filename=filename, ext=ext, fig_ext=fig_ext, fignum=fignum, fillvalue=fillvalue)
            else:
                sj_img2file(self, filename=filename, ext=ext, fig_ext=fig_ext, fignum=fignum, fillvalue=fillvalue)
            return

    if num_lines == 0:
        try:
            if filename is None:
                filename = datetime.now().strftime('%Y%m%d_%H%M%S')

            if '.' in filename:
                ext = ''

            if (ext != '') and ('.' not in ext):
                ext = '.' + ext

            filename = filename + ext

            fig_ext = list(fig_ext)
            for figext in fig_ext:
                ax.get_figure().savefig(filename.split('.')[0]+figext)
            print("Fig %g was saved without an accompanying ascii file."%fignum)
            return
        except:
            print("Something went wrong. Nothing save for Fig %g."%fignum)
            return
    else:
        if len(plt.gcf().get_axes()) > 1: # multiple subplots
            if filename is None:
                filename = datetime.now().strftime('%Y%m%d_%H%M%S')

            if '.' in filename:
                ext = ''

            if (ext != '') and ('.' not in ext):
                ext = '.' + ext

            filename = filename + ext

            fig_ext = list(fig_ext)
            for figext in fig_ext:
                ax.get_figure().savefig(filename.split('.')[0]+figext)
            print("Fig %g was saved without an accompanying ascii file (script not ready for subplots)."%fignum)
            return

        lines = ax.get_lines()

        header = ""

        title = ax.get_title()
        if title:
            header += "title: " + title + '\n'

        figwidth = ax.figure.get_figwidth()
        header += "figwidth(inch): " + str(figwidth) + '\n'
        figheight = ax.figure.get_figheight()
        header += "figheight(inch): " + str(figheight) + '\n'

        xlabel = ax.get_xlabel()
        if xlabel:
            header += "xlabel: " + xlabel + '\n'

        ylabel = ax.get_ylabel()
        if ylabel:
            header += "ylabel: " + ylabel + '\n'
        header += 'xlim: ' + str(ax.get_xlim()) + '\n'
        header += 'ylim: ' + str(ax.get_ylim()) + '\n'

        header += 'line_markers:'
        data_matrix = ()
        for line in lines:
            data_matrix += (list(line.get_xdata()), list(line.get_ydata()))
            header += ' ' + line.get_marker()
        max_len = len(max(map(str, np.hstack(data_matrix)), key=len))
        header += '\n'

        header += 'line_colors:'
        for line in lines:
            header += ' ' + str(line.get_color()).replace(' ','')
        header += '\n'

        header += 'line_linestyles:'
        for line in lines:
            header += ' ' + line.get_linestyle()
        header += '\n'

        header += 'line_drawstyles:'
        for line in lines:
            header += ' ' + line.get_drawstyle()
        header += '\n'

        if num_h_v_lines:
            for i, v in enumerate(h_v_line_list):
                segments = ax.collections[v].get_segments()
                if segments[0][0][0]==segments[0][1][0]: # vline
                    x = [segments[ii][0][0] for ii in range(len(segments))]
                    ymin = segments[0][0][1]
                    ymax = segments[0][1][1]
                    header += 'vline_%s x: '%i + str(x) + '\n'
                    header += 'vline_%s ymin: '%i + str(ymin) + '\n'
                    header += 'vline_%s ymax: '%i + str(ymax) + '\n'
                    header += 'vline_%s linestyle: '%i + str(ax.collections[v].get_linestyle()) + '\n'
                    # header += 'vline_%s color: '%i + str([map(float, str(ax.collections[v].get_color()).strip(']]').split()[1:5])]) + '\n'
                    header += 'vline_%s color: '%i + str([np.asfarray(str(ax.collections[v].get_color()).strip(']]').split()[1:5])]) + '\n'
                else: # hline
                    y = [segments[ii][0][1] for ii in range(len(segments))]
                    xmin = segments[0][0][0]
                    xmax = segments[0][1][0]
                    header += 'hline_%s y: '%i + str(y) + '\n'
                    header += 'hline_%s xmin: '%i + str(xmin) + '\n'
                    header += 'hline_%s xmax: '%i + str(xmax) + '\n'
                    header += 'hline_%s linestyle: '%i + str(ax.collections[v].get_linestyle()) + '\n'
                    # header += 'hline_%s color: '%i + str([map(float, str(ax.collections[v].get_color()).strip(']]').split()[1:5])]) + '\n'
                    header += 'hline_%s color: '%i + str([np.asfarray(str(ax.collections[v].get_color()).strip(']]').split()[1:5])]) + '\n'

        if len(ax.texts):
            text_strings = ''
            text_positinos = ''
            text_colors = ''
            text_alignments = ''
            text_sizes = []
            for text in ax.texts:
                text_strings += text.get_text()+','
                text_positinos += str(text.get_position()).replace(' ', '')+','
                text_colors += str(text.get_color()).replace(' ','')+','
                text_alignments += str(text.get_horizontalalignment())+','
                text_sizes.append(text.get_fontsize())
            header += 'text_strings: ' + text_strings[:-1] +'\n'
            header += 'text_positions: ' + text_positinos[:-1] +'\n'
            header += 'text_colors: ' + text_colors[:-1] +'\n'
            header += 'text_alignments: ' + text_alignments[:-1] +'\n'
            header += 'text_sizes: ' + str(text_sizes) +'\n'

        data_label_str = ""
        if ax.get_legend() is None:
            max_len = max(max_len, len(str(len(lines)-1))+3)
            for i, line in enumerate(lines):
                xlabel_temp = '<x_%d>'%i
                ylabel_temp = '<y_%d>'%i
                data_label_str += "%-*s %-*s "%(max_len,xlabel_temp,max_len,ylabel_temp)
        else:
            legend_title = ax.get_legend().get_title().get_text()
            if legend_title != u'None':
                header += 'legend title: ' + legend_title + '\n'
            #legend_texts = [ax.get_legend().get_texts()[i].get_text() for i in xrange(len(ax.get_legend().get_texts()))]
            legend_texts = []
            for i in xrange(len(ax.get_legend().get_texts())):
                temp = ax.get_legend().get_texts()[i].get_text()
                if '\n' in temp:
                    temp = temp.replace('\n', ' ')
                legend_texts.append(temp)
            if len(legend_texts) < len(lines):
                for _ in xrange(len(lines)-len(legend_texts)):
                    legend_texts.append('')
            #data_label = ['noname' if legend_texts[i] == '' else legend_texts[i] for i in xrange(len(ax.get_lines()))]
            data_label = []
            for i in xrange(len(legend_texts)):
                if legend_texts[i] == "":
                    data_label.append("<x_%d>"%i)
                    data_label.append("<y_%d>"%i)
                else:
                    data_label.append("<"+legend_texts[i]+"_x>")
                    data_label.append("<"+legend_texts[i]+"_y>")
            max_len = max(max_len, len(max(data_label, key=len)))
            for i in xrange(len(ax.get_lines())*2):
                # if '\n' in data_label[i]:
                #     data_label[i].replace('\n', " ")
                data_label_str += "%-*s "%(max_len,data_label[i])
        header += data_label_str

        rows = izip_longest(*data_matrix, fillvalue=fillvalue)
        data_to_write = list(rows)
        # eval_string = "rows = izip_longest(data_matrix[0]"
        # for i in range(1,len(lines)*2):
        #     eval_string += ", data_matrix[%d]"%i
        # eval_string += ", fillvalue='')"
        # exec(eval_string)

        if filename is None:
            if isinstance(self, np.ndarray):
                filename = self[0].dir_p + '_' + datetime.now().strftime('%Y%m%d_%H%M%S')
            else:
                filename = datetime.now().strftime('%Y%m%d_%H%M%S')

        if '.' in filename:
            ext = ''

        if (ext != '') and ('.' not in ext):
            ext = '.' + ext

        filename = filename + ext

        # with open(filename, 'w') as f:
        #     #csv.writer(f).writerows(rows)
        #     writer = csv.writer(f)
        #     writer.writerow(header)
        #     writer.writerows(rows)

        fmt = '  %-'+'%d'%(max_len-2) +'s'
        #with open(filename, 'w') as f:
        np.savetxt(filename, data_to_write, fmt=fmt, delimiter=' ', header=header)

        fig_ext = list(fig_ext)
        for figext in fig_ext:
            ax.get_figure().savefig(filename.split('.')[0]+figext)

mass.TESGroup.sj_fig2file = sj_fig2file


def sj_img2file(self, filename=None, ext='.txt', fig_ext=['.pdf','.png'], fignum=None, fillvalue='NaN'):
    #from astropy.io import ascii
    from datetime import datetime
    import gzip
    try:
        from itertools import izip_longest # does not work in python3
    except:
        from itertools import zip_longest as izip_longest
    #import csv

    if isinstance(filename, int):
        fignum = filename
        filename = None

    if fignum == None:
        ax = plt.gca()
    else:
        if fignum in plt.get_fignums():
            plt.figure(fignum)
            ax = plt.gca()
        else:
            print("Choose an existing figure")
            return

    header = ""

    try:
        a = ax.images[0]
        data_to_write = a.get_array()
        plot_type = 'imshow'
        shape_header = 'data_shape: ' + str(np.shape(data_to_write)) + '\n'
        #a.get_extent()
    except IndexError:
        try:
            plot_data_array = ax.collections[0].get_array()
            xx = ax.collections[0]._coordinates[0,:,0]
            last_xx = xx[-1] # last value stored separately
            xx = xx[:-1] # last value removed to match the data matrix dimension
            yy = ax.collections[0]._coordinates[:,0,1]
            yy_i = yy[0]
            yy_f = yy[-1]
            yy_d = yy[1]-yy[0]
            data_to_write = np.concatenate((xx,plot_data_array))
            data_to_write = np.reshape(data_to_write, (len(yy),len(xx)))
            shape_header = 'data_shape: (%d, %d)\n'%(len(yy)-1,len(xx)) # -1 for y because of mismatch between bin edges and data dimension. x is already reduced.
            # plot_data_array = np.reshape(ax.collections[0].get_array(),(ax.collections[0]._meshHeight,ax.collections[0]._meshWidth))
            # data_to_write = np.reshape(ax.collections[0].get_array(),(ax.collections[0]._meshHeight,ax.collections[0]._meshWidth))
            plot_type = 'pcolormesh'
            ax.collections[0]._rasterized=True # this is to reduce time to save into pdf
        except IndexError:
            print("No image found")
            return

    header += "plot type: " + plot_type + '\n'

    title = ax.get_title()
    if title:
        header += "title: " + title + '\n'

    figwidth = ax.figure.get_figwidth()
    header += "figwidth(inch): " + str(figwidth) + '\n'
    figheight = ax.figure.get_figheight()
    header += "figheight(inch): " + str(figheight) + '\n'

    xlabel = ax.get_xlabel()
    if xlabel:
        header += "xlabel: " + xlabel + '\n'

    ylabel = ax.get_ylabel()
    if ylabel:
        header += "ylabel: " + ylabel + '\n'
    header += 'xlim: ' + str(ax.get_xlim()) + '\n'
    header += 'ylim: ' + str(ax.get_ylim()) + '\n'

    if plot_type == 'imshow':
        cm = a.get_cmap()
        header += 'origin: ' + str(a.origin) + '\n'
        header += 'interpolation: ' + str(a.get_interpolation()) + '\n'
    elif plot_type == 'pcolormesh':
        cm = ax.collections[0].get_cmap()
        header += 'x_edges: first row of data + %s\n'%last_xx
        header += 'y_i, y_f, y_d: (%f, %f, %f)\n'%(yy_i, yy_f, yy_d)

    header += shape_header
    header += 'aspect: ' + str(ax.get_aspect()) + '\n'
    header += 'cmap: ' + str(cm.name) + '\n'

    if filename is None:
        if isinstance(self, np.ndarray):
            filename = self[0].dir_p + '_' + datetime.now().strftime('%Y%m%d_%H%M%S')
        else:
            filename = datetime.now().strftime('%Y%m%d_%H%M%S')

    if '.' in filename:
        ext = ''

    if (ext != '') and ('.' not in ext):
        ext = '.' + ext

    filename = filename + ext

    # with open(filename, 'w') as f:
    #     #csv.writer(f).writerows(rows)
    #     writer = csv.writer(f)
    #     writer.writerow(header)
    #     writer.writerows(rows)

    #fmt = '  %-'+'%d'%(max_len-2) +'s'
    #with open(filename, 'w') as f:
    np.savetxt(filename, data_to_write, delimiter=' ', header=header, fmt='%f')

    # compress the txt file into gzip
    with open(filename, 'rb') as f_in, gzip.open(filename[:-len(ext)]+'.gz', 'wb') as f_out:
        f_out.writelines(f_in)

    fig_ext = list(fig_ext)
    for figext in fig_ext:
        ax.get_figure().savefig(filename.split('.')[0]+figext)

mass.TESGroup.sj_img2file = sj_img2file


def sj_save_all_fig2file(self, filename=None, timestamp=True, ext='.txt', fig_ext = ['.pdf', '.png'], foldername='figures', fillvalue='NaN', plot_only=False):
    """
    save all open plots into ascii files and pdf files
    example usage:
    data.sj_save_all_fig2file()
    - or -
    useful_ftns.sj_save_all_fig2file(reduced_data)

    assigning a foldername is not implemented yet
    """
    from datetime import datetime
    if filename is None:
        if isinstance(self, np.ndarray):
            if filename is None:
                filename1 = self[0].dir_p
            else:
                filename1 = filename
        elif 'TESGroup' in str(type(self)): # if called by e.g. data.sj_save_all_fig2file
            if 'HDF5' in str(type(self)):
                filename1 = self.first_good_dataset.filename.split(os.sep)[-1][:12]
            else:
                filename1 = self.filenames[0].split(os.sep)[-2]
        else:
            filename1 = self
        if timestamp is True:
            filename = filename1+'__'+datetime.now().strftime('%Y%m%d_%H%M%S')
        else:
            filename = filename1
    else:
        if timestamp is True:
            filename = filename+'__'+datetime.now().strftime('%Y%m%d_%H%M%S')

    # if '.' in filename:
    #     filename = filename.split('.')[0]
    #     ext = filename.split('.')[1]

    if '.' not in ext:
        ext = '.' + ext

    fig_list = plt.get_fignums()

    for i in fig_list:
        file_name = filename +'_%02d'%i
        if isinstance(self, np.ndarray):
            sj_fig2file(self, file_name, ext=ext, fignum=i, fig_ext = ['.pdf', '.png'], foldername='figures', fillvalue=fillvalue, plot_only=plot_only)
        elif 'TES' in str(type(self)):
            self.sj_fig2file(file_name, ext=ext, fignum=i, fig_ext = ['.pdf', '.png'], foldername='figures', fillvalue=fillvalue, plot_only=plot_only)
        else:
            sj_fig2file(filename=file_name, ext=ext, fignum=i, fig_ext = ['.pdf', '.png'], foldername='figures', fillvalue=fillvalue, plot_only=plot_only)

mass.TESGroup.sj_save_all_fig2file = sj_save_all_fig2file


def file2fig(filename=None, plot_only=True):

    if filename is None: # not working well, but don't know why. it works well from ipython
        from PyQt4 import QtGui as qt
        import sys
        app = qt.QApplication(sys.argv)
        w = qt.QWidget()
        filename = qt.QFileDialog.getOpenFileName(w, 'Open File')
        filename = "%s"%filename
    title = ''
    xlabel = ''
    ylabel = ''
    legend_title = ''
    figwidth = 8.0
    figheight = 6.0
    num_h_lines = 0
    num_v_lines = 0
    v_x_list=[]
    v_ymin_list=[]
    v_ymax_list=[]
    v_color_list=[]
    v_linestyle_list=[]
    h_y_list=[]
    h_xmin_list=[]
    h_xmax_list=[]
    h_color_list=[]
    h_linestyle_list=[]
    marker_list = []
    color_list = []
    linestyle_list = []
    drawstyle_list = []
    text_strings =[]
    labels = ['']
    num_lines = 1 # let's assume there's at least one line in the file

    with open(filename, 'r') as f:
        # do things with your file
        for line in f:
            if 'title:' in line:
                title = line.split('title: ')[1].strip()
                continue
            if 'figwidth(inch):' in line:
                figwidth = float(line.split('figwidth(inch): ')[1].strip())
                continue
            if 'figheight(inch):' in line:
                figheight = float(line.split('figheight(inch): ')[1].strip())
                continue
            if 'xlabel:' in line:
                xlabel = line.split('xlabel: ')[1].strip()
                continue
            if 'ylabel:' in line:
                ylabel = line.split('ylabel: ')[1].strip()
                continue
            if 'legend title:' in line:
                legend_title = line.split('legend title: ')[1].strip()
                continue
            if 'line_markers' in line:
                marker_list = line.strip().split(':')[1].split()
                continue
            if 'line_colors' in line:
                color_list_str = line.strip().split(':')[1].split()
                for color in color_list_str:
                    if color[0] == '(':
                        # color_list.append(tuple(map(float,color[1:-1].split(','))))
                        color_list.append(tuple(np.asfarray(color[1:-1].split(','))))
                    else:
                        color_list.append(color)
                continue
            if 'line_linestyles' in line:
                linestyle_list = line.strip().split(':')[1].split()
                continue
            if 'line_drawstyles' in line:
                drawstyle_list = line.strip().split(':')[1].split()
                continue
            if 'vline' in line:
                if ' x' in line:
                    num_v_lines += 1
                    v_x_list.append([float(v) for v in line.split(':')[1].replace('[','').replace(']','').split(',')])
                    continue
                if 'ymin' in line:
                    v_ymin_list.append(float(line.split(':')[1]))
                    continue
                if 'ymax' in line:
                    v_ymax_list.append(float(line.split(':')[1]))
                    continue
                if 'linestyle' in line:
                    v_linestyle_list.append(eval(line.strip().split(':')[1]))
                    continue
                if 'color' in line:
                    v_color_list.append(eval(line.strip().split(':')[1]))
                    continue
            if 'hline' in line:
                if ' y' in line:
                    num_h_lines += 1
                    h_y_list.append([float(v) for v in line.split(':')[1].replace('[','').replace(']','').split(',')])
                    continue
                if 'xmin' in line:
                    h_xmin_list.append(float(line.split(':')[1]))
                    continue
                if 'xmax' in line:
                    h_xmax_list.append(float(line.split(':')[1]))
                    continue
                if 'linestyle' in line:
                    h_linestyle_list.append(eval(line.strip().split(':')[1]))
                    continue
                if 'color' in line:
                    h_color_list.append(eval(line.strip().split(':')[1]))
                    continue
            if 'text_strings' in line:
                text_strings = line.split('text_strings: ')[1].replace('\'','').split(',')
                num_text = len(text_strings)
                continue
            if 'text_positions' in line:
                # text_positions = map(float,line.split('text_positions: ')[1].replace('(','').replace(')','').split(','))
                text_positions = np.asfarray(line.split('text_positions: ')[1].replace('(','').replace(')','').split(','))
                text_positions = np.reshape(text_positions, (num_text,2))
                continue
            if 'text_colors' in line:
                text_colors = line.strip().split('text_colors: ')[1].split(',')
                continue
            if 'text_alignments' in line:
                text_alignments = line.strip().split('text_alignments: ')[1].split(',')
                continue
            if 'text_sizes' in line:
                # text_sizes = map(float, line.split('text_sizes: ')[1].replace('[','').replace(']','').split(','))
                text_sizes = np.asfarray(line.split('text_sizes: ')[1].replace('[','').replace(']','').split(','))
                continue
            if ('<' in line) and ('>' in line):
                full_labels = line.replace('_y>','').split('<')[1:]
                num_lines = int(len(full_labels)/2)
                labels = [label.strip().replace('>','') for label in full_labels][1::2]
                break

    read_matrix = np.loadtxt(filename, comments='#')
    #num_lines = np.shape(read_matrix)[1]/2

    # from matplotlib import cm
    if len(color_list)==0:
        color_list = [cm.rainbow_r(x) for x in np.linspace(0,1.,num_lines)]

    if len(marker_list)==0:
        marker_list = ['None']*num_lines

    if len(linestyle_list)==0:
        linestyle_list = ['-']*num_lines

    if len(drawstyle_list)==0:
        drawstyle_list = ['default']*num_lines

    plt.figure(figsize=(figwidth,figheight))
    for i in xrange(num_lines):
        if read_matrix.ndim > 1:
            plt.plot(read_matrix[:,2*i], read_matrix[:,2*i+1], color=color_list[i], label=labels[i], marker=marker_list[i], linestyle=linestyle_list[i], drawstyle=drawstyle_list[i])
        else:
            plt.plot(read_matrix[2*i], read_matrix[2*i+1], color=color_list[i], label=labels[i], marker=marker_list[i], linestyle=linestyle_list[i], drawstyle=drawstyle_list[i])

    if title != '':
        plt.title(title)
    if xlabel != '':
        plt.xlabel(xlabel)
    if ylabel != '':
        plt.ylabel(ylabel)
    if labels != ['']:
        if legend_title != '':
            plt.legend(loc="best", title=legend_title, fontsize='small')
        else:
            plt.legend(loc="best", fontsize='small')

    if num_h_lines:
        for ii in range(num_h_lines):
            ll = plt.hlines(h_y_list[ii], h_xmin_list[ii], h_xmax_list[ii])
            ll.set_color(h_color_list[ii])
            ll.set_linestyle(h_linestyle_list[ii])
    if num_v_lines:
        for ii in range(num_v_lines):
            ll = plt.vlines(v_x_list[ii], v_ymin_list[ii], v_ymax_list[ii])
            ll.set_color(v_color_list[ii])
            ll.set_linestyle(v_linestyle_list[ii])

    if len(text_strings):
        for i, _ in enumerate(text_strings):
            plt.text(text_positions[i][0], text_positions[i][1], text_strings[i], fontsize=text_sizes[i], color=text_colors[i], horizontalalignment=text_alignments[i])

    if plot_only is False:
        return read_matrix


def file2img(filename=None, plot_only=True, cmap=None):
    """
    plot_only=False return the image data
    some of the features are incomplete
    """
    # if filename is None: # not working well
    #     from PyQt4 import QtGui as qt
    #     import sys
    #     app = qt.QApplication(sys.argv)
    #     w = qt.QWidget()
    #     filename = qt.QFileDialog.getOpenFileName(w, 'Open File')
    #     filename = "%s"%filename
    title = ''
    xlabel = ''
    ylabel = ''
    # xlim = ''
    # ylim = ''
    legend_title = ''
    figwidth = 8.0
    figheight = 6.0
    last_xx = None
    with open(filename, 'r') as f:
        # do things with your file
        for line in f:
            if 'pcolormesh' in line:
                plot_type = 'pcolormesh'
                continue
            if 'shape:' in line:
                meshHeight = int(line.split(',')[0].split('(')[-1])
                meshWidth = int(line.split(',')[1].split(')')[0])
                continue
            if 'xlim:' in line:
                xlim = float(line.split(',')[0].split('(')[-1]), float(line.split(',')[1].split(')')[0])
                continue
            if 'ylim:' in line:
                ylim = float(line.split(',')[0].split('(')[-1]), float(line.split(',')[1].split(')')[0])
                continue
            if 'x_edges:' in line: # added 20200629
                if '+' in line:
                    last_xx = float(line.split('+')[-1])
            # else:
            #     meshHeight += 1 # before 20200629, meshHeight included the mono data
            if 'title:' in line:
                title = line.split('title: ')[1].strip()
                continue
            if 'figwidth(inch):' in line:
                figwidth = float(line.split('figwidth(inch): ')[1].strip())
                continue
            if 'figheight(inch):' in line:
                figheight = float(line.split('figheight(inch): ')[1].strip())
                continue
            if 'xlabel:' in line:
                xlabel = line.split('xlabel: ')[1].strip()
                continue
            if 'ylabel:' in line:
                ylabel = line.split('ylabel: ')[1].strip()
                continue
            if 'y_d' in line:
                y_i = float(line.split('(')[1].split(',')[0])
                y_f = float(line.split('(')[1].split(',')[1])
                y_d = float((line.split('(')[1].split(',')[2])[:-2])
            if 'cmap:' in line:
                if cmap is None:
                    cmap = line.split(' ')[-1].strip()
                    # cmap = eval('cm.'+line.split(' ')[-1])
                break # break is here because cmap is the last one.
    if cmap is None:
        cmap = 'nipy_spectral_r_white'

    read_matrix = np.loadtxt(filename, comments='#')
    bin_edges = np.arange(y_i, y_f+y_d, y_d)
    plt.figure(figsize=(figwidth,figheight))

    if plot_type == 'pcolormesh':
        if last_xx is None: # old way before 20200629
            # meshHeight = meshHeight -1 # because the first row is y data
            xx = np.tile(read_matrix[0], (meshHeight,1))
            yy = np.tile(np.arange(y_i,y_f,y_d), (meshWidth,1))
        else:
            xx = np.tile(np.append(read_matrix[0], last_xx), (meshHeight+1,1))
            yy = np.tile(bin_edges, (meshWidth+1,1))
        yy = np.transpose(yy)
        plt.pcolormesh(xx,yy,read_matrix[1:], cmap=cmap)

    if title != '':
        plt.title(title, fontsize='x-small')
    if xlabel != '':
        plt.xlabel(xlabel)
    if ylabel != '':
        plt.ylabel(ylabel)

    if plot_only is False:
        return read_matrix


def grab_htxs(folder_name, dest_folder='.', BL=132):
    """
    copy htxs file from bl101lx
    folder_name = '20181228' (just 20181228 is also fine)
    BL: 132 or 101
    """
    # dest_folder = '/Users/sangjun2/Documents/Data/Analysis/results/htxs_files'
    if not os.path.isdir(dest_folder): # if the folder does not exist, it means it is not run from my laptop
        print('The destination folder does not exist. Will be copied to the current folder.')
        dest_folder = '.'

    # result = os.system("scp 'ssrl\\b_nordlund'@spec:~/data/%s/spec/htxs %s/htxs_%s"%(folder_name,dest_folder,folder_name)) # change it to grab from specfs
    if BL==132:
        result1 = os.system("scp b_ogasawara@bl132lx.slac.stanford.edu:~/data/%s/spec/htxs %s/htxs_%s"%(folder_name,dest_folder,folder_name)) # change it to grab from specfs
        result2 = os.system("scp b_ogasawara@bl132lx.slac.stanford.edu:~/data/%s/spec_nobg/htxs_xas %s/htxs_xas_%s"%(folder_name,dest_folder,folder_name)) # change it to grab from specfs
    elif BL==101:
        result1 = os.system("scp b_nordlund@bl101lx.slac.stanford.edu:~/data/%s/spec/htxs %s/htxs_%s"%(folder_name,dest_folder,folder_name)) # change it to grab from specfs
        result2 = os.system("scp b_nordlund@bl101lx.slac.stanford.edu:~/data/%s/spec_nobg/htxs_xas %s/htxs_xas_%s"%(folder_name,dest_folder,folder_name)) # change it to grab from specfs

    if result1 == 0: # means no error, means run from the beamline pc
        print('htxs file copied to the %s folder.'%dest_folder)
    else:
        print('htxs file NOT copied. Do it manually.')

    if result2 == 0: # means no error, means run from the beamline pc
        print('htxs_xas file copied to the %s folder.'%dest_folder)
    else:
        print('htxs_xas file NOT copied. Do it manually.')


def push_htxs(spec_fname):
    """
    copy updated htxs file to bl101lx
    spec_fname = 'htxs_20220127'
    """
    # result = os.system("scp 'ssrl\\b_nordlund'@spec:~/data/%s/spec/htxs %s/htxs_%s"%(folder_name,dest_folder,folder_name)) # change it to grab from specfs
    result = os.system("scp ../htxs_files/%s_updated b_nordlund@bl101lx.slac.stanford.edu:~/data/%s/htxs_external/%s_updated"%(spec_fname, spec_fname.split('_')[1], spec_fname)) # change it to grab from specfs

    if result == 0: # means no error, means run from the beamline pc
        print('htxs file copied to the spec server')
    else:
        print('htxs file NOT copied. Do it manually.')


def _create_htxs_hdf5_python3(htxs_fname, htxs_hdf5_fname=None, spec_file_first_line=1):
    """
    copied from sass and modified to work with python3
    Given an htxs file location, and an hdf5 file name, parse the htxs file
    and dump all the information into the hdf5.
    """
    from sass import htxs

    htxs_dict, run_list, header_dict, metadata_dict, init_dict = parse_spec(htxs_fname, spec_file_first_line=spec_file_first_line)
    # These values should all be integers, and we should store them as np.int32 later on to save space
    int_list = ['QUAD_ENC', 'CMACT', 'CH', 'FY', 'REF', 'I1', 'keith_I', 'keith_V', 'testfy', 'I0', 'SC']
    if htxs_hdf5_fname is None:
        htxs_hdf5_fname = htxs_fname + '.hdf5'
    with h5py.File(htxs_hdf5_fname, mode='w') as f:
        version = 0.1
        f.attrs['version'] = version
        htxs_grp = f.require_group('htxs')
        parsed_init = htxs.parse_init(init_dict)
        for key, item in parsed_init.items():
            if type(item)==list:
                # complicated because an h5py error. See https://github.com/h5py/h5py/issues/289
                htxs_grp.attrs[key] = [item_i.encode('utf8') for item_i in item if type(item)==list]
            else:
                htxs_grp.attrs[key] = item

        for key, arr in htxs_dict.items():
            scan_grp = htxs_grp.require_group(str(key))
            header_list = header_dict[key]
            scan_mdict = metadata_dict[key]
            parsed_mdict = htxs.parse_metadata(scan_mdict)
            if arr.shape == (0,):
                continue
            for key, item in parsed_mdict.items():
                scan_grp.attrs[key] = item
            for n, header in enumerate(header_list):
                if header in int_list:
                    scan_grp.create_dataset(header, data=arr[:,n], dtype=np.int32)
                else:
                    scan_grp.create_dataset(header, data=arr[:, n], dtype=np.float32)
        # f.close()
    return


def parse_spec(htxs_fname, spec_file_first_line=1):
    """
    SJL: modified Jamie's htxs.parse_all_htxs to handle BL13-3 spec file

    Takes a plaintext htxs file and parses it into a series of dictionaries that contain
    both data and metadata. Intended to be used in the creation of an HDF5 file.

    Currently, metadata is not actually parsed here. Instead, each run gets a dictionary, and
    each metadata line is put into that dictionary, keyed by the first token in the line, so
    that I can parse it later when I can get Dennis to tell me what the hell it means.
    """
    htxs_dict = {}
    run_list = []
    header_dict = {}
    metadata_dict = {}
    init_dict = {}
    lines = 0
    with open(htxs_fname) as f:

        # skip spec_file_first_line number of lines (for a file with multiple #F)
        for _ in range(spec_file_first_line-1):
            next(f)

        state=-1
        htxs_data = []
        current_run = 0
        for line in f:
            tokens = line.split()
            if tokens == []:
                if state==1:
                    htxs_dict[current_run] = np.array(htxs_data, dtype=float)
                    run_list.append(current_run)
                    htxs_data = []
                    state = 0
                continue
            elif tokens[0] == '#S':
                if state==1:
                    htxs_dict[current_run] = np.array(htxs_data, dtype=float)
                    run_list.append(current_run)
                    htxs_data = []
                    state = 0
                current_run = int(tokens[1])
                if current_run in run_list:
                    break
                metadata_dict[current_run] = {tokens[0]:line}
                state = 1
            elif tokens[0] == '#L':
                if state == 1:
                    if tokens[1] == 'Two':
                        header_dict[current_run] = [tokens[1]+'_'+tokens[2]] + tokens[3:]
                    else:
                        header_dict[current_run] = tokens[1:]
                    metadata_dict[current_run][tokens[0]] = line
                continue
            elif tokens[0] == '#C':
                try:
                    if tokens[0] in metadata_dict[current_run]:
                        metadata_dict[current_run][tokens[0]] += [line]
                    else:
                        metadata_dict[current_run][tokens[0]] = [line]
                except KeyError:
                    continue
            elif tokens[0][0] == '#':
                if state == 1:
                    metadata_dict[current_run][tokens[0]] = line
                if state == -1:
                    init_dict[tokens[0]] = line
                continue
            elif state==1:
                if htxs_data == []:
                    htxs_data.append(np.array(tokens))
                else:
                    len_data = len(htxs_data[0])
                    if len(tokens) != len_data:
                        htxs_data.append([-1]*len_data)
                    else:
                        htxs_data.append(np.array(tokens))
        if state==1:
            htxs_dict[current_run] = np.array(htxs_data, dtype=float)
            run_list.append(current_run)
            htxs_data = []
            state = 0

    return htxs_dict, run_list, header_dict, metadata_dict, init_dict


def generate_rixs_dict(data_list, BL=None, spec_fname=None, spec_file_first_line=1, spec_file_update=False, hdf5_type='quick_reduced', energy_param='p_energy', bin_start=0, bin_end=1000, bin_width=0.1, mono_range=None, plot_xes_per_spot=False, generate_spec_hdf5=False, ref_energy=None, ref_data='REF', ref_norm=True, ref_fit_range=None, ref_fit_order=6, num_ref_anchors=1, correct_to_energy=None, plot_mono_correction=True, dont_ask=True, corrected_mono=None, interps=[1], generate_partial_maps=False, skip_scans=[]):
    """
    # returns a dictionary of rixs dictionaries
    # spec_file_update=False is default
    # generate_partial_maps=False is default
    # data_list can be a list of data or dir_ps, in which case quick_reduced.hdf5 will be used
    # ref_data='REF' is default. ref_data='SC' can be used when REF is bad
    # default ref_fit_range=0.5 eV
    # mono_range not being used
    # ref_energy is also the initial energy to look for a peak.
    # if a different value should be assigned, provide correct_to_energy
    """
    if type(data_list) is not list:
        raise Exception('# provide a data list to generate_rixs_dict!')

    global PCNAME, USERNAME

    if BL is None:
        if ('ssrl-pc91893' in PCNAME) or ('BL101PROC01' == PCNAME) or ('bl132proc01' == PCNAME):  # macpro or datapc
            BL = 101
            print('Seems like trying to reduce BL10-1 data based on the PCNAME')
            if spec_fname is None:
                print('!! provide spec_fname, e.g., htxs_20230706.')
                return
            else:
                if spec_file_update is True:
                    if 'ssrl-pc91893' in PCNAME:  # macpro
                        # htxs_files_folder = '/Volumes/Data/results/htxs_files'
                        htxs_files_folder = '/Users/sangjun2/Documents/results/htxs_files'
                    elif ('BL101PROC01' == PCNAME) or ('bl132proc01' == PCNAME):
                        htxs_files_folder = '/home/user/sjlee/results/htxs_files'
                    spec_fname_full_path = os.path.join(htxs_files_folder, spec_fname)
                    spec_xas_fname_full_path = os.path.join(htxs_files_folder, spec_fname.replace('_', '_xas_'))

                    import subprocess
                    try:
                        # to be able to use automatic ssh login, do 1) ssh-keygen on client 2) ssh-copy-id userid@host
                        print('trying to copy htxs file from bl101lx...')
                        p = subprocess.Popen(["scp", 'b_nordlund@bl101lx.slac.stanford.edu:~/data/%s/spec/htxs'%(spec_fname[5:]), spec_fname_full_path])
                        stdoutdata, stderrdata = p.communicate() # communicate does poll() and wait()
                        print('htxs file copied to %s'%spec_fname_full_path)
                        print('trying to copy htxs_xas file from bl101lx...')
                        p = subprocess.Popen(["scp", 'b_nordlund@bl101lx.slac.stanford.edu:~/data/%s/spec_nobg/htxs_xas'%(spec_fname[5:]), spec_xas_fname_full_path])
                        stdoutdata, stderrdata = p.communicate() # communicate does poll() and wait()
                        print('htxs_xas file copied to %s'%spec_xas_fname_full_path)
                    except:
                        print('# Provide a right file path')
                        return
        else:
            BL = 133
            print('Seems like trying to reduce BL13-3 data based on the PCNAME')  # for BL133, spec_fname is optional because log files contain i0
            if spec_fname is not None:
                spec_fname_full_path = os.path.join('./', spec_fname)  # check only the current folder for now
                print('spec_fname_full_path = %s'%spec_fname_full_path)
                if not os.path.isfile(spec_fname_full_path):
                    print('Wrong file name/path provided. Try again.')
                    return
    else:
        print('BL = %s is provided'%BL)

    rixs_dict_dict = {}
    for ii, data in enumerate(data_list):
        if type(ref_energy) in [list, np.ndarray]:
            if num_ref_anchors>2:
                if max(ref_energy) - min(ref_energy) < 10:  # this is when ref_energy is ref for each scan, e.g., 300, 299.5, 300.5.
                    ref_energy_ii = ref_energy[ii]
                    correct_to_energy_ii = correct_to_energy[ii]
                else:  # this is when ref_energy is a list of ref for each edge, e.g., 300, 400, 600.
                    ref_energy_ii = ref_energy
                    correct_to_energy_ii = correct_to_energy
            elif num_ref_anchors==2:
                ref_energy_ii = ref_energy
                correct_to_energy_ii = correct_to_energy
        else:
            ref_energy_ii = ref_energy
            correct_to_energy_ii = correct_to_energy
        if type(data) is str:  # this is the case using jamie's quick_reduced.hdf5
            dir_p = data
            if BASE_FOLDER == "no name yet":
                set_base_folder()
            else:
                print('# BASE_FOLDER: %s'%BASE_FOLDER)
            hdf5_fname = os.path.join(BASE_FOLDER, dir_p.split('_')[0] ,dir_p, dir_p + '_' + hdf5_type +'.hdf5')
            temp_rixs_dict = _generate_rixs(hdf5_fname, BL=BL, spec_fname=spec_fname, spec_file_first_line=spec_file_first_line, spec_file_update=False, partial_scan_numbers=None, energy_param=energy_param, bin_start=bin_start, bin_end=bin_end, bin_width=bin_width, mono_range=mono_range, plot_xes_per_spot=plot_xes_per_spot, generate_spec_hdf5=generate_spec_hdf5, ref_energy=ref_energy_ii, ref_data=ref_data, ref_norm=ref_norm, ref_fit_range=ref_fit_range, ref_fit_order=ref_fit_order, num_ref_anchors=num_ref_anchors, correct_to_energy=correct_to_energy_ii, plot_mono_correction=plot_mono_correction, dont_ask=dont_ask, corrected_mono=corrected_mono, interps=interps, generate_partial_maps=generate_partial_maps, skip_scans=skip_scans)
        else:
            dir_p = data.first_good_dataset.filename.split(os.sep)[-1][:12]
            temp_rixs_dict = _generate_rixs(data, BL=BL, spec_fname=spec_fname, spec_file_first_line=spec_file_first_line, spec_file_update=False, partial_scan_numbers=None, energy_param=energy_param, bin_start=bin_start, bin_end=bin_end, bin_width=bin_width, mono_range=mono_range, plot_xes_per_spot=plot_xes_per_spot, generate_spec_hdf5=generate_spec_hdf5, ref_energy=ref_energy_ii, ref_data=ref_data, ref_norm=ref_norm, ref_fit_range=ref_fit_range, ref_fit_order=ref_fit_order, num_ref_anchors=num_ref_anchors, correct_to_energy=correct_to_energy_ii, plot_mono_correction=plot_mono_correction, dont_ask=dont_ask, corrected_mono=corrected_mono, interps=interps, generate_partial_maps=generate_partial_maps, skip_scans=skip_scans)
        if temp_rixs_dict is not None:
            rixs_dict_dict[dir_p] = temp_rixs_dict
    key_list = list(rixs_dict_dict.keys())
    for ii, key in enumerate(key_list):
        rixs_dict_dict[ii] = rixs_dict_dict[key]
    return rixs_dict_dict


def _generate_rixs(reduced_data, BL=None, spec_fname=None, spec_file_first_line=1, spec_file_update=False, log_fname=None, partial_scan_numbers=None, energy_param='p_energy', bin_start=0, bin_end=1000, bin_width=0.25, mono_range=None, plot_xes_per_spot=False, generate_spec_hdf5=False, ref_energy=None, ref_data='REF', ref_norm=True, ref_fit_range=None, ref_fit_order=6, num_ref_anchors=1, correct_to_energy=None, plot_mono_correction=True, dont_ask=True, corrected_mono=None, interps=[1], select_chans=None, generate_partial_maps=False, skip_scans=[]):
    '''
    # returns rixs_dict
    # spec_fname=None is for 13-3. Otherwise, 10-1.
    # plot_xes_per_spot=True shows XES per spot
    # generate_spec_hdf5=False does not generate any hdf5 (actually create and delete)
    # ref_energy is a starting point for peak search
    # if correcto_to_energy (scalar) is given, the peak is adjusted to it (no effect when corrected by SC).
    # if correcto_to_energy (scalar) is not given and SC is used for mono correction, the peaks are adjusted to the first peak
    # ref_energy=None with interps=1 automatically tries to use pre-set ref 
    # ref_energy can be a list or an array, which can be a list of ref for each edge, e.g., [400, 600, 800] or for each scan, e.g., [399, 400, 399.5]
    # num_ref_anchors=1 (by default), 2 (for two point calib)
    # dont_ask=True is same as answering y after mono correction is done (to prepare for batch processing)
    # mono_range not being used
    # plot_xes_per_spot=False is default
    # select_chans = None uses all the chans.
    # reduced_data can be quick_reduced.hdf5 file name
    # spec_file_update=True will scp the spec file
    '''

    global PCNAME, USERNAME

    if BL is None:
        print('!! provide BL, e.g., BL=101 or 133')
        return

    if BL == 101:
        if 'ssrl-pc91893' in PCNAME:  # macpro
            # htxs_files_folder = '/Volumes/Data/results/htxs_files'
            htxs_files_folder = '/Users/sangjun2/Documents/results/htxs_files'
        elif ('BL101PROC01' == PCNAME) or ('bl132proc01' == PCNAME):
            htxs_files_folder = '/home/user/sjlee/results/htxs_files'
        else:
            htxs_files_folder = '../htxs_files'
        spec_fname_full_path = os.path.join(htxs_files_folder, spec_fname)
        spec_xas_fname_full_path = os.path.join(htxs_files_folder, spec_fname.replace('_', '_xas_'))
        if not os.path.isfile(spec_fname_full_path):  # if not found
            print('# Provide a right file path')
            return
    elif BL == 133:
        if spec_fname is not None:
            spec_fname_full_path = os.path.join('./', spec_fname)  # check only the current folder for now
            print('spec_fname_full_path = %s'%spec_fname_full_path)
            if not os.path.isfile(spec_fname_full_path):
                print('Wrong file name/path provided. Try again.')
                return

    coadded_bin_edges = np.arange(bin_start, bin_end+bin_width, bin_width) # change arange to linsapce?
    coadded_bin_centers = (coadded_bin_edges[1:]+coadded_bin_edges[:-1])*0.5

    # if isinstance(reduced_data, np.ndarray):
    #     dir_p = reduced_data[0].dir_p
    # else:
    #     dir_p = reduced_data.first_good_dataset.filename.split(os.sep)[-2]

    if hasattr(reduced_data, 'filenames'):  # if mass dataset type
        dir_p = reduced_data.first_good_dataset.filename.split(os.sep)[-2]
    elif (type(reduced_data) is str) and ('quick_reduced' in reduced_data):
        dir_p = reduced_data.split('_quick_reduced')[0][-12:]
    elif not hasattr(reduced_data, 'hdf5_noisefile'):  # if mass hdf5
        # dir_p = reduced_data.first_good_dataset.filename.split(': ')[1][:12]
        dir_p = reduced_data.first_good_dataset.filename.split(os.sep)[-1][:12]
    else:  # if my own data type
        # dir_p = reduced_data.first_good_dataset.filename.split(':')[-1].split('.')[0][1:]
        dir_p = reduced_data[0].dir_p

    if log_fname is None:  # if not provided, grab it from the right folder
        if os.path.isfile(dir_p+'_log_old'):  # first look for _log_old in the current folder
            log_fname = dir_p+'_log_old'
        elif os.path.isfile(dir_p+'_log'):  # then look for _log
            log_fname = dir_p+'_log'
        else:
            if BASE_FOLDER == "no name yet":
                set_base_folder()
            else:
                print('# BASE_FOLDER: %s'%BASE_FOLDER)

            path_log = os.path.join(BASE_FOLDER, dir_p.split('_')[0] ,dir_p)
            log_fnames = glob.glob(os.path.join(path_log,'*log*'))
            old_log_fnames = glob.glob(os.path.join(path_log,'*log_old'))
            if len(old_log_fnames) == 0:
                if len(log_fnames) == 0:
                    print('# no log file found. data returned. Provide log file name!')
                    return reduced_data
                else:
                    log_fname = max(log_fnames, key=os.path.getctime)  # the most recent one
            else:
                log_fname = max(old_log_fnames, key=os.path.getctime)  # the most recent one
    print('log_fname: %s'%log_fname)

    (scan_mode, n_spots, scan_numbers, nominal_mono_from_log, i0_matrix, time_start_end, htxs_class) = _log_file_parser(log_fname, BL, partial_scan_numbers, spec_fname=spec_fname_full_path, spec_file_first_line=spec_file_first_line, skip_scans=skip_scans)
    nominal_mono_from_log = np.round(nominal_mono_from_log, decimals=2)  # to handle mismatch between mono from log and spec
    if (scan_mode in ['TES', 'XES', 'noise']) or (n_spots==0):
        return None

    if hasattr(reduced_data, 'first_good_dataset'):  # in the case of MASS data
        if select_chans is None:
            concat_p_energy = np.concatenate([ds.__dict__[energy_param][ds.good()] for ds in reduced_data])
            concat_timestamp = np.concatenate([ds.p_timestamp[ds.good()] for ds in reduced_data])
            concat_p_energy_all = np.concatenate([ds.__dict__[energy_param][:] for ds in reduced_data]) #???
            concat_timestamp_all = np.concatenate([ds.p_timestamp[:] for ds in reduced_data]) #???
            nused = len([ds for ds in reduced_data])  # number of pixels
        else:
            if type(select_chans) is not list:
                select_chans = [select_chans]
            concat_p_energy = np.concatenate([reduced_data.channel[chan].__dict__[energy_param][reduced_data.channel[chan].good()] for chan in select_chans])
            concat_timestamp = np.concatenate([reduced_data.channel[chan].p_timestamp[reduced_data.channel[chan].good()] for chan in select_chans])
            concat_p_energy_all = np.concatenate([reduced_data.channel[chan].__dict__[energy_param][:] for chan in select_chans])
            concat_timestamp_all = np.concatenate([reduced_data.channel[chan].p_timestamp[:] for chan in select_chans])
            nused = len(select_chans)  # number of pixels
        if generate_partial_maps is True:
            if ((reduced_data.first_good_dataset.number_of_rows is None) and (reduced_data.n_channels > 180)) or (reduced_data.first_good_dataset.number_of_rows == 30): # not working for 8 row yet
                partial_concat_p_energy = {}
                partial_concat_p_energy[BL] = {}
                partial_concat_p_timestamp = {}
                partial_concat_p_timestamp[BL] = {}
                missing_chans = {}
                for key, value in PIXEL_GROUP_DICT[BL].items():
                    missing_chans[key] = [channum for channum in value if channum not in reduced_data.good_channels]
                    partial_concat_p_energy[BL][key] = np.concatenate([reduced_data.channel[channum].__dict__[energy_param][reduced_data.channel[channum].good()] for channum in value if channum in reduced_data.good_channels])
                    partial_concat_p_timestamp[BL][key] = np.concatenate([reduced_data.channel[channum].p_timestamp[reduced_data.channel[channum].good()] for channum in value if channum in reduced_data.good_channels])
    elif (type(reduced_data) is str) and ('quick_reduced' in reduced_data):
        concat_p_energy, concat_timestamp, nused = load_jamie_quick_reduced(reduced_data)
        concat_p_energy_all = concat_p_energy
        concat_timestamp_all = concat_timestamp
    else:  # in the case of my weird data format, not updated for long
        temp_energy_list = []
        temp_time_list = []
        nused = 0
        if hasattr(reduced_data[0], 'p_energy_from_cal_dc'):
            for ds in reduced_data:
                if len(ds.good())>0:
                    temp_energy_list.append(ds.p_energy_from_cal_dc[ds.good()])
                    temp_time_list.append(ds.p_timestamp[ds.good()])
                    nused = nused + 1
        else:
            for ds in reduced_data:
                if len(ds.good())>0:
                    temp_energy_list.append(ds.__dict__[energy_param][ds.good()])
                    temp_time_list.append(ds.p_timestamp[ds.good()])
                    nused = nused + 1
        concat_p_energy = np.concatenate(temp_energy_list)
        concat_timestamp = np.concatenate(temp_time_list)

    if plot_xes_per_spot is True:

        coadded_counts_spot = {}
        # coadded_bin_edges = np.arange(0,bin_end,bin_width)
        # coadded_bin_centers = (coadded_bin_edges[1:]+coadded_bin_edges[:-1])*0.5
        for spot in spot_ids:
            print('# histograming for XES on spot/scan # %d'%spot)  # spot means scan
            counts, _ = np.histogram(concat_p_energy[(concat_timestamp>=spot_timestamp_ranges[spot][0])&(concat_timestamp<=spot_timestamp_ranges[spot][1])], coadded_bin_edges)
            coadded_counts_spot[spot]=counts

        plt.figure()
        ax1 = plt.subplot(111)
        # for (mono_e, counts) in coadded_counts_sj.items():
        #     plt.semilogy(coadded_bin_centers, counts,label=mono_e)
        bad_spot_counter = 0
        for i, spot in enumerate(spot_ids):
            # if spot in bad_spot_ids:
            #     bad_spot_counter += 1
            #     print "bad spot #%d skipped"%spot
            #     continue
            counts = coadded_counts_spot[spot]
            ax1.semilogy(coadded_bin_centers, counts,label='%d (%d)'%(spot,scan_numbers[i]), color=rainbow_colors[i-bad_spot_counter])
            #ax1.semilogy(coadded_bin_centers, counts,label=spot, color=rainbow_colors[i-bad_spot_counter])
        plt.draw()
        plt.title('%s, %d pixels coadded, spot by spot'%(dir_p,nused))

        plt.xlabel("energy (eV)")
        plt.ylabel("counts per %0.2f eV bin"%(coadded_bin_edges[1]-coadded_bin_edges[0]))
        #plt.title("%g pixels"%nused)
        leg = plt.legend(loc="best", fontsize='x-small')
        leg.set_title('# (SPEC #S)', prop={'size':'small'})

    concat_timestamp = concat_timestamp[(concat_p_energy>=bin_start) & (concat_p_energy<=bin_end)]
    concat_p_energy = concat_p_energy[(concat_p_energy>=bin_start) & (concat_p_energy<=bin_end)]
    concat_timestamp_all = concat_timestamp_all[(concat_p_energy_all>=bin_start) & (concat_p_energy_all<=bin_end)]
    concat_p_energy_all = concat_p_energy_all[(concat_p_energy_all>=bin_start) & (concat_p_energy_all<=bin_end)]

    if BL == 101:
        if corrected_mono is None:
            corrected_mono = np.zeros((n_spots,len(nominal_mono_from_log)), dtype=np.float32) # changed from nominal_mono to nominal_mono_from_log. 20230925: why need this even before checking interp?
            if interps in [0, [0], []]:  # this is the case when the user does not want to correct mono. This applies to only 10-1 data
                for i, scan_number in enumerate(scan_numbers):
                    if scan_mode == 'XAS':
                        print('# corrected_mono not necessary; nominal mono will be used')
                        # corrected_mono[i] = htxs_class.get_scan_data(scan_number,'ENERGY_ENC') # use ENERGY_ENC or MONO?
                        try:
                            corrected_mono[i] = htxs_class.get_scan_data(scan_number,'mono132')  # use ENERGY_ENC or MONO? 20230925: why need it when it's not mono correcting?
                        except KeyError:
                            corrected_mono[i] = htxs_class.get_scan_data(scan_number,'MONO')  # use ENERGY_ENC or MONO? 20230925: why need it when it's not mono correcting?
                    elif scan_mode == 'else':
                        corrected_mono[i] = nominal_mono_from_log
            else:  # correcting mono..
                if isinstance(interps, int):
                    interps = [interps]

                initial_mono = float(nominal_mono_from_log[0])
                final_mono = float(nominal_mono_from_log[-1])
                if num_ref_anchors==1:
                    if ref_energy is None:  # guess ref_energy
                        Fe_peak_E = 707.0
                        O_peak_E = 530.0
                        Ni_peak_E = 853.0
                        Ti_peak_E = 465.0
                        C_peak_E = 284.0
                        Co_peak_E = 777.0
                        if (Fe_peak_E >= initial_mono) and (Fe_peak_E <= final_mono): # changed from nominal_mono to nominal_mono_from_log
                            ref_energy = Fe_peak_E
                        elif (Ni_peak_E >= initial_mono) and (Ni_peak_E <= final_mono): # changed from nominal_mono to nominal_mono_from_log
                            ref_energy = Ni_peak_E
                        elif (O_peak_E >= initial_mono) and (O_peak_E <= final_mono): # changed from nominal_mono to nominal_mono_from_log
                            ref_energy = O_peak_E
                        elif (Ti_peak_E >= initial_mono) and (Ti_peak_E <= final_mono): # changed from nominal_mono to nominal_mono_from_log
                            ref_energy = Ti_peak_E
                        elif (C_peak_E >= initial_mono) and (C_peak_E <= final_mono): # changed from nominal_mono to nominal_mono_from_log
                            ref_energy = C_peak_E
                        elif (Co_peak_E >= initial_mono) and (Co_peak_E <= final_mono): # changed from nominal_mono to nominal_mono_from_log
                            ref_energy = Co_peak_E
                elif num_ref_anchors==2:  # for now, let's assume just two anchor points
                    if (ref_energy is None) or (len(ref_energy)!=2):
                        print('# Provide ref energies for two point calib')
                        return

                if ref_energy is not None:  # either automatically assigned or provided
                    if num_ref_anchors==1:
                        if type(ref_energy) in [list, np.ndarray]:  # this is when ref_energy is a list of ref energies for each edge, e.g., 300, 400, 600 eV.
                            ref_energy = np.sort(ref_energy)[np.argmax(np.sort(ref_energy)>=initial_mono)]

                        if correct_to_energy is None:  # ref_energy is the initial peak search position, correct_to_energy is energy to be adjusted to
                            correct_to_energy = ref_energy

                        if (ref_energy >= initial_mono) and (ref_energy <= final_mono):
                            # REF_peak_posi = np.zeros((n_spots,1), dtype=np.float32)
                            REF_peak_posi = np.zeros(n_spots, dtype=np.float32)
                            # corrected_mono = np.zeros((n_spots,len(nominal_mono)), dtype=np.float32)

                            print("mono is to be corrected using ref_energy = %s"%ref_energy)

                            # from sass.peakutils import correct_energy

                            # right below is going to be replaced by a function below: correct_mono()
                            if plot_mono_correction:
                                fig, ax = plt.subplots(2, 1, figsize=(6.4, 9))
                                fig.subplots_adjust(bottom=0.05, top=0.95)
                                # fig1 = plt.figure()
                                # ax1 = fig1.gca()
                                # fig2 = plt.figure()
                                # ax2 = fig2.gca()

                            for i, scan_number in enumerate(scan_numbers):
                                # deprecated way of finding a ref point in ref data
                                # try: # fitting part needs improvement
                                #     xd_full = htxs_class.get_scan_data(scan_number,'ENERGY_ENC')
                                #     yd_full = htxs_class.get_scan_data(scan_number,'REF')
                                #     xd = xd_full[np.logical_and(xd_full>ref_energy-5, xd_full<ref_energy+5)]
                                #     yd = yd_full[np.logical_and(xd_full>ref_energy-5, xd_full<ref_energy+5)]
                                #     fit_range = [xd[np.argmax(yd)]-0.7, xd[np.argmax(yd)]+0.7]
                                #     # ref_max_posi = htxs_class.get_scan_data(scan_number,'ENERGY_ENC')[np.argmax(htxs_class.get_scan_data(scan_number,'REF'))]
                                #     # fit_range = [ref_max_posi-0.7, ref_max_posi+0.7]
                                #     popt, pcov, perr = fit_gaussian(sigma=0.4, tailed=False, plot_it=plot_mono_correction, xd=xd_full,
                                #                      yd=yd_full, range=fit_range)
                                #     REF_peak_posi[i] = popt[1]
                                # except (RuntimeError, TypeError):
                                #     print("Failed to find gaussian fit to scan number %s"%scan_number)
                                #     REF_peak_posi[i] = htxs_class.get_scan_data(scan_number,'ENERGY_ENC')[np.argmax(htxs_class.get_scan_data(scan_number,'REF'))]
                                #     if plot_mono_correction:
                                #         plt.sj_vlines(REF_peak_posi[i])
                                #         plt.title('Scan #%s fitting failed, use simple maximum'%scan_number)

                                # new way, not fitting but finding a peak within +/-1 eV from the ref_energy => now fit with 2nd order curve
                                # xd_full = htxs_class.get_scan_data(scan_number,'ENERGY_ENC')
                                try:
                                    xd_full = htxs_class.get_scan_data(scan_number,'mono132')
                                except KeyError:
                                    xd_full = htxs_class.get_scan_data(scan_number,'MONO')
                                #yd_full = htxs_class.get_scan_data(scan_number,'REF')
                                # use just REF (above) or i0 normalized REF (below)?
                                if ref_norm is True:
                                    yd_full = np.asfarray(htxs_class.get_scan_data(scan_number,ref_data))/np.asfarray(htxs_class.get_i0(scan_number))
                                else:
                                    yd_full = np.asfarray(htxs_class.get_scan_data(scan_number,ref_data))
                                if ref_fit_range is None:
                                    ref_fit_range = 0.5 # +/- ref_fit_range in eV
                                left_i = 1
                                right_i = 1
                                while True:
                                    xd = xd_full[np.logical_and(xd_full>ref_energy-ref_fit_range*left_i, xd_full<ref_energy+ref_fit_range*right_i)]
                                    yd = yd_full[np.logical_and(xd_full>ref_energy-ref_fit_range*left_i, xd_full<ref_energy+ref_fit_range*right_i)]
                                    # older way
                                    # if argmax(yd) == len(yd)-1: # if range is too short on the right side
                                    #     xd = xd_full[np.logical_and(xd_full>ref_energy-ref_fit_range, xd_full<ref_energy+2*ref_fit_range)]
                                    #     yd = yd_full[np.logical_and(xd_full>ref_energy-ref_fit_range, xd_full<ref_energy+2*ref_fit_range)]
                                    # elif argmax(yd) == 0: # if range is too short on the left side
                                    #     xd = xd_full[np.logical_and(xd_full>ref_energy-2*ref_fit_range, xd_full<ref_energy+ref_fit_range)]
                                    #     yd = yd_full[np.logical_and(xd_full>ref_energy-2*ref_fit_range, xd_full<ref_energy+ref_fit_range)]
                                    # approx_ref_max_posi = xd[np.argmax(yd)]
                                    # spline = UnivariateSpline(xd, yd, s=0)
                                    # xdense = np.linspace(approx_ref_max_posi-1, approx_ref_max_posi+1, 100)
                                    # REF_peak_posi[i] = xdense[np.argmax(spline(xdense))]
                                    # old way
                                    # p = np.polyfit(xd-ref_energy, yd, 2)
                                    # REF_peak_posi[i] = -p[1]/(2*p[0]) + ref_energy
                                    xd_linspace = np.linspace(xd[0], xd[-1], int((xd[-1]-xd[0])/0.01)+1)
                                    p = np.polyfit(xd-ref_energy, yd, ref_fit_order)
                                    max_arg = np.argmax(np.polyval(p,xd_linspace-ref_energy))
                                    # print('max_arg',max_arg)
                                    # print('len(xd_linspace)-1',len(xd_linspace)-1)
                                    if max_arg < 10:
                                        left_i+=1
                                    elif max_arg > len(xd_linspace)-11:
                                        right_i+=1
                                    else:
                                        REF_peak_posi[i] = xd_linspace[max_arg]
                                        break
                                    # if (REF_peak_posi[i] > xd[0]) and (REF_peak_posi[i] < xd[-1]):
                                    #     print('xd[0]',xd[0],type(xd[0]))
                                    #     print('xd[-1]',xd[-1])
                                    #     print('REF_peak_posi[i]',REF_peak_posi[i],type(REF_peak_posi[i]))
                                    #     break
                                print('Peak position: %g'%REF_peak_posi[i])
                                # corrected_mono[i] = htxs_class.get_scan_data(scan_number,'ENERGY_ENC') - REF_peak_posi[i] + ref_energy
                                if ref_data == 'SC':
                                    corrected_mono[i] = xd_full - REF_peak_posi[i] + REF_peak_posi[0]  # if using SC, use only the first scan
                                else:
                                    corrected_mono[i] = xd_full - REF_peak_posi[i] + correct_to_energy

                                if plot_mono_correction:
                                    rainbow_colors = [cm.rainbow_r(x) for x in np.linspace(0,1.,len(scan_numbers))]
                                    # ax1.plot(htxs_class.get_scan_data(scan_number,'ENERGY_ENC'), htxs_class.get_scan_data(scan_number,'REF'), color=rainbow_colors[i]);ax1.set_title('REF');plt.draw()
                                    ax[0].plot(xd_full, yd_full, color=rainbow_colors[i], label=str(scan_number))
                                    ax[0].plot(xd_linspace, np.polyval(p,xd_linspace-ref_energy), 'k--')
                                    ax[0].set_xlabel('nominal mono')
                                    ax[0].vlines(REF_peak_posi[i], ymin=min(yd_full), ymax=min(yd_full)+(max(yd_full)-min(yd_full))*1.1, color=rainbow_colors[i], linestyles='dashed')
                                    ax[1].plot(corrected_mono[i], yd_full, color=rainbow_colors[i], label=str(scan_number))
                                    if i == len(scan_numbers)-1:  # only for the last scan
                                        if ref_norm is True:
                                            ax[0].set_title('%s, %s/i0'%(dir_p,ref_data))#;plt.draw()
                                        else:
                                            ax[0].set_title('%s, %s'%(dir_p,ref_data))#;plt.draw()
                                        ax[0].vlines(ref_energy, *ax[0].get_ylim(), color='black', linestyles='dashed', label='nominal ref')
                                        ymin, ymax = ax[1].get_ylim()
                                        if ref_data == 'SC':
                                            ax[1].set_xlabel('nomianl_mono - peak position + peak position of the first scan')
                                            ax[1].set_title('%s, %s/i0 aligned to the first scan'%(dir_p,ref_data))#;plt.draw()
                                            ax[1].vlines(ref_energy, ymin, ymax, color='black', linestyles='dashed', label='nominal ref')
                                            ax[1].vlines(REF_peak_posi[0], ymin, ymax, color='gray', label='correct-to energy')
                                        else:
                                            ax[1].set_xlabel('nomianl_mono - peak position + ref_energy')
                                            ax[1].set_title('%s, %s/i0 with corrected mono'%(dir_p,ref_data))#;plt.draw()
                                            ax[1].vlines(correct_to_energy, ymin, ymax, color='gray', label='correct-to energy')
                                            # ax[1].vlines(REF_peak_posi[i], ymin, ymax, color='gray')
                            if plot_mono_correction is True:
                                ax[0].legend()
                                ax[1].legend()

                            if dont_ask is True:
                                pass
                            else:
                                while True:
                                    # answer = raw_input("Continue (y will proceed / n will return corrected_mono)? ")
                                    answer = input("Continue (y will proceed / n will return corrected_mono)? ")
                                    if answer == 'y':
                                        # pass
                                        break
                                    elif answer == 'n':
                                        print('Cancelled. corrected_mono returned, instead of rixs_dict')
                                        return corrected_mono # when cancelled, return corrected_mono
                        else:  # this is the case when ref_energy is given but out of the mono range.
                            interps=[0]
                            for i, scan_number in enumerate(scan_numbers):
                                print('# corrected_mono not found; nominal mono (MONO) will be used')
                                # corrected_mono[i] = htxs_class.get_scan_data(scan_number,'ENERGY_ENC') # use ENERGY_ENC or MONO?
                                try:
                                    corrected_mono[i] = htxs_class.get_scan_data(scan_number, 'mono132')  # use ENERGY_ENC or MONO?
                                except KeyError:
                                    corrected_mono[i] = htxs_class.get_scan_data(scan_number, 'MONO')  # use ENERGY_ENC or MONO?
                    
                    elif num_ref_anchors==2:  # for now, let's assume just two anchor points

                        if correct_to_energy is None:  # ref_energy is the initial peak search position, correct_to_energy is energy to be adjusted to
                            correct_to_energy = ref_energy  # len=2
                        print('Found peaks will be corrected to the following energies:', correct_to_energy)

                        if (ref_energy[0] >= initial_mono) and (ref_energy[0] <= final_mono) and (ref_energy[1] >= initial_mono) and (ref_energy[1] <= final_mono):
                            # REF_peak_posi = np.zeros((n_spots,1), dtype=np.float32)
                            REF_peak_posi_1 = np.zeros(n_spots, dtype=np.float32)
                            REF_peak_posi_2 = np.zeros(n_spots, dtype=np.float32)
                            # corrected_mono = np.zeros((n_spots,len(nominal_mono)), dtype=np.float32)

                            print("mono is to be corrected using ref_energy = %s"%ref_energy)

                            # from sass.peakutils import correct_energy

                            # right below is going to be replaced by a function below: correct_mono()
                            if plot_mono_correction:
                                fig, ax = plt.subplots(2, 1, figsize=(6.4, 9))
                                fig.subplots_adjust(bottom=0.05, top=0.95)
                                # fig1 = plt.figure()
                                # ax1 = fig1.gca()
                                # fig2 = plt.figure()
                                # ax2 = fig2.gca()

                            for i, scan_number in enumerate(scan_numbers):
                                # deprecated way of finding a ref point in ref data
                                # try: # fitting part needs improvement
                                #     xd_full = htxs_class.get_scan_data(scan_number,'ENERGY_ENC')
                                #     yd_full = htxs_class.get_scan_data(scan_number,'REF')
                                #     xd = xd_full[np.logical_and(xd_full>ref_energy-5, xd_full<ref_energy+5)]
                                #     yd = yd_full[np.logical_and(xd_full>ref_energy-5, xd_full<ref_energy+5)]
                                #     fit_range = [xd[np.argmax(yd)]-0.7, xd[np.argmax(yd)]+0.7]
                                #     # ref_max_posi = htxs_class.get_scan_data(scan_number,'ENERGY_ENC')[np.argmax(htxs_class.get_scan_data(scan_number,'REF'))]
                                #     # fit_range = [ref_max_posi-0.7, ref_max_posi+0.7]
                                #     popt, pcov, perr = fit_gaussian(sigma=0.4, tailed=False, plot_it=plot_mono_correction, xd=xd_full,
                                #                      yd=yd_full, range=fit_range)
                                #     REF_peak_posi[i] = popt[1]
                                # except (RuntimeError, TypeError):
                                #     print("Failed to find gaussian fit to scan number %s"%scan_number)
                                #     REF_peak_posi[i] = htxs_class.get_scan_data(scan_number,'ENERGY_ENC')[np.argmax(htxs_class.get_scan_data(scan_number,'REF'))]
                                #     if plot_mono_correction:
                                #         plt.sj_vlines(REF_peak_posi[i])
                                #         plt.title('Scan #%s fitting failed, use simple maximum'%scan_number)

                                # new way, not fitting but finding a peak within +/-1 eV from the ref_energy => now fit with 2nd order curve
                                # xd_full = htxs_class.get_scan_data(scan_number,'ENERGY_ENC')
                                try:
                                    xd_full = htxs_class.get_scan_data(scan_number,'mono132')
                                except KeyError:
                                    xd_full = htxs_class.get_scan_data(scan_number,'MONO')
                                #yd_full = htxs_class.get_scan_data(scan_number,'REF')
                                # use just REF (above) or i0 normalized REF (below)?
                                if ref_norm is True:
                                    yd_full = np.asfarray(htxs_class.get_scan_data(scan_number,ref_data))/np.asfarray(htxs_class.get_i0(scan_number))
                                else:
                                    yd_full = np.asfarray(htxs_class.get_scan_data(scan_number,ref_data))

                                # for the first ref_energy
                                if ref_fit_range is None:
                                    ref_fit_range_provided = None
                                    ref_fit_range = 0.5  # +/- ref_fit_range in eV
                                else:
                                    ref_fit_range_provided = ref_fit_range
                                left_i = 1
                                right_i = 1

                                while True:
                                    xd = xd_full[np.logical_and(xd_full>ref_energy[0]-ref_fit_range*left_i, xd_full<ref_energy[0]+ref_fit_range*right_i)]
                                    yd = yd_full[np.logical_and(xd_full>ref_energy[0]-ref_fit_range*left_i, xd_full<ref_energy[0]+ref_fit_range*right_i)]
                                    # older way
                                    # if argmax(yd) == len(yd)-1: # if range is too short on the right side
                                    #     xd = xd_full[np.logical_and(xd_full>ref_energy-ref_fit_range, xd_full<ref_energy+2*ref_fit_range)]
                                    #     yd = yd_full[np.logical_and(xd_full>ref_energy-ref_fit_range, xd_full<ref_energy+2*ref_fit_range)]
                                    # elif argmax(yd) == 0: # if range is too short on the left side
                                    #     xd = xd_full[np.logical_and(xd_full>ref_energy-2*ref_fit_range, xd_full<ref_energy+ref_fit_range)]
                                    #     yd = yd_full[np.logical_and(xd_full>ref_energy-2*ref_fit_range, xd_full<ref_energy+ref_fit_range)]
                                    # approx_ref_max_posi = xd[np.argmax(yd)]
                                    # spline = UnivariateSpline(xd, yd, s=0)
                                    # xdense = np.linspace(approx_ref_max_posi-1, approx_ref_max_posi+1, 100)
                                    # REF_peak_posi[i] = xdense[np.argmax(spline(xdense))]
                                    # old way
                                    # p = np.polyfit(xd-ref_energy, yd, 2)
                                    # REF_peak_posi[i] = -p[1]/(2*p[0]) + ref_energy
                                    xd_linspace_1 = np.linspace(xd[0], xd[-1], int((xd[-1]-xd[0])/0.01)+1)
                                    p_1 = np.polyfit(xd-ref_energy[0], yd, ref_fit_order)
                                    max_arg = np.argmax(np.polyval(p_1, xd_linspace_1-ref_energy[0]))
                                    # print('max_arg',max_arg)
                                    # print('len(xd_linspace)-1',len(xd_linspace)-1)
                                    if max_arg < 10:
                                        left_i+=1
                                    elif max_arg > len(xd_linspace_1)-11:
                                        right_i+=1
                                    else:
                                        REF_peak_posi_1[i] = xd_linspace_1[max_arg]
                                        break
                                    # if (REF_peak_posi[i] > xd[0]) and (REF_peak_posi[i] < xd[-1]):
                                    #     print('xd[0]',xd[0],type(xd[0]))
                                    #     print('xd[-1]',xd[-1])
                                    #     print('REF_peak_posi[i]',REF_peak_posi[i],type(REF_peak_posi[i]))
                                    #     break
                                print('Peak position: %g'%REF_peak_posi_1[i])

                                # for the second ref_energy
                                if ref_fit_range_provided is None:
                                    ref_fit_range = 0.5  # +/- ref_fit_range in eV
                                else:
                                    ref_fit_range = ref_fit_range_provided
                                left_i = 1
                                right_i = 1

                                while True:
                                    xd = xd_full[np.logical_and(xd_full>ref_energy[1]-ref_fit_range*left_i, xd_full<ref_energy[1]+ref_fit_range*right_i)]
                                    yd = yd_full[np.logical_and(xd_full>ref_energy[1]-ref_fit_range*left_i, xd_full<ref_energy[1]+ref_fit_range*right_i)]
                                    # older way
                                    # if argmax(yd) == len(yd)-1: # if range is too short on the right side
                                    #     xd = xd_full[np.logical_and(xd_full>ref_energy-ref_fit_range, xd_full<ref_energy+2*ref_fit_range)]
                                    #     yd = yd_full[np.logical_and(xd_full>ref_energy-ref_fit_range, xd_full<ref_energy+2*ref_fit_range)]
                                    # elif argmax(yd) == 0: # if range is too short on the left side
                                    #     xd = xd_full[np.logical_and(xd_full>ref_energy-2*ref_fit_range, xd_full<ref_energy+ref_fit_range)]
                                    #     yd = yd_full[np.logical_and(xd_full>ref_energy-2*ref_fit_range, xd_full<ref_energy+ref_fit_range)]
                                    # approx_ref_max_posi = xd[np.argmax(yd)]
                                    # spline = UnivariateSpline(xd, yd, s=0)
                                    # xdense = np.linspace(approx_ref_max_posi-1, approx_ref_max_posi+1, 100)
                                    # REF_peak_posi[i] = xdense[np.argmax(spline(xdense))]
                                    # old way
                                    # p = np.polyfit(xd-ref_energy, yd, 2)
                                    # REF_peak_posi[i] = -p[1]/(2*p[0]) + ref_energy
                                    xd_linspace_2 = np.linspace(xd[0], xd[-1], int((xd[-1]-xd[0])/0.01)+1)
                                    p_2 = np.polyfit(xd-ref_energy[1], yd, ref_fit_order)
                                    max_arg = np.argmax(np.polyval(p_2, xd_linspace_2-ref_energy[1]))
                                    # print('max_arg',max_arg)
                                    # print('len(xd_linspace)-1',len(xd_linspace)-1)
                                    if max_arg < 10:
                                        left_i+=1
                                    elif max_arg > len(xd_linspace_2)-11:
                                        right_i+=1
                                    else:
                                        REF_peak_posi_2[i] = xd_linspace_2[max_arg]
                                        break
                                    # if (REF_peak_posi[i] > xd[0]) and (REF_peak_posi[i] < xd[-1]):
                                    #     print('xd[0]',xd[0],type(xd[0]))
                                    #     print('xd[-1]',xd[-1])
                                    #     print('REF_peak_posi[i]',REF_peak_posi[i],type(REF_peak_posi[i]))
                                    #     break
                                print('Peak position: %g'%REF_peak_posi_2[i])
                                # corrected_mono[i] = htxs_class.get_scan_data(scan_number,'ENERGY_ENC') - REF_peak_posi[i] + ref_energy
                                if ref_data == 'SC':  # this branch not working for two anchor point case
                                    print('Correcting by SC is not working yet. Return.')
                                    return
                                else:
                                    corrected_mono[i] = (xd_full - REF_peak_posi_1[i]) * (correct_to_energy[1] - correct_to_energy[0]) / (REF_peak_posi_2[i] - REF_peak_posi_1[i]) + correct_to_energy[0]

                                if plot_mono_correction:
                                    rainbow_colors = [cm.rainbow_r(x) for x in np.linspace(0,1.,len(scan_numbers))]
                                    # ax1.plot(htxs_class.get_scan_data(scan_number,'ENERGY_ENC'), htxs_class.get_scan_data(scan_number,'REF'), color=rainbow_colors[i]);ax1.set_title('REF');plt.draw()
                                    ax[0].plot(xd_full, yd_full, color=rainbow_colors[i], label=str(scan_number))
                                    ax[0].plot(xd_linspace_1, np.polyval(p_1, xd_linspace_1-ref_energy[0]), 'k--')
                                    ax[0].plot(xd_linspace_2, np.polyval(p_2, xd_linspace_2-ref_energy[1]), 'k--')
                                    ax[0].set_xlabel('nominal mono')
                                    ax[0].vlines(REF_peak_posi_1[i], ymin=min(yd_full), ymax=min(yd_full)+(max(yd_full)-min(yd_full))*1.1, color=rainbow_colors[i], linestyles='dashed')
                                    ax[0].vlines(REF_peak_posi_2[i], ymin=min(yd_full), ymax=min(yd_full)+(max(yd_full)-min(yd_full))*1.1, color=rainbow_colors[i], linestyles='dashdot')
                                    ax[1].plot(corrected_mono[i], yd_full, color=rainbow_colors[i], label=str(scan_number))
                                    if i == len(scan_numbers)-1:  # only for the last scan
                                        if ref_norm is True:
                                            ax[0].set_title('%s, %s/i0'%(dir_p,ref_data))#;plt.draw()
                                        else:
                                            ax[0].set_title('%s, %s'%(dir_p,ref_data))#;plt.draw()
                                        ax[0].vlines(ref_energy[0], *ax[0].get_ylim(), color='black', linestyles='dashed', label='nominal ref 1')
                                        ax[0].vlines(ref_energy[1], *ax[0].get_ylim(), color='black', linestyles='dashdot', label='nominal ref 2')
                                        ymin, ymax = ax[1].get_ylim()
                                        if ref_data == 'SC':  # not used for now
                                            ax[1].set_xlabel('nomianl_mono - peak position + peak position of the first scan')
                                            ax[1].set_title('%s, %s/i0 aligned to the first scan'%(dir_p,ref_data))#;plt.draw()
                                            ax[1].vlines(ref_energy[0], ymin, ymax, color='black', linestyles='dashed', label='nominal ref 1')
                                            ax[1].vlines(ref_energy[1], ymin, ymax, color='black', linestyles='dashed', label='nominal ref 2')
                                            ax[1].vlines(REF_peak_posi_1[0], ymin, ymax, color='gray', label='correct-to energy 1')
                                            ax[1].vlines(REF_peak_posi_2[0], ymin, ymax, color='gray', label='correct-to energy 2')
                                        else:  # correcting by ref data
                                            ax[1].set_xlabel('nomianl_mono - peak position + ref_energy')
                                            ax[1].set_title('%s, %s/i0 with corrected mono'%(dir_p,ref_data))#;plt.draw()
                                            ax[1].vlines(correct_to_energy[0], ymin, ymax, color='gray', label='correct-to energy 1')
                                            ax[1].vlines(correct_to_energy[1], ymin, ymax, color='gray', label='correct-to energy 2')
                                            # ax[1].vlines(REF_peak_posi[i], ymin, ymax, color='gray')

                            if plot_mono_correction is True:
                                ax[0].legend()
                                ax[1].legend()

                            if dont_ask is True:
                                pass
                            else:
                                while True:
                                    # answer = raw_input("Continue (y will proceed / n will return corrected_mono)? ")
                                    answer = input("Continue (y will proceed / n will return corrected_mono)? ")
                                    if answer == 'y':
                                        # pass
                                        break
                                    elif answer == 'n':
                                        print('Cancelled. corrected_mono returned, instead of rixs_dict')
                                        return corrected_mono # when cancelled, return corrected_mono
                        else:  # this is the case when ref_energy is given but out of the mono range.
                            print('# provide proper ref energy (two values)')
                            return
                            # for i, scan_number in enumerate(scan_numbers):
                            #     print('# corrected_mono not found; nominal mono (MONO) will be used')
                            #     # corrected_mono[i] = htxs_class.get_scan_data(scan_number,'ENERGY_ENC') # use ENERGY_ENC or MONO?
                            #     corrected_mono[i] = htxs_class.get_scan_data(scan_number, 'MONO')  # use ENERGY_ENC or MONO?

                else:  # if ref_energy is None; this is the case when ref_energy is not determined (still None) or ref_energy is out of the range; use just MONO or ENERGY_ENC (not decided)
                    for i, scan_number in enumerate(scan_numbers):
                        print('# corrected_mono not found; nominal mono (MONO) will be used')
                        # corrected_mono[i] = htxs_class.get_scan_data(scan_number,'ENERGY_ENC') # use ENERGY_ENC or MONO?
                        try:
                            corrected_mono[i] = htxs_class.get_scan_data(scan_number, 'mono132')  # use ENERGY_ENC or MONO?
                        except KeyError:
                            corrected_mono[i] = htxs_class.get_scan_data(scan_number, 'MONO')  # use ENERGY_ENC or MONO?
        elif corrected_mono == 'E':  # haven't used in a while
            corrected_mono = np.zeros((n_spots,len(nominal_mono_from_log)), dtype=np.float32)  # changed from nominal_mono to nominal_mono_from_log
            for i, scan_number in enumerate(scan_numbers):
                if i==0:
                    print('\n# ENERGY_ENC will be used')
                # corrected_mono[i] = htxs_class.get_scan_data(scan_number,'ENERGY_ENC')  # use ENERGY_ENC or MONO?
                corrected_mono[i] = htxs_class.get_scan_data(scan_number,'ENERGY_ENC')  # use ENERGY_ENC or MONO?
        elif corrected_mono == 'M':  # haven't used in a while
            corrected_mono = np.zeros((n_spots,len(nominal_mono_from_log)), dtype=np.float32) # changed from nominal_mono to nominal_mono_from_log
            for i, scan_number in enumerate(scan_numbers):
                if i==0:
                    print('\n# MONO will be used')
                # corrected_mono[i] = htxs_class.get_scan_data(scan_number,'ENERGY_ENC') # use ENERGY_ENC or MONO?
                try:
                    corrected_mono[i] = htxs_class.get_scan_data(scan_number, 'mono132')  # use ENERGY_ENC or MONO?
                except KeyError:
                    corrected_mono[i] = htxs_class.get_scan_data(scan_number, 'MONO')  # use ENERGY_ENC or MONO?
        else:
            corrected_mono = corrected_mono # this line looks funny but helps to understand the code
        corrected_mono = np.round(corrected_mono, decimals=2)  # to handle mismatch between mono from log and spec
        corrected_I0 = np.array([np.asfarray(htxs_class.get_scan_data(scan_number, 'I0')) for scan_number in scan_numbers])  # to be corrected
        corrected_REF = np.array([np.asfarray(htxs_class.get_scan_data(scan_number, 'REF')) for scan_number in scan_numbers])  # to be corrected
        corrected_SC = np.array([np.asfarray(htxs_class.get_scan_data(scan_number, 'SC')) for scan_number in scan_numbers])  # to be corrected
        valid_nominal_mono = nominal_mono_from_log[(np.asfarray(nominal_mono_from_log)-max(corrected_mono[:,0])>=0) & (np.asfarray(nominal_mono_from_log)-min(corrected_mono[:,-1])<=0)] # changed from nominal_mono to nominal_mono_from_log
        sample_id = htxs_class.get_scan_attr(scan_numbers[0],'sampleid')
        sample_name = htxs_class.get_scan_attr(scan_numbers[0],'name')
    elif BL == 133:
        interps = [0]
        valid_nominal_mono = nominal_mono_from_log  # in 13-3 case, 'mono' means scanned motor
        corrected_mono = [valid_nominal_mono] * n_spots  # in 13-3 mono (or motor values) are not corrected yet
        sample_id = None
        sample_name = None  # spec filename can be used

    spot_ids = np.arange(n_spots) # needs to modify

    bin_centers = coadded_bin_centers
    rixs_plane0 = np.zeros((len(spot_ids),len(nominal_mono_from_log),len(bin_centers)),dtype=np.float32) # changed from nominal_mono to nominal_mono_from_log
    rixs_plane0_norm = np.zeros((len(spot_ids),len(nominal_mono_from_log),len(bin_centers)),dtype=np.float32) # changed from nominal_mono to nominal_mono_from_log
    rixs_plane0_norm2 = np.zeros((len(spot_ids),len(nominal_mono_from_log),len(bin_centers)),dtype=np.float32) # changed from nominal_mono to nominal_mono_from_log
    rixs_plane0_part_arr = {}
    rixs_plane0_norm_part_arr = {}
    for key in PIXEL_GROUP_DICT[BL].keys():
        rixs_plane0_part_arr[key] = np.zeros((len(spot_ids),len(nominal_mono_from_log),len(bin_centers)),dtype=np.float32)
        rixs_plane0_norm_part_arr[key] = np.zeros((len(spot_ids),len(nominal_mono_from_log),len(bin_centers)),dtype=np.float32)
    if 1 in interps:
        rixs_plane1 = np.zeros((len(spot_ids),len(valid_nominal_mono),len(bin_centers)),dtype=np.float32)
        rixs_plane1_norm = np.zeros((len(spot_ids),len(valid_nominal_mono),len(bin_centers)),dtype=np.float32)
        I0_2 = np.zeros((len(spot_ids),len(valid_nominal_mono)), dtype=np.float32)
        REF_2 = np.zeros((len(spot_ids),len(valid_nominal_mono)), dtype=np.float32)
        SC_2 = np.zeros((len(spot_ids),len(valid_nominal_mono)), dtype=np.float32)
    if 2 in interps:
        rixs_plane2 = np.zeros((len(spot_ids),len(valid_nominal_mono),len(bin_centers)),dtype=np.float32)
        rixs_plane2_norm = np.zeros((len(spot_ids),len(valid_nominal_mono),len(bin_centers)),dtype=np.float32)

    # if norm is True: # maybe not needed because it always calculated normalized map
    #     if BL == 101:
    #         i0_matrix = []
    #         for idx_spot, spot in enumerate(spot_ids[:num_scans]):
    #             i0_matrix.append([htxs_class.get_i0(scan_numbers[idx_spot]) for ii in range(len(coadded_bin_centers))])
    #         i0_matrix = np.asfarray(i0_matrix)
    #     elif BL == 133:
    #         i0_matrix = np.reshape(monitors, (n_scans, n_steps))

    num_scans = n_spots # num_scans was used for testing the code (to run a small number of scans)
    for idx_spot, spot in enumerate(spot_ids[:num_scans]): # spot means one SPEC scan. This is confusing, so will change to scan
        # if spot in bad_spot_ids:
        #     #bad_spot_counter += 1
        #     print "bad spot #%d skipped"%spot
        #     continue
        print("working on spot/scan #%d (SPEC #S %d)"%(spot, scan_numbers[idx_spot]))


        t_bin_edges = time_start_end[idx_spot*len(nominal_mono_from_log):(idx_spot+1)*len(nominal_mono_from_log)].flatten()
        emi_t_2d_hist, _, _ = np.histogram2d(concat_p_energy, concat_timestamp, bins=(coadded_bin_edges, t_bin_edges))
        emi_mono_2d_hist = emi_t_2d_hist[:,::2]

        # first trial, using 2d histograming
        # good_hist = np.histogram2d(concat_p_energy, concat_timestamp, bins=(coadded_bin_edges, t_bin_edges))[0][:,::2]
        # total_hist = np.histogram2d(concat_p_energy_all, concat_timestamp_all, bins=(coadded_bin_edges, t_bin_edges))[0][:,::2]
        # for i, good_hist_row in enumerate(good_hist):
        #     for j, value in enumerate(good_hist_row):
        #         if value == 0:
        #             good_hist[i][j] = 1
        #             total_hist[i][j] = 0
        # total_to_good_ratio_hist = total_hist / good_hist
        # second trial, using 1d histograming
        good_hist = np.histogram(concat_timestamp, bins=t_bin_edges)[0][::2]
        total_hist = np.histogram(concat_timestamp_all, bins=t_bin_edges)[0][::2]
        for i, value in enumerate(good_hist):
            if value == 0:
                good_hist[i] = 1
                total_hist[i] = 0
        total_to_good_ratio_hist = total_hist / good_hist


        rixs_plane0[idx_spot] = np.transpose(emi_mono_2d_hist)
        # if norm is True:
        emi_mono_2d_hist_norm = np.asfarray(emi_mono_2d_hist) / i0_matrix[idx_spot]
        # first trial, using 2d histograming
        # emi_mono_2d_hist_norm2 = np.asfarray(emi_mono_2d_hist) / i0_matrix[idx_spot] * total_to_good_ratio_hist
        # second trial, using 1d histograming
        emi_mono_2d_hist_norm2 = np.asfarray(emi_mono_2d_hist) / i0_matrix[idx_spot] * total_to_good_ratio_hist
        rixs_plane0_norm[idx_spot] = np.transpose(emi_mono_2d_hist_norm)
        rixs_plane0_norm2[idx_spot] = np.transpose(emi_mono_2d_hist_norm2)
        # else:
        #     rixs_plane0_norm[idx_spot] = rixs_plane0[idx_spot] # copy for now

        if generate_partial_maps is True:
            if ((reduced_data.first_good_dataset.number_of_rows is None) and (reduced_data.n_channels > 180)) or (reduced_data.first_good_dataset.number_of_rows == 30): # not working for 8 row yet
                for key in PIXEL_GROUP_DICT[BL].keys():
                    emi_t_2d_hist_partial, _, _ = np.histogram2d(partial_concat_p_energy[BL][key], partial_concat_p_timestamp[BL][key], bins=(coadded_bin_edges, t_bin_edges))
                    emi_mono_2d_hist_partial = emi_t_2d_hist_partial[:,::2]
                    emi_mono_2d_hist_norm_partial = np.asfarray(emi_mono_2d_hist_partial)/i0_matrix[idx_spot]
                    rixs_plane0_part_arr[key][idx_spot] = np.transpose(emi_mono_2d_hist_partial)
                    rixs_plane0_norm_part_arr[key][idx_spot] = np.transpose(emi_mono_2d_hist_norm_partial)

        x = corrected_mono[idx_spot]
        y = coadded_bin_centers
        if 1 in interps:
            f1 = interpolate.interp2d(x, y, emi_mono_2d_hist, kind='linear')  # this fails for 13-3.
            mono_corrected_counts_lin = f1(valid_nominal_mono,y)
            rixs_plane1[idx_spot] = np.transpose(mono_corrected_counts_lin)
            # if norm is True:
            f1_norm = interpolate.interp2d(x, y, emi_mono_2d_hist_norm, kind='linear')
            mono_corrected_counts_norm_lin = f1_norm(valid_nominal_mono,y)
            rixs_plane1_norm[idx_spot] = np.transpose(mono_corrected_counts_norm_lin)
            f1_I0 = interpolate.interp1d(x, corrected_I0[idx_spot], kind='linear')
            I0_2[idx_spot] = f1_I0(valid_nominal_mono)
            f1_REF = interpolate.interp1d(x, corrected_REF[idx_spot], kind='linear')
            REF_2[idx_spot] = f1_REF(valid_nominal_mono)
            f1_SC = interpolate.interp1d(x, corrected_SC[idx_spot], kind='linear')
            SC_2[idx_spot] = f1_SC(valid_nominal_mono)
        if 2 in interps: # actually third order, not second
            f2 = interpolate.interp2d(x, y, emi_mono_2d_hist, kind='cubic')
            mono_corrected_counts_cub = f2(valid_nominal_mono,y)
            rixs_plane2[idx_spot] = np.transpose(mono_corrected_counts_cub)
            # if norm is True:
            f2_norm = interpolate.interp2d(x, y, emi_mono_2d_hist_norm, kind='cubic')
            mono_corrected_counts_norm_cub = f2_norm(valid_nominal_mono,y)
            rixs_plane2_norm[idx_spot] = np.transpose(mono_corrected_counts_norm_cub)
        # corrected_mono[idx_spot] = corrected_mono[idx_spot][(corrected_mono[idx_spot]>=valid_nominal_mono[0]) & (corrected_mono[idx_spot]<=valid_nominal_mono[-1])]

    rixs_dict = {}
    rixs_dict['interps'] = interps # todo: will be used to save and load valid params only
    rixs_dict['dir_p'] = dir_p
    rixs_dict['nominal_mono'] = nominal_mono_from_log
    rixs_dict['valid_nominal_mono'] = valid_nominal_mono
    rixs_dict['rixs_plane0'] = rixs_plane0
    rixs_dict['rixs_plane0_norm'] = rixs_plane0_norm
    rixs_dict['rixs_plane0_norm2'] = rixs_plane0_norm2
    if generate_partial_maps is True:
        if ((reduced_data.first_good_dataset.number_of_rows is None) and (reduced_data.n_channels > 180)) or (reduced_data.first_good_dataset.number_of_rows == 30): # not working for 8 row yet
            rixs_dict['rixs_plane0_part_arr'] = rixs_plane0_part_arr # newly added 20190325
            rixs_dict['rixs_plane0_norm_part_arr'] = rixs_plane0_norm_part_arr # newly added 20190325
            rixs_dict['missing_chans'] = missing_chans
    if 1 in interps:
        rixs_dict['rixs_plane1'] = rixs_plane1
        rixs_dict['rixs_plane1_norm'] = rixs_plane1_norm
        rixs_dict['I0_2'] = I0_2
        rixs_dict['REF_2'] = REF_2
        rixs_dict['SC_2'] = SC_2
    if 2 in interps:
        rixs_dict['rixs_plane2'] = rixs_plane2
        rixs_dict['rixs_plane2_norm'] = rixs_plane2_norm
    rixs_dict['bin_centers'] = coadded_bin_centers
    rixs_dict['scan_numbers'] = scan_numbers
    if (1 in interps) or (2 in interps):
        rixs_dict['corrected_mono'] = corrected_mono
    rixs_dict['BL'] = BL
    rixs_dict['sample_id'] = sample_id
    rixs_dict['sample_name'] = sample_name

    return rixs_dict


def generate_energy_loss_map(rixs_dict, mono_offset=0, mono_scaling=1):
    '''
    returns rixs_dict_loss
    shape of energy loss map: (number_of_scans, number_of_monos, number_of_emission_bins)
    '''
    adjusted_mono = rixs_dict['nominal_mono'] - mono_offset
    # emission_bins = rixs_dict['bin_centers']
    # bin_width = emission_bins[1]-emission_bins[0]
    bin_width = rixs_dict['bin_centers'][1]-rixs_dict['bin_centers'][0]
    # number_of_scans = len(rixs_dict['scan_numbers'])
    loss_map = np.zeros_like(rixs_dict['rixs_plane0'])
    loss_map_norm = np.zeros_like(rixs_dict['rixs_plane0_norm'])
    for i in range(np.shape(loss_map)[0]):
        for j, mono in enumerate(adjusted_mono):
            loss_map[i,j,:] = np.roll(rixs_dict['rixs_plane0'][i,j,:], shift=-int(round((mono-adjusted_mono[0])/bin_width*mono_scaling,ndigits=0)))
            loss_map_norm[i,j,:] = np.roll(rixs_dict['rixs_plane0_norm'][i,j,:], shift=-int(round((mono-adjusted_mono[0])/bin_width*mono_scaling,ndigits=0)))

    rixs_dict_loss = {}
    rixs_dict_loss['dir_p'] = rixs_dict['dir_p']
    rixs_dict_loss['mono_offset'] = mono_offset
    rixs_dict_loss['mono_scaling'] = mono_scaling
    rixs_dict_loss['nominal_mono'] = rixs_dict['nominal_mono']
    rixs_dict_loss['adjusted_mono'] = adjusted_mono
    rixs_dict_loss['bin_centers'] = adjusted_mono[0] - rixs_dict['bin_centers']
    rixs_dict_loss['rixs_plane0'] = loss_map
    rixs_dict_loss['rixs_plane0_norm'] = loss_map_norm
    rixs_dict_loss['scan_numbers'] = rixs_dict['scan_numbers']
    return rixs_dict_loss

"""
def offset_mono_rixs_dict(rixs_dict, rixs_type='rixs_plane1_norm', mono_offset=0):
    '''
    returns a new rixs_dict with corrected_mono offset by mono_offset
    '''
    import copy
    rixs_dict_new = copy.deepcopy(rixs_dict)
    for key in rixs_dict.keys():

    adjusted_mono = rixs_dict['valid_nominal_mono'] - mono_offset
    emission_bins = rixs_dict['bin_centers']
    bin_width = emission_bins[1]-emission_bins[0]
    number_of_scans = len(rixs_dict['scan_numbers'])
    loss_map = np.zeros_like(rixs_dict[rixs_type])
    for i in range(np.shape(loss_map)[0]):
        for j, mono in enumerate(adjusted_mono):
            loss_map[i,j,:] = np.roll(rixs_dict[rixs_type][i,j,:], shift=-int(round((mono-adjusted_mono[0])/bin_width*mono_scaling,ndigits=0)))
    return rixs_dict_new
"""


def generate_pfy_dict(rixs_dict, roi_dict, keys=None, interp=0):
    '''
    The generated pfy_dict can be used to update the spec file
    e.g., keys = ['20220101_001', '20220101_002']
    roi_dict = {'OK': [480, 580], 'FeLab': [680, 830], 'FeLl': [580, 640]}
    interp=1 uses rixs_plane1
    '''
    pfy_dict = {}
    if keys is None:
        keys = [key for key in rixs_dict.keys() if (type(key) is str)]  # added to ignore alias integer keys
    for key, value in rixs_dict.items():
        if key in keys:
            if interp==1:
                if 'rixs_plane1' in value.keys():
                    if np.sum(value['rixs_plane1_norm']) > 0:
                        interp=1
                    else:
                        print('Corrected rixs map does not exist')
                        interp=0
                else:
                    print('Corrected rixs map does not exist')
                    interp=0
            for ii, scan_number in enumerate(value['scan_numbers']):
                print('# working on scan number', scan_number, 'of folder', key)
                pfy_dict[scan_number] = {}
                if interp==1:
                    pfy_dict[scan_number]['MONO_2'] = value['valid_nominal_mono']
                    if 'I0_2' in value.keys():
                        pfy_dict[scan_number]['I0_2'] = value['I0_2'][ii]
                    if 'REF_2' in value.keys():
                        pfy_dict[scan_number]['REF_2'] = value['REF_2'][ii]
                    if 'SC_2' in value.keys():
                        pfy_dict[scan_number]['SC_2'] = value['SC_2'][ii]
                for roi_name, roi_range in roi_dict.items():
                    pfy_dict[scan_number][roi_name] = plot_pfy(value, spot_ids=value['scan_numbers'].index(scan_number), roi=roi_range, interp=interp, norm=False, return_only=True)[1]
    return pfy_dict


def _is_data_folder(log_fname):
    '''
    if noise: returns False
    if cal: returns 'cal'
    if data: returns 'data'
    '''
    with open(log_fname, 'r') as f:
        for line in f:
            if 'dtype' in line:
                if "noise" in line.split():
                    return False
                elif ("cal" in line.split()) or ("cal," in line.split()):  # cal, is the case for new style log
                    return "cal"
                elif ("data" in line.split()) or ("data," in line.split()):
                    return "data"


def _log_file_parser(log_fname, BL, scan_numbers=None, spec_fname=None, spec_file_first_line=1, skip_scans=[]):
    # does not work for log files from May 2016
    n_spots = 0 # n_spots is the number of scans in one TES folder
    time_start_end = []
    nominal_mono = []
    nominal_mono_from_log = []
    scan_numbers_from_log = []
    i0_matrix = []
    state_stop = False
    scan_mode = None # XAS/XES/etc.
    htxs_class = None
    if isinstance(scan_numbers, int):
        scan_numbers = [scan_numbers]
    with open(log_fname, 'r') as f:
        if BL == 101:
            from sass import htxs
            spec_hdf5_fname = spec_fname + '.hdf5'
            if os.path.isfile(spec_hdf5_fname):
                os.remove(spec_hdf5_fname)
            # htxs.create_htxs_hdf5(spec_fname, spec_hdf5_fname)
            _create_htxs_hdf5_python3(spec_fname, spec_hdf5_fname, spec_file_first_line=spec_file_first_line)
            htxs_class = htxs.HTXS(spec_hdf5_fname)
            if os.path.isfile(spec_hdf5_fname):
                os.remove(spec_hdf5_fname) # don't keep the hdf5

            for line in f:
                if line[0] == '-':  # new stye log
                    print('# new style log is being used')
                    from sass import log
                    l = log.Log(log_fname)
                    scan_numbers_from_log = l.get_vvd()['htxs']
                    if 'mono' in l.get_vvd().keys():
                        scan_mode = 'XAS'
                    n_spots = len(l.get_cvd()['mono'].keys())
                    for spot in range(n_spots):
                        for time_start_end_i in l.get_cvd()['mono'][spot]:
                            time_start_end += time_start_end_i
                    nominal_mono_from_log = l.get_vvd()['mono']
                    break
                else:  # old style log
                    if line.strip().split()[-1] == "noise":
                        scan_mode = 'noise'
                        break
                    # n_lines += 1
                    if "STOP" in line:
                        state_stop = True # not being used
                        if (scan_mode=='XAS') and (has_spot_completed is False): # this is to handle the situation when the scan stopped without 'interrupt' logged
                            scan_numbers_from_log.pop()
                        has_spot_completed = True  # not completed but needed to handle the case without STOP or interrupt
                        break # probably not needed because this would be already at the end of the for loop
                    if ("scan_start" in line) or ((line.strip().split()[-1] == "1") and ("scan" not in line)):  # 2nd condition is for very old old_log
                        if ('mono' in line) or ('fscan' in line):
                            scan_mode = 'XAS'
                        else:
                            scan_mode = 'else'
                            scanned_motor = (line.strip().split()[get_index(line.strip().split(), 'gscan_start')[0] + 2]).upper()
                        counting_time = float(line.strip().split()[-1])
                        if (line.strip().split()[-1] == "1") and ("scan" not in line):
                            has_spot_started = True
                            has_spot_completed = False
                        if "scan_start" in line:
                            current_scan_num = int(line.split()[4])
                            if scan_numbers is None:
                                if current_scan_num not in skip_scans:
                                    scan_numbers_from_log.append(current_scan_num)
                                    has_spot_started = True
                                    has_spot_completed = False
                            else:
                                if (current_scan_num in scan_numbers) and (current_scan_num not in skip_scans):
                                    scan_numbers_from_log.append(current_scan_num)
                                    has_spot_started = True
                                    has_spot_completed = False
                        continue
                    if scan_mode in ['XAS', 'else']:
                        if scan_numbers is None:  # ugly
                            if current_scan_num not in skip_scans:
                                if (("_unpause" in line) or (" unpause" in line)):
                                    if has_spot_started is True:
                                        time_start_end.append(line.strip().split()[0])
                                        time_start_end.append(float(line.strip().split()[0])+counting_time)
                                        if n_spots == 0:
                                            try:
                                                _ = float(line.strip().split()[3])
                                                nominal_mono_from_log.append(line.strip().split()[3]) # now: 4, before: 3
                                            except ValueError:
                                                nominal_mono_from_log.append(line.strip().split()[4]) # now: 4, before: 3
                                        has_spot_started = False
                                    else: # this branch can be incorporated to the above
                                        time_start_end.append(line.strip().split()[0])
                                        time_start_end.append(float(line.strip().split()[0])+counting_time)
                                        if n_spots == 0:
                                            try:
                                                _ = float(line.strip().split()[3])
                                                nominal_mono_from_log.append(line.strip().split()[3]) # now: 4, before: 3
                                            except ValueError:
                                                nominal_mono_from_log.append(line.strip().split()[4]) # now: 4, before: 3
                                    # has_spot_completed = False # moved to gscan_start branch on 20210204
                        else:  # ugly
                            if (current_scan_num in scan_numbers) and (current_scan_num not in skip_scans):
                                if (("_unpause" in line) or (" unpause" in line)):
                                    if has_spot_started is True:
                                        time_start_end.append(line.strip().split()[0])
                                        time_start_end.append(float(line.strip().split()[0])+counting_time)
                                        if n_spots == 0:
                                            try:
                                                _ = float(line.strip().split()[3])
                                                nominal_mono_from_log.append(line.strip().split()[3]) # now: 4, before: 3
                                            except ValueError:
                                                nominal_mono_from_log.append(line.strip().split()[4]) # now: 4, before: 3
                                        has_spot_started = False
                                    else: # this branch can be incorporated to the above
                                        time_start_end.append(line.strip().split()[0])
                                        time_start_end.append(float(line.strip().split()[0])+counting_time)
                                        if n_spots == 0:
                                            try:
                                                _ = float(line.strip().split()[3])
                                                nominal_mono_from_log.append(line.strip().split()[3]) # now: 4, before: 3
                                            except ValueError:
                                                nominal_mono_from_log.append(line.strip().split()[4]) # now: 4, before: 3
                                    # has_spot_completed = False # moved to gscan_start branch on 20210204
                        # if ("_pause" in line) or (" pause" in line):  # commented out because I now want to use nominal counting time
                        #     time_start_end.append(line.strip().split()[0])
                        if ('scan_stop' in line) or (' done' in line): # 'done' worked before, but jamie added 'spot_done' so it had to be changed
                            if scan_numbers is None:
                                if current_scan_num not in skip_scans:
                                    n_spots+=1
                                    has_spot_started = False
                                    has_spot_completed = True
                            else:
                                if (current_scan_num in scan_numbers) and (current_scan_num not in skip_scans):
                                    n_spots+=1
                                    has_spot_started = False
                                    has_spot_completed = True
                        if 'interrupt' in line: # removed because this is handled by the 'STOP' branch  # restored because somethings log does not have STOP
                            scan_numbers_from_log.pop()
                            has_spot_completed = True  # not completed but needed to handle the case without STOP or interrupt
                            break
                        # previous_line = line
            if has_spot_completed is False:  # this is to handle the case without STOP or interrupt in the log file
                scan_numbers_from_log.pop()
            nominal_mono_from_log = np.asfarray(nominal_mono_from_log)
            nominal_mono = nominal_mono_from_log

            if (scan_mode not in ['XAS', 'else']): # for now, do not continue if not XAS
                if scan_mode != 'noise':
                    scan_mode = 'XES'
                # print('Not XAS/RIXS data. Returning None..')
                # return scan_mode, None, None, None, None, None, None

            print('number of spots found: %d'%n_spots)
            print('length of nominal_mono_from_log: %d'%len(nominal_mono_from_log))
            print('length of time_start_end: %d'%len(time_start_end))
            time_start_end = time_start_end[:n_spots*len(nominal_mono_from_log)*2]
            # time_start_end = map(float, time_start_end)
            time_start_end = np.asfarray(time_start_end)
            time_start_end = np.reshape(time_start_end, (int(len(time_start_end)/2),2))
            # print np.shape(time_start_end)
            # spot_ids = np.arange(n_spots)+1 ##corresponding spec run number = scan_numbers
            spot_timestamp_ranges = [(time_start_end[i*len(nominal_mono_from_log),0], time_start_end[(i+1)*len(nominal_mono_from_log)-1,1]) for i in range(n_spots)]

            # todo: add a feature that can run partial scans
            # 4 lines below are temporary
            # num_scans = 99999 # arbitrary big number # commented out 20190403
            # if isinstance(scan_numbers, int):
            #     num_scans = scan_numbers
            #     scan_numbers = None

            if (scan_mode != 'XES') and (scan_mode != 'noise') and (n_spots != 0):
                if scan_numbers is None: # this is the case when user did not provide scan numbers for running a smaller number of scans
                    if scan_numbers_from_log != []:
                        scan_numbers = scan_numbers_from_log
                        print("Scan numbers found from log file: ", scan_numbers)
                    else:
                        tolerance = 60 # 60 sec difference between TES time and SPEC time
                        scan_numbers = []
                        scan_counter = 0
                        for scan_number in htxs_class.get_scan_list():
                            if abs(htxs_class.get_epoch(scan_number)[0]+htxs_class.get_htxs_epoch()-spot_timestamp_ranges[scan_counter][0]) < tolerance:
                                scan_numbers.append(scan_number)
                                scan_counter += 1
                        print("Scan numbers found from epoch comparison: ", scan_numbers)
                    # num_scans = min(num_scans, len(scan_numbers)) # check later!!!
                    assert n_spots == len(scan_numbers)
                else:
                    n_spots = len(scan_numbers)
                if scan_mode == 'XAS':
                    nominal_mono_from_spec = htxs_class.get_mono(scan_numbers[0]) ################ needs to modify
                elif scan_mode == 'else':
                    nominal_mono_from_spec = htxs_class.get_scan_data(scan_numbers[0], scanned_motor)
                rainbow_colors = [cm.rainbow_r(x) for x in np.linspace(0,1.,len(scan_numbers))] ################ needs to modify

        elif BL == 133: # as of 20181107
            monitors = [] # i0 on the log file
            scanned_motor_vals = []
            n_scans = 0 # this is the same as n_spots in BL 10-1 case from above
            n_steps = 0
            scan_ids = []
            n_scanned_motor_vals = 0
            current_step_num = 0
            # counting_started = False
            if spec_fname is not None:

                from sass import htxs
                spec_hdf5_fname = spec_fname + '.hdf5'
                if os.path.isfile(spec_hdf5_fname):
                    os.remove(spec_hdf5_fname)
                # htxs.create_htxs_hdf5(spec_fname, spec_hdf5_fname)
                _create_htxs_hdf5_python3(spec_fname, spec_hdf5_fname, spec_file_first_line=spec_file_first_line)
                htxs_class = htxs.HTXS(spec_hdf5_fname)
                if os.path.isfile(spec_hdf5_fname):
                    os.remove(spec_hdf5_fname) # don't keep the hdf5

                for line in f:
                    if "START" in line:
                        start_time_log = float(line.strip().split()[0])
                    if "STOP" in line:
                        stop_time_log = float(line.strip().split()[0])
                        state_stop = True
                tolerance = 60 # 60 sec difference between TES time and SPEC time
                # first_scan_number = 0
                scan_numbers = []
                scan_counter = 0
                first_scan_number = 0
                for scan_number in sorted(htxs_class.get_scan_list()):
                    if abs(htxs_class.get_epoch(scan_number)[0] + htxs_class.get_htxs_epoch() - start_time_log) < tolerance:
                        time_spec_minus_tes = htxs_class.get_epoch(scan_number)[0] + htxs_class.get_htxs_epoch() - start_time_log
                        first_scan_number = scan_number
                        scan_numbers.append(scan_number)
                        scan_counter += 1
                        break
                if first_scan_number == 0:
                    raise Exception('A matching scan number not found. Check SPEC file')
                for scan_number in sorted(htxs_class.get_scan_list()):
                    if (scan_number > first_scan_number) and (htxs_class.get_epoch(scan_number)[-1]+htxs_class.get_htxs_epoch() < stop_time_log + tolerance):
                        scan_numbers.append(scan_number)
                        scan_counter += 1
                print("Scan numbers found from epoch comparison: ", scan_numbers)
                scan_mode = htxs_class.get_scan_cmd(first_scan_number).split()[0] # XAS or ascan or loopscan
                if scan_mode == 'loopscan':
                    scan_mode = 'XES'
                elif scan_mode in ['ascan', 'XAS']:
                    scanned_motor = htxs_class.get_scan_cmd(first_scan_number).split()[1] # XAS or ascan or loopscan
                    print('scanned motor:', scanned_motor)
                    scan_time = float(htxs_class.get_scan_cmd(first_scan_number).split()[-1])
                    print('counting time:', scan_time)
                    nominal_mono = htxs_class.get_scan_data(first_scan_number, scanned_motor)
                    print('nominal mono:', nominal_mono)
                    time_start_end = np.zeros((len(nominal_mono)*len(scan_numbers),2))
                    for ii, scan_number in enumerate(scan_numbers):
                        time_start_end[ii*len(nominal_mono):(ii+1)*len(nominal_mono),0] = htxs_class.get_htxs_epoch() + htxs_class.get_epoch(scan_number) - time_spec_minus_tes
                        time_start_end[ii*len(nominal_mono):(ii+1)*len(nominal_mono),1] = htxs_class.get_htxs_epoch() + htxs_class.get_epoch(scan_number) + scan_time - time_spec_minus_tes
                    print('1st step start stop epoch:', time_start_end[0,0], time_start_end[0,1])
                    print('2nd step start stop epoch:', time_start_end[1,0], time_start_end[1,1])
            else:
                for line in f:
                    if line.strip().split()[-1] == "noise":
                        scan_mode = 'noise'
                        break
                    if "START" in line:
                        scan_mode = 'TES'
                    if "STOP" in line:
                        state_stop = True
                        break # probably not needed because this would be already at the end of the for loop
                    if 'SCAN_N' in line:
                        if n_scans == 0:
                            scan_mode = line.strip().split()[5] # e.g., ascan, XAS
                            if scan_mode not in ['ascan', 'XAS']: # this part will probably not be executed because we use simple loopscan for xes so far.
                                # scan_mode = 'XES'
                                print('Not an ascan or XAS data. Script is not ready for analysis yet..')
                                break
                            scanned_motor = line.strip().split()[6] # e.g., mono, th
                        n_scans += 1
                        scan_numbers_from_log.append(int(line.split()[4])) # SPEC scan #
                        current_step_num = 0
                        # state_begin_scan = True
                        # continue # needed?
                    if ("scan_unpause" in line) or ("scan_pause" in line):
                        if scan_mode not in  ['ascan', 'XAS']: # this is to handel the case when loopscan accidentally records each step values
                            scan_mode = 'XES'
                            print('Not a scan data, but maybe XES.')
                            break
                        time_start_end.append(line.strip().split()[0])
                        monitors.append(line.strip().split()[-1])
                        scanned_motor_vals.append(line.strip().split()[-2])
                        if n_scans == 1: # meaning the first scan
                            n_steps += 1 # double counting, so should be halved later
                        current_step_num += 1 # double counting as well
                    # if 'interrupt' in line:
                    #     scan_numbers_from_log.pop()
                # if scan_mode in ['TES', 'XES', 'noise']:
                #     return scan_mode, None, None, None, None, None, None

                if current_step_num != n_steps: # incomplete scan
                    scan_numbers_from_log.pop()
                    n_scans -= 1

                n_steps = int(n_steps/2.)
                # spot_ids = np.arange(n_scans)+1 # to make 13-3 log parsing compatible with the rest of the code
                nominal_mono = np.asfarray(scanned_motor_vals[::2])[:n_steps] # to make 13-3 log parsing compatible with the rest of the code
                # nominal_mono_from_log = nominal_mono # to make 13-3 log parsing compatible with the rest of the code
                monitors = np.asfarray(monitors)[1::2] # len = n_scans * n_steps

                print('number of scans found: %d'%n_scans)
                print('number of steps found: %d'%n_steps)
                # print('length of scanned_motor_vals: %d'%len(scanned_motor_vals))
                # print(scanned_motor_vals[:10])
                print('length of time_start_end: %d'%len(time_start_end))
                time_start_end = time_start_end[:n_scans*n_steps*2] # to remove
                # time_start_end = map(float, time_start_end)
                time_start_end = np.asfarray(time_start_end)
                time_start_end = np.reshape(time_start_end, (int(len(time_start_end)/2),2))
                # print np.shape(time_start_end)
                spot_timestamp_ranges = [(time_start_end[i*n_steps,0], time_start_end[(i+1)*n_steps-1,1]) for i in range(n_scans)]

                # todo: add a feature that can run partial scans
                # 4 lines below are temporary
                # num_scans = 99999 # arbitrary big number # commented out 20190403
                # if isinstance(scan_numbers, int):
                #     num_scans = scan_numbers
                #     scan_numbers = None

                if scan_mode in ['ascan', 'XAS']:
                    if scan_numbers is None:
                        if scan_numbers_from_log != []:
                            scan_numbers = scan_numbers_from_log
                            print("Scan numbers found from log file: ", scan_numbers)
                        # else: # for 10-1 old data
                        #     tolerance = 30 # 30 sec difference between TES time and SPEC time
                        #     scan_numbers = []
                        #     scan_counter = 0
                        #     for scan_number in run_list:
                        #         if abs(htxs_class.get_epoch(scan_number)[0]+htxs_class.get_htxs_epoch()-spot_timestamp_ranges[scan_counter][0]) < tolerance:
                        #             scan_numbers.append(scan_number)
                        #             scan_counter += 1
                        #     print("Scan numbers found from epoch comparison: ", scan_numbers)
                        # num_scans = min(num_scans, len(scan_numbers)) # check later!!!
                    # elif isinstance(scan_numbers, int):
                        assert n_scans == len(scan_numbers)
                        scan_indices = range(n_scans)
                    else:
                        n_scans = 0
                        scan_indices = []
                        for scan_number in scan_numbers:
                            if scan_number in scan_numbers_from_log:
                                n_scans += 1
                                scan_indices += get_index(scan_numbers_from_log, scan_number)
                        selected_indices = [range(scan_index*n_steps, (scan_index+1)*n_steps) for scan_index in scan_indices]
                        selected_indices_flat = [item for sublist in selected_indices for item in sublist] # probably same as np.array(selected_indices).flatten()
                        time_start_end = time_start_end[np.r_[selected_indices_flat], :] # np.r_ might not be necessary
                        monitors = monitors[np.r_[selected_indices_flat]] # np.r_ might not be necessary
                        n_scans = len(scan_numbers)
                    rainbow_colors = [cm.rainbow_r(x) for x in np.linspace(0,1.,len(scan_numbers))] ################ needs to modify
        else:
            raise Exception('Neither 10-1 nor 13-3. Halted!')

    if BL == 101:
        # for idx_spot, spot in enumerate(spot_ids[:num_scans]): # these two lines look strange. modified to below on 20190403
        #     i0_matrix.append([htxs_class.get_i0(scan_numbers[idx_spot]) for ii in range(len(coadded_bin_centers))])
        if (scan_mode != 'XES') and (scan_mode != 'noise') and (n_spots != 0):
            for scan_number in scan_numbers:
                i0_matrix.append([htxs_class.get_i0(scan_number)])
            i0_matrix = np.asfarray(i0_matrix)
    elif BL == 133:
        if spec_fname is None:
            # if monitors != []:
            n_spots = n_scans # to make 13-3 log parsing compatible with the rest of the code
            monitors = monitors[:n_scans*n_steps] # to handle incomplete scans
            i0_matrix = np.reshape(monitors, (n_scans, n_steps))
            htxs_class = None
        else:
            n_spots = len(scan_numbers)
            if scan_mode in ['ascan', 'XAS']:
                for scan_number in scan_numbers:
                    i0_matrix.append([htxs_class.get_scan_data(scan_number, 'Monitor')])
                i0_matrix = np.asfarray(i0_matrix)

    return scan_mode, n_spots, scan_numbers, nominal_mono, i0_matrix, time_start_end, htxs_class


def generate_pfy(reduced_data, spec_fname, spec_file_first_line=1, log_fname=None, scan_numbers=None, bin_start=0, bin_end=1000, plot_xes_per_spot=True, ref_energy=None, ref_data='REF', plot_mono_correction=True, dont_ask=False, corrected_mono=None, interps=[1], norm=True):

    '''
    # incomplete
    # returns pfy_dict
    # plot_xes_per_spot=True shows XES per spot
    # ref_energy=None gives
    # dont_ask=True is same as answering y after mono correction is done (to prepare for batch processing)
    # mono_range not being used
    '''
    from sass import htxs

    spec_hdf5_fname = spec_fname + '_temp.hdf5'
    if os.path.isfile(spec_hdf5_fname):
        os.remove(spec_hdf5_fname)

    if isinstance(reduced_data, np.ndarray):
        dir_p = reduced_data[0].dir_p
    else:
        dir_p = reduced_data.first_good_dataset.filename.split(os.sep)[-2]

    if log_fname is None: # if not provided, grab it from the right folder
        if BASE_FOLDER == "no name yet":
            set_base_folder()
        else:
            print('# BASE_FOLDER: %s'%BASE_FOLDER)

            path_log = os.path.join(BASE_FOLDER, dir_p.split('_')[0] ,dir_p)
            log_fnames = glob.glob(os.path.join(path_log,'*log*'))
            old_log_fnames = glob.glob(os.path.join(path_log,'*log_old'))
            if len(old_log_fnames) == 0:
                if len(log_fnames) == 0:
                    print('# no log file found. data returned. Provide log file name!')
                    return reduced_data
                else:
                    log_fname = max(log_fnames, key=os.path.getctime) # the most recent one
            else:
                log_fname = max(old_log_fnames,key=os.path.getctime) # the most recent one

    # parsing log file begins, does not work for log files from May 2016
    n_spots = 0
    time_start_end = []
    nominal_mono_from_log = [] # this becomes a strings array
    scan_numbers_from_log = []
    state_stop = False
    with open(log_fname, 'r') as f:
        #print len(f)
        for line in f:
            # n_lines += 1
            if "STOP" in line:
                state_stop = True
            if ("scan_start" in line) or ((line.strip().split()[-1] == "1") and ("scan" not in line)):
                state_begin_spot = True
                if "scan_start" in line:
                    scan_numbers_from_log.append(int(line.split()[4]))
            if ("_unpause" in line) or (" unpause" in line):
                if state_begin_spot is True:
                    time_start_end.append(line.strip().split()[0])
                    if n_spots == 0:
                        try:
                            _ = line.strip().split()[3]
                            nominal_mono_from_log.append(line.strip().split()[3]) # now: 4, before: 3
                        except ValueError:
                            nominal_mono_from_log.append(line.strip().split()[4]) # now: 4, before: 3
                    state_begin_spot = False
                else:
                    time_start_end.append(line.strip().split()[0])
                    if n_spots == 0:
                        try:
                            _ = line.strip().split()[3]
                            nominal_mono_from_log.append(line.strip().split()[3]) # now: 4, before: 3
                        except ValueError:
                            nominal_mono_from_log.append(line.strip().split()[4]) # now: 4, before: 3
            if ("_pause" in line) or (" pause" in line):
                time_start_end.append(line.strip().split()[0])
            if ('scan_stop' in line) or (' done' in line): # 'done' worked before, but jamie added 'spot_done' so it had to be changed
                n_spots+=1
                state_begin_spot = False
            if 'interrupt' in line:
                scan_numbers_from_log.pop()
            # previous_line = line
    print('number of spots found: %d'%n_spots)
    print('length of nominal_mono_from_log: %d'%len(nominal_mono_from_log))
    print('length of time_start_end: %d'%len(time_start_end))
    time_start_end = time_start_end[:n_spots*len(nominal_mono_from_log)*2]
    # time_start_end = map(float, time_start_end)
    time_start_end = np.asfarray(time_start_end)
    time_start_end = np.reshape(time_start_end, (int(len(time_start_end)/2),2))
    # print np.shape(time_start_end)
    spot_ids = np.arange(n_spots) ##corresponding spec run number = scan_numbers
    spot_timestamp_ranges = [(time_start_end[i*len(nominal_mono_from_log),0], time_start_end[(i+1)*len(nominal_mono_from_log)-1,1]) for i in range(n_spots)]


    # htxs.create_htxs_hdf5(spec_fname, spec_hdf5_fname)
    _create_htxs_hdf5_python3(spec_fname, spec_hdf5_fname, spec_file_first_line=spec_file_first_line)
    htxs_class = htxs.HTXS(spec_hdf5_fname)

    if os.path.isfile(spec_hdf5_fname):
        os.remove(spec_hdf5_fname)

    htxs_dict, run_list, header_dict, metadata_dict, init_dict = htxs.parse_all_htxs(spec_fname)

    # todo: add a feature that can run partial scans
    # 4 lines below are temporary
    num_scans = 99999 # arbitrary big number
    if isinstance(scan_numbers, int):
        num_scans = scan_numbers
        scan_numbers = None

    if scan_numbers is None:
        if scan_numbers_from_log != []:
            scan_numbers = scan_numbers_from_log
            print("Scan numbers found from log file: ", scan_numbers)
        else:
            tolerance = 30 # 30 sec difference between TES time and SPEC time
            scan_numbers = []
            scan_counter = 0
            for scan_number in run_list:
                if abs(htxs_class.get_epoch(scan_number)[0]+htxs_class.get_htxs_epoch()-spot_timestamp_ranges[scan_counter][0]) < tolerance:
                    scan_numbers.append(scan_number)
                    scan_counter += 1
            print("Scan numbers found from epoch comparison: ", scan_numbers)
        num_scans = min(num_scans, len(scan_numbers)) # check later!!!
    # elif isinstance(scan_numbers, int):

    assert n_spots == len(scan_numbers)

    nominal_mono = htxs_class.get_mono(scan_numbers[0])


    rainbow_colors = [cm.rainbow_r(x) for x in np.linspace(0,1.,len(spot_ids))]


    ####### new
    # concat_p_energy = np.concatenate([ds.p_energy_from_cal_dc[ds.good()] for ds in reduced_data])
    concat_p_energy = np.concatenate([ds.p_energy[ds.good()] for ds in reduced_data])
    concat_timestamp = np.concatenate([ds.p_timestamp[ds.good()] for ds in reduced_data])

    nused = len([ds for ds in reduced_data]) # number of pixels

    if plot_xes_per_spot is True:

        coadded_bin_edges = np.arange(0, 1000+0.1, 0.1) # hard-coded range # change arange to linsapce?
        coadded_bin_centers = (coadded_bin_edges[1:]+coadded_bin_edges[:-1])*0.5

        coadded_counts_spot = {}
        # coadded_bin_edges = np.arange(0,bin_end,bin_width)
        # coadded_bin_centers = (coadded_bin_edges[1:]+coadded_bin_edges[:-1])*0.5
        for spot in spot_ids:
            print('# histograming for XES on spot/scan # %d'%spot)
            counts, _ = np.histogram(concat_p_energy[(concat_timestamp>=spot_timestamp_ranges[spot-1][0])&(concat_timestamp<=spot_timestamp_ranges[spot-1][1])], coadded_bin_edges)
            coadded_counts_spot[spot]=counts

        plt.figure()
        ax1 = plt.subplot(111)
        # for (mono_e, counts) in coadded_counts_sj.items():
        #     plt.semilogy(coadded_bin_centers, counts,label=mono_e)
        bad_spot_counter = 0
        for i, spot in enumerate(spot_ids):
            # if spot in bad_spot_ids:
            #     bad_spot_counter += 1
            #     print "bad spot #%d skipped"%spot
            #     continue
            counts = coadded_counts_spot[spot]
            ax1.semilogy(coadded_bin_centers, counts,label='%d (%d)'%(spot,scan_numbers[i]), color=rainbow_colors[i-bad_spot_counter])
            #ax1.semilogy(coadded_bin_centers, counts,label=spot, color=rainbow_colors[i-bad_spot_counter])
        plt.draw()
        plt.title('%d pixels coadded, spot by spot'%nused)

        plt.xlabel("energy (eV)")
        plt.ylabel("counts per %0.2f eV bin"%(coadded_bin_edges[1]-coadded_bin_edges[0]))
        #plt.title("%g pixels"%nused)
        plt.legend(loc="best", title='spot/scan #\n(SPEC #S)', fontsize='x-small')

    # concat_p_energy = concat_p_energy[(concat_p_energy>=bin_start) & (concat_p_energy<=bin_end)]
    concat_timestamp = concat_timestamp[(concat_p_energy>=bin_start) & (concat_p_energy<=bin_end)]

    # if corrected_mono is None:
    #     corrected_mono = np.zeros((n_spots,len(nominal_mono)), dtype=np.float32)
    #     for i, scan_number in enumerate(scan_numbers):
    #         corrected_mono[i] = htxs_class.get_scan_data(scan_number,'MONO') # use ENERGY_ENC or MONO?
    # else:
    #     corrected_mono = corrected_mono
    if corrected_mono is None:
        if [] == interps:
            for i, scan_number in enumerate(scan_numbers):
                print('# corrected_mono not necessary; nominal mono will be used')
                # corrected_mono[i] = htxs_class.get_scan_data(scan_number,'ENERGY_ENC') # use ENERGY_ENC or MONO?
                try:
                    corrected_mono[i] = htxs_class.get_scan_data(scan_number,'mono132') # use ENERGY_ENC or MONO?
                except KeyError:
                    corrected_mono[i] = htxs_class.get_scan_data(scan_number,'MONO') # use ENERGY_ENC or MONO?
        else:
            if ref_energy is None: # guess ref_energy
                Fe_peak_E = 706.9
                if (Fe_peak_E >= nominal_mono[0]) and (Fe_peak_E <= nominal_mono[-1]):
                    ref_energy = Fe_peak_E

            if ref_energy is not None:
                REF_peak_posi = np.zeros((n_spots,1), dtype=np.float32)
                corrected_mono = np.zeros((n_spots,len(nominal_mono)), dtype=np.float32)
                print("mono is to be corrected using ref_energy = %s"%ref_energy)

                # right below is going to be replaced by a function below: correct_mono()
                if plot_mono_correction:
                    fig1 = plt.figure()
                    ax1 = fig1.gca()
                    fig2 = plt.figure()
                    ax2 = fig2.gca()

                for i, scan_number in enumerate(scan_numbers):
                    try: # fitting part needs improvement
                        xd_full = htxs_class.get_scan_data(scan_number,'ENERGY_ENC')
                        yd_full = htxs_class.get_scan_data(scan_number,ref_data)
                        xd = xd_full[np.logical_and(xd_full>ref_energy-5, xd_full<ref_energy+5)]
                        yd = yd_full[np.logical_and(xd_full>ref_energy-5, xd_full<ref_energy+5)]
                        fit_range = [xd[np.argmax(yd)]-0.7, xd[np.argmax(yd)]+0.7]
                        # ref_max_posi = htxs_class.get_scan_data(scan_number,'ENERGY_ENC')[np.argmax(htxs_class.get_scan_data(scan_number,'REF'))]
                        # fit_range = [ref_max_posi-0.7, ref_max_posi+0.7]
                        popt, pcov, perr = fit_gaussian(sigma=0.4, tailed=False, plot_it=plot_mono_correction, xd=xd_full,
                                         yd=yd_full, range=fit_range)
                        REF_peak_posi[i] = popt[1]
                    except (RuntimeError, TypeError):
                        print("Failed to find gaussian fit to scan number %s"%scan_number)
                        REF_peak_posi[i] = htxs_class.get_scan_data(scan_number,'ENERGY_ENC')[np.argmax(htxs_class.get_scan_data(scan_number,ref_data))]
                        if plot_mono_correction:
                            plt.sj_vlines(REF_peak_posi[i])
                            plt.title('Scan #%s fitting failed, use simple maximum'%scan_number)
                    if plot_mono_correction:
                        ax1.plot(htxs_class.get_scan_data(scan_number,'ENERGY_ENC'), htxs_class.get_scan_data(scan_number,ref_data), color=rainbow_colors[i]);ax1.set_title(ref_data);plt.draw()
                        ax2.plot(htxs_class.get_scan_data(scan_number,'ENERGY_ENC'), htxs_class.get_scan_data(scan_number,ref_data)/np.asfarray(htxs_class.get_i0(scan_number)), color=rainbow_colors[i]);ax2.set_title('Normalized REF');plt.draw()
                    corrected_mono[i] = htxs_class.get_scan_data(scan_number,'ENERGY_ENC') - REF_peak_posi[i] + ref_energy

                if dont_ask is True:
                    pass
                else:
                    while True:
                        # answer = raw_input("Continue (y wii proceed / n will return corrected_mono)? ")
                        answer = input("Continue (y wii proceed / n will return corrected_mono)? ")
                        if answer == 'y':
                            # pass
                            break
                        elif answer == 'n':
                            print('Cancelled. corrected_mono returned, instead of rixs_dict')
                            return corrected_mono # when cancelled, return corrected_mono
            else: # this is the case when ref_energy is not determined; use just MONO or ENERGY_ENC (not decided)
                print('# correct_mono not found; nominal mono will be used')
                for i, scan_number in enumerate(scan_numbers):
                    # corrected_mono[i] = htxs_class.get_scan_data(scan_number,'ENERGY_ENC') # use ENERGY_ENC or MONO?
                    try:
                        corrected_mono[i] = htxs_class.get_scan_data(scan_number,'mono132') # use ENERGY_ENC or MONO?
                    except KeyError:
                        corrected_mono[i] = htxs_class.get_scan_data(scan_number,'MONO') # use ENERGY_ENC or MONO?
    else:
        corrected_mono = corrected_mono

    valid_nominal_mono = nominal_mono[(nominal_mono-max(corrected_mono[:,0])>=0) & (nominal_mono-min(corrected_mono[:,-1])<=0)]

    pfy0 = np.zeros((len(spot_ids),len(nominal_mono)),dtype=np.float32)
    pfy1 = np.zeros((len(spot_ids),len(valid_nominal_mono)),dtype=np.float32)
    pfy2 = np.zeros((len(spot_ids),len(valid_nominal_mono)),dtype=np.float32)
    pfy0_norm = np.zeros((len(spot_ids),len(nominal_mono)),dtype=np.float32)
    pfy1_norm = np.zeros((len(spot_ids),len(valid_nominal_mono)),dtype=np.float32)
    pfy2_norm = np.zeros((len(spot_ids),len(valid_nominal_mono)),dtype=np.float32)

    if norm is True:
        i0_matrix = []
        for idx_spot, spot in enumerate(spot_ids[:num_scans]):
            i0_matrix.append(htxs_class.get_i0(scan_numbers[idx_spot]))
        i0_matrix = np.asfarray(i0_matrix)

    for idx_spot, spot in enumerate(spot_ids[:num_scans]):
        # if spot in bad_spot_ids:
        #     #bad_spot_counter += 1
        #     print "bad spot #%d skipped"%spot
        #     continue
        print("working on spot/scan #%d (SPEC #S %d)"%(spot, scan_numbers[idx_spot]))

        # new way #1

        t_bin_edges = time_start_end[idx_spot*len(nominal_mono_from_log):(idx_spot+1)*len(nominal_mono_from_log)][:,0]-0.001 # list of mono starts, 0.001 subtracted to control counts on bin edges
        if spot == spot_ids[:num_scans][-1]:
            t_bin_edges = np.append(t_bin_edges, time_start_end[-1][1]+0.001) # appended to control bin edges
        else:
            t_bin_edges = np.append(t_bin_edges, time_start_end[(idx_spot+1)*len(nominal_mono_from_log)-1][1]+0.001) # appended to control bin edges
        t_hist, _ = np.histogram(concat_timestamp, bins=t_bin_edges)
        pfy0[idx_spot] = t_hist
        if norm is True:
            pfy0_norm[idx_spot] = np.asfarray(pfy0[idx_spot])/i0_matrix[idx_spot]
        else:
            pfy0_norm[idx_spot] = pfy0[idx_spot] # copy for now

        x = corrected_mono[idx_spot]
        if 1 in interps:
            f1 = interpolate.interp1d(x, pfy0[idx_spot], kind='linear')
            pfy1[idx_spot] = f1(valid_nominal_mono)
            if norm is True:
                f1_norm = interpolate.interp1d(x, pfy0_norm[idx_spot], kind='linear')
                pfy1_norm[idx_spot] = f1_norm(valid_nominal_mono)
        if 2 in interps: # actually third order, not second
            f2 = interpolate.interp1d(x, pfy0[idx_spot], kind='cubic')
            pfy2[idx_spot] = f2(valid_nominal_mono)
            if norm is True:
                f2_norm = interpolate.interp1d(x, pfy0_norm[idx_spot], kind='cubic')
                pfy2_norm[idx_spot] = f2_norm(valid_nominal_mono)

    pfy_dict = {}
    pfy_dict['interps'] = interps # todo: will be used to save and load valid params only
    pfy_dict['dir_p'] = dir_p
    pfy_dict['nominal_mono'] = nominal_mono
    pfy_dict['valid_nominal_mono'] = valid_nominal_mono
    pfy_dict['pfy0'] = pfy0
    pfy_dict['pfy0_norm'] = pfy0_norm
    pfy_dict['pfy1'] = pfy1
    pfy_dict['pfy1_norm'] = pfy1_norm
    pfy_dict['pfy2'] = pfy2
    pfy_dict['pfy2_norm'] = pfy2_norm
    # pfy_dict['bin_centers'] = coadded_bin_centers
    pfy_dict['scan_numbers'] = scan_numbers
    pfy_dict['corrected_mono'] = corrected_mono
    pfy_dict['roi'] = [bin_start, bin_end]

    return pfy_dict


def correct_mono(htxs_class, scan_numbers, ref_energy, ref_data, plot_mono_correction=True):
    """
    To-do: add checker for REF_peak_posi if it's an outlier
    """
    REF_peak_posi = np.zeros((len(scan_numbers),1), dtype=np.float32)
    corrected_mono = np.zeros((len(scan_numbers),len(nominal_mono)), dtype=np.float32)
    print("mono is to be corrected using ref_energy = %s"%ref_energy)

    rainbow_colors = [cm.rainbow_r(x) for x in np.linspace(0,1.,len(scan_numbers))]

    if plot_mono_correction:
        fig1 = plt.figure()
        ax1 = fig1.gca()
        fig2 = plt.figure()
        ax2 = fig2.gca()

    for i, scan_number in enumerate(scan_numbers):
        try: # fitting part need improvement
            popt, pcov, perr = fit_gaussian(sigma=0.4, tailed=False, plot_it=plot_mono_correction, xd=htxs_class.get_scan_data(scan_number,'ENERGY_ENC'),
                             yd=htxs_class.get_scan_data(scan_number,ref_data), range=[htxs_class.get_scan_data(scan_number,'ENERGY_ENC')[np.argmax(htxs_class.get_scan_data(scan_number,ref_data))]-0.7, htxs_class.get_scan_data(scan_number,'ENERGY_ENC')[np.argmax(htxs_class.get_scan_data(scan_number,ref_data))]+0.7])
            REF_peak_posi[i] = popt[1]
        except RuntimeError:
            print("Failed to find gaussian fit to scan number %s"%scan_number)
            REF_peak_posi[i] = htxs_class.get_scan_data(scan_number,'ENERGY_ENC')[np.argmax(htxs_class.get_scan_data(scan_number,ref_data))]
            if plot_mono_correction:
                plt.sj_vlines(REF_peak_posi[i])
                plt.title('Scan #%s fitting failed, use simple maximum'%scan_number)
        if plot_mono_correction:
            ax1.plot(htxs_class.get_scan_data(scan_number,'ENERGY_ENC'), htxs_class.get_scan_data(scan_number,ref_data), color=rainbow_colors[i]);ax1.set_title(ref_data);plt.draw()
            ax2.plot(htxs_class.get_scan_data(scan_number,'ENERGY_ENC'), htxs_class.get_scan_data(scan_number,ref_data)/np.asfarray(htxs_class.get_i0(scan_number)), color=rainbow_colors[i]);ax2.set_title('Normalized REF');plt.draw()
        corrected_mono[i] = htxs_class.get_scan_data(scan_number,'ENERGY_ENC') - REF_peak_posi[i] + ref_energy

    return corrected_mono


def plot_rixs(rixs_dict, spot_ids=None, bad_spot_ids=None, bad_scan_ids=None, neg_int_ids=None, sum_over_spot=True, roi=None, interp=0, norm=True, plot_only=True, colorbar=False, vmin=None, vmax=None, use_imshow=False, cmap=None, guide=False, ylim=None, rebin=None, smoothing=1, subtract_mono=False, mono_offset=0, mono_scaling=1.0, select_corners=None, plot_bad_corrected=False, swap_xy=False, shading='flat'):
    """
    interp=0 & norm=False & subtract_mono=False is the default
    spot_ids should be a list that begins from 0 or an integer
    plot_only=True is supposed to return the 2d map, but not working yet.
    select_corners=None uses the entire array. 'all' will show all the corners. 'center', 'top', 'bl', 'tr', etc.
    subtract_mono=True now uses mono_offset and mono_scaling
    a list of rixs_dict or a dict of rixs_dict can be provided. e.g. plot_rixs([rixs1, rixs2]) or plot_rixs(rixs_dict_dict)
    plot_bad_corrected=True makes one more plot that simply corrects for lost traces flagged bad
    swap_xy=True will make emission the x-axis. The default is False. Currently works only with sum_over_spot=True & interp=0 & norm=True
    neg_int_ids=None for regular RIXS, a list (e.g., [1,3,5]) for XMCD
    """

    if type(rixs_dict) is list: # a list of rixs dictionaries. Only available for plane0's. Seems not ready yet
        print('# A list of dictionary is given')
        print("Multiple scans are temporarily merged and plotted")
        rixs_dict = merge_multiple_scans(rixs_dict)
    else:
        str_key_list = [key for key in rixs_dict.keys() if (type(key) is str)]  # added to ignore alias integer keys
        if ('201' in str_key_list[0]) or ('202' in str_key_list[0]):
            if len(rixs_dict.keys())>1:
                print('# A dictionary of dictionary is given')
                print("Multiple scans are temporarily merged and plotted")
            rixs_dict = merge_multiple_scans(rixs_dict)

    bin_centers = rixs_dict['bin_centers']
    bin_width = bin_centers[1]-bin_centers[0]
    bin_edges = np.arange(bin_centers[0]-bin_width/2, bin_centers[-1]+bin_width, bin_width)  # the last point shows unexpected behavior, so changed to +bin_width
    if isinstance(rixs_dict['dir_p'], str):
        dir_p = rixs_dict['dir_p']
    elif isinstance(rixs_dict['dir_p'], list):
        dir_p = rixs_dict['dir_p'][0] + '-' + rixs_dict['dir_p'][-1]
    # bin_start = bin_centers[0]+bin_width/2
    # bin_end = bin_centers[-1]+bin_width/2

    scan_numbers = rixs_dict['scan_numbers']
    sign_list = [1]*len(scan_numbers)

    if neg_int_ids is not None:
        if cmap is None:
            cmap = 'bwr'
        for neg_int_id in neg_int_ids:
            sign_list[neg_int_id] = -1

    if spot_ids is None:
        spot_ids = list(range(np.shape(rixs_dict['rixs_plane0'])[0]))
    elif isinstance(spot_ids, int):
        spot_ids = [spot_ids]
        sum_over_spot = False

    assert min(spot_ids) >= 0  # spot_ids should begin from 0

    if bad_spot_ids is not None:
        if isinstance(bad_spot_ids, int):
            bad_spot_ids = [bad_spot_ids]
        bad_scan_ids = [scan_numbers[spot_ids.index(bad_spot_id)] for bad_spot_id in bad_spot_ids]
        for bad_spot_id in bad_spot_ids:
            bad_spot_index = spot_ids.index(bad_spot_id)
            spot_ids.remove(bad_spot_id)
            sign_list.pop(bad_spot_index)
        assert len(spot_ids)==len(sign_list)
    else:
        if bad_scan_ids is not None:
            if isinstance(bad_scan_ids, int):
                bad_scan_ids = [bad_scan_ids]
            bad_spot_ids = [spot_ids[scan_numbers.index(bad_scan_id)] for bad_scan_id in bad_scan_ids]
            for bad_spot_id in bad_spot_ids:
                bad_spot_index = spot_ids.index(bad_spot_id)
                spot_ids.remove(bad_spot_id)
                sign_list.pop(bad_spot_index)
            assert len(spot_ids)==len(sign_list)

    # if (roi is None) and (not subtract_mono):
    #     # roi = [bin_centers[0], bin_centers[-1]]
    #     roi = [bin_centers[0], bin_centers[-1]] # this needs to be changed, and also second argument in all pcolormesh's below or use ylim? => plt.ylim seems to work

    if cmap is None:
        cmap = nipy_spectral_r_white
        # cmap = cm.CMRmap_r # this is another okay one

    fontsize = 'x-small'

    if select_corners is not None:
        interp = 0
        norm = True
        sum_over_spot = True
        subtract_mono = False
        if type(select_corners) is str:
            if select_corners == 'all':
                select_corners = list(PIXEL_GROUP_DICT[101])
            elif select_corners in list(PIXEL_GROUP_DICT[101]):
                select_corners = [select_corners]

    if 'mono_scaling' in rixs_dict.keys():
        eloss_map = True
    else:
        eloss_map = False

    if interp == 0:
        if sum(sum(sum(rixs_dict['rixs_plane0']))) == 0:
            print('A meaningful RIXS map does not exist with interp = %d. Try a different interp value.'%interp)
            return
        x = np.append(rixs_dict['nominal_mono'], 2*rixs_dict['nominal_mono'][-1]-rixs_dict['nominal_mono'][-2])  # forgot why. probably not to miss the last bin
        # x = rixs_dict['nominal_mono']  # changed 20210904
        if norm is True:
            if sum_over_spot is True:
                if select_corners is None:
                    z = np.zeros_like(rixs_dict['rixs_plane0_norm'][0], dtype=np.float32)
                    if 'rixs_plane0_norm2' in rixs_dict.keys():
                        z2 = np.zeros_like(rixs_dict['rixs_plane0_norm2'][0], dtype=np.float32)
                    for ii, spot_id in enumerate(spot_ids):
                        z += rixs_dict['rixs_plane0_norm'][spot_id] * sign_list[ii]
                        if 'rixs_plane0_norm2' in rixs_dict.keys():
                            z2 += rixs_dict['rixs_plane0_norm2'][spot_id] * sign_list[ii]
                else:
                    z = {}
                    for select_corner in select_corners:
                        z[select_corner] = np.zeros_like(rixs_dict['rixs_plane0_norm'][0], dtype=np.float32)
                        for spot_id in spot_ids:
                            z[select_corner] += rixs_dict['rixs_plane0_norm_part_arr'][select_corner][spot_id]
                if subtract_mono: # need to fix
                    for i in range(np.shape(z)[0]):
                        z[i] = np.roll(z[i], shift=-int(round((x[i]-x[0])/bin_width*mono_scaling, ndigits=0)))
                    if roi is None:
                        plt.figure()
                        if swap_xy is False: # emission is y-axis (the default)
                            if shading in ['nearest', 'gouraud']:
                                plt.pcolormesh(x[:-1]*mono_scaling-mono_offset, x[0]-bin_edges[:-1]-mono_offset, np.transpose(z), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                            else:
                                plt.pcolormesh(x*mono_scaling-mono_offset, x[0]-bin_edges-mono_offset, np.transpose(z), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                        else:
                            if shading in ['nearest', 'gouraud']:
                                plt.pcolormesh(x[0]-bin_edges[:-1]-mono_offset, x[:-1]*mono_scaling-mono_offset, z, cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                            else:
                                plt.pcolormesh(x[0]-bin_edges-mono_offset, x*mono_scaling-mono_offset, z, cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                    else: # not used. not updated
                        # plt.pcolormesh(rixs_dict['nominal_mono'], rixs_dict['nominal_mono'][0]-bin_centers[int((roi[0])/bin_width):int((roi[1])/bin_width)], np.transpose(z[:,int((roi[0])/bin_width):int((roi[1])/bin_width)]), cmap=cmap)
                        plt.figure()
                        if shading in ['nearest', 'gouraud']:
                            plt.pcolormesh(x[:-1]*mono_scaling-mono_offset, x[0]-bin_centers[int((roi[0]-x[-1]+x[0])/bin_width):int((roi[1])/bin_width)], np.transpose(z[:,int((roi[0]-rixs_dict['nominal_mono'][-1]+rixs_dict['nominal_mono'][0])/bin_width):int((roi[1])/bin_width)+1]), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                        else:
                            plt.pcolormesh(x*mono_scaling-mono_offset, x[0]-bin_centers[int((roi[0]-x[-1]+x[0])/bin_width):int((roi[1])/bin_width)], np.transpose(z[:,int((roi[0]-rixs_dict['nominal_mono'][-1]+rixs_dict['nominal_mono'][0])/bin_width):int((roi[1])/bin_width)]), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                        # plt.pcolormesh(rixs_dict['nominal_mono'], rixs_dict['nominal_mono'][0]-bin_centers[int((roi[0])/bin_width):int((roi[1])/bin_width)], np.transpose(z[:,int((bin_centers[-1]-roi[1])/bin_width):int((bin_centers[-1]-roi[0])/bin_width)]), cmap=cmap)
                    if swap_xy is False: # emission is y-axis (the default)
                        plt.ylabel('Energy transfer (eV)')
                    # plt.pcolormesh(rixs_dict['nominal_mono'], rixs_dict['nominal_mono'][0]-bin_centers, np.transpose(z), cmap=cmap)
                    # plt.ylim()
                else:
                    if select_corners is None:
                        fig1, ax1 = plt.subplots()
                        # ax1.pcolormesh(rixs_dict['nominal_mono'], bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.transpose(z[:,int(roi[0]/bin_width):int(roi[1]/bin_width)]), cmap=cmap)
                        if swap_xy is False: # emission is y-axis (the default)
                            if shading in ['nearest', 'gouraud']:
                                im1 = ax1.pcolormesh(x[:-1]*mono_scaling-mono_offset, bin_edges[:-1], np.transpose(z), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                            else:
                                im1 = ax1.pcolormesh(x*mono_scaling-mono_offset, bin_edges, np.transpose(z), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                        else:
                            if shading in ['nearest', 'gouraud']:
                                im1 = ax1.pcolormesh(bin_edges[:-1], x[:-1]*mono_scaling-mono_offset, z, cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                            else:
                                im1 = ax1.pcolormesh(bin_edges, x*mono_scaling-mono_offset, z, cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                        if guide is True:
                            ax1.plot(x*mono_scaling-mono_offset, x*mono_scaling-mono_offset, 'k--')
                        if colorbar is True:
                            fig1.colorbar(im1, ax=ax1)
                        # ax1.set_ylim(roi[0],roi[1])
                        if ylim is not None:
                            ax1.set_ylim(ylim)
                        if swap_xy is False: # emission is y-axis (the default)
                            if eloss_map is True:
                                ax1.set_ylabel('Energy transfer (eV)')
                            else:
                                ax1.set_ylabel('Emission energy (eV)')
                        else:
                            ax1.set_ylabel('Scanned motor values')
                        if ('rixs_plane0_norm2' in rixs_dict.keys()) and (plot_bad_corrected is True):
                            fig2, ax2 = plt.subplots()
                            # ax2.pcolormesh(rixs_dict['nominal_mono'], bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.transpose(z2[:,int(roi[0]/bin_width):int(roi[1]/bin_width)]), cmap=cmap)
                            if swap_xy is False: # emission is y-axis (the default)
                                if shading in ['nearest', 'gouraud']:
                                    im2 = ax2.pcolormesh(x[:-1]*mono_scaling-mono_offset, bin_edges[:-1], np.transpose(z2), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                                else:
                                    im2 = ax2.pcolormesh(x*mono_scaling-mono_offset, bin_edges, np.transpose(z2), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                            else:
                                if shading in ['nearest', 'gouraud']:
                                    im2 = ax2.pcolormesh(bin_edges[:-1], x[:-1]*mono_scaling-mono_offset, z2, cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                                else:
                                    im2 = ax2.pcolormesh(bin_edges, x*mono_scaling-mono_offset, z2, cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                            if guide is True:
                                ax2.plot(x*mono_scaling-mono_offset, x*mono_scaling-mono_offset, 'k--')
                            if colorbar is True:
                                fig2.colorbar(im2, ax=ax2)
                            # ax2.set_ylim(roi[0],roi[1])
                            if ylim is not None:
                                ax2.set_ylim(ylim)
                            if eloss_map is True:
                                ax2.set_ylabel('Energy transfer (eV)')
                            else:
                                ax2.set_ylabel('Emission energy (eV)')
                    else:
                        for select_corner in select_corners:
                            plt.figure()
                            # plt.pcolormesh(rixs_dict['nominal_mono'], bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.transpose(z[select_corner][:,int(roi[0]/bin_width):int(roi[1]/bin_width)]), cmap=cmap)
                            if shading in ['nearest', 'gouraud']:
                                plt.pcolormesh(x[:-1]*mono_scaling-mono_offset, bin_edges[:-1], np.transpose(z[select_corner]), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                            else:
                                plt.pcolormesh(x*mono_scaling-mono_offset, bin_edges, np.transpose(z[select_corner]), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                            if guide is True:
                                plt.plot(x*mono_scaling-mono_offset, x*mono_scaling-mono_offset, 'k--')
                            # plt.ylim(roi[0],roi[1])
                            if ylim is not None:
                                plt.ylim(ylim)
                            if eloss_map is True:
                                plt.ylabel('Energy transfer (eV)')
                            else:
                                plt.ylabel('Emission energy (eV)')
                            # plt.xlim(rixs_dict['nominal_mono'][0], rixs_dict['nominal_mono'][-1])
                            plt.xlabel('Scanned motor values')
                            if ('sample_name' in rixs_dict.keys()):
                                if rixs_dict['sample_name'] != '':
                                    plt.title('%s, %s, RIXS, normalized, mono not corrected, summed over spots\nbad scan: %s, %s corner of the array'%(dir_p,rixs_dict['sample_name'],bad_scan_ids,elect_corner),fontsize=fontsize)
                            else:
                                plt.title('%s, RIXS, normalized, mono not corrected, summed over spots\nbad scan: %s, %s corner of the array'%(dir_p,bad_scan_ids,elect_corner),fontsize=fontsize)
                if (select_corners is None) and (subtract_mono is not True):
                    # ax1.set_xlim(rixs_dict['nominal_mono'][0], rixs_dict['nominal_mono'][-1])
                    if swap_xy is False: # emission is y-axis (the default)
                        ax1.set_xlabel('Scanned motor values')
                    else:
                        if eloss_map is True:
                            ax1.set_xlabel('Energy transfer (eV)')
                        else:
                            ax1.set_xlabel('Emission energy (eV)')
                    if ('sample_name' in rixs_dict.keys()):
                        if rixs_dict['sample_name'] != '':
                            ax1.set_title('%s, %s, RIXS, normalized, mono not corrected\nsummed over spots, bad scan: %s'%(dir_p,rixs_dict['sample_name'],bad_scan_ids),fontsize=fontsize)
                    else:
                        ax1.set_title('%s, RIXS, normalized, mono not corrected\nsummed over spots, bad scan: %s'%(dir_p,bad_scan_ids),fontsize=fontsize)
                    if ('rixs_plane0_norm2' in rixs_dict.keys()) and (plot_bad_corrected is True):
                        # ax2.set_xlim(rixs_dict['nominal_mono'][0], rixs_dict['nominal_mono'][-1])
                        ax2.set_xlabel('Scanned motor values')
                        if ('sample_name' in rixs_dict.keys()):
                            if rixs_dict['sample_name'] != '':
                                ax2.set_title('%s, %s, RIXS, i0 & good_pulse_ratio normalized, mono not corrected\nsummed over spots, bad scan: %s'%(dir_p,rixs_dict['sample_name'],bad_scan_ids),fontsize=fontsize)
                        else:
                            ax2.set_title('%s, RIXS, i0 & good_pulse_ratio normalized, mono not corrected\nsummed over spots, bad scan: %s'%(dir_p,bad_scan_ids),fontsize=fontsize)
            else: # sum_over_spot is False
                if subtract_mono:
                    print('# this feature is available only for interp=0, sum_over_spot=True')
                    return
                else:
                    for spot_id in spot_ids:
                        plt.figure()
                        # plt.pcolormesh(rixs_dict['nominal_mono'], bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.transpose(rixs_dict['rixs_plane0_norm'][spot_id,:,int(roi[0]/bin_width):int(roi[1]/bin_width)]), cmap=cmap)
                        if shading in ['nearest', 'gouraud']:
                            plt.pcolormesh(x[:-1], bin_edges[:-1], np.transpose(rixs_dict['rixs_plane0_norm'][spot_id,:,:]), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                        else:
                            plt.pcolormesh(x, bin_edges, np.transpose(rixs_dict['rixs_plane0_norm'][spot_id,:,:]), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                        if guide is True:
                            plt.plot(x, x, 'k--')
                        # plt.xlim(rixs_dict['nominal_mono'][0], rixs_dict['nominal_mono'][-1])
                        plt.xlabel('Scanned motor values')
                        # plt.ylim(roi[0],roi[1])
                        if ylim is not None:
                            plt.ylim(ylim)
                        if ('sample_name' in rixs_dict.keys()):
                            if rixs_dict['sample_name'] != '':
                                plt.title('%s, %s, RIXS, normalized, mono not corrected\nspot/scan #%s'%(dir_p,rixs_dict['sample_name'],spot_id),fontsize=fontsize)
                        else:
                            plt.title('%s, RIXS, normalized, mono not corrected\nspot/scan #%s'%(dir_p,spot_id),fontsize=fontsize)
                        if ('rixs_plane0_norm2' in rixs_dict.keys()) and (plot_bad_corrected is True):
                            plt.figure()
                            # plt.pcolormesh(rixs_dict['nominal_mono'], bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.transpose(rixs_dict['rixs_plane0_norm2'][spot_id,:,int(roi[0]/bin_width):int(roi[1]/bin_width)]), cmap=cmap)
                            if shading in ['nearest', 'gouraud']:
                                plt.pcolormesh(x[:-1], bin_edges[:-1], np.transpose(rixs_dict['rixs_plane0_norm2'][spot_id,:,:]), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                            else:
                                plt.pcolormesh(x, bin_edges, np.transpose(rixs_dict['rixs_plane0_norm2'][spot_id,:,:]), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                            if guide is True:
                                plt.plot(x, x, 'k--')
                            # plt.xlim(rixs_dict['nominal_mono'][0], rixs_dict['nominal_mono'][-1])
                            plt.xlabel('Scanned motor values')
                            # plt.ylim(roi[0],roi[1])
                            if ylim is not None:
                                plt.ylim(ylim)
                            if ('sample_name' in rixs_dict.keys()):
                                if rixs_dict['sample_name'] != '':
                                    plt.title('%s, %s, RIXS, i0 & good_pulse_ratio normalized, mono not corrected\nspot/scan #%s'%(dir_p,rixs_dict['sample_name'],spot_id),fontsize=fontsize)
                            else:
                                plt.title('%s, RIXS, i0 & good_pulse_ratio normalized, mono not corrected\nspot/scan #%s'%(dir_p,spot_id),fontsize=fontsize)
        else: # not normalized
            if sum_over_spot is True:
                plt.figure()
                z = np.zeros_like(rixs_dict['rixs_plane0'][0], dtype=np.float32)
                for spot_id in spot_ids:
                    if select_corners is None:
                        z += rixs_dict['rixs_plane0'][spot_id]
                    # elif select_corners == 'all': # 'all' not ready yet
                    #     for key in PIXEL_GROUP_DICT.keys():
                    #         z += rixs_dict['rixs_plane0_part_arr'][key][spot_id]
                    else:
                        z += rixs_dict['rixs_plane0_part_arr'][select_corners][spot_id]
                if subtract_mono:
                    for i in range(np.shape(z)[0]):
                        z[i] = np.roll(z[i], -int((x[i]-x[0])/bin_width))
                    if roi is None:
                        if shading in ['nearest', 'gouraud']:
                            plt.pcolormesh(x[:-1], x[0]-bin_edges[:-1], np.transpose(z), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                        else:
                            plt.pcolormesh(x, x[0]-bin_edges, np.transpose(z), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                        if ylim is not None:
                            plt.ylim(ylim)
                    else: # below not executed
                        # plt.pcolormesh(rixs_dict['nominal_mono'], rixs_dict['nominal_mono'][0]-bin_centers[int((roi[0])/bin_width):int((roi[1])/bin_width)], np.transpose(z[:,int((roi[0])/bin_width):int((roi[1])/bin_width)]), cmap=cmap)
                        if shading in ['nearest', 'gouraud']:
                            plt.pcolormesh(x[:-1], x[0]-bin_centers[int((roi[0]-x[-1]+x[0])/bin_width):int((roi[1])/bin_width)], np.transpose(z[:,int((roi[0]-rixs_dict['nominal_mono'][-1]+rixs_dict['nominal_mono'][0])/bin_width):int((roi[1])/bin_width)+1]), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                        else:
                            plt.pcolormesh(x, x[0]-bin_centers[int((roi[0]-x[-1]+x[0])/bin_width):int((roi[1])/bin_width)], np.transpose(z[:,int((roi[0]-rixs_dict['nominal_mono'][-1]+rixs_dict['nominal_mono'][0])/bin_width):int((roi[1])/bin_width)]), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                        plt.figure()
                        z = np.transpose(z[:,int((roi[0]-rixs_dict['nominal_mono'][-1]+rixs_dict['nominal_mono'][0])/bin_width):int((roi[1])/bin_width)])
                        if shading in ['nearest', 'gouraud']:
                            plt.pcolormesh(x[:-1], x[0]-bin_edges[int((roi[0]-rixs_dict['nominal_mono'][-1]+rixs_dict['nominal_mono'][0])/bin_width):int((roi[1])/bin_width)+1], z, cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                        else:
                            plt.pcolormesh(x, x[0]-bin_edges[int((roi[0]-rixs_dict['nominal_mono'][-1]+rixs_dict['nominal_mono'][0])/bin_width):int((roi[1])/bin_width)], z, cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                        # plt.pcolormesh(rixs_dict['nominal_mono'], rixs_dict['nominal_mono'][0]-bin_centers[int((roi[0])/bin_width):int((roi[1])/bin_width)], np.transpose(z[:,int((bin_centers[-1]-roi[1])/bin_width):int((bin_centers[-1]-roi[0])/bin_width)]), cmap=cmap)

                        # 1) using contourf (additional plot)
                        plt.figure()
                        X = rixs_dict['nominal_mono']
                        Y = bin_centers[int((roi[0]-rixs_dict['nominal_mono'][-1]+rixs_dict['nominal_mono'][0])/bin_width):int((roi[1])/bin_width)]
                        Z = z
                        # X,Y = np.meshgrid(X,Y)
                        # plt.contourf(X,rixs_dict['nominal_mono'][0]-Y,Z)#,cmap=cm.nipy_spectral_r)

                        # 2) using imshow
                        f = interpolate.interp2d(X, Y, Z, kind='linear')
                        X_uniform = np.arange(X[0],X[-1],0.1)
                        X_uniform = np.append(X_uniform,X[-1])
                        Z_new = f(X_uniform, Y)
                        plt.imshow(Z_new, interpolation='bicubic', aspect='auto', cmap=cmap, origin='lower')

                    plt.ylabel('Energy transfer (eV)')
                    # plt.pcolormesh(rixs_dict['nominal_mono'], rixs_dict['nominal_mono'][0]-bin_centers, np.transpose(z), cmap=cmap)
                    # plt.ylim()
                else:
                    # plt.pcolormesh(rixs_dict['nominal_mono'], bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.transpose(z[:,int(roi[0]/bin_width):int(roi[1]/bin_width)]), cmap=cmap)
                    if shading in ['nearest', 'gouraud']:
                        plt.pcolormesh(x[:-1], bin_edges[:-1], np.transpose(z), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                    else:
                        plt.pcolormesh(x, bin_edges, np.transpose(z), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                    if guide is True:
                        plt.plot(x, x, 'k--')
                    # plt.ylim(roi[0],roi[1])
                    if ylim is not None:
                        plt.ylim(ylim)
                    if eloss_maps is True:
                        plt.ylabel('Energy transfer (eV)')
                    else:
                        plt.ylabel('Emission energy (eV)')
                # plt.xlim(rixs_dict['nominal_mono'][0], rixs_dict['nominal_mono'][-1])
                plt.xlabel('Scanned motor values')
                plt.title('%s, RIXS, mono not corrected, summed over spots\nbad scan: %s'%(dir_p,bad_scan_ids),fontsize=fontsize)
            else:
                if subtract_mono:
                    print('# this feature is available only for interp=0, sum_over_spot=True')
                    return
                else:
                    for spot_id in spot_ids:
                        plt.figure()
                        # plt.pcolormesh(rixs_dict['nominal_mono'], bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.transpose(rixs_dict['rixs_plane0'][spot_id,:,int(roi[0]/bin_width):int(roi[1]/bin_width)]), cmap=cmap)
                        if shading in ['nearest', 'gouraud']:
                            plt.pcolormesh(x[:-1], bin_edges[:-1], np.transpose(rixs_dict['rixs_plane0'][spot_id,:,:]), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                        else:
                            plt.pcolormesh(x, bin_edges, np.transpose(rixs_dict['rixs_plane0'][spot_id,:,:]), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                        if guide is True:
                            plt.plot(x, x, 'k--')
                        # plt.xlim(rixs_dict['nominal_mono'][0], rixs_dict['nominal_mono'][-1])
                        plt.xlabel('Scanned motor values')
                        # plt.ylim(roi[0],roi[1])
                        if ylim is not None:
                            plt.ylim(ylim)
                        plt.title('%s, RIXS, mono not corrected\nspot/scan #%s'%(dir_p,spot_id),fontsize=fontsize)
    elif interp == 1:
        if ('rixs_plane1' not in rixs_dict.keys()) or sum(sum(sum(rixs_dict['rixs_plane1']))) == 0:
            print('A meaningful RIXS map does not exist with interp = %d. Try a different interp value.'%interp)
            return
        x = np.append(rixs_dict['valid_nominal_mono'], 2*rixs_dict['valid_nominal_mono'][-1]-rixs_dict['valid_nominal_mono'][-2])
        if norm is True:
            if sum_over_spot is True:
                plt.figure()
                z = np.zeros_like(rixs_dict['rixs_plane1_norm'][0], dtype=np.float32)
                for spot_id in spot_ids:
                    z += rixs_dict['rixs_plane1_norm'][spot_id]
                if subtract_mono:
                    print('# this feature is available only for interp=0, sum_over_spot=True')
                    return
                else:
                    # plt.pcolormesh(rixs_dict['valid_nominal_mono'], bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.transpose(z[:,int(roi[0]/bin_width):int(roi[1]/bin_width)]), cmap=cmap)
                    if shading in ['nearest', 'gouraud']:
                        plt.pcolormesh(x[:-1], bin_edges[:-1], np.transpose(z), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                    else:
                        plt.pcolormesh(x, bin_edges, np.transpose(z), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                    if guide is True:
                        plt.plot(rixs_dict['nominal_mono'], rixs_dict['nominal_mono'], 'k--')
                    # plt.xlim(rixs_dict['valid_nominal_mono'][0], rixs_dict['valid_nominal_mono'][-1])
                    plt.xlabel('Scanned motor values')
                    # plt.ylim(roi[0],roi[1])
                    if ylim is not None:
                        plt.ylim(ylim)
                    if ('sample_name' in rixs_dict.keys()):
                        if rixs_dict['sample_name'] != '':
                            plt.title('%s, %s, RIXS, normalized, mono corrected (lin)\nsummed over spots, bad scan: %s'%(dir_p,rixs_dict['sample_name'],bad_scan_ids),fontsize=fontsize)
                    else:
                        plt.title('%s, RIXS, normalized, mono corrected (lin)\nsummed over spots, bad scan: %s'%(dir_p,bad_scan_ids),fontsize=fontsize)
            else:
                if subtract_mono:
                    print('# this feature is available only for interp=0, sum_over_spot=True')
                    return
                else:
                    for spot_id in spot_ids:
                        plt.figure()
                        # plt.pcolormesh(rixs_dict['valid_nominal_mono'], bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.transpose(rixs_dict['rixs_plane1_norm'][spot_id,:,int(roi[0]/bin_width):int(roi[1]/bin_width)]), cmap=cmap)
                        if shading in ['nearest', 'gouraud']:
                            plt.pcolormesh(x[:-1], bin_edges[:-1], np.transpose(rixs_dict['rixs_plane1_norm'][spot_id,:,:]), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                        else:
                            plt.pcolormesh(x, bin_edges, np.transpose(rixs_dict['rixs_plane1_norm'][spot_id,:,:]), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                        if guide is True:
                            plt.plot(rixs_dict['nominal_mono'], rixs_dict['nominal_mono'], 'k--')
                        # plt.xlim(rixs_dict['valid_nominal_mono'][0], rixs_dict['valid_nominal_mono'][-1])
                        plt.xlabel('Scanned motor values')
                        # plt.ylim(roi[0],roi[1])
                        if ylim is not None:
                            plt.ylim(ylim)
                        if ('sample_name' in rixs_dict.keys()):
                            if rixs_dict['sample_name'] != '':
                                plt.title('%s, %s, RIXS, mono corrected (lin), normalized\nspot/scan #%s'%(dir_p,rixs_dict['sample_name'], spot_id),fontsize=fontsize)
                        else:
                            plt.title('%s, RIXS, mono corrected (lin), normalized\nspot/scan #%s'%(dir_p,spot_id),fontsize=fontsize)
        else:
            if sum_over_spot is True:
                plt.figure()
                z = np.zeros_like(rixs_dict['rixs_plane1'][0], dtype=np.float32)
                for spot_id in spot_ids:
                    z += rixs_dict['rixs_plane1'][spot_id]
                if subtract_mono:
                    print('# this feature is available only for interp=0, sum_over_spot=True')
                    return
                else:
                    # plt.pcolormesh(rixs_dict['valid_nominal_mono'], bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.transpose(z[:,int(roi[0]/bin_width):int(roi[1]/bin_width)]), cmap=cmap)
                    if shading in ['nearest', 'gouraud']:
                        plt.pcolormesh(x[:-1], bin_edges[:-1], np.transpose(z), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                    else:
                        plt.pcolormesh(x, bin_edges, np.transpose(z), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                    if guide is True:
                        plt.plot(rixs_dict['nominal_mono'], rixs_dict['nominal_mono'], 'k--')
                    # plt.xlim(rixs_dict['valid_nominal_mono'][0], rixs_dict['valid_nominal_mono'][-1])
                    plt.xlabel('Scanned motor values')
                    # plt.ylim(roi[0],roi[1])
                    if ylim is not None:
                        plt.ylim(ylim)
                    if ('sample_name' in rixs_dict.keys()):
                        if rixs_dict['sample_name'] != '':
                            plt.title('%s, %s, RIXS, mono corrected (lin)\nsummed over spots, bad scan: %s'%(dir_p,rixs_dict['sample_name'],bad_scan_ids),fontsize=fontsize)
                    else:
                        plt.title('%s, RIXS, mono corrected (lin)\nsummed over spots, bad scan: %s'%(dir_p,bad_scan_ids),fontsize=fontsize)
            else:
                if subtract_mono:
                    print('# this feature is available only for interp=0, sum_over_spot=True')
                    return
                else:
                    for spot_id in spot_ids:
                        plt.figure()
                        # plt.pcolormesh(rixs_dict['valid_nominal_mono'], bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.transpose(rixs_dict['rixs_plane1'][spot_id,:,int(roi[0]/bin_width):int(roi[1]/bin_width)]), cmap=cmap)
                        if shading in ['nearest', 'gouraud']:
                            plt.pcolormesh(x, bin_edges, np.transpose(rixs_dict['rixs_plane1'][spot_id,:,:]), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                        else:
                            plt.pcolormesh(x[:-1], bin_edges[:-1], np.transpose(rixs_dict['rixs_plane1'][spot_id,:,:]), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                        if guide is True:
                            plt.plot(rixs_dict['nominal_mono'], rixs_dict['nominal_mono'], 'k--')
                        # plt.xlim(rixs_dict['valid_nominal_mono'][0], rixs_dict['valid_nominal_mono'][-1])
                        plt.xlabel('Scanned motor values')
                        # plt.ylim(roi[0],roi[1])
                        if ylim is not None:
                            plt.ylim(ylim)
                        if ('sample_name' in rixs_dict.keys()):
                            if rixs_dict['sample_name'] != '':
                                plt.title('%s, %s, RIXS, mono corrected (lin)\nspot/scan #%s'%(dir_p,rixs_dict['sample_name'],spot_id),fontsize=fontsize)
                        else:
                            plt.title('%s, RIXS, mono corrected (lin)\nspot/scan #%s'%(dir_p,spot_id),fontsize=fontsize)
    elif interp == 2: # not much updated
        if ('rixs_plane2' not in rixs_dict.keys()) or sum(sum(sum(rixs_dict['rixs_plane2']))) == 0:
            print('A meaningful RIXS map does not exist with interp = %d. Try a different interp value.'%interp)
            return
        if norm is True:
            if sum_over_spot is True:
                plt.figure()
                z = np.zeros_like(rixs_dict['rixs_plane2_norm'][0], dtype=np.float32)
                for spot_id in spot_ids:
                    z += rixs_dict['rixs_plane2_norm'][spot_id]
                if subtract_mono:
                    print('# this feature is available only for interp=0, sum_over_spot=True')
                    return
                else:
                    # plt.pcolormesh(rixs_dict['valid_nominal_mono'], bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.transpose(z[:,int(roi[0]/bin_width):int(roi[1]/bin_width)]), cmap=cmap)
                    if shading in ['nearest', 'gouraud']:
                        plt.pcolormesh(rixs_dict['valid_nominal_mono'][:-1], bin_centers[:-1], np.transpose(z), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                    else:
                        plt.pcolormesh(rixs_dict['valid_nominal_mono'], bin_centers, np.transpose(z), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                    if guide is True:
                        plt.plot(rixs_dict['nominal_mono'], rixs_dict['nominal_mono'], 'k--')
                    plt.xlim(rixs_dict['valid_nominal_mono'][0], rixs_dict['valid_nominal_mono'][-1])
                    plt.xlabel('Scanned motor values')
                    # plt.ylim(roi[0],roi[1])
                    if ylim is not None:
                        plt.ylim(ylim)
                    plt.title('%s, RIXS, normalized, mono corrected (cube)\nsummed over spots, bad scan: %s'%(dir_p,bad_scan_ids),fontsize=fontsize)
            else:
                if subtract_mono:
                    print('# this feature is available only for interp=0, sum_over_spot=True')
                    return
                else:
                    for spot_id in spot_ids:
                        plt.figure()
                        # plt.pcolormesh(rixs_dict['valid_nominal_mono'], bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.transpose(rixs_dict['rixs_plane2_norm'][spot_id,:,int(roi[0]/bin_width):int(roi[1]/bin_width)]), cmap=cmap)
                        if shading in ['nearest', 'gouraud']:
                            plt.pcolormesh(rixs_dict['valid_nominal_mono'][:-1], bin_centers[:-1], np.transpose(rixs_dict['rixs_plane2_norm'][spot_id,:,:]), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                        else:
                            plt.pcolormesh(rixs_dict['valid_nominal_mono'], bin_centers, np.transpose(rixs_dict['rixs_plane2_norm'][spot_id,:,:]), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                        if guide is True:
                            plt.plot(rixs_dict['nominal_mono'], rixs_dict['nominal_mono'], 'k--')
                        plt.xlim(rixs_dict['valid_nominal_mono'][0], rixs_dict['valid_nominal_mono'][-1])
                        plt.xlabel('Scanned motor values')
                        # plt.ylim(roi[0],roi[1])
                        if ylim is not None:
                            plt.ylim(ylim)
                        plt.title('%s, RIXS, normalized, mono corrected (cube)\nspot/scan #%s'%(dir_p,spot_id),fontsize=fontsize)
        else:
            if sum_over_spot is True:
                plt.figure()
                z = np.zeros_like(rixs_dict['rixs_plane2'][0], dtype=np.float32)
                for spot_id in spot_ids:
                    z += rixs_dict['rixs_plane2'][spot_id]
                if subtract_mono:
                    print('# this feature is available only for interp=0, sum_over_spot=True')
                    return
                else:
                    # plt.pcolormesh(rixs_dict['valid_nominal_mono'], bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.transpose(z[:,int(roi[0]/bin_width):int(roi[1]/bin_width)]), cmap=cmap)
                    if shading in ['nearest', 'gouraud']:
                        plt.pcolormesh(rixs_dict['valid_nominal_mono'][:-1], bin_centers[:-1], np.transpose(z), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                    else:
                        plt.pcolormesh(rixs_dict['valid_nominal_mono'], bin_centers, np.transpose(z), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                    if guide is True:
                        plt.plot(rixs_dict['nominal_mono'], rixs_dict['nominal_mono'], 'k--')
                    plt.xlim(rixs_dict['valid_nominal_mono'][0], rixs_dict['valid_nominal_mono'][-1])
                    plt.xlabel('Scanned motor values')
                    # plt.ylim(roi[0],roi[1])
                    if ylim is not None:
                        plt.ylim(ylim)
                    plt.title('%s, RIXS, mono corrected (cube)\nsummed over spots, bad scan: %s'%(dir_p,bad_scan_ids),fontsize=fontsize)
            else:
                if subtract_mono:
                    print('# this feature is available only for interp=0, sum_over_spot=True')
                    return
                else:
                    for spot_id in spot_ids:
                        plt.figure()
                        # plt.pcolormesh(rixs_dict['valid_nominal_mono'], bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.transpose(rixs_dict['rixs_plane2'][spot_id,:,int(roi[0]/bin_width):int(roi[1]/bin_width)]), cmap=cmap)
                        if shading in ['nearest', 'gouraud']:
                            plt.pcolormesh(rixs_dict['valid_nominal_mono'][:-1], bin_centers[:-1], np.transpose(rixs_dict['rixs_plane2'][spot_id,:,:]), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                        else:
                            plt.pcolormesh(rixs_dict['valid_nominal_mono'], bin_centers, np.transpose(rixs_dict['rixs_plane2'][spot_id,:,:]), cmap=cmap, vmin=vmin, vmax=vmax, shading=shading)
                        if guide is True:
                            plt.plot(rixs_dict['nominal_mono'], rixs_dict['nominal_mono'], 'k--')
                        plt.xlim(rixs_dict['valid_nominal_mono'][0], rixs_dict['valid_nominal_mono'][-1])
                        plt.xlabel('Scanned motor values')
                        # plt.ylim(roi[0],roi[1])
                        if ylim is not None:
                            plt.ylim(ylim)
                        plt.title('%s, RIXS, mono corrected (cube)\nspot/scan #%s'%(dir_p,spot_id),fontsize=fontsize)


def plot_pfy(rixs_dict, spot_ids=None, bad_spot_ids=None, bad_scan_ids=None, sum_over_spot=True, roi=None, interp=0, norm=True, invert=False, offset=0., hold_on=False, return_data=False, return_only=False, rebin=None, smoothing=1, subtract_mono=False, norm_by_mono=False, select_corners=None, plot_bad_corrected=False):
    '''
    usage: useful_ftns.plot_pfy(rixs_dict, coadded_bin_centers, [690,730], interp=1, norm=True)
    spot_ids begin from 0
    available interp values: 0, 1, 2
    todo: scan number as an input should be added
    subtract_mono = False by default. subtract_mono=1.5 can be used
    interp=0 is default
    '''

    if return_only is True:
        return_data = True

    if len(rixs_dict.keys()) == 1:
        print('# A dictionary of dictionary is given')
        rixs_dict = rixs_dict[next(iter(rixs_dict))] # maybe not good to overwrite to itself

    if isinstance(rixs_dict['dir_p'], str):
        dir_p = rixs_dict['dir_p']
    elif isinstance(rixs_dict['dir_p'], list):
        dir_p = rixs_dict['dir_p'][0] + '-' + rixs_dict['dir_p'][-1]

    if 'pfy0' in rixs_dict.keys(): # this means the data is a pfy dictionary, not rixs

        plot_type = 'PFY'
        scan_numbers = rixs_dict['scan_numbers']
        try:
            roi = rixs_dict['roi']
        except KeyError:
            print("# 'roi' does not exist, so roi = [0, 0] is used")
            roi = [0, 0]

        if spot_ids is None:
            spot_ids = list(range(np.shape(rixs_dict['pfy0'])[0]))
        elif isinstance(spot_ids, int):
            spot_ids = [spot_ids]
            sum_over_spot = False

        assert min(spot_ids) >= 0 # spot_ids should begin from 0

        if bad_spot_ids is not None:
            if isinstance(bad_spot_ids, int):
                bad_spot_ids = [bad_spot_ids]
            bad_scan_ids = [scan_numbers[spot_ids.index(bad_spot_id)] for bad_spot_id in bad_spot_ids]
            for bad_spot_id in bad_spot_ids:
                spot_ids.remove(bad_spot_id)
        else:
            if bad_scan_ids is not None:
                if isinstance(bad_scan_ids, int):
                    bad_scan_ids = [bad_scan_ids]
                bad_spot_ids = [spot_ids[scan_numbers.index(bad_scan_id)] for bad_scan_id in bad_scan_ids]
                for bad_spot_id in bad_spot_ids:
                    spot_ids.remove(bad_spot_id)

        if invert is False:
            invert = 1
        elif invert is True:
            invert = -1

        # if hold_on is False:
        plt.figure()

        fontsize = 'x-small'

        if interp == 0:
            if norm is True:
                if sum_over_spot is True:
                    z = np.zeros_like(rixs_dict['pfy0_norm'][0], dtype=np.float32)
                    for spot_id in spot_ids:
                        z += rixs_dict['pfy0_norm'][spot_id]
                    plt.plot(rixs_dict['nominal_mono'], offset+invert*z, label='ROI = [%s, %s]'%(roi[0], roi[1]))
                    plt.legend(fontsize=fontsize); plt.title('Normalized %s, mono not corrected, summed over spots\nbad scan: %s'%(plot_type,bad_scan_ids), fontsize=fontsize)
                    if return_data is True:
                        return rixs_dict['nominal_mono'], z
                else:
                    for spot_id in spot_ids:
                        if (hold_on is False) and (spot_id != spot_ids[0]):
                            plt.figure()
                        plt.plot(rixs_dict['nominal_mono'], offset+invert*rixs_dict['pfy0_norm'][spot_id], label='ROI = [%s, %s], spot/scan #%s'%(roi[0], roi[1], spot_id))
                        plt.legend(fontsize=fontsize); plt.title('Normalized %s, mono not corrected\nbad scan: %s'%(plot_type,bad_scan_ids), fontsize=fontsize)
                    if return_data is True:
                        print('no data returned')
            else:
                if sum_over_spot is True:
                    z = np.zeros_like(rixs_dict['pfy0'][0], dtype=np.float32)
                    for spot_id in spot_ids:
                        z += rixs_dict['pfy0'][spot_id]
                    plt.plot(rixs_dict['nominal_mono'], offset+invert*z, label='ROI = [%s, %s]'%(roi[0], roi[1]))
                    plt.legend(fontsize=fontsize); plt.title('%s, mono not corrected, summed over spots\nbad scan: %s'%(plot_type,bad_scan_ids), fontsize=fontsize)
                    if return_data is True:
                        return rixs_dict['nominal_mono'], z
                else:
                    for spot_id in spot_ids:
                        if (hold_on is False) and (spot_id != spot_ids[0]):
                            plt.figure()
                        plt.plot(rixs_dict['nominal_mono'], offset+invert*rixs_dict['pfy0'][spot_id], label='ROI = [%s, %s], spot/scan #%s'%(roi[0], roi[1], spot_id))
                        plt.legend(fontsize=fontsize); plt.title('%s, mono not corrected\nbad scan: %s'%(plot_type,bad_scan_ids), fontsize=fontsize)
        elif interp == 1:
            if norm is True:
                if sum_over_spot is True:
                    z = np.zeros_like(rixs_dict['pfy1_norm'][0], dtype=np.float32)
                    for spot_id in spot_ids:
                        z += rixs_dict['pfy1_norm'][spot_id]
                    plt.plot(rixs_dict['valid_nominal_mono'], offset+invert*z, label='ROI = [%s, %s]'%(roi[0], roi[1]))
                    plt.legend(fontsize=fontsize); plt.title('Normalized %s, mono corrected (lin), summed over spots\nbad scan: %s'%(plot_type,bad_scan_ids), fontsize=fontsize)
                    if (return_data is True) or (return_only is True):
                        return rixs_dict['valid_nominal_mono'], z
                else:
                    for spot_id in spot_ids:
                        if (hold_on is False) and (spot_id != spot_ids[0]):
                            plt.figure()
                        plt.plot(rixs_dict['valid_nominal_mono'], offset+invert*rixs_dict['pfy1_norm'][spot_id], label='ROI = [%s, %s], spot/scan #%s'%(roi[0], roi[1], spot_id))
                        plt.legend(fontsize=fontsize); plt.title('Normalized %s, mono corrected (lin)\nbad scan: %s'%(plot_type,bad_scan_ids), fontsize=fontsize)
            else:
                if sum_over_spot is True:
                    z = np.zeros_like(rixs_dict['pfy1'][0], dtype=np.float32)
                    for spot_id in spot_ids:
                        z += rixs_dict['pfy1'][spot_id]
                    plt.plot(rixs_dict['valid_nominal_mono'], offset+invert*z, label='ROI = [%s, %s]'%(roi[0], roi[1]))
                    plt.legend(fontsize=fontsize); plt.title('%s, mono corrected (lin), summed over spots\nbad scan: %s'%(plot_type,bad_scan_ids), fontsize=fontsize)
                    if (return_data is True) or (return_only is True):
                        return rixs_dict['valid_nominal_mono'], z
                else:
                    for spot_id in spot_ids:
                        if (hold_on is False) and (spot_id != spot_ids[0]):
                            plt.figure()
                        plt.plot(rixs_dict['valid_nominal_mono'], offset+invert*rixs_dict['pfy1'][spot_id], label='ROI = [%s, %s], spot/scan #%s'%(roi[0], roi[1], spot_id))
                        plt.legend(fontsize=fontsize); plt.title('%s, mono corrected (lin)\nbad scan: %s'%(plot_type,bad_scan_ids), fontsize=fontsize)
        elif interp == 2:
            if norm is True:
                if sum_over_spot is True:
                    z = np.zeros_like(rixs_dict['pfy2_norm'][0], dtype=np.float32)
                    for spot_id in spot_ids:
                        z += rixs_dict['pfy2_norm'][spot_id]
                    plt.plot(rixs_dict['valid_nominal_mono'], offset+invert*z, label='ROI = [%s, %s]'%(roi[0], roi[1]))
                    plt.legend(fontsize=fontsize); plt.title('Normalized %s, mono corrected (cube), summed over spots\nbad scan: %s'%(plot_type,bad_scan_ids), fontsize=fontsize)
                    if (return_data is True) or (return_only is True):
                        return rixs_dict['valid_nominal_mono'], z
                else:
                    for spot_id in spot_ids:
                        if (hold_on is False) and (spot_id != spot_ids[0]):
                            plt.figure()
                        plt.plot(rixs_dict['valid_nominal_mono'], offset+invert*rixs_dict['pfy2_norm'][spot_id], label='ROI = [%s, %s], spot/scan #%s'%(roi[0], roi[1], spot_id))
                        plt.legend(fontsize=fontsize); plt.title('Normalized %s, mono corrected (cube)\nbad scan: %s'%(plot_type,bad_scan_ids), fontsize=fontsize)
            else:
                if sum_over_spot is True:
                    z = np.zeros_like(rixs_dict['pfy2'][0], dtype=np.float32)
                    for spot_id in spot_ids:
                        z += rixs_dict['pfy2'][spot_id]
                    plt.plot(rixs_dict['valid_nominal_mono'], offset+invert*z, label='ROI = [%s, %s]'%(roi[0], roi[1]))
                    plt.legend(fontsize=fontsize); plt.title('%s, mono corrected (cube), summed over spots\nbad scan: %s'%(plot_type,bad_scan_ids), fontsize=fontsize)
                    if (return_data is True) or (return_only is True):
                        return rixs_dict['valid_nominal_mono'], z
                else:
                    for spot_id in spot_ids:
                        if (hold_on is False) and (spot_id != spot_ids[0]):
                            plt.figure()
                        plt.plot(rixs_dict['valid_nominal_mono'], offset+invert*rixs_dict['pfy2'][spot_id], label='ROI = [%s, %s], spot/scan #%s'%(roi[0], roi[1], spot_id))
                        plt.legend(fontsize=fontsize); plt.title('%s, mono corrected (cube)\nbad scan: %s'%(plot_type,bad_scan_ids), fontsize=fontsize)
        return

    '''
    pfy generated from rixs_dict (not pfy_dict) begins
    need to reorganize
    '''
    bin_centers = rixs_dict['bin_centers']
    bin_width = bin_centers[1]-bin_centers[0]
    bin_end = bin_centers[-1]+bin_width/2

    scan_numbers = rixs_dict['scan_numbers']

    if spot_ids is None:
        # spot_ids = range(np.shape(rixs_dict['rixs_plane0'])[0])
        spot_ids = list(range(len(scan_numbers)))
    elif isinstance(spot_ids, int):
        spot_ids = [spot_ids]
        sum_over_spot = True # changed from False to make it work with single scan data return for append_to_spec_file

    rainbow_colors = [cm.rainbow_r(x) for x in np.linspace(0,1.,len(spot_ids))]
    solid_or_dash = ['-', '--']
    assert min(spot_ids) >= 0 # spot_ids should begin from 0

    if bad_spot_ids is not None:
        if isinstance(bad_spot_ids, int):
            bad_spot_ids = [bad_spot_ids]
        bad_scan_ids = [scan_numbers[spot_ids.index(bad_spot_id)] for bad_spot_id in bad_spot_ids]
        for bad_spot_id in bad_spot_ids:
            spot_ids.remove(bad_spot_id)
    else:
        if bad_scan_ids is not None:
            if isinstance(bad_scan_ids, int):
                bad_scan_ids = [bad_scan_ids]
            bad_spot_ids = [spot_ids[scan_numbers.index(bad_scan_id)] for bad_scan_id in bad_scan_ids]
            for bad_spot_id in bad_spot_ids:
                spot_ids.remove(bad_spot_id)
    # print len(spot_ids), spot_ids
    if (roi is None) and (not subtract_mono):
        plot_type = 'TFY'
        roi = bin_centers[0], bin_centers[-1]
    else:
        plot_type = 'PFY'

    if invert is False:
        invert = 1
    elif invert is True:
        invert = -1

    # if hold_on is False:

    fontsize = 'x-small'

    if select_corners is not None:
        interp = 0
        norm = True
        sum_over_spot = True
        subtract_mono = False
        if type(select_corners) is str:
            if select_corners == 'all':
                select_corners = list(PIXEL_GROUP_DICT[101])
            elif select_corners in list(PIXEL_GROUP_DICT[101]):
                select_corners = [select_corners]
        if len(select_corners) > 1:
            return_data = False # if more than one corners were selected, do not return

    if interp == 0:
        if sum(sum(sum(rixs_dict['rixs_plane0']))) == 0:
            print('A meaningful RIXS map does not exist with interp = %d. Try a different interp value.'%interp)
            return
        if norm is True:
            if sum_over_spot is True:
                # if hold_on is False:
                #     plt.figure()
                if select_corners is None:
                    z = np.zeros_like(rixs_dict['rixs_plane0_norm'][0], dtype=np.float32)
                    for spot_id in spot_ids:
                        z += rixs_dict['rixs_plane0_norm'][spot_id]
                    if 'rixs_plane0_norm2' in rixs_dict.keys():
                        z2 = np.zeros_like(rixs_dict['rixs_plane0_norm2'][0], dtype=np.float32)
                        for spot_id in spot_ids:
                            z2 += rixs_dict['rixs_plane0_norm2'][spot_id]
                else:
                    z = {}
                    for select_corner in select_corners:
                        z[select_corner] = np.zeros_like(rixs_dict['rixs_plane0_norm'][0], dtype=np.float32)
                        for spot_id in spot_ids:
                            z[select_corner] += rixs_dict['rixs_plane0_norm_part_arr'][select_corner][spot_id]
                if subtract_mono:
                    for i in range(np.shape(z)[0]):
                        z[i] = np.roll(z[i], -int((rixs_dict['nominal_mono'][i]-rixs_dict['nominal_mono'][0])/bin_width))
                    if roi is None:
                        print('# provide ROI!')
                        return
                    else:
                        if return_only is True:
                            return rixs_dict['nominal_mono'], np.sum(z[:,int((roi[0]-rixs_dict['nominal_mono'][-1]+rixs_dict['nominal_mono'][0])/bin_width):int((rixs_dict['nominal_mono'][0]-subtract_mono)/bin_width)], axis=1)
                        else:
                            plt.figure()
                            plt.plot(rixs_dict['nominal_mono'], np.sum(z[:,int((roi[0]-rixs_dict['nominal_mono'][-1]+rixs_dict['nominal_mono'][0])/bin_width):int((rixs_dict['nominal_mono'][0]-subtract_mono)/bin_width)], axis=1))
                            plt.legend(fontsize=fontsize); plt.title('%s, Normalized %s, mono not corrected, summed over spots\nbad scan: %s'%(dir_p,plot_type,bad_scan_ids), fontsize=fontsize)
                            if return_data is True:
                                return rixs_dict['nominal_mono'], np.sum(z[:,int((roi[0]-rixs_dict['nominal_mono'][-1]+rixs_dict['nominal_mono'][0])/bin_width):int((rixs_dict['nominal_mono'][0]-subtract_mono)/bin_width)], axis=1)
                else:
                    if select_corners is None:
                        if return_only is True:
                            if 'rixs_plane0_norm2' in rixs_dict.keys():
                                return rixs_dict['nominal_mono'], np.sum(z[:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1), np.sum(z2[:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1)
                            else:
                                return rixs_dict['nominal_mono'], np.sum(z[:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1)
                        else:
                            fig1, ax1 = plt.subplots()
                            ax1.plot(rixs_dict['nominal_mono'], offset+invert*np.sum(z[:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1), label='ROI = [%s, %s]'%(roi[0], roi[1]))
                            ax1.legend(fontsize=fontsize)
                            if ('sample_name' in rixs_dict.keys()):
                                if rixs_dict['sample_name'] != '':
                                    ax1.set_title('%s, %s, Normalized %s, mono not corrected, summed over spots\nbad scan: %s'%(dir_p,rixs_dict['sample_name'],plot_type,bad_scan_ids), fontsize=fontsize)
                            else:
                                ax1.set_title('%s, Normalized %s, mono not corrected, summed over spots\nbad scan: %s'%(dir_p,plot_type,bad_scan_ids), fontsize=fontsize)
                            if ('rixs_plane0_norm2' in rixs_dict.keys()) and (plot_bad_corrected is True):
                                fig2, ax2 = plt.subplots()
                                ax2.plot(rixs_dict['nominal_mono'], offset+invert*np.sum(z2[:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1), label='ROI = [%s, %s]'%(roi[0], roi[1]))
                                ax2.legend(fontsize=fontsize)
                                if ('sample_name' in rixs_dict.keys()):
                                    if rixs_dict['sample_name'] != '':
                                        ax2.set_title('%s, %s, i0 & good_pulse_ratio Normalized %s, mono not corrected, summed over spots\nbad scan: %s'%(dir_p,rixs_dict['sample_name'],plot_type,bad_scan_ids), fontsize=fontsize)
                                else:
                                    ax2.set_title('%s, i0 & good_pulse_ratio Normalized %s, mono not corrected, summed over spots\nbad scan: %s'%(dir_p,plot_type,bad_scan_ids), fontsize=fontsize)
                            if return_data is True:
                                if 'rixs_plane0_norm2' in rixs_dict.keys():
                                    return rixs_dict['nominal_mono'], np.sum(z[:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1), np.sum(z2[:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1)
                                else:
                                    return rixs_dict['nominal_mono'], np.sum(z[:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1)
                    else:
                        max_count = np.max([np.max(np.sum(z[select_corner][:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1)) for select_corner in select_corners])
                        for select_corner in select_corners:
                            plt.figure()
                            plt.plot(rixs_dict['nominal_mono'], offset+invert*np.sum(z[select_corner][:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1), label='ROI = [%s, %s]'%(roi[0], roi[1]))
                            plt.ylim(0, max_count)
                            plt.legend(fontsize=fontsize); plt.title('%s, Normalized %s, mono not corrected, summed over spots\nbad scan: %s, %s corner of the array'%(dir_p,plot_type,bad_scan_ids,select_corner), fontsize=fontsize)
                            if (len(select_corners) == 1) and (return_data is True): # this is True only when there is only one corner provided
                                return rixs_dict['nominal_mono'], np.sum(z[select_corner][:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1)
            else: # if sum_over_spot is not True
                if 'rixs_plane0_norm2' in rixs_dict.keys():
                    if return_only is False:  # show plots; can return or not return
                        if hold_on is True:
                            if plot_bad_corrected is True:
                                fig, ax = plt.subplots(2, 1, figsize=(6.4, 9))
                                fig.subplots_adjust(bottom=0.05, top=0.95)
                                for ii, spot_id in enumerate(spot_ids):
                                    ax[0].plot(rixs_dict['nominal_mono'], offset+invert*np.sum(rixs_dict['rixs_plane0_norm'][spot_id,:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=0), label='ROI = [%s, %s], spot/scan #%s'%(roi[0], roi[1], spot_id), color=rainbow_colors[ii], linestyle=solid_or_dash[ii%2])
                                    ax[1].plot(rixs_dict['nominal_mono'], offset+invert*np.sum(rixs_dict['rixs_plane0_norm2'][spot_id,:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=0), label='ROI = [%s, %s], spot/scan #%s'%(roi[0], roi[1], spot_id), color=rainbow_colors[ii], linestyle=solid_or_dash[ii%2])
                                ax[0].legend(fontsize=fontsize)
                                if ('sample_name' in rixs_dict.keys()):
                                    if rixs_dict['sample_name'] != '':
                                        ax[0].set_title('%s, %s, Normalized %s, mono not corrected\nbad scan: %s'%(dir_p,rixs_dict['sample_name'],plot_type,bad_scan_ids), fontsize=fontsize)
                                else:
                                    ax[0].set_title('%s, Normalized %s, mono not corrected\nbad scan: %s'%(dir_p,plot_type,bad_scan_ids), fontsize=fontsize)
                                ax[1].legend(fontsize=fontsize)
                                if ('sample_name' in rixs_dict.keys()):
                                    if rixs_dict['sample_name'] != '':
                                        ax[1].set_title('%s, %s, i0 & good_pulse_ratio Normalized %s, mono not corrected\nbad scan: %s'%(dir_p,rixs_dict['sample_name'],plot_type,bad_scan_ids), fontsize=fontsize)
                                else:
                                    ax[1].set_title('%s, i0 & good_pulse_ratio Normalized %s, mono not corrected\nbad scan: %s'%(dir_p,plot_type,bad_scan_ids), fontsize=fontsize)
                            else:
                                plt.figure()
                                for ii, spot_id in enumerate(spot_ids):
                                    plt.plot(rixs_dict['nominal_mono'], offset+invert*np.sum(rixs_dict['rixs_plane0_norm'][spot_id,:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=0), label='ROI = [%s, %s], spot/scan #%s'%(roi[0], roi[1], spot_id), color=rainbow_colors[ii], linestyle=solid_or_dash[ii%2])
                                plt.legend(fontsize=fontsize)
                                if ('sample_name' in rixs_dict.keys()):
                                    if rixs_dict['sample_name'] != '':
                                        plt.title('%s, %s, Normalized %s, mono not corrected\nbad scan: %s'%(dir_p,rixs_dict['sample_name'],plot_type,bad_scan_ids), fontsize=fontsize)
                                else:
                                    plt.title('%s, Normalized %s, mono not corrected\nbad scan: %s'%(dir_p,plot_type,bad_scan_ids), fontsize=fontsize)
                        else:
                            for ii, spot_id in enumerate(spot_ids):
                                plt.figure()
                                plt.plot(rixs_dict['nominal_mono'], offset+invert*np.sum(rixs_dict['rixs_plane0_norm'][spot_id,:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=0), label='ROI = [%s, %s], spot/scan #%s'%(roi[0], roi[1], spot_id), color=rainbow_colors[ii], linestyle=solid_or_dash[ii%2])
                                plt.legend()
                                if ('sample_name' in rixs_dict.keys()):
                                    if rixs_dict['sample_name'] != '':
                                        plt.title('%s, %s, Normalized %s, mono not corrected\nbad scan: %s'%(dir_p,rixs_dict['sample_name'],plot_type,bad_scan_ids), fontsize=fontsize)
                                else:
                                    plt.title('%s, Normalized %s, mono not corrected\nbad scan: %s'%(dir_p,plot_type,bad_scan_ids), fontsize=fontsize)
                                if plot_bad_corrected is True:
                                    plt.figure()
                                    plt.plot(rixs_dict['nominal_mono'], offset+invert*np.sum(rixs_dict['rixs_plane0_norm2'][spot_id,:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=0), label='ROI = [%s, %s], spot/scan #%s'%(roi[0], roi[1], spot_id), color=rainbow_colors[ii], linestyle=solid_or_dash[ii%2])
                                    plt.legend()
                                    if ('sample_name' in rixs_dict.keys()):
                                        if rixs_dict['sample_name'] != '':
                                            plt.title('%s, %s, i0 & good_pulse_ratio Normalized %s, mono not corrected\nbad scan: %s'%(dir_p,rixs_dict['sample_name'],plot_type,bad_scan_ids), fontsize=fontsize)
                                    else:
                                        plt.title('%s, i0 & good_pulse_ratio Normalized %s, mono not corrected\nbad scan: %s'%(dir_p,plot_type,bad_scan_ids), fontsize=fontsize)
                    if return_data is True:
                        return rixs_dict['nominal_mono'], [offset+invert*np.sum(rixs_dict['rixs_plane0_norm'][spot_id,:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=0) for spot_id in spot_ids], [offset+invert*np.sum(rixs_dict['rixs_plane0_norm2'][spot_id,:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=0) for spot_id in spot_ids]
                else: # if rixs_plane0_norm2 does not exist
                    if return_only is False:  # show plots; can return or not return
                        plt.figure()
                        for ii, spot_id in enumerate(spot_ids):
                            if (hold_on is False) and (spot_id != spot_ids[0]):
                                plt.figure()
                            plt.plot(rixs_dict['nominal_mono'], offset+invert*np.sum(rixs_dict['rixs_plane0_norm'][spot_id,:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=0), label='ROI = [%s, %s], spot/scan #%s'%(roi[0], roi[1], spot_id), color=rainbow_colors[ii], linestyle=solid_or_dash[ii%2])
                            plt.legend(fontsize=fontsize)
                            if ('sample_name' in rixs_dict.keys()):
                                if rixs_dict['sample_name'] != '':
                                    plt.title('%s, %s, Normalized %s, mono not corrected\nbad scan: %s'%(dir_p,rixs_dict['sample_name'],plot_type,bad_scan_ids), fontsize=fontsize)
                            else:
                                plt.title('%s, Normalized %s, mono not corrected\nbad scan: %s'%(dir_p,plot_type,bad_scan_ids), fontsize=fontsize)
                    if return_data is True:
                        return rixs_dict['nominal_mono'], [offset+invert*np.sum(rixs_dict['rixs_plane0_norm'][spot_id,:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=0) for spot_id in spot_ids]
                        # if plot_only is not True:
                        #     return rixs_dict['nominal_mono'], np.sum(np.sum(rixs_dict['rixs_plane0_norm'], axis=0)[:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1)
        else: # not Normalized. Not updated much
            if sum_over_spot is True:
                if return_only is False:  # show plots; can return or not return
                    if hold_on is False:
                        plt.figure()
                z = np.zeros_like(rixs_dict['rixs_plane0'][0], dtype=np.float32)
                for spot_id in spot_ids:
                    z += rixs_dict['rixs_plane0'][spot_id]
                if subtract_mono:
                    for i in range(np.shape(z)[0]):
                        z[i] = np.roll(z[i], -int((rixs_dict['nominal_mono'][i]-rixs_dict['nominal_mono'][0])/bin_width))
                    if norm_by_mono is True:
                        mono_intensity = np.sum(z[:,int((rixs_dict['nominal_mono'][0]-0.5)/bin_width):int((rixs_dict['nominal_mono'][0]+0.5)/bin_width)], axis=1)
                    if roi is None:
                        print('# provide ROI!')
                        return
                    else: # roi provided
                        if norm_by_mono is False:
                            if return_only is False:  # show plots; can return or not return
                                plt.plot(rixs_dict['nominal_mono'], np.sum(z[:,int((roi[0]-rixs_dict['nominal_mono'][-1]+rixs_dict['nominal_mono'][0])/bin_width):int((rixs_dict['nominal_mono'][0]-subtract_mono)/bin_width)], axis=1))
                                plt.legend(fontsize=fontsize); plt.title('%s, %s, mono not corrected, summed over spots\nbad scan: %s'%(dir_p,plot_type,bad_scan_ids), fontsize=fontsize)
                            if return_data is True:
                                return rixs_dict['nominal_mono'], np.sum(z[:,int((roi[0]-rixs_dict['nominal_mono'][-1]+rixs_dict['nominal_mono'][0])/bin_width):int((rixs_dict['nominal_mono'][0]-subtract_mono)/bin_width)], axis=1)
                        else:
                            if shading in ['nearest', 'gouraud']:
                                plt.pcolormesh(rixs_dict['nominal_mono'][:-1], rixs_dict['nominal_mono'][0]-bin_centers[int((roi[0]-rixs_dict['nominal_mono'][-1]+rixs_dict['nominal_mono'][0])/bin_width):int((roi[1])/bin_width)], np.transpose(z[:,int((roi[0]-rixs_dict['nominal_mono'][-1]+rixs_dict['nominal_mono'][0])/bin_width):int((roi[1])/bin_width)+1]), cmap=nipy_spectral_r_white, shading=shading)
                            else:
                                plt.pcolormesh(rixs_dict['nominal_mono'], rixs_dict['nominal_mono'][0]-bin_centers[int((roi[0]-rixs_dict['nominal_mono'][-1]+rixs_dict['nominal_mono'][0])/bin_width):int((roi[1])/bin_width)], np.transpose(z[:,int((roi[0]-rixs_dict['nominal_mono'][-1]+rixs_dict['nominal_mono'][0])/bin_width):int((roi[1])/bin_width)]), cmap=nipy_spectral_r_white, shading=shading)
                            # plt.sj_hlines([z[0,int((rixs_dict['nominal_mono'][0]-0.5)/bin_width)], z[0,int((rixs_dict['nominal_mono'][0]+0.5)/bin_width)]])

                            # using contourf
                            # plt.figure()
                            # X = rixs_dict['nominal_mono']
                            # Y = bin_centers[int((roi[0]-rixs_dict['nominal_mono'][-1]+rixs_dict['nominal_mono'][0])/bin_width):int((roi[1])/bin_width)]
                            # Z = np.transpose(z[:,int((roi[0]-rixs_dict['nominal_mono'][-1]+rixs_dict['nominal_mono'][0])/bin_width):int((roi[1])/bin_width)])
                            # X,Y = np.meshgrid(X,Y)
                            # plt.contourf(X,rixs_dict['nominal_mono'][0]-Y,Z,50,cmap=cm.nipy_spectral_r)

                            # using imshow
                            # f = interpolate.interp2d(X, Y, Z, kind='linear')
                            # X_uniform = np.arange(X[0],X[-1],0.1)
                            # X_uniform = np.append(X_uniform,X[-1])
                            # Z_new = f(X_uniform, Y)
                            # plt.imshow(Z_new, interpolation='bicubic', aspect='auto', cmap=cm.nipy_spectral_r, origin='lower')

                            # using interp2d
                            # x_finer = np.array([])
                            # for i, _ in enumerate(X):
                            #     if i==len(X)-1:
                            #         break
                            #     elif i==0:
                            #         x_finer = np.append(x_finer, np.linspace(X[i],X[i+1],10))
                            #     else:
                            #         x_finer = np.append(x_finer, np.linspace(X[i],X[i+1],10)[1:])
                            # y_finer = np.array([])
                            # for i, _ in enumerate(Y):
                            #     if i==len(Y)-1:
                            #         break
                            #     elif i==0:
                            #         y_finer = np.append(y_finer, np.linspace(Y[i],Y[i+1],5))
                            #     else:
                            #         y_finer = np.append(y_finer, np.linspace(Y[i],Y[i+1],5)[1:])
                            # x_finer = np.arange(X[0],X[-1],0.01)
                            # x_finer = np.append(x_finer, X[-1])
                            # y_finer = np.arange(Y[0],Y[-1],(Y[1]-Y[0])/10)
                            # y_finer = np.append(y_finer, Y[-1])
                            # z_finer = f(x_finer, y_finer)
                            # plt.pcolormesh(x_finer, rixs_dict['nominal_mono'][0]-y_finer, z_finer, cmap=cm.nipy_spectral_r)
                            # plt.pcolormesh(X, rixs_dict['nominal_mono'][0]-Y, f(X,Y), cmap=cm.nipy_spectral_r)
                            plt.figure()
                            plt.plot(rixs_dict['nominal_mono'], np.sum(z[:,int((roi[0]-rixs_dict['nominal_mono'][-1]+rixs_dict['nominal_mono'][0])/bin_width):int((rixs_dict['nominal_mono'][0]-subtract_mono)/bin_width)], axis=1))
                            plt.legend(fontsize=fontsize)
                            plt.title('%s, %s, mono not corrected, summed over spots\nbad scan: %s'%(dir_p,plot_type,bad_scan_ids), fontsize=fontsize)
                            plt.figure()
                            plt.plot(rixs_dict['nominal_mono'], np.sum(z[:,int((roi[0]-rixs_dict['nominal_mono'][-1]+rixs_dict['nominal_mono'][0])/bin_width):int((rixs_dict['nominal_mono'][0]-subtract_mono)/bin_width)], axis=1)/mono_intensity)
                            plt.legend(fontsize=fontsize)
                            plt.title('%s, Normalized (by elastic) %s, mono not corrected, summed over spots\nbad scan: %s'%(dir_p,plot_type,bad_scan_ids), fontsize=fontsize)
                            plt.figure()
                            plt.plot(rixs_dict['nominal_mono'], mono_intensity)
                            plt.title('Elastic peak intensity')
                            # return rixs_dict['nominal_mono'],
                else:
                    if return_only is False:
                        plt.plot(rixs_dict['nominal_mono'], offset+invert*np.sum(z[:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1), label='ROI = [%s, %s]'%(roi[0], roi[1]))
                        plt.legend(fontsize=fontsize)
                        plt.title('%s, %s, mono not corrected, summed over spots\nbad scan: %s'%(dir_p,plot_type,bad_scan_ids), fontsize=fontsize)
                    if return_data is True:
                        return rixs_dict['nominal_mono'], np.sum(z[:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1)
            else:  # sum_over_spot is False
                if return_only is False:
                    plt.figure()
                    for ii, spot_id in enumerate(spot_ids):
                        if (hold_on is False) and (spot_id != spot_ids[0]):
                            plt.figure()
                        plt.plot(rixs_dict['nominal_mono'], offset+invert*np.sum(rixs_dict['rixs_plane0_norm'][spot_id,:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=0), label='ROI = [%s, %s], spot/scan #%s'%(roi[0], roi[1], spot_id), color=rainbow_colors[ii], linestyle=solid_or_dash[ii%2])
                    plt.legend(fontsize=fontsize)
                    plt.title('%s, %s, mono not corrected\nbad scan: %s'%(dir_p,plot_type,bad_scan_ids), fontsize=fontsize)
                    # if plot_only is not True:
                    #     return rixs_dict['nominal_mono'], np.sum(np.sum(rixs_dict['rixs_plane0_norm'], axis=0)[:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1)
    elif interp == 1:
        if ('rixs_plane1' not in rixs_dict.keys()) or sum(sum(sum(rixs_dict['rixs_plane1']))) == 0:
            print('A meaningful RIXS map does not exist with interp = %d. Try a different interp value.'%interp)
            return
        if norm is True:
            if sum_over_spot is True:
                if return_only is False:  # show plots; can return or not return
                    if hold_on is False:
                        plt.figure()
                z = np.zeros_like(rixs_dict['rixs_plane1_norm'][0], dtype=np.float32)
                for spot_id in spot_ids:
                    z += rixs_dict['rixs_plane1_norm'][spot_id]
                if subtract_mono:
                    for i in range(np.shape(z)[0]):
                        z[i] = np.roll(z[i], -int((rixs_dict['valid_nominal_mono'][i]-rixs_dict['valid_nominal_mono'][0])/bin_width))
                    if roi is None:
                        print('# provide ROI!')
                        return
                    else:
                        if return_only is False:  # show plots; can return or not return
                            plt.plot(rixs_dict['valid_nominal_mono'], np.sum(z[:,int((roi[0]-rixs_dict['valid_nominal_mono'][-1]+rixs_dict['valid_nominal_mono'][0])/bin_width):int((rixs_dict['valid_nominal_mono'][0]-subtract_mono)/bin_width)], axis=1))
                            plt.legend(fontsize=fontsize)
                            if ('sample_name' in rixs_dict.keys()):
                                if rixs_dict['sample_name'] != '':
                                    plt.title('%s, %s, Normalized %s, mono corrected (lin), summed over spots\nbad scan: %s'%(dir_p,rixs_dict['sample_name'],plot_type,bad_scan_ids), fontsize=fontsize)
                            else:
                                plt.title('%s, Normalized %s, mono corrected (lin), summed over spots\nbad scan: %s'%(dir_p,plot_type,bad_scan_ids), fontsize=fontsize)
                        if return_data is True:
                            return rixs_dict['valid_nominal_mono'], np.sum(z[:,int((roi[0]-rixs_dict['valid_nominal_mono'][-1]+rixs_dict['valid_nominal_mono'][0])/bin_width):int((rixs_dict['valid_nominal_mono'][0]-subtract_mono)/bin_width)], axis=1)
                else:
                    if return_only is False:  # show plots; can return or not return
                        plt.plot(rixs_dict['valid_nominal_mono'], offset+invert*np.sum(z[:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1), label='ROI = [%s, %s]'%(roi[0], roi[1]))
                        plt.legend(fontsize=fontsize)
                        if ('sample_name' in rixs_dict.keys()):
                            if rixs_dict['sample_name'] != '':
                                plt.title('%s, %s, Normalized %s, mono corrected (lin), summed over spots\nbad scan: %s'%(dir_p,rixs_dict['sample_name'],plot_type,bad_scan_ids), fontsize=fontsize)
                        else:
                            plt.title('%s, Normalized %s, mono corrected (lin), summed over spots\nbad scan: %s'%(dir_p,plot_type,bad_scan_ids), fontsize=fontsize)
                    if return_data is True:
                        return rixs_dict['valid_nominal_mono'], np.sum(z[:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1)
            else:
                if return_only is False:  # show plots; can return or not return
                    plt.figure()
                    for ii, spot_id in enumerate(spot_ids):
                        if (hold_on is False) and (spot_id != spot_ids[0]):
                            plt.figure()
                        plt.plot(rixs_dict['valid_nominal_mono'], offset+invert*np.sum(rixs_dict['rixs_plane1_norm'][spot_id,:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=0), label='ROI = [%s, %s], spot/scan #%s'%(roi[0], roi[1], spot_id), color=rainbow_colors[ii], linestyle=solid_or_dash[ii%2])
                        plt.legend(fontsize=fontsize)
                        if ('sample_name' in rixs_dict.keys()):
                            if rixs_dict['sample_name'] != '':
                                plt.title('%s, %s, Normalized %s, mono corrected (lin)\nbad scan: %s'%(dir_p,rixs_dict['sample_name'],plot_type,bad_scan_ids), fontsize=fontsize)
                        else:
                            plt.title('%s, Normalized %s, mono corrected (lin)\nbad scan: %s'%(dir_p,plot_type,bad_scan_ids), fontsize=fontsize)
                if return_data is True:
                    print('return data not available yet')
                #     return rixs_dict['nominal_mono'], np.sum(np.sum(rixs_dict['rixs_plane0_norm'], axis=0)[:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1)
        else: # no normalization
            if sum_over_spot is True:
                if return_only is False:  # show plots; can return or not return
                    if hold_on is False:
                        plt.figure()
                z = np.zeros_like(rixs_dict['rixs_plane1'][0], dtype=np.float32)
                for spot_id in spot_ids:
                    z += rixs_dict['rixs_plane1'][spot_id]
                if subtract_mono:
                    for i in range(np.shape(z)[0]):
                        z[i] = np.roll(z[i], -int((rixs_dict['valid_nominal_mono'][i]-rixs_dict['valid_nominal_mono'][0])/bin_width))
                    if roi is None:
                        print('# provide ROI!')
                        return
                    else:
                        if return_only is False:  # show plots; can return or not return
                            plt.plot(rixs_dict['valid_nominal_mono'], np.sum(z[:,int((roi[0]-rixs_dict['valid_nominal_mono'][-1]+rixs_dict['valid_nominal_mono'][0])/bin_width):int((rixs_dict['valid_nominal_mono'][0]-subtract_mono)/bin_width)], axis=1))
                            plt.legend(fontsize=fontsize)
                            if ('sample_name' in rixs_dict.keys()):
                                if rixs_dict['sample_name'] != '':
                                    plt.title('%s, %s, %s, mono corrected (lin), summed over spots\nbad scan: %s'%(dir_p,rixs_dict['sample_name'],plot_type,bad_scan_ids), fontsize=fontsize)
                            else:
                                plt.title('%s, %s, mono corrected (lin), summed over spots\nbad scan: %s'%(dir_p,plot_type,bad_scan_ids), fontsize=fontsize)
                        if return_data is True:
                            return rixs_dict['valid_nominal_mono'], np.sum(z[:,int((roi[0]-rixs_dict['valid_nominal_mono'][-1]+rixs_dict['valid_nominal_mono'][0])/bin_width):int((rixs_dict['valid_nominal_mono'][0]-subtract_mono)/bin_width)], axis=1)
                else:
                    if return_only is False:  # show plots; can return or not return
                        plt.plot(rixs_dict['valid_nominal_mono'], offset+invert*np.sum(z[:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1), label='ROI = [%s, %s]'%(roi[0], roi[1]))
                        plt.legend(fontsize=fontsize)
                        if ('sample_name' in rixs_dict.keys()):
                            if rixs_dict['sample_name'] != '':
                                plt.title('%s, %s, %s, mono corrected (lin), summed over spots\nbad scan: %s'%(dir_p,rixs_dict['sample_name'],plot_type,bad_scan_ids), fontsize=fontsize)
                        else:
                            plt.title('%s, %s, mono corrected (lin), summed over spots\nbad scan: %s'%(dir_p,plot_type,bad_scan_ids), fontsize=fontsize)
                    if return_data is True:
                        return rixs_dict['valid_nominal_mono'], np.sum(z[:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1)
            else:
                if return_only is False:  # show plots; can return or not return
                    plt.figure()
                    for ii, spot_id in enumerate(spot_ids):
                        if (hold_on is False) and (spot_id != spot_ids[0]):  # for 2nd and later spots
                            plt.figure()
                        plt.plot(rixs_dict['valid_nominal_mono'], offset+invert*np.sum(rixs_dict['rixs_plane1'][spot_id,:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=0), label='ROI = [%s, %s], spot/scan #%s'%(roi[0], roi[1], spot_id), color=rainbow_colors[ii], linestyle=solid_or_dash[ii%2])
                        plt.legend(fontsize=fontsize)
                        if ('sample_name' in rixs_dict.keys()):
                            if rixs_dict['sample_name'] != '':
                                plt.title('%s, %s, %s, mono corrected (lin)\nbad scan: %s'%(dir_p,rixs_dict['sample_name'],plot_type,bad_scan_ids), fontsize=fontsize)
                        else:
                            plt.title('%s, %s, mono corrected (lin)\nbad scan: %s'%(dir_p,plot_type,bad_scan_ids), fontsize=fontsize)
                    if return_data is True:
                        print('return data not available yet')
                        #     return rixs_dict['nominal_mono'], np.sum(np.sum(rixs_dict['rixs_plane0_norm'], axis=0)[:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1)
    elif interp == 2: # not much updated; will be removed
        if ('rixs_plane2' not in rixs_dict.keys()) or sum(sum(sum(rixs_dict['rixs_plane2']))) == 0:
            print('A meaningful RIXS map does not exist with interp = %d. Try a different interp value.'%interp)
            return
        if norm is True:
            if sum_over_spot is True:
                if hold_on is False:
                    plt.figure()
                z = np.zeros_like(rixs_dict['rixs_plane2_norm'][0], dtype=np.float32)
                for spot_id in spot_ids:
                    z += rixs_dict['rixs_plane2_norm'][spot_id]
                if subtract_mono:
                    for i in range(np.shape(z)[0]):
                        z[i] = np.roll(z[i], -int((rixs_dict['valid_nominal_mono'][i]-rixs_dict['valid_nominal_mono'][0])/bin_width))
                    if roi is None:
                        print('# provide ROI!')
                        return
                    else:
                        plt.plot(rixs_dict['valid_nominal_mono'], np.sum(z[:,int((roi[0]-rixs_dict['valid_nominal_mono'][-1]+rixs_dict['valid_nominal_mono'][0])/bin_width):int((rixs_dict['valid_nominal_mono'][0]-subtract_mono)/bin_width)], axis=1))
                        plt.legend(fontsize=fontsize); plt.title('%s, Normalized %s, mono corrected (cube), summed over spots\nbad scan: %s'%(dir_p,plot_type,bad_scan_ids), fontsize=fontsize)
                        return rixs_dict['valid_nominal_mono'], np.sum(z[:,int((roi[0]-rixs_dict['valid_nominal_mono'][-1]+rixs_dict['valid_nominal_mono'][0])/bin_width):int((rixs_dict['valid_nominal_mono'][0]-subtract_mono)/bin_width)], axis=1)
                else:
                    plt.plot(rixs_dict['valid_nominal_mono'], offset+invert*np.sum(z[:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1), label='ROI = [%s, %s]'%(roi[0], roi[1]))
                    plt.legend(fontsize=fontsize); plt.title('%s, Normalized %s, mono corrected (cube), summed over spots\nbad scan: %s'%(dir_p,plot_type,bad_scan_ids), fontsize=fontsize)
                    if (return_data is True) or (return_only is True):
                        return rixs_dict['valid_nominal_mono'], np.sum(z[:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1)
            else:
                for spot_id in spot_ids:
                    if (hold_on is False) and (spot_id != spot_ids[0]):
                        plt.figure()
                    plt.plot(rixs_dict['valid_nominal_mono'], offset+invert*np.sum(rixs_dict['rixs_plane2_norm'][spot_id,:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=0), label='ROI = [%s, %s], spot/scan #%s'%(roi[0], roi[1], spot_id))
                    plt.legend(fontsize=fontsize); plt.title('%s, Normalized %s, mono corrected (cube)\nbad scan: %s'%(dir_p,plot_type,bad_scan_ids), fontsize=fontsize)
                    # if plot_only is not True:
                    #     return rixs_dict['nominal_mono'], np.sum(np.sum(rixs_dict['rixs_plane0_norm'], axis=0)[:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1)
        else:
            if sum_over_spot is True:
                # if hold_on is False:
                #     plt.figure()
                z = np.zeros_like(rixs_dict['rixs_plane2'][0], dtype=np.float32)
                for spot_id in spot_ids:
                    z += rixs_dict['rixs_plane2'][spot_id]
                if subtract_mono:
                    for i in range(np.shape(z)[0]):
                        z[i] = np.roll(z[i], -int((rixs_dict['valid_nominal_mono'][i]-rixs_dict['valid_nominal_mono'][0])/bin_width))
                    if roi is None:
                        print('# provide ROI!')
                        return
                    else:
                        plt.plot(rixs_dict['valid_nominal_mono'], np.sum(z[:,int((roi[0]-rixs_dict['valid_nominal_mono'][-1]+rixs_dict['valid_nominal_mono'][0])/bin_width):int((rixs_dict['valid_nominal_mono'][0]-subtract)/bin_width)], axis=1))
                        plt.legend(fontsize=fontsize); plt.title('%s, %s, mono corrected (cube), summed over spots\nbad scan: %s'%(dir_p,plot_type,bad_scan_ids), fontsize=fontsize)
                        return rixs_dict['valid_nominal_mono'], np.sum(z[:,int((roi[0]-rixs_dict['valid_nominal_mono'][-1]+rixs_dict['valid_nominal_mono'][0])/bin_width):int((rixs_dict['valid_nominal_mono'][0]-subtract)/bin_width)], axis=1)
                else:
                    plt.plot(rixs_dict['valid_nominal_mono'], offset+invert*np.sum(z[:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1), label='ROI = [%s, %s]'%(roi[0], roi[1]))
                    plt.legend(fontsize=fontsize); plt.title('%s, %s, mono corrected (cube), summed over spots\nbad scan: %s'%(dir_p,plot_type,bad_scan_ids), fontsize=fontsize)
                    if (return_data is True) or (return_only is True):
                        return rixs_dict['valid_nominal_mono'], np.sum(z[:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1)
            else:
                for spot_id in spot_ids:
                    if (hold_on is False) and (spot_id != spot_ids[0]):
                        plt.figure()
                    plt.plot(rixs_dict['valid_nominal_mono'], offset+invert*np.sum(rixs_dict['rixs_plane2'][spot_id,:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=0), label='ROI = [%s, %s], spot/scan #%s'%(roi[0], roi[1], spot_id))
                    plt.legend(fontsize=fontsize); plt.title('%s, %s, mono corrected (cube)\nbad scan: %s'%(dir_p,plot_type,bad_scan_ids), fontsize=fontsize)
                    # if plot_only is not True:
                    #     return rixs_dict['nominal_mono'], np.sum(np.sum(rixs_dict['rixs_plane0_norm'], axis=0)[:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=1)


def plot_rixs_slicing(rixs_dict, slicing_value, spot_ids=None, bad_spot_ids=None, bad_scan_ids=None, sum_over_spot=True, interp=0, norm=True, hold_on=False, return_only=False, rebin=None, smoothing=1, subtract_mono=False, norm_by_mono=False, xlim=None, ylim=None):
    '''
    slicing_value: error if None, if a scalar => sum between that value and one on the right, if the last value => do nothing
    '''

    if type(rixs_dict) is list: # a list of rixs dictionaries. Only available for plane0's. Seems not ready yet
        print('# A list of dictionary is given')
        print("Multiple scans are temporarily merged and plotted")
        rixs_dict = merge_multiple_scans(rixs_dict)
    else:
        str_key_list = [key for key in rixs_dict.keys() if (type(key) is str)]  # added to ignore alias integer keys
        if ('201' in str_key_list[0]) or ('202' in str_key_list[0]):
            if len(str_key_list)>1:
                print('# A dictionary of dictionary is given')
                print("Multiple scans are temporarily merged and plotted")
            rixs_dict = merge_multiple_scans(rixs_dict)

    bin_centers = rixs_dict['bin_centers']
    bin_width = bin_centers[1]-bin_centers[0]
    # bin_start = bin_centers[0]+bin_width/2
    # bin_end = bin_centers[-1]+bin_width/2

    scan_numbers = rixs_dict['scan_numbers']
    dir_p = rixs_dict['dir_p']

    if spot_ids is None:
        spot_ids = list(range(np.shape(rixs_dict['rixs_plane0'])[0]))
    elif isinstance(spot_ids, int):
        spot_ids = [spot_ids]
        sum_over_spot = False

    assert min(spot_ids) >= 0 # spot_ids should begin from 0

    if bad_spot_ids is not None:
        if isinstance(bad_spot_ids, int):
            bad_spot_ids = [bad_spot_ids]
        bad_scan_ids = [scan_numbers[spot_ids.index(bad_spot_id)] for bad_spot_id in bad_spot_ids]
        for bad_spot_id in bad_spot_ids:
            spot_ids.remove(bad_spot_id)
    else:
        if bad_scan_ids is not None:
            if isinstance(bad_scan_ids, int):
                bad_scan_ids = [bad_scan_ids]
            bad_spot_ids = [spot_ids[scan_numbers.index(bad_scan_id)] for bad_scan_id in bad_scan_ids]
            for bad_spot_id in bad_spot_ids:
                spot_ids.remove(bad_spot_id)

    roi = [0, bin_centers[-1]] # this needs to be changed, but is okay for now

    # if slicing_value is None:
    #     print('# provide a slicing value or a range')
    #     return

    try:
        num_slicing_values = len(slicing_value)
    except TypeError:
        slicing_value = [slicing_value]
        num_slicing_values = len(slicing_value)

    if num_slicing_values == 1:
        if interp == 0:
            # rhs_index = np.argmax(rixs_dict['nominal_mono']>=slicing_value[0])
            # lhs_index = rhs_index - 1
            lhs_index = np.argmax(rixs_dict['nominal_mono']>=slicing_value[0])
            rhs_index = lhs_index + 1
            if rhs_index >= len(rixs_dict['nominal_mono']):
                rhs_index = -1
            rhs_bin = rixs_dict['nominal_mono'][rhs_index] # right or bang on
            lhs_bin = rixs_dict['nominal_mono'][lhs_index]
        else:
            # rhs_index = np.argmax(rixs_dict['valid_nominal_mono']>=slicing_value[0])
            # lhs_index = rhs_index - 1
            lhs_index = np.argmax(rixs_dict['valid_nominal_mono']>=slicing_value[0])
            rhs_index = lhs_index + 1
            if rhs_index >= len(rixs_dict['valid_nominal_mono']):
                rhs_index = -1
            rhs_bin = rixs_dict['valid_nominal_mono'][rhs_index] # right or bang on
            lhs_bin = rixs_dict['valid_nominal_mono'][lhs_index]
    else:
        if interp == 0:
            rhs_index = np.argmax(rixs_dict['nominal_mono']>=slicing_value[1])
            if rhs_index == 0:
                rhs_index = -1
            lhs_index = np.argmax(rixs_dict['nominal_mono']>=slicing_value[0])
            rhs_bin = rixs_dict['nominal_mono'][rhs_index] # right or bang on
            lhs_bin = rixs_dict['nominal_mono'][lhs_index]
        else:
            rhs_index = np.argmax(rixs_dict['valid_nominal_mono']>=slicing_value[1])
            if rhs_index == 0:
                rhs_index = -1
            lhs_index = np.argmax(rixs_dict['valid_nominal_mono']>=slicing_value[0])
            rhs_bin = rixs_dict['valid_nominal_mono'][rhs_index] # right or bang on
            lhs_bin = rixs_dict['valid_nominal_mono'][lhs_index]
    if lhs_bin == rhs_bin:
         print('# please check the slicing value/range')
         return

    print('# slicing between %g eV and %g eV' %(lhs_bin, rhs_bin))

    if return_only is False:
        plt.figure()

    if interp == 0:
        if norm is True:
            if sum_over_spot is True:
                z = np.zeros_like(rixs_dict['rixs_plane0_norm'][0], dtype=np.float32)
                for spot_id in spot_ids:
                    z += rixs_dict['rixs_plane0_norm'][spot_id]
                if subtract_mono:
                    print('# not ready yet!!!')
                    return # below is ignored
                    for i in range(np.shape(z)[0]):
                        z[i] = np.roll(z[i], -int((rixs_dict['nominal_mono'][i]-rixs_dict['nominal_mono'][0])/bin_width))
                    plt.ylabel('Energy transfer (eV)')
                else:
                    if return_only is not True:
                        plt.plot(bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.sum(z[lhs_index:rhs_index,int(roi[0]/bin_width):int(roi[1]/bin_width)], axis=0))
                        plt.xlim(xlim)
                        plt.ylim(ylim)
                        plt.xlabel('Emission energy (eV)')
                        plt.title('slicing over %s'%[lhs_bin,rhs_bin])
                    return bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.sum(z[lhs_index:rhs_index,int(roi[0]/bin_width):int(roi[1]/bin_width)], axis=0)
                        # if return_data is True:
                        #     return bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.sum(z[lhs_index:rhs_index,int(roi[0]/bin_width):int(roi[1]/bin_width)],axis=0)
                if return_only is not True:
                    plt.title('%s, XES, normalized, mono not corrected, summed over spots'%dir_p)
            else:
                if subtract_mono:
                    print('# this feature is not ready yet')
                    return
                else:
                    if return_only is False:
                        for spot_id in spot_ids:
                            if (hold_on is False) and (spot_id != spot_ids[0]):
                                plt.figure()
                            if hold_on is False:
                                plt.title('%s, spot number: %g'%(dir_p,spot_id))
                            plt.plot(bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.sum(rixs_dict['rixs_plane0_norm'][spot_id][lhs_index:rhs_index,int(roi[0]/bin_width):int(roi[1]/bin_width)], axis=0))
                            plt.xlim(xlim)
                            plt.ylim(ylim)
                            plt.xlabel('Emission energy (eV)')
                    return bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], [np.sum(rixs_dict['rixs_plane0_norm'][spot_id][lhs_index:rhs_index,int(roi[0]/bin_width):int(roi[1]/bin_width)], axis=0) for spot_id in spot_ids]
        else: # not normalized
            if sum_over_spot is True:
                z = np.zeros_like(rixs_dict['rixs_plane0'][0], dtype=np.float32)
                for spot_id in spot_ids:
                    z += rixs_dict['rixs_plane0'][spot_id]
                if subtract_mono:
                    print('# not ready yet!!!')
                    return # below is ignored
                    for i in range(np.shape(z)[0]):
                        z[i] = np.roll(z[i], -int((rixs_dict['nominal_mono'][i]-rixs_dict['nominal_mono'][0])/bin_width))
                    plt.ylabel('Energy transfer (eV)')
                else:
                    if return_only is not True:
                        plt.plot(bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.sum(z[lhs_index:rhs_index,int(roi[0]/bin_width):int(roi[1]/bin_width)], axis=0))
                        plt.xlim(xlim)
                        plt.ylim(ylim)
                        plt.xlabel('Emission energy (eV)')
                        plt.title('slicing over %s'%[lhs_bin,rhs_bin])
                    return bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.sum(z[lhs_index:rhs_index,int(roi[0]/bin_width):int(roi[1]/bin_width)], axis=0)
            else:
                if subtract_mono:
                    print('# this feature is not ready yet')
                    return
                else:
                    for spot_id in spot_ids:
                        if (hold_on is False) and (spot_id != spot_ids[0]):
                            plt.figure()
                        if hold_on is False:
                            plt.title('%s, spot number: %g'%(dir_p,spot_id))
                        plt.plot(bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.sum(rixs_dict['rixs_plane0'][spot_id][lhs_index:rhs_index,int(roi[0]/bin_width):int(roi[1]/bin_width)], axis=0))
                        plt.xlim(xlim)
                        plt.ylim(ylim)
                        plt.xlabel('Emission energy (eV)')
    elif interp == 1:
        if norm is True:
            if sum_over_spot is True:
                z = np.zeros_like(rixs_dict['rixs_plane1_norm'][0], dtype=np.float32)
                for spot_id in spot_ids:
                    z += rixs_dict['rixs_plane1_norm'][spot_id]
                if subtract_mono:
                    print('# this feature is not ready yet')
                    return
                else:
                    if return_only is not True:
                        plt.plot(bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.sum(z[lhs_index:rhs_index,int(roi[0]/bin_width):int(roi[1]/bin_width)], axis=0))
                        plt.xlim(xlim)
                        plt.ylim(ylim)
                        plt.xlabel('Emission energy (eV)')
                        plt.title('slicing over %s'%[lhs_bin,rhs_bin])
                    return bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.sum(z[lhs_index:rhs_index,int(roi[0]/bin_width):int(roi[1]/bin_width)], axis=0)
                        # if return_data is True:
                        #     return bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.sum(z[lhs_index:rhs_index,int(roi[0]/bin_width):int(roi[1]/bin_width)],axis=0)
                if return_only is not True:
                    plt.title('%s, XES, normalized, mono corrected, summed over spots'%dir_p)
            else:
                if subtract_mono:
                    print('# this feature is not ready yet')
                    return
                else:
                    if return_only is not True:
                        for spot_id in spot_ids:
                            if (hold_on is False) and (spot_id != spot_ids[0]):
                                plt.figure()
                            if hold_on is False:
                                plt.title('%s, spot number: %g'%(dir_p,spot_id))
                            plt.plot(bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.sum(rixs_dict['rixs_plane1_norm'][spot_id][lhs_index:rhs_index,int(roi[0]/bin_width):int(roi[1]/bin_width)], axis=0))
                            plt.xlim(xlim)
                            plt.ylim(ylim)
                            plt.xlabel('Emission energy (eV)')
                    return bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], [np.sum(rixs_dict['rixs_plane1_norm'][spot_id][lhs_index:rhs_index,int(roi[0]/bin_width):int(roi[1]/bin_width)], axis=0) for spot_id in spot_ids]
        else: # not normalized
            if sum_over_spot is True:
                z = np.zeros_like(rixs_dict['rixs_plane1'][0], dtype=np.float32)
                for spot_id in spot_ids:
                    z += rixs_dict['rixs_plane1'][spot_id]
                if subtract_mono:
                    print('# not ready yet!!!')
                    return # below is ignored
                    for i in range(np.shape(z)[0]):
                        z[i] = np.roll(z[i], -int((rixs_dict['nominal_mono'][i]-rixs_dict['nominal_mono'][0])/bin_width))
                    plt.ylabel('Energy transfer (eV)')
                else:
                    if return_only is not True:
                        plt.plot(bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.sum(z[lhs_index:rhs_index,int(roi[0]/bin_width):int(roi[1]/bin_width)], axis=0))
                        plt.xlim(xlim)
                        plt.ylim(ylim)
                        plt.xlabel('Emission energy (eV)')
                        plt.title('slicing over %s'%[lhs_bin,rhs_bin])
                    return bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.sum(z[lhs_index:rhs_index,int(roi[0]/bin_width):int(roi[1]/bin_width)], axis=0)
            else:
                if subtract_mono:
                    print('# this feature is not ready yet')
                    return
                else:
                    for spot_id in spot_ids:
                        if (hold_on is False) and (spot_id != spot_ids[0]):
                            plt.figure()
                        if hold_on is False:
                            plt.title('%s, spot number: %g'%(dir_p,spot_id))
                        plt.plot(bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.sum(rixs_dict['rixs_plane0'][spot_id][lhs_index:rhs_index,int(roi[0]/bin_width):int(roi[1]/bin_width)], axis=0))
                        plt.xlim(xlim)
                        plt.ylim(ylim)
                        plt.xlabel('Emission energy (eV)')
    elif interp == 2:
        print('Not ready yet. Use a different interp')
        return
        if norm is True:
            if sum_over_spot is True:
                plt.figure()
                z = np.zeros_like(rixs_dict['rixs_plane2_norm'][0], dtype=np.float32)
                for spot_id in spot_ids:
                    z += rixs_dict['rixs_plane2_norm'][spot_id]
                if subtract_mono:
                    print('# this feature is not ready yet')
                    return
                else:
                    plt.pcolormesh(rixs_dict['valid_nominal_mono'], bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.transpose(z[:,int(roi[0]/bin_width):int(roi[1]/bin_width)]), cmap=cmap)
                    if guide is True:
                        plt.plot(rixs_dict['nominal_mono'], rixs_dict['nominal_mono'], 'k--')
                    plt.xlim(rixs_dict['valid_nominal_mono'][0], rixs_dict['valid_nominal_mono'][-1])
                    plt.ylim(roi[0],roi[1])
                    plt.title('%s, RIXS, normalized, mono corrected (cube), summed over spots'%dir_p)
            else:
                if subtract_mono:
                    print('# this feature is not ready yet')
                    return
                else:
                    for spot_id in spot_ids:
                        plt.figure()
                        plt.pcolormesh(rixs_dict['valid_nominal_mono'], bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.transpose(rixs_dict['rixs_plane2_norm'][spot_id,:,int(roi[0]/bin_width):int(roi[1]/bin_width)]), cmap=cmap)
                        if guide is True:
                            plt.plot(rixs_dict['nominal_mono'], rixs_dict['nominal_mono'], 'k--')
                        plt.xlim(rixs_dict['valid_nominal_mono'][0], rixs_dict['valid_nominal_mono'][-1])
                        plt.ylim(roi[0],roi[1])
                        plt.title('%s, RIXS, normalized, mono corrected (cube)\nspot/scan #%s'%(dir_p,spot_id))
        else:
            if sum_over_spot is True:
                plt.figure()
                z = np.zeros_like(rixs_dict['rixs_plane2'][0], dtype=np.float32)
                for spot_id in spot_ids:
                    z += rixs_dict['rixs_plane2'][spot_id]
                if subtract_mono:
                    print('# this feature is not ready yet')
                    return
                else:
                    plt.pcolormesh(rixs_dict['valid_nominal_mono'], bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.transpose(z[:,int(roi[0]/bin_width):int(roi[1]/bin_width)]), cmap=cmap)
                    if guide is True:
                        plt.plot(rixs_dict['nominal_mono'], rixs_dict['nominal_mono'], 'k--')
                    plt.xlim(rixs_dict['valid_nominal_mono'][0], rixs_dict['valid_nominal_mono'][-1])
                    plt.ylim(roi[0],roi[1])
                    plt.title('%s, RIXS, mono corrected (cube), summed over spots'%dir_p)
            else:
                if subtract_mono:
                    print('# this feature is not ready yet')
                    return
                else:
                    for spot_id in spot_ids:
                        plt.figure()
                        plt.pcolormesh(rixs_dict['valid_nominal_mono'], bin_centers[int(roi[0]/bin_width):int(roi[1]/bin_width)], np.transpose(rixs_dict['rixs_plane2'][spot_id,:,int(roi[0]/bin_width):int(roi[1]/bin_width)]), cmap=cmap)
                        if guide is True:
                            plt.plot(rixs_dict['nominal_mono'], rixs_dict['nominal_mono'], 'k--')
                        plt.xlim(rixs_dict['valid_nominal_mono'][0], rixs_dict['valid_nominal_mono'][-1])
                        plt.ylim(roi[0],roi[1])
                        plt.title('%s, RIXS, mono corrected (cube)\nspot/scan #%s'%(dir_p,spot_id))


def plot_rixs_offset_slicing(rixs_dict, slicing_value_list, offset, slicing_width=0, plot_eloss=False, mono_scaling=1, mono_offset=0, color=None, xlim=None, spot_ids=None, bad_spot_ids=None, bad_scan_ids=None, sum_over_spot=True, interp=0, norm=True, overlay=False, rebin=None, smoothing=1, subtract_mono=False, norm_by_mono=False):
    '''
    slicing_value_list for a list of lower bounds
    without slicing_width provided, it will use mono steps
    plot_eloss=True needs mono_scaling and mono_offset
    overlay=True overlays a new plot on an existing figure
    '''

    num_slicings = len(slicing_value_list)
    offset_curves = {}

    if overlay is False:
        plt.figure()
    for ii, slicing_value in enumerate(slicing_value_list):
        if slicing_width == 0:
            offset_curves[slicing_value] = plot_rixs_slicing(rixs_dict, slicing_value=slicing_value, spot_ids=spot_ids, bad_spot_ids=bad_spot_ids, bad_scan_ids=bad_scan_ids, sum_over_spot=True, interp=interp, norm=norm, return_only=True, rebin=rebin, smoothing=smoothing, subtract_mono=subtract_mono, norm_by_mono=norm_by_mono)
        else:
            offset_curves[slicing_value] = plot_rixs_slicing(rixs_dict, slicing_value=[slicing_value,slicing_value+slicing_width], spot_ids=spot_ids, bad_spot_ids=bad_spot_ids, bad_scan_ids=bad_scan_ids, sum_over_spot=True, interp=interp, norm=norm, return_only=True, rebin=rebin, smoothing=smoothing, subtract_mono=subtract_mono, norm_by_mono=norm_by_mono)

        if offset_curves[slicing_value] is None:  # this is to handle the first or the last sliced curve?
            continue

        if plot_eloss is True:
            # eloss_offset_value = (slicing_value - mono_offset) * mono_scaling
            eloss_offset_value = (slicing_value +slicing_width/2 - slicing_value_list[0]) * mono_scaling + slicing_value_list[0] - mono_offset
        else:
            eloss_offset_value = slicing_width/2

        x = offset_curves[slicing_value][0] - eloss_offset_value
        y = offset_curves[slicing_value][1] + offset * ii
        if color is None:
            plt.plot(x, y, label=slicing_value)
        else:
            plt.plot(x, y, color=color, label=slicing_value)
        if xlim is not None:
            plt.xlim(xlim)
    plt.xlabel('Emission energy (eV)')


def save_rixs_pfy(rixs_dict, save_rixs=True, save_pfy=True, rixs_ylim=None, pfy_roi=[0,10000], interp=0):
    '''
    batch saving of rixs and pfy fig files if rixs_dict is a dict of rixs_dict
    also works for a single map rixs_dict
    '''
    str_key_list = [key for key in rixs_dict.keys() if (type(key) is str)]  # added to ignore alias integer keys
    if np.all(['201' in key for key in str_key_list]) or np.all(['202' in key for key in str_key_list]): # this being true means rixs_dict is a dictionary of rixs_dict.
        for key in str_key_list:
            if save_rixs:
                plot_rixs(rixs_dict[key], interp=interp, ylim=rixs_ylim)
            if save_pfy:
                plot_pfy(rixs_dict[key], interp=interp, roi=pfy_roi)
            sj_save_all_fig2file(rixs_dict[key]['sample_name']+'_scan'+str(rixs_dict[key]['scan_numbers'][0])+'-'+str(rixs_dict[key]['scan_numbers'][-1]), timestamp=False)
            plt.close('all')
    else:
        if save_rixs:
            plot_rixs(rixs_dict, interp=interp, ylim=rixs_ylim)
        if save_pfy:
            plot_pfy(rixs_dict, interp=interp, roi=pfy_roi)
        sj_save_all_fig2file(rixs_dict['sample_name']+'_scan'+str(rixs_dict['scan_numbers'][0])+'-'+str(rixs_dict['scan_numbers'][-1]), timestamp=False)
        plt.close('all')


def append_to_spec_file(data, spec_fname, suffix=None, BL=101, update=True, overwrite=False):
    '''
    If the beamtime is done (no more scans being added), use update=True
    If not done,
    update=True is used when pfy data already exists in the updated htxs file and it needs to be updated
    update=False & overwrite=True seems to overwrite the original spec file, so don't use it
    What if there's already appended pfy columns? check what happens.
    '''
    if BL == 101:
        # from sass.htxs import append_to_htxs
        if os.path.exists(spec_fname) is True:  # this is when a file name is provided with a path
            spec_file = spec_fname
        else:
            spec_file = '../htxs_files/' + spec_fname
            if os.path.exists(spec_file) is False:
                spec_file = '../../htxs_files/' + spec_fname
                if os.path.exists(spec_file) is False:
                    print("# file not found")
                    return
        append_to_htxs(spec_file, data, suffix=suffix, update=update, overwrite=overwrite)


def append_and_push(data, spec_fname, BL=101, update=True, overwrite=False):
    '''
    first run append_to_spec_file and then push_htxs
    '''
    append_to_spec_file(data, spec_fname, BL=BL, update=update, overwrite=overwrite)
    push_htxs(spec_fname)


def append_to_htxs(htxs_file, data, suffix=None, update=True, overwrite=False):
    """
    copied from SASS. needs to be modified
    :param htxs_file: Location of an ASCII htxs text file
    :param data: A dictionary, where the keys are HTXS scan numbers, and each value is a dictionary whose keys are new column names, and values are an array to be inserted
    """
    new_file = htxs_file + '_updated'
    if suffix is not None:
        new_file = new_file + '_' + suffix
    if os.path.exists(new_file) and update:
        htxs_file = new_file
        new_file = htxs_file + '_tmp'
        overwrite = True
    print("Appending from %s to %s"%(htxs_file, new_file))
    print(data.keys())
    with open(htxs_file, 'r') as f1:
        with open(new_file, 'w') as f2:
            state = 0
            n = 0
            currentData = 0
            currentKeys = []
            currentScan = 0
            for line in f1:
                if state == 0:  # #S 1 not reached yet or just finished processing a #S block
                    if "#S" not in line:
                        f2.write(line)
                    else:
                        try:
                            currentScan = int(line.split()[1])
                        except:
                            print("Conversion of scanno to int failed")
                            print(line)
                            currentScan = -1
                        f2.write(line)
                        if currentScan in data:
                            print("Replacing scan %d"%currentScan)
                            state = 1
                elif state == 1:  # PFY exists, and currently in that scan
                    if '#N' in line:  # number of counters
                        nprev = int(line.split()[1])  # original
                        ncurr = nprev + len(data[currentScan].keys())  # original + pfy channels
                        f2.write("#N %d\n"%ncurr)
                    elif '#L' in line:  # list of counter names
                        # currentKeys = sorted(list(data[currentScan].keys()))  # pfy channel names are sorted
                        currentKeys = list(data[currentScan].keys())  # pfy channel names as is
                        k = "  ".join(currentKeys)
                        f2.write(line.rstrip('\n') + '  ' + k + '\n')
                    elif '#' in line:  # other header lines of #S block
                        f2.write(line)
                    else:  # data line
                        n=0  # counter for the number of data rows
                        currentData = data[currentScan]  # pfy data in dictionary
                        state = 2  # state=2 means it started reading the data block
                if state == 2:  # reading the data block started
                    if line == '\n' or "#" in line:  # if the line is empty or a comment (#C) line
                        state = 0  # reset the state
                        f2.write(line)
                    else:  # if the line is data
                        newdata = ""
                        for k in currentKeys:
                            try:
                                newdata += " %.2f"%currentData[k][n]
                            except IndexError:
                                pass
                        f2.write(line.rstrip('\n') + newdata + '\n')
                        n += 1  # counter for the number of data rows;

    if overwrite:
        import shutil
        shutil.move(new_file, htxs_file)


# def add_to_spec_file(rixs_dict, spec_fname, roi, interp=0): # outdated
#     '''
#     interp=1 (linear case) does not do anything for now;
#     spec_fname is htxs file from spec
#     !!! some info dropped from comments
#     '''
#     new_spec_fname = spec_fname.split('.')[0]+'+TES_'+rixs_dict['dir_p']
#
#     bin_centers = rixs_dict['bin_centers']
#     bin_width = bin_centers[1]-bin_centers[0]
#     bin_end = bin_centers[-1]+bin_width/2
#
#     nominal_mono = rixs_dict['nominal_mono']
#     valid_nominal_mono = rixs_dict['valid_nominal_mono']
#     first_index = np.argmin(abs(nominal_mono - valid_nominal_mono[0]))
#     last_index = first_index + len(valid_nominal_mono) - 1
#     len_unvalid_mono = len(nominal_mono) - len(valid_nominal_mono)
#
#     scan_numbers = rixs_dict['scan_numbers']
#     corrected_mono = rixs_dict['corrected_mono']
#     corrected_mono = np.reshape(corrected_mono, (len(scan_numbers), len(nominal_mono)))
#     # scan_len = len(rixs_dict['nominal_mono'])
#
#
#     detector_list = ''
#     meas_list = ''
#     if np.array(roi).ndim > 1:
#         for roi_i in roi:
#             meas_list += '  PFY_%s_%s'%(roi_i[0], roi_i[1])
#     else:
#         meas_list += '  PFY_%s_%s'%(roi[0], roi[1])
#
#     with open(spec_fname, 'r') as f_in:
#         in_scan = -1
#         new_file_contents = ''
#         scan_part_started = 0
#         scan_start_counter = 0
#         scan_finish_counter = 0
#         for line in f_in:
#             if ('#S' in line) and (scan_part_started==0):
#                 scan_part_started = 1
#             if ('#S' in line) and ('tes' in line) and (int(line.split()[1]) in scan_numbers):
#                 in_scan = 1
#                 scan_start_counter += 1
#                 current_scan_num = int(line.split()[1])
#                 new_file_contents += line
#                 data_line = 0
#                 continue
#             if ('#L' in line) and (in_scan == 1):
#                 new_file_contents += line.strip() + '  corrected_mono  valid_nominal_mono' + meas_list +'\n'
#                 continue
#             if ('#' not in line) and (line.strip()!=[]) and (in_scan==1): # line is at data line
#                 new_file_contents += line.strip()
#                 new_file_contents += ' %s'%a[list(scan_numbers).index(current_scan_num)][data_line]
#                 if data_line < first_index: # line is not yet at valid data line
#                     new_file_contents += ' %s'%valid_nominal_mono[0]
#                     if np.array(roi).ndim > 1:
#                         for roi_i in roi:
#                             # for now, just use linear interp
#                             new_file_contents += ' %s'%np.sum(rixs_dict['rixs_plane1'][list(scan_numbers).index(current_scan_num),:,np.logical_and(bin_centers>=roi_i[0], bin_centers<roi_i[1])], axis=0)[0]
#                     else:
#                         new_file_contents += ' %s'%np.sum(rixs_dict['rixs_plane1'][list(scan_numbers).index(current_scan_num),:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=0)[0]
#                 else:
#                     if data_line <= last_index:
#                         new_file_contents += ' %s'%valid_nominal_mono[data_line-first_index]
#                         if np.array(roi).ndim > 1:
#                             for roi_i in roi:
#                                 # for now, just use linear interp
#                                 new_file_contents += ' %s'%np.sum(rixs_dict['rixs_plane1'][list(scan_numbers).index(current_scan_num),:,np.logical_and(bin_centers>=roi_i[0], bin_centers<roi_i[1])], axis=0)[data_line-first_index]
#                         else:
#                             new_file_contents += ' %s'%np.sum(rixs_dict['rixs_plane1'][list(scan_numbers).index(current_scan_num),:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=0)[data_line-first_index]
#                     else:
#                         new_file_contents += ' %s'%valid_nominal_mono[-1]
#                         if np.array(roi).ndim > 1:
#                             for roi_i in roi:
#                                 # for now, just use linear interp
#                                 new_file_contents += ' %s'%np.sum(rixs_dict['rixs_plane1'][list(scan_numbers).index(current_scan_num),:,np.logical_and(bin_centers>=roi_i[0], bin_centers<roi_i[1])], axis=0)[-1]
#                         else:
#                             new_file_contents += ' %s'%np.sum(rixs_dict['rixs_plane1'][list(scan_numbers).index(current_scan_num),:,np.logical_and(bin_centers>=roi[0], bin_centers<roi[1])], axis=0)[-1]
#                 new_file_contents += '\n'
#                 data_line += 1
#                 if data_line == len(nominal_mono):
#                     data_line = 0
#                     in_scan = 0
#                     scan_finish_counter += 1
#                     continue
#             if scan_part_started==0: # for saving headers
#                 new_file_contents += line
#             if scan_finish_counter == len(scan_numbers):
#                 break
#
#     with open(new_spec_fname, 'w') as f_out:
#         f_out.write(new_file_contents)
#
#     return


def gscan_args_to_grid(gscan_args):
    """
    gscan_args is a string, e.g., '910 915 1 916 0.2 920 1', not including counting time
    returns gscan_grid
    """
    gscan_args = gscan_args.split()
    gscan_grid = [float(gscan_args[0])]
    pos_init = gscan_grid[0]
    npts_grid = 1
    for n in range(1, len(gscan_args)-1, 2):
        pos_end = float(gscan_args[n])
        stepsize = float(gscan_args[n+1])
        npts_rgn = int((pos_end - pos_init)/stepsize)
        if npts_rgn <= 0:
            continue
        else:
            for _ in range(npts_rgn):
                gscan_grid.append(gscan_grid[-1] + stepsize)
            npts_grid += npts_rgn
            pos_init = pos_end
    return gscan_grid


def cal_lines_energies(mix_ver=3, mono=1000, no_l=True, skip=None):

    if mix_ver > 200:
        mono = mix_ver
        mix_ver = 3

    # calibration_energies_full = np.array(sorted(CAL_MIX_KL_FULL[mix_ver].values()))
    # calibration_lines_full = np.array(sorted(CAL_MIX_KL_FULL[mix_ver].keys(), key=CAL_MIX_KL_FULL[mix_ver].__getitem__))

    # calibration_energies = np.array(sorted(CAL_MIX_KL[mix_ver].values()))
    # calibration_lines = np.array(sorted(CAL_MIX_KL[mix_ver].keys(), key=CAL_MIX_KL[mix_ver].__getitem__))

    if mix_ver == 1:
        calibration_energies = np.array(sorted(CAL_MIX1_KL.values()))
        calibration_lines = np.array(sorted(CAL_MIX1_KL.keys(), key=CAL_MIX1_KL.__getitem__))
    else:
        calibration_energies = np.array(sorted(CAL_MIX_TITUS[mix_ver].values()))
        calibration_lines = np.array(sorted(CAL_MIX_TITUS[mix_ver].keys(), key=CAL_MIX_TITUS[mix_ver].__getitem__))

    cal_energies = []
    cal_lines = []

    for i, energy in enumerate(calibration_energies):
        if energy <= mono:
            cal_energies.append(energy)
            cal_lines.append(calibration_lines[i])
        else:
            cal_energies.append(mono)
            cal_lines.append('mono')
            break
    if 'mono' not in cal_lines:
        cal_energies.append(mono)
        cal_lines.append('mono')

    cal_energies_copy = cal_energies[:]
    cal_lines_copy = cal_lines[:]

    cal_lines_str = str(cal_lines)
    for i, line in enumerate(cal_lines_copy):
        if 'Ll' == line.split()[-1]:
            if cal_lines_str.count(line.split()[0]+' ') == 1:
                cal_energies.remove(cal_energies_copy[i])
                cal_lines.remove(line)

    cal_energies = np.array(cal_energies)
    cal_lines = np.array(cal_lines)

    if skip is not None:
        mask = np.ones_like(cal_lines, dtype=bool)
        for i, cal_line in enumerate(cal_lines):
            if skip in cal_line:
                mask[i] = False
        cal_energies = cal_energies[mask]
        cal_lines = cal_lines[mask]

    if no_l is True:
        mask = np.ones_like(cal_lines, dtype=bool)
        for i, cal_line in enumerate(cal_lines):
            if 'Ll' in cal_line:
                mask[i] = False
        cal_energies = cal_energies[mask]
        cal_lines = cal_lines[mask]

    return cal_energies, cal_lines


# CAL_MIX3_E_KL_FULL = [    183,   277,   393,    525,    615,     705,     719,     743,    811,     852,     869,      930,     950]
# CAL_MIX3_LINE_KL_FULL = [ 'B K', 'C K', 'N K', 'O K', 'Fe Ll', 'Fe La', 'Fe Lb', 'Ni Ll', 'Cu Ll', 'Ni La', 'Ni Lb', 'Cu La', 'Cu Lb']
# CAL_MIX3_KL_FULL = {}

def subtract_bg():
    return


def merge_two_dicts(x, y):
    '''
    # can be used for any two dicts, but main use is to merge two rixs_dict, e.g., one with '20190325_001' key and another with '20190325_002'
    # e.g. rixs_dict = merge_two_dicts(rixs_dict1, rixs_dict2)
    # but it does not work well for rixs_plane1's because the length of corrected monos could vary
    '''
    import copy
    z = copy.deepcopy(x)   # start with x's keys and values
    y_copy = copy.deepcopy(y)
    int_keys_in_x = [key for key in x.keys() if isinstance(key, int)]
    int_keys_in_y = [key for key in y.keys() if isinstance(key, int)]
    max_int_key_in_x = max(int_keys_in_x)
    for key in int_keys_in_y:
        y_copy[key+max_int_key_in_x+1] = y_copy.pop(key)  # this is to prevent overwriting integer key values
    z.update(y_copy)    # modifies z with y's keys and values & returns None (overwritten)
    return z


def merge_multiple_dicts(*args):
    '''
    what this does is reduce(merge_two_dicts, [rixs1, rixs2])
    '''
    from functools import reduce    
    return reduce(merge_two_dicts, args)


def merge_multiple_scans(*args):
    '''
    This is to combine multiple rixs_dicts for a same sample.
    usage: merged_rixs_dict = merge_multiple_scans(rixs1, rixs2, rixs3), where rixs1 is from a data folder
    merged_rixs_dict = merge_multiple_scans([rixs1, rixs2, rixs3])
    merged_rixs_dict = merge_multiple_scans(rixs_dict)

    '''
    import copy
    # print(type(args)) # tuple even if one arg is provided
    if len(args) == 1:  # this is the case when the arg is rixs_dict or a single entry list
        if type(args[0]) is dict:  # this is the case when the arg is rixs_dict
            if 'dir_p' in args[0].keys():
                print('Provide at least two rixs_dicts')
                return
            else:
                args = list(set(args[0].values()))  # change the dict of dictionaries to a set (to remove redundancy) and to a list of dictionaries
        elif type(args[0]) is list:  # this is the case when the arg is a single entry list
            args = args[0]
        else:
            print('Check the input args')
            return
    merged = copy.deepcopy(args[0])
    common_valid_nominal_mono = np.sort(list(set.intersection(*[set(arg['valid_nominal_mono']) for arg in args])))
    merged['valid_nominal_mono'] = common_valid_nominal_mono
    merged['scan_numbers'] = list(merged['scan_numbers'])  # needed because range(n) cannot be combined with list
    if type(merged['dir_p']) is str:
        merged['dir_p'] = [merged['dir_p']] # if already a list, then leave it as is
    rixs_plane0_types = [key for key in args[0].keys() if (('rixs_plane0' in key) and ('part_arr' not in key))]
    for arg in args[1:]:
        merged['scan_numbers'] += list(arg['scan_numbers'])
        if type(arg['dir_p']) is list:
            merged['dir_p'] += arg['dir_p']
        elif type(arg['dir_p']) is str:
            merged['dir_p'] += [arg['dir_p']]
    for rixs_plane0_type in rixs_plane0_types:
        merged[rixs_plane0_type] = _merge_rixs_dict_list(args, rixs_plane0_type)
    if 'rixs_plane1' in arg.keys():
        merged['rixs_plane1'] = np.concatenate([arg['rixs_plane1'][:, get_index(arg['valid_nominal_mono'], common_valid_nominal_mono[0])[0]:get_index(arg['valid_nominal_mono'], common_valid_nominal_mono[-1])[0]+1,:] for arg in args])
        merged['rixs_plane1_norm'] = np.concatenate([arg['rixs_plane1_norm'][:, get_index(arg['valid_nominal_mono'], common_valid_nominal_mono[0])[0]:get_index(arg['valid_nominal_mono'], common_valid_nominal_mono[-1])[0]+1,:] for arg in args])

    return merged


def _merge_rixs_dict_list(rixs_dict_list, key='rixs_plane0_norm'):
    '''
    # usage: rixs_map_array = _merge_rixs_dict_list([rixs_dict1, rixs_dict2]), where rixs_dict1 is not a dict of rixs_dict
    # to be used in plot_rixs
    # not done yet. work in process
    '''
    if type(rixs_dict_list) not in [list, tuple, set]:
        print('# provide a list, tuple or set')
        return
    return np.concatenate([rixs_dict[key] for rixs_dict in rixs_dict_list])
    # merged_rixs_dict = {}
    # return merged_rixs_dict


# class RIXS(): # under construction
#     def __init__():
#         print('nothing yet')
#     def plot_rixs():
#         # plt.plot()
#         return
#     def plot_pfy():
#         return
#     def plot_rixs_slicing():
#         return


def grab_line(fig_num=None):
    # work in progress
    line = []
    return line


def get_index(list, value):
    '''
    get a list of indices corresponding to a certain value
    '''
    index_list = [i for i, x in enumerate(list) if x == value]
    return index_list


def delete_index(input_list, indices):
    '''
    get a new list with certain indices deleted
    caveat: can take both list and array, but returns only a list.
    takes only non-negative index
    '''
    temp_list = input_list[:]
    if type(temp_list) is not list:
        temp_list = list(temp_list)
    if type(indices) is int:
        indices = [indices]
    for index in sorted(indices, reverse=True):
        del temp_list[index]
    return temp_list


def isnum(value):
    try:
        value = float(value)
        return True
    except:
        return False


def send_email(user, pwd, recipient, subject, body):
    import smtplib

    gmail_user = user
    gmail_pwd = pwd
    FROM = user
    TO = recipient if type(recipient) == list else [recipient]
    SUBJECT = subject
    TEXT = body

    # Prepare actual message
    message = """\From: %s\nTo: %s\nSubject: %s\n\n%s
    """ % (FROM, ", ".join(TO), SUBJECT, TEXT)
    try:
        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.ehlo()
        server.starttls()
        server.login(gmail_user, gmail_pwd)
        server.sendmail(FROM, TO, message)
        server.close()
        print('successfully sent the mail')
    except:
        print("failed to send mail")


def copy2clipboard(fig=None):
    '''
    reference: http://stackoverflow.com/questions/17676373/python-matplotlib-pyqt-copy-image-to-clipboard
    '''
    if fig is None:
        fig = plt.gcf()
    else:
        fig = plt.figure(fig)
    if use_pyqt4:
        QApplication.clipboard().setImage(QPixmap.grabWidget(fig.canvas).toImage())
    else:
        # p = QWidget.grab()
        widget = fig.canvas
        QApplication.clipboard().setImage(widget.grab().toImage())
        # QApplication.clipboard().setImage(QScreen.grabWindow(fig.canvas).toImage())
    print('figure %d copied to clipboard' % fig.number)


def add_clipboard_to_figures():
    '''
    reference: http://stackoverflow.com/questions/31607458/how-to-add-clipboard-support-to-matplotlib-figures
    '''
    import io

    # use monkey-patching to replace the original plt.figure() function with
    # our own, which supports clipboard-copying
    oldfig = plt.figure

    def newfig(*args, **kwargs):
        fig = oldfig(*args, **kwargs)
        def clipboard_handler(event):
            if event.key == 'ctrl+c':
                # store the image in a buffer using savefig(), this has the
                # advantage of applying all the default savefig parameters
                # such as background color; those would be ignored if you simply
                # grab the canvas using Qt
                buf = io.BytesIO()
                fig.savefig(buf)
                QApplication.clipboard().setImage(QImage.fromData(buf.getvalue()))
                buf.close()

        fig.canvas.mpl_connect('key_press_event', clipboard_handler)
        return fig

    plt.figure = newfig

add_clipboard_to_figures()


def datacursor(fig_num=None, draggable=True, **kwargs):
    print('mplcursors package (https://mplcursors.readthedocs.io/en/stable/) might be better.')
    import mpldatacursor
    if fig_num is None:
        mpldatacursor.datacursor(draggable=draggable, **kwargs)
    else:
        fig = plt.figure(fig_num)
        mpldatacursor.datacursor(axes=fig.axes, draggable=draggable, **kwargs)


class LineDrawer(object):
    lines = []
    def draw_line(self):
        ax = plt.gca()
        xy = plt.ginput(2)

        x = [p[0] for p in xy]
        y = [p[1] for p in xy]
        line = plt.plot(x,y)
        ax.figure.canvas.draw()

        self.lines.append(line)


nipy_spectral_r_10 = cm.nipy_spectral_r.from_list('nipy_spectral_r_10', [cm.nipy_spectral_r(i) for i in range(256)], 10)
cmaplist_nipy_spectral_r_10 = [nipy_spectral_r_10(i) for i in range(10)]
cmaplist_nipy_spectral_r_10[0] = (1.0, 1.0, 1.0, 1.0)
nipy_spectral_r_white = cm.nipy_spectral_r.from_list('nipy_spectral_r_white', cmaplist_nipy_spectral_r_10, 256)
cm.register_cmap(name='nipy_spectral_r_white', cmap=nipy_spectral_r_white)


CNAMES = {
'aliceblue':            '#F0F8FF',
'antiquewhite':         '#FAEBD7',
'aqua':                 '#00FFFF',
'aquamarine':           '#7FFFD4',
'azure':                '#F0FFFF',
'beige':                '#F5F5DC',
'bisque':               '#FFE4C4',
'black':                '#000000',
'blanchedalmond':       '#FFEBCD',
'blue':                 '#0000FF',
'blueviolet':           '#8A2BE2',
'brown':                '#A52A2A',
'burlywood':            '#DEB887',
'cadetblue':            '#5F9EA0',
'chartreuse':           '#7FFF00',
'chocolate':            '#D2691E',
'coral':                '#FF7F50',
'cornflowerblue':       '#6495ED',
'cornsilk':             '#FFF8DC',
'crimson':              '#DC143C',
'cyan':                 '#00FFFF',
'darkblue':             '#00008B',
'darkcyan':             '#008B8B',
'darkgoldenrod':        '#B8860B',
'darkgray':             '#A9A9A9',
'darkgreen':            '#006400',
'darkkhaki':            '#BDB76B',
'darkmagenta':          '#8B008B',
'darkolivegreen':       '#556B2F',
'darkorange':           '#FF8C00',
'darkorchid':           '#9932CC',
'darkred':              '#8B0000',
'darksalmon':           '#E9967A',
'darkseagreen':         '#8FBC8F',
'darkslateblue':        '#483D8B',
'darkslategray':        '#2F4F4F',
'darkturquoise':        '#00CED1',
'darkviolet':           '#9400D3',
'deeppink':             '#FF1493',
'deepskyblue':          '#00BFFF',
'dimgray':              '#696969',
'dodgerblue':           '#1E90FF',
'firebrick':            '#B22222',
'floralwhite':          '#FFFAF0',
'forestgreen':          '#228B22',
'fuchsia':              '#FF00FF',
'gainsboro':            '#DCDCDC',
'ghostwhite':           '#F8F8FF',
'gold':                 '#FFD700',
'goldenrod':            '#DAA520',
'gray':                 '#808080',
'green':                '#008000',
'greenyellow':          '#ADFF2F',
'honeydew':             '#F0FFF0',
'hotpink':              '#FF69B4',
'indianred':            '#CD5C5C',
'indigo':               '#4B0082',
'ivory':                '#FFFFF0',
'khaki':                '#F0E68C',
'lavender':             '#E6E6FA',
'lavenderblush':        '#FFF0F5',
'lawngreen':            '#7CFC00',
'lemonchiffon':         '#FFFACD',
'lightblue':            '#ADD8E6',
'lightcoral':           '#F08080',
'lightcyan':            '#E0FFFF',
'lightgoldenrodyellow': '#FAFAD2',
'lightgreen':           '#90EE90',
'lightgray':            '#D3D3D3',
'lightpink':            '#FFB6C1',
'lightsalmon':          '#FFA07A',
'lightseagreen':        '#20B2AA',
'lightskyblue':         '#87CEFA',
'lightslategray':       '#778899',
'lightsteelblue':       '#B0C4DE',
'lightyellow':          '#FFFFE0',
'lime':                 '#00FF00',
'limegreen':            '#32CD32',
'linen':                '#FAF0E6',
'magenta':              '#FF00FF',
'maroon':               '#800000',
'mediumaquamarine':     '#66CDAA',
'mediumblue':           '#0000CD',
'mediumorchid':         '#BA55D3',
'mediumpurple':         '#9370DB',
'mediumseagreen':       '#3CB371',
'mediumslateblue':      '#7B68EE',
'mediumspringgreen':    '#00FA9A',
'mediumturquoise':      '#48D1CC',
'mediumvioletred':      '#C71585',
'midnightblue':         '#191970',
'mintcream':            '#F5FFFA',
'mistyrose':            '#FFE4E1',
'moccasin':             '#FFE4B5',
'navajowhite':          '#FFDEAD',
'navy':                 '#000080',
'oldlace':              '#FDF5E6',
'olive':                '#808000',
'olivedrab':            '#6B8E23',
'orange':               '#FFA500',
'orangered':            '#FF4500',
'orchid':               '#DA70D6',
'palegoldenrod':        '#EEE8AA',
'palegreen':            '#98FB98',
'paleturquoise':        '#AFEEEE',
'palevioletred':        '#DB7093',
'papayawhip':           '#FFEFD5',
'peachpuff':            '#FFDAB9',
'peru':                 '#CD853F',
'pink':                 '#FFC0CB',
'plum':                 '#DDA0DD',
'powderblue':           '#B0E0E6',
'purple':               '#800080',
'red':                  '#FF0000',
'rosybrown':            '#BC8F8F',
'royalblue':            '#4169E1',
'saddlebrown':          '#8B4513',
'salmon':               '#FA8072',
'sandybrown':           '#FAA460',
'seagreen':             '#2E8B57',
'seashell':             '#FFF5EE',
'sienna':               '#A0522D',
'silver':               '#C0C0C0',
'skyblue':              '#87CEEB',
'slateblue':            '#6A5ACD',
'slategray':            '#708090',
'snow':                 '#FFFAFA',
'springgreen':          '#00FF7F',
'steelblue':            '#4682B4',
'tan':                  '#D2B48C',
'teal':                 '#008080',
'thistle':              '#D8BFD8',
'tomato':               '#FF6347',
'turquoise':            '#40E0D0',
'violet':               '#EE82EE',
'wheat':                '#F5DEB3',
'white':                '#FFFFFF',
'whitesmoke':           '#F5F5F5',
'yellow':               '#FFFF00',
'yellowgreen':          '#9ACD32'}
