def reduce_data(dir_p=None, dir_n=None, forceNew=False, startOver=False, write2file=True, load_cuts=True, ptm_median_or_max='median', new_gain=False, brent=True, is_cal=True, import_drift=True, use_imported_ptm=True, part_of_array=False, close_plots=False, quick_cut=True, summarize_only=False, cut_only=False, fast=False, use_tallest=True, new_hist=False, restrain_ptm=False):
    """
    # reduces data up to before energy calibration for cal data, and up to energy cal for sci data
    # usage: reduced_data = reduce_data() if cal data
    # load_cuts = True will try to grab one from a saved cut dict file. if no file found, it call sj_cut_guide()
    # load_cuts = False or None will execute cut_guide()
    # load_cuts = 'default' will use default cuts
    # is_cal = True (cal data) / False (sci data) / '20161209_001' (manually setting cal data folder)
    # import_drift = True / False (drift on its own) / '20161209_002' (manually setting drift folder)
    # part_of_array = True uses first 20 chans, = False uses full chans, = # uses first # number of chans
    """
    global PULSE_FOLDER
    global NOISE_FOLDER
    global PULSE_FOLDER_CAL

    if BASE_FOLDER == "no name yet":
        # print '# BASE_FOLDER is not set up yet. Run:\nuseful_ftns.set_base_folder()'
        # return
        set_base_folder()
    else:
        print('# BASE_FOLDER: %s'%BASE_FOLDER)

    # if (dir_p is None) and (dir_n is None):
    #     if (PULSE_FOLDER == 'no name yet') or (NOISE_FOLDER == 'no name yet'):
    #         # print '# PULSE_FOLDER or NOISE_FOLDER is not set up yet. Run:\nuseful_ftns.set_pulse_noise_folders()'
    #         # return
    #         set_pulse_noise_folders()

    #     print 'PULSE_FOLDER: %s'%PULSE_FOLDER
    #     print 'NOISE_FOLDER: %s'%NOISE_FOLDER

    #     dir_p = PULSE_FOLDER
    #     dir_n = NOISE_FOLDER

    if dir_p is None:
        if PULSE_FOLDER == 'no name yet':
            set_pulse_folder()
        print('PULSE_FOLDER: %s'%PULSE_FOLDER)
        dir_p = PULSE_FOLDER
    else:
        PULSE_FOLDER = dir_p

    if dir_n is None:
        if NOISE_FOLDER == 'no name yet':
            set_noise_folder()
        print('NOISE_FOLDER: %s'%NOISE_FOLDER)
        dir_n = NOISE_FOLDER
    else:
        NOISE_FOLDER = dir_n

    if isinstance(is_cal, str):
        PULSE_FOLDER_CAL = is_cal

    path_p = os.path.join(BASE_FOLDER, dir_p.split('_')[0] ,dir_p) # full path for pulse folder
    path_n = os.path.join(BASE_FOLDER, dir_n.split('_')[0], dir_n) # full path for noise folder

    hdf5_filename = os.path.join(path_p, dir_p.strip(os.sep)+'_mass.hdf5')
    hdf5_filename_n = os.path.join(path_n, dir_n.strip(os.sep)+'_mass.hdf5')
    if startOver is True:
        if os.path.isfile(hdf5_filename):
            os.remove(hdf5_filename)

    if mass.__version__ < '0.6.2':
        available_chans = list(set(mass.ljh_get_channels_both(os.path.join(BASE_FOLDER, dir_p.split('_')[0],dir_p),os.path.join(BASE_FOLDER, dir_n.split('_')[0],dir_n))))
    else:
        available_chans = list(set(mass.ljh_util.ljh_get_channels_both(os.path.join(BASE_FOLDER, dir_p.split('_')[0],dir_p),os.path.join(BASE_FOLDER, dir_n.split('_')[0],dir_n))))

    if part_of_array is True:
        chan_nums = available_chans[:20]
    elif part_of_array is False:
        chan_nums = available_chans[:]
    elif isinstance(part_of_array, int):
        chan_nums = available_chans[:part_of_array]
    if mass.__version__ < '0.6.2':
        pulse_files = mass.ljh_chan_names(path_p, chan_nums)
        noise_files = mass.ljh_chan_names(path_n, chan_nums)
    else:
        pulse_files = mass.ljh_util.ljh_chan_names(path_p, chan_nums)
        noise_files = mass.ljh_util.ljh_chan_names(path_n, chan_nums)
        pulse_files = [pulse_file+'.ljh' for pulse_file in pulse_files]
        noise_files = [noise_file+'.ljh' for noise_file in noise_files]
    try:
        data = mass.TESGroup(pulse_files, noise_files)
    # except IndexError:
    except IOError as e:
        print('\n', e)
        print('''\n# It seems like hdf5_noisefilename got corrupt. If so, run 'useful_ftns.delete_noise_hdf5()' to delete it and restart the script.''')
        return
    data.summarize_data(forceNew=forceNew)

    if summarize_only is True:
        return data

    if fast is True:
        if is_cal is True:
            quick_cut = True
        else:
            load_cuts = 'default'

    if load_cuts is True: # if load_cuts is True, it will try to load the most recent cut file
        cut_fnames = glob.glob(dir_p+'_cut*')
        if len(cut_fnames) > 0:
            most_recent_cut_fname = max(cut_fnames, key=os.path.getctime)
            try:
                cuts_from_file, ptm_median_or_max_from_file = load_cuts(most_recent_cut_fname)
                print('# cut file %s is being applied.'%most_recent_cut_fname)
                data.sj_apply_cuts(cuts_from_file, forceNew=forceNew, write2file=False, ptm_median_or_max=ptm_median_or_max_from_file)
            except NameError:
                print('# cut file %s found, but does not contain cut values.'%most_recent_cut_fname)
                print('# New cut is being generated.')
                data.sj_cut_guide(forceNew=True, write2file=True, ptm_median_or_max=ptm_median_or_max, close_plots=close_plots, quick_cut=quick_cut)
        else: # if cut dict file does not exist, generate new one
            data.sj_cut_guide(forceNew=True, write2file=True, ptm_median_or_max=ptm_median_or_max, close_plots=close_plots, quick_cut=quick_cut)
    elif (load_cuts is False) or (load_cuts is None):
        data.sj_cut_guide(forceNew=True, write2file=True, ptm_median_or_max=ptm_median_or_max, close_plots=close_plots, quick_cut=quick_cut)
    elif load_cuts == 'default': # use default values
        print('# some default cuts are applied')
        default_cuts = mass.core.controller.AnalysisControl(
            #pulse_average=(0.0, None), #0
            pretrigger_rms=(None, 55.0), #1
            pretrigger_mean_departure_from_median=(-40.0, 40.0), #2, pretty big drift
            peak_value=(0.0, None), #3
            postpeak_deriv=(None, 35.0), #4
            rise_time_ms=(0, 0.12), #5
            peak_time_ms=(0, 0.17) #6
        )
        data.sj_apply_cuts(default_cuts, forceNew=forceNew, write2file=forceNew, ptm_median_or_max=ptm_median_or_max)
    else: # if cuts is provided
        data.sj_apply_cuts(load_cuts, forceNew=forceNew, write2file=forceNew, ptm_median_or_max=ptm_median_or_max)

    # if cut_exists is None:
    #     print '# reduce_data script cancelled because cut parameters are not present'
    #     return

    if (cut_only is True) or ((fast is True) and (is_cal is True)):
        print('data returned right before making average pulses (right after cut)')
        return data

    if is_cal is True: # when the data to be analyzed is a calibration data, also fast=False, because otherwise the script should have ended
        # if (PULSE_FOLDER_CAL == "no name yet"):# or (PULSE_FOLDER_CAL == dir_p):
        data.sj_avg_pulses_auto_masks([0.99, 1.01], quantity="p_peak_value", forceNew=forceNew, use_tallest=use_tallest, restrain_ptm=restrain_ptm)
        if mass.__version__ >= '0.7.0':
            data.compute_noise_spectra(max_excursion=500)
            data.compute_ats_filter(f_3db=10e3, forceNew=forceNew)
            print('# compute_ats_filter is called')
        else:
            data.sj_compute_filters(f_3db=20000.0, forceNew=forceNew) # option to choose old / new filter?
            print('# sj_compute_filters is called')
        data.filter_data(forceNew=forceNew)
        if isinstance(import_drift, str): # if one wants to import drift, provide its dir_p
            drift_fnames = glob.glob(import_drift+'_drift*')
            if len(drift_fnames) == 0:
                print('# no drift correction file found. Drift correction not performed.')
                return data
            else:
                most_recent_drfit_fname = max(drift_fnames, key=os.path.getctime)
                try:
                    data.sj_import_drift(most_recent_drfit_fname, forceNew=True, category=None, use_imported_ptm=use_imported_ptm, plot_it=False, ptm_median_or_max=None, new_gain=None)
                except AttributeError as err:
                    print('caught this error: ' + repr(error))
                    return data
        else:
            print('# fresh drift correction started')
            data.sj_drift_correct(forceNew=True, write2file=write2file, combined=False, ptm_median_or_max=ptm_median_or_max, new_gain=new_gain, brent=brent, new_hist=new_hist)
            # data.sj_phase_correct(forceNew=forceNew, write2file=write2file)
        # else:
        #     if (PULSE_FOLDER_CAL != dir_p) and (forceNew is True):
        #         print '# is_cal is True, and PULSE_FOLDER_CAL exists, so use is_cal=False if science data or set up the cal folder again'
        #         return data
    else: # science data case
        if is_cal is False: # when the data to be analyzed is a science data
            if PULSE_FOLDER_CAL == "no name yet":
                set_cal_folder()
            else:
                while True:
                    print('# PULSE_FOLDER_CAL: %s'%PULSE_FOLDER_CAL)
                    if ''==PULSE_FOLDER_CAL:
                        continue
                    else:
                        break
            dir_p_cal = PULSE_FOLDER_CAL
        elif isinstance(is_cal, str): # when a calibration data has been already analyzed
            dir_p_cal = is_cal

        if fast is True: # new way to do fast calibration of science data
            cal_fnames = glob.glob(dir_p_cal+'_fast_cal*')
            if len(cal_fnames) == 0:
                print('# no calibration information found. Calibration not performed.')
                return data
            else:
                most_recent_cal_fname = max(cal_fnames, key=os.path.getctime)
                with open(most_recent_cal_fname, "rb") as f:
                    cal_dict = pkl.load(f)
                    print("\n## calibration file (%s) (fast) loaded ##"%most_recent_cal_fname)

            for ds in data:
                try:
                    # temp_spl = mass.mathstat.interpolate.CubicSpline(map(np.float32, cal_dict[ds.channum]), cal_dict['cal'])
                    temp_spl = mass.mathstat.interpolate.CubicSpline(np.asfarray(cal_dict[ds.channum]), cal_dict['cal'])
                    ds.p_energy_from_cal = np.array(temp_spl(ds.p_peak_value), dtype=np.float32)
                    popt, pcov = curve_fit(ph2e_poly4th_thru_origin, np.sort(cal_dict[ds.channum]), np.sort(cal_dict['cal']), p0=[0.1, 0.1, 0.1, 0.1])
                    ds.p_energy_from_cal_poly = np.array(ph2e_poly4th_thru_origin(ds.p_peak_value[:], *popt), dtype=np.float32)
                    ds.p_energy = ds.p_energy_from_cal
                except KeyError as err:
                    data.set_chan_bad(ds.channum, "calibration information doesn't exist")
        else: # regular way
            path_cal = os.path.join(BASE_FOLDER, dir_p_cal.split('_')[0] ,dir_p_cal)
            hdf5_filename_cal = os.path.join(path_cal, dir_p_cal.strip(os.sep)+'_mass.hdf5')
            available_chans_cal = mass.ljh_get_channels_both(path_cal, path_n)
            chan_nums_cal = available_chans_cal[:] # might use part_of_pixel here as well
            pulse_files_cal = mass.ljh_chan_names(path_cal, chan_nums_cal)
            noise_files_cal = mass.ljh_chan_names(path_n, chan_nums_cal)
            data_cal = mass.TESGroup(pulse_files_cal, noise_files_cal)
            if mass.__version__ >= '0.7.0':
                data_cal.compute_noise_spectra(max_excursion=500)
                data_cal.compute_ats_filter(f_3db=10e3, forceNew=forceNew)
            else:
                data_cal.compute_filters()

            for channum in data_cal.why_chan_bad.keys():
                data.set_chan_bad(channum, "bad channel in cal data")

            # instead of computing filters, grab them from data_cal
            for ds in data:
                try:
                    ds_cal = data_cal.channel[ds.channum]
                    ds.filter = ds_cal.filter
                    ds.average_pulse = ds_cal.average_pulse
                except:
                    data.set_chan_bad(ds.channum, "copying avg pulse from cal failed")

            data.filter_data(forceNew=forceNew)
            # work out drift corr

            if import_drift is True: # cal data already exists, so use cal's drift
                drift_fnames = glob.glob(dir_p_cal+'_drift*')
                if len(drift_fnames) == 0:
                    print('# no drift correction file found. Drift correction not performed.')
                    return data
                else:
                    most_recent_drfit_fname = max(drift_fnames, key=os.path.getctime)
                    try:
                        data.sj_import_drift(most_recent_drfit_fname, forceNew=True, category=None, use_imported_ptm=use_imported_ptm, plot_it=False, ptm_median_or_max=None)
                    except AttributeError as err:
                        print('caught this error: ' + repr(error))
                        return data
            elif import_drift is False: # do not import drift, but drift correct on its own
                data.sj_drift_correct(forceNew=True, write2file=write2file, combined=False, ptm_median_or_max=ptm_median_or_max, new_hist=new_hist)
            elif isinstance(import_drift, str): # if one wants to import a specific drift, provide its dir_p
                drift_fnames = glob.glob(import_drift+'_drift*')
                if len(drift_fnames) == 0:
                    print('# no drift correction file found. Drift correction not performed.')
                    return data
                else:
                    most_recent_drfit_fname = max(drift_fnames, key=os.path.getctime)
                    try:
                        data.sj_import_drift(most_recent_drfit_fname, forceNew=True, category=None, use_imported_ptm=use_imported_ptm, plot_it=False, ptm_median_or_max=None)
                    except AttributeError as err:
                        print('caught this error: ' + repr(error))
                        return data
            elif 'TESGroup' in str(type(import_drift)):
                data = import_drift_combined(data, import_drift, forceNew=True, category=None, ptm_median_or_max=ptm_median_or_max, new_gain=new_gain, combined=True, brent=brent, new_hist=new_hist)

            cal_fnames = glob.glob(dir_p_cal+'_cal*')
            if len(cal_fnames) == 0:
                print('# no calibration information found. Calibration not performed.')
                return data
            else:
                most_recent_cal_fname = max(cal_fnames, key=os.path.getctime)
                with open(most_recent_cal_fname, "rb") as f:
                    cal_dict = pkl.load(f)
                    print("\n## calibration file (%s) loaded ##"%most_recent_cal_fname)

            for ds in data:
                try:
                    # temp_spl = mass.mathstat.interpolate.CubicSpline(map(np.float32, cal_dict[ds.channum]), cal_dict['cal'])
                    temp_spl = mass.mathstat.interpolate.CubicSpline(np.asfarray(cal_dict[ds.channum]), cal_dict['cal'])
                    ds.p_energy_from_cal_dc = np.array(temp_spl(ds.p_filt_value_dc), dtype=np.float32)
                    ds.p_energy_from_cal = np.array(temp_spl(ds.p_filt_value), dtype=np.float32)
                    popt, pcov = curve_fit(ph2e_poly4th_thru_origin, np.sort(cal_dict[ds.channum]), np.sort(cal_dict['cal']), p0=[0.1, 0.1, 0.1, 0.1])
                    ds.p_energy_from_cal_poly_dc = np.array(ph2e_poly4th_thru_origin(ds.p_filt_value_dc[:], *popt), dtype=np.float32)
                    ds.p_energy = ds.p_energy_from_cal_dc
                except KeyError:
                    data.set_chan_bad(ds.channum, "calibration information doesn't exist")
    return data


def reduce_batch_old(dir_ps, dir_n=None, dir_p_cal=None, forceNew=False, write2file=False, load_cuts='default', ptm_median_or_max='max', import_drift=True, close_plots=True, part_of_array=False):
    '''
    only for science data
    import_drift = True / False (use its own) / str
    write2file = True / False
    '''

    if dir_p_cal is None: # when the data to be analyzed is a science data
        if PULSE_FOLDER_CAL == "no name yet":
            set_cal_folder()
        else:
            print('# PULSE_FOLDER_CAL: %s'%PULSE_FOLDER_CAL)
        dir_p_cal = PULSE_FOLDER_CAL

    if dir_n is None:
        if NOISE_FOLDER == "no name yet":
            print('dir_n is needed')
            return
            set_cal_folder()
        else:
            dir_n = NOISE_FOLDER

    data_list = [reduce_data(dir_p=dir_p, dir_n=dir_n, is_cal=dir_p_cal, forceNew=forceNew, write2file=write2file, load_cuts=load_cuts, ptm_median_or_max=ptm_median_or_max, import_drift=import_drift, close_plots=close_plots, part_of_array=part_of_array) for dir_p in dir_ps]

    return data_list


def calibrate_reduced(data, is_cal_data=True, cal_lines=None, smoothing_range=range(10,25), nextra=10, maxacc=0.01, linearity=1.1, neighbor_limit=80, auto_inclusion=5, fast=False, num_rounds=3, write2file=True, rms_threshold=2, use_dc=True):
    ''' returns data
    do check_calibration and see if paramters are okay (using raw_input), and then proceed with sj_calibrate
    for now, cal_lines is list of energies
    ex) cal_lines = useful_ftns.cal_lines_energies(mono=800) or [277, 393, 525, 573, 615, 678, 705, 719, 776, 791, 852, 869, 930, 950, 1012, 1035, 1100] # maybe should be converted to float first?
    for now only is_cal_data=True works
    '''

    if is_cal_data is True:
        cal_params = {}
        cal_params['cal_lines'] = cal_lines
        cal_params['smoothing_range'] = smoothing_range
        cal_params['nextra'] = nextra
        cal_params['neighbor_limit'] = neighbor_limit
        cal_params['auto_inclusion'] = auto_inclusion
        cal_params['fast'] = fast
        cal_params['maxacc'] = maxacc
        cal_params['linearity'] = linearity
        cal_params['num_rounds'] = num_rounds

        if cal_lines is None:
            print("cal_lines not provided")
            return data
        elif type(cal_lines) == tuple:
            cal_lines = cal_lines[0]
            print('calibration lines', cal_lines)

        rerun_chans = np.asarray(data.good_channels)
        for i in range(num_rounds):
            print('## round %d'%(i+1))
            if fast is True:
                result_matrix = data.sj_calibrate("p_peak_value", cal_lines, size_related_to_energy_resolution=smoothing_range,
                                maxacc=0.1, nextra=nextra+i, plot_on_fail=False, bin_size_ev=0.2, forceNew=True, verbosity=1,
                                neighbor_limit=neighbor_limit+10*i, auto_inclusion=auto_inclusion-i, linearity=1.1, rerun_chans=rerun_chans)
            else:
                if use_dc is True:
                    result_matrix = data.sj_calibrate("p_filt_value_dc", cal_lines, size_related_to_energy_resolution=smoothing_range,
                                    maxacc=0.1, nextra=nextra+i, plot_on_fail=False, bin_size_ev=0.2, forceNew=True, verbosity=1,
                                    neighbor_limit=neighbor_limit+10*i, auto_inclusion=auto_inclusion-i, linearity=1.1, rerun_chans=rerun_chans)
                else:
                    result_matrix = data.sj_calibrate("p_filt_value", cal_lines, size_related_to_energy_resolution=smoothing_range,
                                    maxacc=0.1, nextra=nextra+i, plot_on_fail=False, bin_size_ev=0.2, forceNew=True, verbosity=1,
                                    neighbor_limit=neighbor_limit+10*i, auto_inclusion=auto_inclusion-i, linearity=1.1, rerun_chans=rerun_chans)

            # rerun_chans=rerun_chans[(result_matrix[9]==0) | (result_matrix[9]==1000)]
            rerun_chans=rerun_chans[(result_matrix[9]==0) | (result_matrix[9]>rms_threshold)] # if residual is worse than 1.5 eV
        if len(rerun_chans) == len(data.good_channels):
            print('# All failed. Calibration parameters need to be changed. Use data.sj_plot_quick_spectra()')
        data.set_chan_bad(rerun_chans, "failed sj_calibrate")

    cal_params['failed_chans'] = rerun_chans
    data.cal_params = cal_params

    if write2file is True:
        data.sj_save_cal()

    return data


def load_data_list(dir_ps, dir_n=None):
    """
    # just call TESGroup
    """
    global PULSE_FOLDER
    global NOISE_FOLDER

    if BASE_FOLDER == "no name yet":
        # print '# BASE_FOLDER is not set up yet. Run:\nuseful_ftns.set_base_folder()'
        # return
        set_base_folder()
    else:
        print('# BASE_FOLDER: %s'%BASE_FOLDER)

    if dir_n is None:
        if NOISE_FOLDER == "no name yet":
            # print 'dir_n is needed'
            # return
            set_noise_folder()
            dir_n = NOISE_FOLDER
        else:
            dir_n = NOISE_FOLDER

    path_n = os.path.join(BASE_FOLDER, dir_n.split('_')[0], dir_n) # full path for noise folder

    data_list = []

    if type(dir_ps) is not list: # if a string is given
        dir_ps = [dir_ps]

    available_chans = [None] * len(dir_ps)
    for ii, dir_p in enumerate(dir_ps):
        available_chans[ii]=set(mass.ljh_util.ljh_get_channels_both(os.path.join(BASE_FOLDER, dir_p.split('_')[0],dir_p),os.path.join(BASE_FOLDER, dir_n.split('_')[0],dir_n)))
    common_chans = reduce(set.intersection, available_chans)

    for dir_p in dir_ps:
        path_p = os.path.join(BASE_FOLDER, dir_p.split('_')[0] ,dir_p) # full path for pulse folder
        if mass.__version__ < '0.6.2':
            # common_chans = mass.ljh_get_channels_both(path_p, path_n) # for older mass
            pulse_files = mass.ljh_chan_names(path_p, common_chans) # for older mass
            noise_files = mass.ljh_chan_names(path_n, common_chans) # for older mass
        else:
            pulse_files = mass.ljh_util.ljh_chan_names(os.path.join(BASE_FOLDER, dir_p.split('_')[0],dir_p),common_chans)
            pulse_files = [pulse_file+'.ljh' for pulse_file in pulse_files]
            noise_files = mass.ljh_util.ljh_chan_names(os.path.join(BASE_FOLDER, dir_n.split('_')[0],dir_n),common_chans)
            noise_files = [noise_file+'.ljh' for noise_file in noise_files]

        data = mass.TESGroup(pulse_files, noise_files)
        data_list.append(data)

    return data_list


def load_reduced(filename):
    """
    This is to generate an array of class instances to make the array look as if TESData
    Usage: reduced_data = load_reduced(filename) or load_reduced(dir_p)

    """
    if '.hdf5' in filename: # in case full hdf5 filename is given
        loaded_hdf5 = h5py.File(filename, 'r')
    elif len(glob.glob('*'+filename+'_reduced*'))>0: # in case dir_p is given
        print('Full filename is not given. %s will be used'%(glob.glob('*'+filename+'_reduced*')[-1]))
        loaded_hdf5 = h5py.File(max(glob.glob('*'+filename+'_reduced*'),key=os.path.getctime), 'r')
    else:
        print('filename does not exist')
        return

    import sys
    available_chans = map(int, loaded_hdf5.keys())
    available_chans.sort()
    available_params = loaded_hdf5[str(available_chans[0])].keys()
    class ReducedData(object):
        def __init__(self,n):
            self.nPulses=n
        def good(self):
            return np.array([True]*self.nPulses)
    reduced_data = []
    for i, chan in enumerate(available_chans):
        #exec('ch%03d = ReducedData()'%chan)
        n = len(loaded_hdf5['%s/%s'%(chan,available_params[0])][:])
        reduced_data = np.append(reduced_data, ReducedData(n))
    for ii, ds in enumerate(reduced_data):
        if ii==0:
            ds.dir_p = '_'.join(filename.split('_')[0:2])
        ds.channum = available_chans[ii]
        for available_param in available_params:
            #if sys.version_info < (2,7,10):
            #    exec "'ds.%s = loaded_hdf5[\'%s/%s\'][:]'"%(available_param,available_chans[ii],available_param) in globals(), locals()
            #else:
            exec('ds.%s = loaded_hdf5[\'%s/%s\'][:]'%(available_param,available_chans[ii],available_param), globals(), locals()) # this works on my mac (ver 2.7.11), but not on TES ubuntu (2.7.6)
    return reduced_data